</td>
</tr>
</tbody>
</table>
<div class="footer" style="font-family: Verdana, Geneva, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 20px;">
<table style="font-family: Verdana, Geneva, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;" width="100%">
<tbody>
   <tr style="font-family: Verdana, Geneva, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
      <td align="center" class="aligncenter content-block" style="font-family: Verdana, Geneva, sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; color: #999; text-align: center; margin: 0; padding: 0 0 20px;" valign="top"><span style="font-family: Verdana, Geneva, sans-serif; box-sizing: border-box; font-size: 12px; color: #999; margin: 0;">{{ $providername ?? '' }} {{ $provideraddress ?? '' }}</span></td>
   </tr>
</tbody>
</table>
</div>
</div>
</td>
<td style="font-family: Verdana, Geneva, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"><br></td>
</tr>
</tbody>
</table>
