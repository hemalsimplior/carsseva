<meta name="viewport" content="width=device-width, initial-scale=1">
<table bgcolor="#f6f6f6" class="body-wrap" style="font-family: Verdana, Geneva, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #fafafa; margin: 0;">
   <tbody>
      <tr style="font-family: Verdana, Geneva, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
         <td style="font-family: Verdana, Geneva, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"><br></td>
         <td class="container" style="font-family: Verdana, Geneva, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;" valign="top" width="600">
            <div class="content" style="font-family: Verdana, Geneva, sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;">
               <table bgcolor="#fff" cellpadding="0" cellspacing="0" class="main" style="font-family: Verdana, Geneva, sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0;" width="100%">
                  <tbody>
                     <tr style="font-family: Verdana, Geneva, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <td align="center" bgcolor="#FF9F00" class="alert alert-warning" style="font-family: Verdana, Geneva, sans-serif;text-align: left; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; border-radius: 3px 3px 0 0; border-radius: 3px 0px 0 0;width: 50%;background-color: #fafafa; margin: 0; padding: 20px;" valign="top">
                           <div style="text-align: left;"><img height="40px" src="{{ $providerlogo ?? '' }}" class="fr-fic fr-dii"></div>
                        </td>
                     </tr>
                     <tr style="font-family: Verdana, Geneva, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
                        <td class="content-wrap" colspan="2" style="font-family: Verdana, Geneva, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;border: 1px solid #e9e9e9;" valign="top">
<p>Hello {{ $name }},</p>
<p>Here is your verification link, You can verify your email using below link.</p>
<p><a style="text-decoration: none; font-weight: bold;" href="{!! $verification_link ?? '' !!}">Click here to verify</a></p>
<p>Thank you!<br />Team - {{ $providername ?? '' }}</p>
</td>
</tr>
</tbody>
</table>
<div class="footer" style="font-family: Verdana, Geneva, sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 20px;">
<table style="font-family: Verdana, Geneva, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;" width="100%">
<tbody>
   <tr style="font-family: Verdana, Geneva, sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;">
      <td align="center" class="aligncenter content-block" style="font-family: Verdana, Geneva, sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; color: #999; text-align: center; margin: 0; padding: 0 0 20px;" valign="top"><span style="font-family: Verdana, Geneva, sans-serif; box-sizing: border-box; font-size: 12px; color: #999; margin: 0;">{{ $providername ?? '' }} {{ $provideraddress ?? '' }}</span></td>
   </tr>
</tbody>
</table>
</div>
</div>
</td>
<td style="font-family: Verdana, Geneva, sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;" valign="top"><br></td>
</tr>
</tbody>
</table>
