@extends('layouts/contentLayoutMaster')

@section('title', 'Promotions')

@section('vendor-style')
        {{-- vendor css files --}}
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/plugins/forms/validation/form-validation.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection

@section('page-style')
        <!-- Page css files -->
@endsection

@section('content')
        
        <!-- Column selectors with Export Options and print table -->
        <section id="column-selectors">
            <div class="row">
                <div class="col-12 col-lg-12 col-xl-5">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" id="card-title">Add Promotion</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <form class="form-horizontal" id="promotionModleFrm" method="post" action="{{ route('promotions.store') }}" novalidate enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="first-name-vertical">Name *</label>
                                                <div class="controls">
                                                    <input type="text" id="name" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="Name" value="{{ old('name') }}" required>

                                                    @error('name')
                                                        <span class="invalid-feedback help-block" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="first-name-vertical">Code *</label>
                                                <div class="controls">
                                                    <input type="text" id="code" name="code" class="form-control @error('code') is-invalid @enderror" placeholder="Code" value="{{ old('code') }}" required>

                                                    @error('code')
                                                        <span class="invalid-feedback help-block" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <ul class="nav nav-tabs nav-justified" id="myTab2" role="tablist">
                                      <li class="nav-item">
                                        <a class="nav-link active" id="general-justified" data-toggle="tab" href="#general" role="tab" aria-controls="general" aria-selected="true">General</a>
                                      </li>
                                      @if ( $currentUser['role'] == 'admin' )
                                        <li class="nav-item">
                                            <a class="nav-link" id="usage-restriction-justified" data-toggle="tab" href="#usage-restriction" role="tab" aria-controls="usage-restriction" aria-selected="false">Usage Restriction</a>
                                        </li>
                                      @endif
                                      <li class="nav-item">
                                        <a class="nav-link" id="usage-limits-justified" data-toggle="tab" href="#usage-limits" role="tab" aria-controls="usage-limits" aria-selected="false">Usage Limits</a>
                                      </li>
                                    </ul>

                                    <div class="tab-content pt-1">
                                      <div class="tab-pane active" id="general" role="tabpanel" aria-labelledby="general">

                                        <div class="row">
                                            <div class="col-12 col-md-4">
                                                <div class="form-group">
                                                    <label for="first-name-vertical">Date Range *</label>
                                                    <div class="controls">
                                                        <input type="text" id="range" name="range" class="form-control @error('range') is-invalid @enderror" placeholder="Date Range" value="{{ old('range') }}" required>

                                                        @error('range')
                                                            <span class="invalid-feedback help-block" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-4">
                                                <div class="form-group">
                                                    <label for="first-name-vertical">Sign Off Type *</label>
                                                    <div class="controls">
                                                        <select name="sign_off_type" id="sign_off_type" class="form-control select2" required>
                                                            <option value="fixed_amount">Fixed Amount</option>
                                                            <option value="percentage">Percentage</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-4">
                                                <div class="form-group">
                                                    <label for="first-name-vertical">Amount *</label>
                                                    <div class="controls">
                                                        <input type="number" id="amount" name="amount" min="0" class="form-control @error('amount') is-invalid @enderror" placeholder="Amount" value="{{ old('amount') }}" required>

                                                        @error('amount')
                                                            <span class="invalid-feedback help-block" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="first-name-vertical">Description</label>
                                                    <div class="controls">
                                                        <textarea name="description" id="description" class="form-control"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      </div>
                                      @if ( $currentUser['role'] == 'admin' )
                                        <div class="tab-pane" id="usage-restriction" role="tabpanel" aria-labelledby="usage-restriction">
                                            
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="first-name-vertical">Vendors *</label>
                                                        <div class="controls">
                                                            <select name="vendors[]" id="vendors" @error('vendors[]') is-invalid @enderror class="form-control select2" required multiple="multiple">
                                                                <option value="all">All</option>
                                                                @foreach($vendors as $inv => $vendor)
                                                                    <option value="{{ $vendor['id'] }}">{{ $vendor['name'] }}</option>
                                                                @endforeach
                                                            </select>

                                                            @error('vendors[]')
                                                                <span class="invalid-feedback help-block" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                      @endif
                                      <div class="tab-pane" id="usage-limits" role="tabpanel" aria-labelledby="usage-limits">

                                        <div class="row">
                                            <div class="col-12 col-md-4">
                                                <div class="form-group">
                                                    <label for="first-name-vertical">Usage limit per coupon</label>
                                                    <div class="controls">
                                                        <input type="number" id="limit_per_coupon" name="limit_per_coupon" class="form-control" placeholder="1">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-4">
                                                <div class="form-group">
                                                    <label for="first-name-vertical">Limit usage to X services</label>
                                                    <div class="controls">
                                                        <select name="services[]" id="services" placeholder="Apply to all qualifying services in cart" class="form-control select2" multiple="multiple">
                                                            @foreach($services as $ins => $service)
                                                                <option value="{{ $service['id'] }}">{{ $service['name'] }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-4">
                                                <div class="form-group">
                                                    <label for="first-name-vertical">Usage limit per customer</label>
                                                    <div class="controls">
                                                        <input type="number" id="limit_per_customer" name="limit_per_customer" class="form-control" placeholder="Unlimited usage">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                      </div>
                                      
                                    </div>

                                    <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Save</button>
                                    <button type="reset" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light add-action">Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-12 col-xl-7">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Promotions</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body card-dashboard">
                                <div class="table-responsive">
                                    <table class="table promotion-table compact-table table-data-nowrap">
                                        <thead>
                                            <tr>
                                                <th>Sr</th>
                                                <th>Name</th>
                                                <th>Code</th>
                                                <th>Campaign</th>
                                                <th>Deal</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          @php
                                            $sr = 1;
                                          @endphp
                                          @foreach($promotions as $in => $promotion)
                                            @php
                                                $promotionVendorArr = json_decode($promotion['vendors'], 1);
                                            @endphp
                                            <tr>
                                                <td>{{ $sr }}</td>
                                                <td>{{ $promotion->name }}</td>
                                                <td>{{ $promotion->code }}</td>
                                                <td style="font-size: 13px;">
                                                    <span><strong>Type:</strong> {{ (($promotion->sign_off_type == 'fixed_amount') ? 'Fixed Amount' : 'Percentage') }}</span><br>
                                                    <span><strong>Start On:</strong> {{ $promotion->start_date->format('d/m/Y') }}</span><br>
                                                    <span><strong>Expires On:</strong> {{ $promotion->end_date->format('d/m/Y') }}</span>
                                                </td>
                                                <td style="font-size: 13px;">
                                                    <span><strong>Discount:</strong> {{ (($promotion->sign_off_type == 'fixed_amount') ? '₹ ' . $promotion->amount . ' Flat' : $promotion->amount . '% on Cost' ) }}</span>
                                                </td>
                                                <td>
                                                    @if ( $currentUser['role'] == 'admin' || ($currentUser['role'] != 'admin' && isset($promotionVendorArr[$currentUser['vendor_id']]) && $promotionVendorArr[$currentUser['vendor_id']]) )
                                                        <div class="custom-control custom-switch custom-switch-success custom-control-inline">
                                                            <input type="checkbox" class="custom-control-input item-status-change" data-item="{{ $promotion }}" data-sr="status{{ $sr }}" id="status{{ $in }}" {{ (($promotion->status == '1') ? 'checked="checked"' : '') }}>
                                                            <label class="custom-control-label" for="status{{ $in }}"></label>
                                                        </div>
                                                    @else 
                                                        -
                                                    @endif
                                                </td>
                                                <td class="action-btn">
                                                    @if ( $currentUser['role'] == 'admin' || ($currentUser['role'] != 'admin' && isset($promotionVendorArr[$currentUser['vendor_id']]) && $promotionVendorArr[$currentUser['vendor_id']]) )
                                                        <button type="button" class="btn btn-sm btn-icon btn-icon rounded-circle btn-flat-light mr-1 mb-1 waves-effect waves-light edit-action" data-item="{{ $promotion }}"><i class="feather icon-edit"></i></button>
                                                        <button type="button" class="btn btn-sm btn-icon btn-icon rounded-circle btn-flat-light mr-1 mb-1 waves-effect waves-light delete-item" data-item="{{ $promotion }}"><i class="feather icon-trash-2"></i></button>
                                                    @else 
                                                        -
                                                    @endif
                                                </td>
                                            </tr>
                                          @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Sr</th>
                                                <th>Name</th>
                                                <th>Code</th>
                                                <th>Campaign</th>
                                                <th>Deal</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Column selectors with Export Options and print table -->

        @section('carsseva-alert')

            <!-- Alerts -->
            @foreach (['danger', 'warning', 'success', 'info'] as $key)
                @if(Session::has($key))
                    <div class="cws-alert">
                        <div class="alert alert-{{ $key }} alert-dismissible alert-auto-hide fade show" role="alert">
                            <div class="alert-text">{{ Session::get($key) }}</div>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
                            </button>
                        </div>
                    </div>
                @endif
            @endforeach

            <!-- Confirmation alert msg -->
            <div class="cws-alert promotion-alert" style="display: none;">
                <div class="background-overlay promotion-reject"></div>
                <div class="alert alert-dark-danger alert-dismissible fade show" role="alert">
                    <div class="alert-text">Are you sure you want to delete <strong class="promotion-name"></strong>? 
                        <div class="alert-btn">
                            <a href="#" class="btn btn-primary promotion-confirm">Yes</a>
                            <button class="btn btn-white promotion-reject">Cancel</button>
                        </div>
                    </div>
                    <button type="button" class="close promotion-reject">
                        <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
                    </button>
                </div>
            </div>

            <!-- Confirmation alert msg for status change -->
            <div class="cws-alert status-change-alert" style="display: none;">
                <div class="background-overlay status-change-reject"></div>
                <div class="alert alert-dark-danger alert-dismissible fade show" role="alert">
                    <div class="alert-text">Are you sure you want to <strong class="status-change-name"></strong>? 
                        <div class="alert-btn">
                            <button class="btn btn-primary status-change-confirm">Yes</button>
                            <button class="btn btn-white status-change-reject">Cancel</button>
                        </div>
                    </div>
                    <button type="button" class="close status-change-reject">
                        <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
                    </button>
                </div>
            </div>

        @endsection

@endsection

@section('vendor-script')
        {{-- vendor files --}}
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>

        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
@endsection

@section('page-script')
        {{-- Page js files --}}
        <script type="text/javascript">
            $(document).ready(function() {

                // Form
                var validator = $( "#promotionModleFrm" ).validate({
                    ignore: "",
                    rules: {
                        amount: {
                            required: true,
                            digits: true
                        }
                    }
                });

                $('#range').daterangepicker({
                    timePicker: false,
                    startDate: moment().format('DD/MM/YYYY'),
                    endDate: moment().format('DD/MM/YYYY'),
                    locale: {
                      format: 'DD/MM/YYYY'
                    }
                });

                $('.select2').select2({
                    dropdownAutoWidth: true,
                    width: '100%',
                    placeholder: "Select Vendors"
                });

                // Datatable
                $('.promotion-table').DataTable({
                    "ordering": false,
                    "columnDefs": [
                        { "visible": false, "targets": 0},
                        { "width": '12%', "targets": 1},
                        { "width": '10%', "targets": 2},
                    ],
                    "order": [
                        [0, 'asc']
                    ]
                });

                // Edit Form
                $('.edit-action').click( function() {
                    var item = $(this).data('item');
                    console.log(item);
                    $('#promotionModleFrm').attr('action', 'promotions/'+item.id);
                    $('#promotionModleFrm').append('<input name="_method" id="_method" type="hidden" value="PUT">');
                    
                    $("#card-title").html('Edit Promotion');

                    $("#name").val(item.name);
                    $("#code").val(item.code);
                    $("#sign_off_type").val(item.sign_off_type).trigger('change');
                    $("#amount").val(item.amount);
                    $("#description").val(item.description);

                    $("#limit_per_coupon").val(item.limit_per_coupon);
                    $("#limit_per_customer").val(item.limit_per_customer);

                    $("#vendors").val($.map(JSON.parse(item.vendors), function(element,index) {return index})).trigger('change');
                    $("#services").val($.map(JSON.parse(item.services), function(element,index) {return index})).trigger('change');
                    
                    $('#range').daterangepicker({
                        timePicker: false,
                        startDate: moment(item.start_date).format("DD/MM/YYYY"),
                        endDate: moment(item.end_date).format("DD/MM/YYYY"),
                        locale: {
                          format: 'DD/MM/YYYY'
                        }
                    });

                });

                // Add Form
                $('.add-action').click(function(){
                    
                    $('#_method').remove();
                    $("#card-title").html('Add Promotion');
                    $('#promotionModleFrm').attr('action', 'promotions');
                    
                    $('#promotionModleFrm')[0].reset();
                    $("#vendors").val([]).trigger('change');
                    $("#services").val([]).trigger('change');
                    $('#range').daterangepicker({
                        timePicker: false,
                        startDate: moment().format('DD/MM/YYYY'),
                        endDate: moment().format('DD/MM/YYYY'),
                        locale: {
                          format: 'DD/MM/YYYY'
                        }
                    });
                });

                // Delete Services/SubServices
                $('.delete-item').click( function() {
                    var item = $(this).data('item');
                    
                    $('.promotion-name').html(item.name);
                    $('.promotion-confirm').attr('href', "{{ URL::to('promotions') }}/"+item.id+"/delete");
                    $('.promotion-alert').show();
                });

                $('.promotion-reject').click( function() {
                    $('.promotion-alert').hide();
                });

                // Status change
                $('.item-status-change').change( function() {
                    var item = $(this).data('item');
                    var status = ((this.checked) ? 'active' : 'inactive');
                    
                    $('.status-change-confirm').attr('data-ids', item.id);

                    $('.status-change-name').html(status + ' ' + item.name);
                    $('.status-change-alert').show();
                    $('.status-change-confirm').attr('data-action', status);
                    $('.status-change-reject').attr('data-action', status);
                    $('.status-change-reject').attr('data-sr', $(this).data('sr'));
                });

                $('.status-change-confirm').click( function() {
                    
                    // trigger status-change event
                    var ids = $(this).attr('data-ids');
                    var action = $(this).attr('data-action');

                    $.post('promotions/bulk-action', {'action': action, 'ids': ids}, function(response){
                        if ( response.status == 200 ) {
                            window.location = "{{ url('/promotions') }}"
                        }
                    });
                });

                $('.status-change-reject').click( function() {

                    var action = $(this).attr('data-action');
                    var sr = $(this).attr('data-sr');
                    
                    if ( action == 'active' ) {
                        $("#"+sr).prop("checked", false);
                    } else {
                        $("#"+sr).prop("checked", true);
                    }

                    $('.status-change-alert').hide();
                });


                $('#vendors').on('select2:select', function (e) {
                    var select = this;
                    var selections = $(select).select2('data');

                    var Values = new Array();
                    if ( e.params.data.id == 'all' ) {
                        Values.push('all');
                    } else {
                        for (var i = 0; i < selections.length; i++) {
                            if ('all' != selections[i].id) {
                                Values.push(selections[i].id);
                            }
                        }
                    }

                    $(select).val(Values).trigger('change');
                });
            });
        </script>
@endsection
