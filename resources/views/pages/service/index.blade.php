@extends('layouts/contentLayoutMaster')

@section('title', 'Manage Cars')

@section('vendor-style')
        {{-- vendor css files --}}
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/plugins/forms/validation/form-validation.css')) }}">
        {{-- <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.min.css')) }}"> --}}
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection

@section('page-style')
        <!-- Page css files -->
        <link rel="stylesheet" href="{{ asset(mix('css/plugins/file-uploaders/dropzone.css')) }}">
@endsection

@section('content')
        
        <!-- Column selectors with Export Options and print table -->
        <section id="column-selectors">
            <div class="row">
                <div class="col-12 col-xl-4">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" id="card-title">Add Service/SubService</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <form class="form-horizontal" id="serviceModleFrm" method="post" action="{{ route('services.store') }}" novalidate enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="first-name-vertical">Select Type *</label>
                                                <ul class="list-unstyled mb-0">
                                                    <li class="d-inline-block mr-2">
                                                        <fieldset>
                                                            <div class="vs-radio-con">
                                                                <input type="radio" name="type" id="service-radio" checked value="service">
                                                                <span class="vs-radio">
                                                                <span class="vs-radio--border"></span>
                                                                <span class="vs-radio--circle"></span>
                                                                </span>
                                                                <span class="">Parent Service</span>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2">
                                                        <fieldset>
                                                            <div class="vs-radio-con">
                                                                <input type="radio" name="type" id="subservice-radio" value="subservice">
                                                                <span class="vs-radio">
                                                                <span class="vs-radio--border"></span>
                                                                <span class="vs-radio--circle"></span>
                                                                </span>
                                                                <span class="">Sub Service</span>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        {{-- Parent Service --}}
                                        <div class="col-sm-12 service-section">
                                            <div class="form-group">
                                                <label for="first-name-vertical">Name *</label>
                                                <div class="controls">
                                                    <input type="text" id="name" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="Name" value="{{ old('name') }}" required data-validation-required-message="This name field is required">

                                                    @error('name')
                                                        <span class="invalid-feedback help-block" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="first-name-vertical">Images</label>
                                                <div class="controls">
                                                    <div id="serviceImage" class="dropzone dropzone-area" style="min-height: 220px;"><div class="dz-message">Drop Files Here To Upload</div></div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="first-name-vertical">Order Number</label>
                                                <div class="controls">
                                                    <input type="number" id="order_number" name="order_number" class="form-control @error('order_number') is-invalid @enderror" placeholder="Order Number" value="{{ old('order_number') }}">
                                                </div>
                                            </div>
                                        </div>

                                        {{-- Sub Service --}}
                                        <div class="col-sm-12 subservice-section" style="display: none;">
                                            <div class="form-group">
                                                <label for="first-name-vertical">Select Parent Service *</label>
                                                <div class="controls">
                                                    
                                                    <select name="parent_id" id="parent_id" class="form-control select2" required data-validation-required-message="This brand field is required">
                                                        <option value='' selected="selected">Select Parent Service</option>
                                                        @foreach($services as $ins => $service)
                                                            <option value="{{ $service['id'] }}">{{ $service['name'] }}</option>
                                                        @endforeach
                                                    </select>

                                                    @error('name')
                                                        <span class="invalid-feedback help-block" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>     
                                            </div>
                                            <div class="media-list media-bordered subservice-list-section" data-current="0">
                                                <div class="media" style="padding: 15px 0;" id="subservice0">
                                                    <div class="media-body row">
                                                        <div class="col-sm-5 col-lg-5">
                                                            <div class="form-group">
                                                                <label for="first-name-vertical">Name *</label>
                                                                <div class="controls">
                                                                    <input type="text" name="model_name[]" id="model_name" class="form-control subservice-name" placeholder="Name" required>
                                                                    {{-- data-rule-checkexist="true" --}}
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="first-name-vertical">Small Seg. Price*</label>
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text">₹</span>
                                                                    </div>
                                                                    <input type="text" name="small[]" id="small" class="form-control" placeholder="Price" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="first-name-vertical">Medium Seg. Price*</label>
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text">₹</span>
                                                                    </div>
                                                                    <input type="text" name="medium[]" id="medium" class="form-control" placeholder="Price" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="first-name-vertical">Large Seg. Price*</label>
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text">₹</span>
                                                                    </div>
                                                                    <input type="text" name="large[]" id="large" class="form-control" placeholder="Price" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="first-name-vertical">Premium Seg. Price*</label>
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <span class="input-group-text">₹</span>
                                                                    </div>
                                                                    <input type="text" name="premium[]" id="premium" class="form-control" placeholder="Price" required>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="first-name-vertical">Order Number</label>
                                                                <div class="controls">
                                                                    <input type="number" name="model_order_number[]" id="model_order_number" class="form-control" placeholder="Order Number">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-5 col-lg-6">
                                                            <div class="form-group">
                                                                <label for="first-name-vertical">Description</label>
                                                                <div class="controls">
                                                                    <textarea name="model_description[]" id="model_description" class="form-control" rows="15"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-2 col-lg-1">
                                                            <div class="form-group">
                                                                <label for="first-name-vertical"></label>
                                                                <div class="controls add-btn">
                                                                    <i class="feather icon-plus-circle cursor-pointer add-subservice" style="font-size: 20px;margin-right: 5px;"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Save</button>
                                    <button type="reset" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light add-action">Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-xl-8">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">Manage Services</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body card-dashboard">
                                <div class="table-responsive">
                                    <table class="table service-table compact-table table-data-nowrap">
                                        <thead>
                                            <tr>
                                                <th rowspan="2">Sr</th>
                                                <th rowspan="2">Image</th>
                                                <th rowspan="2">Name</th>
                                                <th rowspan="2">Model</th>
                                                <th colspan="4" class="text-center">Segments</th>
                                                <th rowspan="2">Status</th>
                                                <th rowspan="2">Action</th>
                                            </tr>
                                            <tr>
                                                <th>S</th>
                                                <th>M</th>
                                                <th>L</th>
                                                <th>P</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          @php
                                            $sr = 1;
                                          @endphp
                                          @foreach($services as $in => $service)
                                            <tr class="group">
                                                <td>{{ $sr }}</td>
                                                <td>
                                                  <ul class="list-unstyled users-list m-0 d-flex align-items-center">
                                                    <li data-toggle="tooltip" data-popup="tooltip-custom" data-placement="bottom" data-original-title="{{ $service->name }}" class="avatar pull-up" style="background-color: transparent;">
                                                      <img class="media-object rounded-circle" src="{{ $service->image_url }}" alt="Avatar" height="30" width="30">
                                                    </li>
                                                  </ul>
                                                </td>
                                                <td>{{ $service->name }}</td>
                                                <td>{{ $service->service }}</td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>
                                                    <div class="custom-control custom-switch custom-switch-success custom-control-inline">
                                                        <input type="checkbox" class="custom-control-input item-status-change" data-item="{{ $service }}" data-sr="status{{ $sr }}" id="status{{ $sr }}" {{ (($service->status == '1') ? 'checked="checked"' : '') }}>
                                                        <label class="custom-control-label" for="status{{ $sr }}"></label>
                                                    </div>
                                                </td>
                                                <td class="action-btn">
                                                  <button type="button" class="btn btn-sm btn-icon btn-icon rounded-circle btn-flat-light mr-1 mb-1 waves-effect waves-light edit-action" data-item="{{ $service }}"><i class="feather icon-edit"></i></button>
                                                  <button type="button" class="btn btn-sm btn-icon btn-icon rounded-circle btn-flat-light mr-1 mb-1 waves-effect waves-light delete-item" data-item="{{ $service }}"><i class="feather icon-trash-2"></i></button>
                                                </td>
                                            </tr>
                                            @php
                                                $sr += 1;
                                            @endphp
                                            @if ( isset($service->subservices) && count($service->subservices) > 0 )
                                                @foreach($service->subservices as $inm => $subservice)
                                                    <tr>
                                                        <td>{{ $sr }}</td>
                                                        <td>&nbsp;</td>
                                                        <td>{{ $subservice->name }}</td>
                                                        <td>{{ $service->name }}</td>
                                                        <td><small class="text-muted">₹{{ $subservice->segment_price->small ?? '' }}</small></td>
                                                        <td><small class="text-muted">₹{{ $subservice->segment_price->medium ?? '' }}</small></td>
                                                        <td><small class="text-muted">₹{{ $subservice->segment_price->large ?? '' }}</small></td>
                                                        <td><small class="text-muted">₹{{ $subservice->segment_price->premium ?? '' }}</small></td>
                                                        <td>
                                                            <div class="custom-control custom-switch custom-switch-success custom-control-inline">
                                                                <input type="checkbox" class="custom-control-input item-status-change" data-item="{{ $subservice }}" data-sr="status{{ $sr }}" id="status{{ $sr }}" {{ (($subservice->status == '1') ? 'checked="checked"' : '') }}>
                                                                <label class="custom-control-label" for="status{{ $sr }}"></label>
                                                            </div>
                                                        </td>
                                                        <td class="action-btn">
                                                          <button type="button" class="btn btn-sm btn-icon btn-icon rounded-circle btn-flat-light mr-1 mb-1 waves-effect waves-light edit-action" data-item="{{ $subservice }}"><i class="feather icon-edit"></i></button>
                                                          <button type="button" class="btn btn-icon btn-icon rounded-circle btn-flat-light mr-1 mb-1 waves-effect waves-light delete-item" data-item="{{ $subservice }}"><i class="feather icon-trash-2"></i></button>
                                                        </td>
                                                    </tr>
                                                    @php
                                                        $sr += 1;
                                                    @endphp
                                                @endforeach
                                            @endif
                                          @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Sr</th>
                                                <th>Image</th>
                                                <th>Name</th>
                                                <th>Model</th>
                                                <th colspan="4" class="text-center">Segments</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Column selectors with Export Options and print table -->

        @section('carsseva-alert')

            <!-- Alerts -->
            @foreach (['danger', 'warning', 'success', 'info'] as $key)
                @if(Session::has($key))
                    <div class="cws-alert">
                        <div class="alert alert-{{ $key }} alert-dismissible alert-auto-hide fade show" role="alert">
                            <div class="alert-text">{{ Session::get($key) }}</div>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
                            </button>
                        </div>
                    </div>
                @endif
            @endforeach

            <!-- Confirmation alert msg -->
            <div class="cws-alert service-alert" style="display: none;">
                <div class="background-overlay service-reject"></div>
                <div class="alert alert-dark-danger alert-dismissible fade show" role="alert">
                    <div class="alert-text">Are you sure you want to delete <strong class="service-name"></strong>? 
                        <div class="alert-btn">
                            <a href="#" class="btn btn-primary service-confirm">Yes</a>
                            <button class="btn btn-white service-reject">Cancel</button>
                        </div>
                    </div>
                    <button type="button" class="close service-reject">
                        <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
                    </button>
                </div>
            </div>

            <!-- Confirmation alert msg for status change -->
            <div class="cws-alert status-change-alert" style="display: none;">
                <div class="background-overlay status-change-reject"></div>
                <div class="alert alert-dark-danger alert-dismissible fade show" role="alert">
                    <div class="alert-text">Are you sure you want to <strong class="status-change-name"></strong>? 
                        <div class="alert-btn">
                            <button class="btn btn-primary status-change-confirm">Yes</button>
                            <button class="btn btn-white status-change-reject">Cancel</button>
                        </div>
                    </div>
                    <button type="button" class="close status-change-reject">
                        <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
                    </button>
                </div>
            </div>

        @endsection

@endsection

@section('vendor-script')
        {{-- vendor files --}}
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
        {{-- <script src="{{ asset(mix('vendors/js/extensions/dropzone.min.js')) }}"></script> --}}
        <script src="{{ asset(mix('vendors/js/extensions/dropzone.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
@endsection
@section('page-script')
        
        {{-- Page js files --}}
        <script type="text/javascript">
            
            Dropzone.autoDiscover = false;

            $(document).ready(function() {

                // Form
                var validator = $( "#serviceModleFrm" ).validate();
                
                $.validator.addMethod("checkexist", function(value, element) {
                    var models = '{!! $subservices_name !!}';
                    if ( $.inArray(value.toLowerCase(), jQuery.parseJSON(models)) > -1 ) {
                        return false;
                    } else {
                        return true;
                    }
                }, "Service name already exist");

                $('.select2').select2({
                    dropdownAutoWidth: true,
                    width: '100%',
                    placeholder: "Select Parent Service"
                });

                $('input[name=type]').change(function() {
                    if (this.value == 'service') {
                        $('.service-section').show();
                        $('.subservice-section').hide();
                    } else {
                        $('.service-section').hide();
                        $('.subservice-section').show();
                    }
                });

                // File Upload
                var myDropzone = new Dropzone('#serviceImage', {
                    url: "{{ URL::to('upload') }}",
                    paramName: "image",
                    addRemoveLinks: true,
                    acceptedFiles: 'image/*',
                    maxFiles:1,
                    init: function () {
                        this.on("maxfilesexceeded", function(file) {
                            this.removeAllFiles();
                            this.addFile(file);
                        });
                    },
                    success: function (file, response) {
                        file.previewElement.classList.add("dz-success");
                        $('#serviceModleFrm').append('<input type="hidden" name="image_file" id="image_file" value="' + file.dataURL + '">');
                    },
                    reset: function () {
                        $('#image').val('');
                        $('#image_file').val('');
                    },
                    error: function (file, response) {
                        file.previewElement.classList.add("dz-error");
                        $('#image').val('');
                        $('#image_file').val('');
                    }
                });

                // Edit Form
                $('.edit-action').click( function() {
                    var item = $(this).data('item');
                    console.log(item);

                    $('#serviceModleFrm').attr('action', 'services/'+item.id);
                    $('input[name=type]').attr("disabled",true);
                    $('#serviceModleFrm').append('<input name="_method" id="_method" type="hidden" value="PUT">');

                    if ( item.parent_id != undefined && item.parent_id != 0 ) {
                        $('#serviceModleFrm').append('<input type="hidden" name="type" id="type" value="subservice">');
                        $('#serviceModleFrm').append('<input type="hidden" name="parent_id" value="'+item.parent_id+'">');
                        $("#card-title").html('Edit '+item.name+' Sub Service');
                        $("#subservice-radio").prop("checked", true);

                        $(".service-section").hide();
                        $(".subservice-section").show();

                        $("#parent_id").val(item.parent_id).trigger('change');
                        $("#parent_id").select2({disabled: 'readonly'});
                        $("#model_name").val(item.name);
                        $("#model_description").val(item.description);
                        $("#small").val(item.segment_price.small);
                        $("#medium").val(item.segment_price.medium);
                        $("#large").val(item.segment_price.large);
                        $("#premium").val(item.segment_price.premium);
                        $("#model_order_number").val(item.order_number);

                    } else {
                        $('#serviceModleFrm').append('<input type="hidden" name="type" id="type" value="service">');

                        $("#card-title").html('Edit '+item.name+' Service');
                        $("#service-radio").prop("checked", true);
                        $("#name").val(item.name);
                        $("#order_number").val(item.order_number);

                        $(".subservice-section").hide();
                        $(".service-section").show();

                        // Edit Image
                        if ( item.image != '' ) {
                            var mockFile = { name: item.image };
                            myDropzone.options.addedfile.call(myDropzone, mockFile);
                            myDropzone.options.thumbnail.call(myDropzone, mockFile, item.image_url);
                            mockFile.previewElement.classList.add('dz-success');
                            mockFile.previewElement.classList.add('dz-complete');
                            $('#serviceModleFrm').append('<input type="hidden" name="image_file" id="image_file" value="'+item.image+'">');
                        }
                    }
                });

                // Add Form
                $('.add-action').click(function(){
                    
                    $('#type').remove();
                    $('#_method').remove();
                    $("#card-title").html('Add Service/SubService');
                    $('#serviceModleFrm').attr('action', 'services');
                    $('input[name=type]').attr("disabled", false);
                    $("#service-radio").prop("checked", true);
                    $('#serviceModleFrm')[0].reset();

                    $('.dz-remove')[0].click();
                });

                // Datatable
                $('.service-table').DataTable({
                    "ordering": false,
                    "columnDefs": [
                        { "visible": false, "targets": 0},
                        { "width": '8%', "targets": 1},
                        { "width": '170px', "targets": 2},
                        { "visible": false, "targets": 3},
                        { "width": '8%', "targets": 4},
                        { "width": '8%', "targets": 5},
                        { "width": '8%', "targets": 6},
                        { "width": '8%', "targets": 7},
                        { "width": '8%', "targets": 8},
                        { "width": '15%', "targets": 9},
                    ],
                    "order": [
                        [0, 'asc']
                    ]
                });

                $('.service-reject').click( function() {
                    $('.service-alert').hide();
                });

                $('.status-change-confirm').click( function() {
                    
                    // trigger status-change event
                    var ids = $(this).attr('data-ids');
                    var action = $(this).attr('data-action');
                    var type = $(this).attr('data-type');

                    $.post('services/bulk-action', {'action': action, 'ids': ids, 'type': type}, function(response){
                        if ( response.status == 200 ) {
                            window.location = "{{ url('/services') }}"
                        }
                    });
                });

                $('.status-change-reject').click( function() {

                    var action = $(this).attr('data-action');
                    var sr = $(this).attr('data-sr');
                    
                    if ( action == 'active' ) {
                        $("#"+sr).prop("checked", false);
                    } else {
                        $("#"+sr).prop("checked", true);
                    }

                    $('.status-change-alert').hide();
                });
            });

            // Delete Services/SubServices
            $(document).on('click', '.delete-item', function() {
                var item = $(this).data('item');

                if ( item.parent_id != undefined && item.parent_id != 0 ) {
                    item.type = 'subservice';
                } else {
                    item.type = 'service';
                }
                
                $('.service-name').html(item.name + ' ' + item.type);
                $('.service-confirm').attr('href', "{{ URL::to('services') }}/"+item.id+"/delete");
                $('.service-alert').show();
            });

            // Status change
            $(document).on('change', '.item-status-change', function() {
                var item = $(this).data('item');
                var status = ((this.checked) ? 'active' : 'inactive');
                
                if ( item.parent_id != undefined && item.parent_id != 0 ) {
                    $('.status-change-confirm').attr('data-ids', item.id);
                    $('.status-change-confirm').attr('data-type', 'subservice');
                } else {
                    $('.status-change-confirm').attr('data-ids', item.id);
                    $('.status-change-confirm').attr('data-type', 'service');
                }

                $('.status-change-name').html(status + ' ' + item.name);
                $('.status-change-alert').show();
                $('.status-change-confirm').attr('data-action', status);
                $('.status-change-reject').attr('data-action', status);
                $('.status-change-reject').attr('data-sr', $(this).data('sr'));
            });

            $(document).on('click', '.add-subservice', function() {
                var current = $('.subservice-list-section').attr('data-current');
                var next = parseFloat(current) + 1;

                var frmHtml = '';
                frmHtml += '<div class="media" style="padding: 15px 0;" id="subservice'+next+'">';
                    frmHtml += '<div class="media-body row">';
                        frmHtml += '<div class="col-sm-5 col-lg-5">';
                            frmHtml += '<div class="form-group">';
                                frmHtml += '<label for="first-name-vertical">Name *</label>';
                                frmHtml += '<div class="controls">';
                                    frmHtml += '<input type="text" name="model_name['+next+']" class="form-control subservice-name" placeholder="Name" required data-rule-checkexist="true">';
                                frmHtml += '</div>';
                            frmHtml += '</div>';
                            frmHtml += '<div class="form-group">';
                                frmHtml += '<label for="first-name-vertical">Small Seg. Price*</label>';
                                frmHtml += '<div class="input-group">';
                                    frmHtml += '<div class="input-group-prepend">';
                                        frmHtml += '<span class="input-group-text">$</span>';
                                    frmHtml += '</div>';
                                    frmHtml += '<input type="text" name="small['+next+']" class="form-control" placeholder="Price" required>';
                                frmHtml += '</div>';
                            frmHtml += '</div>';
                            frmHtml += '<div class="form-group">';
                                frmHtml += '<label for="first-name-vertical">Medium Seg. Price*</label>';
                                frmHtml += '<div class="input-group">';
                                    frmHtml += '<div class="input-group-prepend">';
                                        frmHtml += '<span class="input-group-text">$</span>';
                                    frmHtml += '</div>';
                                    frmHtml += '<input type="text" name="medium['+next+']" class="form-control" placeholder="Price" required>';
                                frmHtml += '</div>';
                            frmHtml += '</div>';
                            frmHtml += '<div class="form-group">';
                                frmHtml += '<label for="first-name-vertical">Large Seg. Price*</label>';
                                frmHtml += '<div class="input-group">';
                                    frmHtml += '<div class="input-group-prepend">';
                                        frmHtml += '<span class="input-group-text">$</span>';
                                    frmHtml += '</div>';
                                    frmHtml += '<input type="text" name="large['+next+']" class="form-control" placeholder="Price" required>';
                                frmHtml += '</div>';
                            frmHtml += '</div>';
                            frmHtml += '<div class="form-group">';
                                frmHtml += '<label for="first-name-vertical">Premium Seg. Price*</label>';
                                frmHtml += '<div class="input-group">';
                                    frmHtml += '<div class="input-group-prepend">';
                                        frmHtml += '<span class="input-group-text">$</span>';
                                    frmHtml += '</div>';
                                    frmHtml += '<input type="text" name="premium['+next+']" class="form-control" placeholder="Price" required>';
                                frmHtml += '</div>';
                            frmHtml += '</div>';
                        frmHtml += '</div>';
                        frmHtml += '<div class="col-sm-5 col-lg-6">';
                            frmHtml += '<div class="form-group">';
                                frmHtml += '<label for="first-name-vertical">Description</label>';
                                frmHtml += '<div class="controls">';
                                    frmHtml += '<textarea name="model_description['+next+']" class="form-control" rows="15"></textarea>';
                                frmHtml += '</div>';
                            frmHtml += '</div>';
                        frmHtml += '</div>';
                        frmHtml += '<div class="col-sm-2 col-lg-1">';
                            frmHtml += '<div class="form-group">';
                                frmHtml += '<label for="first-name-vertical"></label>';
                                frmHtml += '<div class="controls add-btn">';
                                    frmHtml += '<i class="feather icon-plus-circle cursor-pointer add-subservice" style="font-size: 20px;margin-right: 5px;"></i>';
                                    frmHtml += '<i class="feather icon-x-circle cursor-pointer remove-subservice" data-current="'+next+'" style="font-size: 20px;"></i>';
                                frmHtml += '</div>';
                            frmHtml += '</div>';
                        frmHtml += '</div>';
                    frmHtml += '</div>';
                frmHtml += '</div>';

                $('.subservice-list-section').append(frmHtml);
                $('.subservice-list-section').attr('data-current', next);
            });

            $(document).on('click', '.remove-subservice', function() {
                var current = $(this).data('current');
                $('#subservice'+current).remove();
            });

        </script>
@endsection
