@extends('layouts/contentLayoutMaster')

@section('title', $vendor->name)

@section('vendor-style')
        {{-- vendor css files --}}
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection

@section('page-style')
        <!-- Page css files -->
        <link rel="stylesheet" href="{{ asset(mix('css/pages/users.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/pages/app-ecommerce-shop.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/plugins/file-uploaders/dropzone.css')) }}">
@endsection

@section('content')
    

    {{-- <form id="vendorFrm" method="POST" action="{{ URL::to('vendor/') }}/{{ $vendor->id }}">
        @csrf
        <input name="_method" type="hidden" value="PUT"> --}}
        
        <div class="content-header row">
            <div class="content-header-left col-md-7 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <div class="breadcrumb-wrapper col-12" style="padding: 0;">
                            @if(@isset($breadcrumbs_custom))
                                <ol class="breadcrumb" style="border: none;padding-left: 0px !important;">
                                    @foreach ($breadcrumbs_custom as $breadcrumb)
                                        <li class="breadcrumb-item">
                                            @if(isset($breadcrumb['link']))
                                                <a href="/{{ $breadcrumb['link'] }}"> @endif{{$breadcrumb['name']}}@if(isset($breadcrumb['link'])) </a>
                                            @endif
                                        </li>
                                    @endforeach
                                </ol> 
                            @endisset
                        </div>
                    </div>
                </div>
            </div>
            {{-- <div class="content-header-right text-md-right col-md-5 col-12 d-md-block d-none">
                <button type="submit" id="save_vendor" disabled="disabled" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Update Vendor</button>
            </div> --}}
        </div>

        <div class="row">
            <div class="col-xl-3 col-lg-4 col-12">
                
                <div class="card">
                    <div class="card-header mx-auto pb-0">
                        <div class="row m-0">
                            <div class="col-sm-12 text-center">
                                <h4 class="label-control label-vendor_name">{{ $vendor->name }}<a href="{{ URL::to('vendor/' . $vendor->id . '/edit') }}" class="inline-edit-icon"><i class="feather icon-edit"></i></a></h4>
                                {{-- <div class="inline-edit-section vendor_name-edit">
                                    <h4 class="label-control label-vendor_name">{{ $vendor->name }}<span class="inline-edit-icon d-none" data-field="vendor_name"><i class="feather icon-edit"></i></span></h4>
                                    <div class="controls edit-control">
                                        <input type="text" name="vendor-name" value="{{ $vendor->name }}" class="form-control vendor_name-input" required>
                                    </div>
                                </div> --}}
                            </div>
                            <div class="col-sm-12 text-center">
                                <p class="">{{ $vendor->code }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="card-body text-center mx-auto pb-0">
                            <div class="avatar avatar-xl">
                                <img class="img-fluid" src="{{ $vendor->logo_url }}" alt="img placeholder">
                            </div>
                            <div class="d-flex justify-content-between mt-2">
                                <div class="uploads">
                                    <p class="font-weight-bold font-medium-2 mb-0">{{ $vendor->active_orders_count }}</p>
                                    <span class="">Active Orders</span>
                                </div>
                                <div class="followers">
                                    <p class="font-weight-bold font-medium-2 mb-0">₹ {{ isset($vendor['total_revenue']) ?  number_format(round($vendor['total_revenue']->total_amount, 2), 2, '.', '') : 0 }}</p>
                                    <span class="">Total Revenue</span>
                                </div>
                                <div class="following">
                                    <p class="font-weight-bold font-medium-2 mb-0">{{ $vendor->completed_orders_count }}</p>
                                    <span class="">Completed Orders</span>
                                </div>
                            </div>
                            <hr class="my-2">
                            <dl class="text-left">
                                <dt>Contact No<span class="inline-edit-icon d-none" data-field="vendor_contact_no"><i class="feather icon-edit"></i></span></dt>
                                <dd>
                                    <div class="inline-edit-section vendor_contact_no-edit">
                                        <small class="label-control label-vendor_contact_no">+91 {{ $vendor->contact_no }}</small>
                                        <div class="controls edit-control">
                                            <input type="text" name="vendor-contact_no" value="{{ $vendor->contact_no }}" class="form-control vendor_contact_no-input" required>
                                        </div>
                                    </div>
                                </dd>
                                <dt>GST Number<span class="inline-edit-icon d-none" data-field="vendor_GST_number"><i class="feather icon-edit"></i></span></dt>
                                <dd>
                                    <div class="inline-edit-section vendor_GST_number-edit">
                                        <small class="label-control label-vendor_GST_number">{{ $vendor->GST_number }}</small>
                                        <div class="controls edit-control">
                                            <input type="text" name="vendor-GST_number" value="{{ $vendor->GST_number }}" class="form-control vendor_GST_number-input">
                                        </div>
                                    </div>
                                </dd>
                                <dt>Agreement Number<span class="inline-edit-icon d-none" data-field="vendor_agreement_number"><i class="feather icon-edit"></i></span></dt>
                                <dd>
                                    <div class="inline-edit-section vendor_agreement_number-edit">
                                        <small class="label-control label-vendor_agreement_number">{{ $vendor->agreement_number }}</small>
                                        <div class="controls edit-control">
                                            <input type="text" name="vendor-agreement_number" value="{{ $vendor->agreement_number }}" class="form-control vendor_agreement_number-input">
                                        </div>
                                    </div>
                                </dd>
                                <dt>Address<span class="inline-edit-icon d-none" data-field="vendor_address"><i class="feather icon-edit"></i></span></dt>
                                <dd>
                                    <div class="inline-edit-section vendor_address-edit">
                                        <small class="label-control label-vendor_address">{{ $vendor->address_line_1 }} <br> {{ $vendor->address_line_2 }}, <br>{{ $vendor->city->name }}, {{ $vendor->state->name }} - {{ $vendor->zipcode }}</small>
                                        <div class="edit-control my-1">
                                            <label for="first-name-vertical">Address Line 1</label>
                                            <div class="controls mb-1">
                                                <input type="text" name="vendor-address_line_1" value="{{ $vendor->address_line_1 }}" class="form-control">
                                            </div>
                                            <label for="first-name-vertical">Address Line 2</label>
                                            <div class="controls mb-1">
                                                <input type="text" name="vendor-address_line_2" value="{{ $vendor->address_line_2 }}" class="form-control">
                                            </div>
                                            <label for="first-name-vertical">State</label>
                                            <div class="controls mb-1">
                                                <!-- <input type="text" name="vendor-state_id" value="{{ $vendor->state->name }}" class="form-control"> -->
                                                <select name="vendor-state_id" id="state_id" class="form-control select2">
                                                    <option value='{{ $vendor->state_id }}' selected="selected">{{ $vendor->state->name }}</option>
                                                </select>
                                            </div>
                                            <label for="first-name-vertical">City</label>
                                            <div class="controls mb-1">
                                                <!-- <input type="text" name="vendor-city_id" value="{{ $vendor->city->name }}" class="form-control"> -->
                                                <select name="vendor-city_id" id="city_id" class="form-control select2">
                                                    <option value='{{ $vendor->city_id }}' selected="selected">{{ $vendor->city->name }}</option>
                                                </select>
                                            </div>
                                            <label for="first-name-vertical">Zipcode</label>
                                            <div class="controls">
                                                <input type="text" name="vendor-zipcode" value="{{ $vendor->zipcode }}" class="form-control">
                                            </div>
                                            <button type="button" class="btn btn-primary mt-1 waves-effect waves-light save-address">Save</button>
                                            <button type="button" class="btn btn-light mt-1 waves-effect waves-light inline-edit-icon d-none" data-field="vendor_address">Cancel</button>
                                        </div>
                                    </div>
                                </dd>
                                <dt>Email<span class="inline-edit-icon d-none" data-field="vendor_email"><i class="feather icon-edit"></i></span></dt>
                                <dd>
                                    <div class="inline-edit-section vendor_email-edit">
                                        <small class="label-control label-vendor_email">{{ $vendor->email }}</small>
                                        <div class="controls edit-control">
                                            <input type="text" name="vendor-email" value="{{ $vendor->email }}" class="form-control vendor_email-input">
                                        </div>
                                    </div>
                                </dd>
                            </dl>
                        </div>

                        <div class="card-header pt-0">
                            <h4 class="card-title">Owner Information</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body pt-1">
                                <dl>
                                    <dt>Name<span class="inline-edit-icon d-none" data-field="vendor_admin_name"><i class="feather icon-edit"></i></span></dt>
                                    <dd>
                                        <div class="inline-edit-section vendor_admin_name-edit">
                                            <small class="label-control label-vendor_admin_name">{{ $vendor->admin->name }}</small>
                                            <div class="controls edit-control">
                                                <input type="text" name="vendorAdmin-name" value="{{ $vendor->admin->name }}" class="form-control vendor_admin_name-input" required>
                                            </div>
                                        </div>
                                    </dd>
                                    <dt>Contact No<span class="inline-edit-icon d-none" data-field="vendor_admin_contact_no"><i class="feather icon-edit"></i></span></dt>
                                    <dd>
                                        <div class="inline-edit-section vendor_admin_contact_no-edit">
                                            <small class="label-control label-vendor_admin_contact_no">+91 {{ $vendor->admin->contact_no }}</small>
                                            <div class="controls edit-control">
                                                <input type="text" name="vendorAdmin-contact_no" value="{{ $vendor->admin->contact_no }}" class="form-control vendor_admin_contact_no-input">
                                            </div>
                                        </div>
                                    </dd>
                                    <dt>Aadhar Card Number<span class="inline-edit-icon d-none" data-field="vendor_admin_aadhar_card_number"><i class="feather icon-edit"></i></span></dt>
                                    <dd>
                                        <div class="inline-edit-section vendor_admin_aadhar_card_number-edit">
                                            <small class="label-control label-vendor_admin_aadhar_card_number">{{ $vendor->admin->aadhar_card_number }}</small>
                                            <div class="controls edit-control">
                                                <input type="text" name="vendorAdmin-aadhar_card_number" value="{{ $vendor->admin->aadhar_card_number }}" class="form-control vendor_admin_aadhar_card_number-input" required>
                                            </div>
                                        </div>
                                    </dd>
                                    <dt>PAN Number<span class="inline-edit-icon d-none" data-field="vendor_admin_PAN_number"><i class="feather icon-edit"></i></span></dt>
                                    <dd>
                                        <div class="inline-edit-section vendor_admin_PAN_number-edit">
                                            <small class="label-control label-vendor_admin_PAN_number">{{ $vendor->admin->PAN_number }}</small>
                                            <div class="controls edit-control">
                                                <input type="text" name="vendorAdmin-PAN_number" value="{{ $vendor->admin->PAN_number }}" class="form-control vendor_admin_PAN_number-input" required>
                                            </div>
                                        </div>
                                    </dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="row">
                <!-- Manage Pickup Persons -->
              <div class="col-md-6 col-lg-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <h4>Pickup Persons</h4>
                    </div>
                    <div class="card-body">

                        @if ( isset($vendor->pickup_persons) && count($vendor->pickup_persons) > 0 )
                            @foreach($vendor->pickup_persons as $inc => $pickup_person)
                            <div class="d-flex justify-content-start align-items-center mb-1 pickup_person-item-{{ $pickup_person->id }}">
                                <div class="avatar mr-50">
                                    <img src="{{ $pickup_person->profile_pic_url }}" alt="avtar img holder" height="35" width="35">
                                </div>
                                <div class="user-page-info">
                                    <h6 class="mb-0">{{ $pickup_person->name }}</h6>
                                    <span class="font-small-2">{{ $pickup_person->contact_no }}</span>
                                </div>
                                
                                <span class="inline-tr-edit-icon ml-auto cursor-pointer mr-25 edit-pickup_person pickup_person-{{ $pickup_person['id'] }}" data-edit="true" data-pickup_person="{{ $pickup_person }}"><i class="feather icon-edit"></i></span>
                                <span class="inline-tr-edit-icon cursor-pointer delete-pickup_person" data-item="{{ $pickup_person }}"><i class="feather icon-trash-2"></i></span>
                            </div>
                            @endforeach
                        @endif

                        <div class="justify-content-start align-items-center mb-1 add-pickup_person-section d-none">
                            <!-- <form id="pickup_personFrmAdd" method="post" action="{{ URL::to('vendorPickupPerson') }}" >
                                @csrf
                                <input type="hidden" name="vendor_id" value="{{ $vendor->id }}"> -->
                                <div class="row">
                                    <div class="col-xl-12 col-12">
                                        <label for="first-name-vertical">Name</label>
                                        <div class="controls">
                                            <input type="text" placeholder="Name" name="name" id="name" class="form-control name_error">
                                            <label id="name_error" class="error" for="name"></label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-xl-12 col-12">
                                        <label for="first-name-vertical">Contact No</label>
                                        <div class="controls">
                                            <input type="text" placeholder="Contact No" id="contact_no" name="contact_no" class="form-control contact_no_error">
                                            <label id="contact_no_error" class="error" for="contact_no"></label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-xl-6 col-6">
                                        <button type="button" class="btn btn-primary btn-block mt-1 waves-effect waves-light save-pickup_person">Save</button>
                                    </div>
                                    <div class="col-xl-6 col-6">
                                        <button type="button" class="btn btn-light btn-block mt-1 waves-effect waves-light add-pickup_person">Cancel</button>
                                    </div>
                                </div>
                            <!-- </form> -->
                        </div>
                        
                        <button type="button" class="btn btn-primary w-100 mt-1 waves-effect waves-light add-pickup_person"><i class="feather icon-plus mr-25"></i>Add Pickup Person</button>
                    </div>
                </div>
              </div> 

                <!-- Manage Documents -->
            <div class="col-md-6 col-lg-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center border-bottom pb-1">
                        <div>
                            <p class="mb-75"><strong>Documents</strong></p>
                        </div>
                    </div>
                    <div class="card-body">
                        <ul class="nav nav-tabs nav-justified" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#logo" role="tab">Logo</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#gst" role="tab">GST</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#agreement" role="tab">Agreement</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#aadhar" role="tab">Aadhar Card</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#pan" role="tab">PAN Card</a>
                            </li>
                        </ul>
                        <div class="tab-content pt-1">
                            <div class="tab-pane active" id="logo">
                                <div class="row">
                                    <div class="col-md-12 col-12 mb-50">
                                        <div class="controls">
                                            <div class="user-latest-img">
                                                <img src="{{ $vendor->logo_url }}" width="115" class="img-fluid mb-1 rounded-sm" alt="{{ $vendor->logo }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="gst">
                                <div class="row">
                                    <div class="col-md-12 col-12 mb-50">
                                        <div class="controls">
                                            
                                            @if ( $vendor->GST_certificate )
                                                <div class="user-latest-img">
                                                    @if (pathinfo($vendor->GST_certificate, PATHINFO_EXTENSION) == 'docx')
                                                        <a href="{{ $vendor->GST_certificate_url }}" download><img src="{{ asset('images/files/doc.png') }}" class="img-fluid mb-1 rounded-sm" alt="{{ $vendor->GST_certificate }}" width="115"></a>
                                                    @elseif ( pathinfo($vendor->GST_certificate, PATHINFO_EXTENSION) == 'pdf' )
                                                        <a href="{{ $vendor->GST_certificate_url }}" download><img src="{{ asset('images/files/pdf.png') }}" class="img-fluid mb-1 rounded-sm" alt="{{ $vendor->GST_certificate }}" width="115"></a>
                                                    @else
                                                        <img src="{{ $vendor->GST_certificate_url }}" width="250" class="img-fluid mb-1 rounded-sm" alt="{{ $vendor->GST_certificate }}">
                                                    @endif
                                                </div>
                                            @endif
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="agreement">
                                <div class="row">
                                    <div class="col-md-12 col-12 mb-50">
                                        <div class="controls">

                                            @if ( $vendor->agreement )
                                                <div class="user-latest-img">
                                                    @if (pathinfo($vendor->agreement, PATHINFO_EXTENSION) == 'docx')
                                                        <a href="{{ $vendor->agreement_url }}" download><img src="{{ asset('images/files/doc.png') }}" class="img-fluid mb-1 rounded-sm" alt="{{ $vendor->agreement }}" width="115"></a>
                                                    @elseif ( pathinfo($vendor->agreement, PATHINFO_EXTENSION) == 'pdf' )
                                                        <a href="{{ $vendor->agreement_url }}" download><img src="{{ asset('images/files/pdf.png') }}" class="img-fluid mb-1 rounded-sm" alt="{{ $vendor->agreement }}" width="115"></a>
                                                    @else
                                                        <img src="{{ $vendor->agreement_url }}" width="250" class="img-fluid mb-1 rounded-sm" alt="{{ $vendor->agreement }}">
                                                    @endif
                                                </div>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="aadhar">
                                <div class="row">
                                    <div class="col-md-12 col-12 mb-50">
                                        <div class="controls">
                                            
                                            @if ( $vendor->admin->aadhar_card )
                                                <div class="user-latest-img">
                                                    @if (pathinfo($vendor->admin->aadhar_card, PATHINFO_EXTENSION) == 'docx')
                                                        <a href="{{ $vendor->admin->aadhar_card_url }}" download><img src="{{ asset('images/files/doc.png') }}" class="img-fluid mb-1 rounded-sm" alt="{{ $vendor->admin->aadhar_card }}" width="115"></a>
                                                    @elseif ( pathinfo($vendor->admin->aadhar_card, PATHINFO_EXTENSION) == 'pdf' )
                                                        <a href="{{ $vendor->admin->aadhar_card_url }}" download><img src="{{ asset('images/files/pdf.png') }}" class="img-fluid mb-1 rounded-sm" alt="{{ $vendor->admin->aadhar_card }}" width="115"></a>
                                                    @else
                                                        <img src="{{ $vendor->admin->aadhar_card_url }}" width="250" class="img-fluid mb-1 rounded-sm" alt="{{ $vendor->admin->aadhar_card }}">
                                                    @endif
                                                </div>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="pan">
                                <div class="row">
                                    <div class="col-md-12 col-12 mb-50">
                                        <div class="controls">
                                            
                                            @if ( $vendor->admin->PAN_card )
                                                <div class="user-latest-img">
                                                    @if (pathinfo($vendor->admin->PAN_card, PATHINFO_EXTENSION) == 'docx')
                                                        <a href="{{ $vendor->admin->PAN_card_url }}" download><img src="{{ asset('images/files/doc.png') }}" class="img-fluid mb-1 rounded-sm" alt="{{ $vendor->admin->PAN_card }}" width="115"></a>
                                                    @elseif ( pathinfo($vendor->admin->PAN_card, PATHINFO_EXTENSION) == 'pdf' )
                                                        <a href="{{ $vendor->admin->PAN_card_url }}" download><img src="{{ asset('images/files/pdf.png') }}" class="img-fluid mb-1 rounded-sm" alt="{{ $vendor->admin->PAN_card }}" width="115"></a>
                                                    @else
                                                        <img src="{{ $vendor->admin->PAN_card_url }}" width="250" class="img-fluid mb-1 rounded-sm" alt="{{ $vendor->admin->PAN_card }}">
                                                    @endif
                                                </div>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

            </div>
            <div class="col-xl-9 col-lg-8 col-12">

                <div class="card">
                    <div class="card-content">
                        <div class="card-body card-dashboard">
                            
                            <ul class="nav nav-tabs nav-justified" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#orders" role="tab">Orders</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#transactionHistory" role="tab">Transaction History</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#revenue" role="tab">Revenue</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#activityLog" role="tab">Activity Log</a>
                                </li>
                            </ul>
                            <div class="tab-content pt-1">
                                <div class="tab-pane active" id="orders">
                                    {{-- Orders --}}
                                    <div class="table-responsive">
                                        <table class="table order-table compact-table table-data-nowrap">
                                            <thead>
                                                <tr>
                                                    <th>Sr</th>
                                                    <th>Order No</th>
                                                    <th>Status</th>
                                                    <th>Order Date</th>
                                                    <th>Customer Name</th>
                                                    <th>Amount</th>
                                                    <th>Payment Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                              @php
                                                $sr = 1;
                                              @endphp
                                              @foreach($orders as $in => $order)
                                                <tr class="group">
                                                    <td>{{ $sr }}</td>
                                                    <td><a href="{{ URL::to('orders/' . $order->order_id) }}">#{{ $order->sequence_id }}</a></td>
                                                    <td><div class="badge badge-secondary">{{ $status[$order->status] }}</div></td>
                                                    <td>{{ $order->created_at->format('d/m/Y') }}</td>
                                                    <td>{{ $order->customer->name }}</td>
                                                    <td>₹{{ $order->total }}</td>
                                                    <td>-</td>
                                                    <td>
                                                        @if ( !$order->is_awaiting_for_approval )
                                                            <div class="dropdown">
                                                                <button type="button" class="btn btn-primary btn-sm dropdown-toggle waves-effect waves-light" data-toggle="dropdown">Action</button>
                                                                <div class="dropdown-menu">
                                                                    
                                                                    @foreach ($allow_status[$order->status] as $status1)
                                                                        @if ( $status1['id'] != $order->status )
                                                                            <a class="dropdown-item status-change" data-status="{{ json_encode($status1) }}" data-order="{{ json_encode(array('id' => $order->order_id, 'sequence_id' => $order->sequence_id, 'vendor_id' => $order->vendor_id)) }}" href="javascript:void(0)">{{ $status1['name'] }}</a>
                                                                        @endif
                                                                    @endforeach
                                                                    <div class="dropdown-divider"></div>
                                                                    
                                                                    @if ( $order->status == 'service_completed' )
                                                                        <a class="dropdown-item request-for-payment" data-order="{{ json_encode(array('id' => $order->order_id, 'sequence_id' => $order->sequence_id, 'vendor_id' => $order->vendor_id)) }}" href="javascript:void(0)">Request for Payment</a>
                                                                    @endif
                                                                    
                                                                    <a class="dropdown-item" href="{{ URL::to('orders/' . $order->order_id . '/edit') }}">Edit</a>
                                                                </div>
                                                            </div>
                                                        @else - @endif
                                                    </td>
                                                </tr>
                                              @endforeach
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>Sr</th>
                                                    <th>Order No</th>
                                                    <th>Status</th>
                                                    <th>Order Date</th>
                                                    <th>Customer Name</th>
                                                    <th>Amount</th>
                                                    <th>Payment Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane" id="transactionHistory">
                                    Transaction History
                                </div>
                                <div class="tab-pane" id="revenue">
                                    Revenue
                                </div>
                                <div class="tab-pane" id="activityLog">
                                    <ul class="activity-timeline timeline-left list-unstyled">
                                        <li>
                                            <div class="timeline-icon bg-primary">
                                                <i class="feather icon-plus font-medium-2"></i>
                                            </div>
                                            <div class="timeline-info">
                                                <p class="font-weight-bold">Task Added</p>
                                                <span>Bonbon macaroon jelly beans gummi bears jelly lollipop apple</span>
                                            </div>
                                            <small class="">25 days ago</small>
                                        </li>
                                        <li>
                                            <div class="timeline-icon bg-warning">
                                                <i class="feather icon-alert-circle font-medium-2"></i>
                                            </div>
                                            <div class="timeline-info">
                                                <p class="font-weight-bold">Task Updated</p>
                                                <span>Cupcake gummi bears soufflé caramels candy</span>
                                            </div>
                                            <small class="">15 days ago</small>
                                        </li>
                                        <li>
                                            <div class="timeline-icon bg-success">
                                                <i class="feather icon-check font-medium-2"></i>
                                            </div>
                                            <div class="timeline-info">
                                                <p class="font-weight-bold">Task Completed</p>
                                                <span>Candy ice cream cake. Halvah gummi bears
                                                </span>
                                            </div>
                                            <small class="">20 minutes ago</small>
                                        </li>
                                        <li>
                                            <div class="timeline-icon bg-primary">
                                                <i class="feather icon-plus font-medium-2"></i>
                                            </div>
                                            <div class="timeline-info">
                                                <p class="font-weight-bold">Task Added</p>
                                                <span>Bonbon macaroon jelly beans gummi bears jelly lollipop apple</span>
                                            </div>
                                            <small class="">25 days ago</small>
                                        </li>
                                        <li>
                                            <div class="timeline-icon bg-warning">
                                                <i class="feather icon-alert-circle font-medium-2"></i>
                                            </div>
                                            <div class="timeline-info">
                                                <p class="font-weight-bold">Task Updated</p>
                                                <span>Cupcake gummi bears soufflé caramels candy</span>
                                            </div>
                                            <small class="">15 days ago</small>
                                        </li>
                                        <li>
                                            <div class="timeline-icon bg-success">
                                                <i class="feather icon-check font-medium-2"></i>
                                            </div>
                                            <div class="timeline-info">
                                                <p class="font-weight-bold">Task Completed</p>
                                                <span>Candy ice cream cake. Halvah gummi bears
                                                </span>
                                            </div>
                                            <small class="">20 minutes ago</small>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    {{-- </form> --}}

    @section('carsseva-alert')

        <!-- Alerts -->
        @foreach (['danger', 'warning', 'success', 'info'] as $key)
            @if(Session::has($key))
                <div class="cws-alert">
                    <div class="alert alert-{{ $key }} alert-dismissible alert-auto-hide fade show" role="alert">
                        <div class="alert-text">{{ Session::get($key) }}</div>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
                        </button>
                    </div>
                </div>
            @endif
        @endforeach

        <!-- Confirmation alert msg -->
        <div class="cws-alert pickup_person-alert" style="display: none;">
            <div class="background-overlay pickup_person-reject"></div>
            <div class="alert alert-dark-danger alert-dismissible fade show" role="alert">
                <div class="alert-text">Are you sure you want to delete <strong class="pickup_person-name"></strong>? 
                    <div class="alert-btn">
                        <a href="#" class="btn btn-primary pickup_person-confirm">Yes</a>
                        <button class="btn btn-white pickup_person-reject">Cancel</button>
                    </div>
                </div>
                <button type="button" class="close pickup_person-reject">
                    <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
                </button>
            </div>
        </div>

        <div class="cws-alert form-change" style="display: none;">
            <div class="alert alert-warning alert-dismissible fade show" role="alert" style="padding: 10px 27px;background: #ff9f43 !important;">
                <div class="alert-text">Vendor information have changed, you should save them!</div>
            </div>
        </div>

        <!-- Confirmation alert msg for status change -->
        <div class="cws-alert status-change-alert" style="display: none;">
            <div class="background-overlay status-change-reject"></div>
            <div class="alert alert-dark-danger alert-dismissible fade show" role="alert">
                <div class="alert-text">Are you sure you want to <strong class="status-change-name"></strong>? 
                    <div class="alert-btn">
                        <button class="btn btn-primary status-change-confirm">Yes</button>
                        <button class="btn btn-white status-change-reject">Cancel</button>
                    </div>
                </div>
                <button type="button" class="close status-change-reject">
                    <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
                </button>
            </div>
        </div>

        {{-- Modal --}}
        <div class="modal fade text-left" id="changeStatus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h4 class="modal-title">Change Status</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <input type="hidden" name="ids" id="ids">
                            <input type="hidden" name="newStatus" id="newStatus">
                            <input type="hidden" name="vendor_id" id="vendor_id1">

                            <div class="form-group pickup_per-section" style="display: none;">
                                <label for="first-name-vertical">Pickup Person *</label>
                                <div class="w-100">
                                    <select class="form-control select2" id="pickup_per">
                                        <option value="">Select Pickup Person</option>
                                        {{-- @foreach ($pickup_persons as $person)
                                            <option value="{{ $person['id'] }}">{{ $person['name'] }}</option>
                                        @endforeach --}}
                                    </select>
                                    <label id="pickup_per-error" class="error" for="pickup_per" style="display: none;">This field is required.</label>
                                </div>
                            </div>

                            <div class="form-group reason-section" style="display: none;">
                                <label for="first-name-vertical">Reason for Cancellation *</label>
                                <div class="w-100">
                                    <fieldset>
                                        <div class="vs-radio-con">
                                            <input type="radio" name="reason_for_cancellation" checked="" value="Reason 1">
                                            <span class="vs-radio">
                                            <span class="vs-radio--border"></span>
                                            <span class="vs-radio--circle"></span>
                                            </span>
                                            <span class="">Reason 1</span>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <div class="vs-radio-con">
                                            <input type="radio" name="reason_for_cancellation" value="Reason 2">
                                            <span class="vs-radio">
                                            <span class="vs-radio--border"></span>
                                            <span class="vs-radio--circle"></span>
                                            </span>
                                            <span class="">Reason 2</span>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <div class="vs-radio-con">
                                            <input type="radio" name="reason_for_cancellation" value="Reason 3">
                                            <span class="vs-radio">
                                            <span class="vs-radio--border"></span>
                                            <span class="vs-radio--circle"></span>
                                            </span>
                                            <span class="">Reason 3</span>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary allow-change-status">Change</button>
                </div>
            </div>
            </div>
        </div>

    @endsection

@endsection

@section('vendor-script')
        {{-- vendor files --}} 
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
@endsection

@section('page-script')
        {{-- Page js files --}}
        <script type="text/javascript">

            var originalFormData = $('#vendorFrm').serialize();

            $(document).ready(function() {
                
                $('.select2').select2({
                    dropdownAutoWidth: true,
                    width: '100%',
                });

                // Datatable
                var table = $('.order-table').DataTable({
                    "ordering": false,
                    "columnDefs": [
                        { "visible": false, "targets": 0},
                        { "width": '10%', "targets": 1},
                        { "width": '10%', "targets": 2},
                        { "width": '10%', "targets": 3},
                        { "width": '18%', "targets": 4},
                        { "width": '10%', "targets": 5},
                        { "width": '18%', "targets": 6},
                    ],
                    "order": [
                        [0, 'asc']
                    ]
                });

                // Delete Pickup Person
                $('.delete-pickup_person').click( function() {
                    var item = $(this).data('item');

                    $('.pickup_person-name').html('pickup person');
                    $('.pickup_person-confirm').attr('href', "{{ URL::to('vendorPickupPerson') }}/"+item.id+"/delete");
                    $('.pickup_person-alert').show();
                });

                $('.pickup_person-reject').click( function() {
                    $('.pickup_person-alert').hide();
                });

                window.originalFormData = $('#vendorFrm').serialize();
            });

            $(document).on('click', '.status-change', function() {
                var order = $(this).data('order');
                var status = $(this).data('status');

                $('.status-change-name').html(status.name + ' ' + order.sequence_id);
                $('.status-change-alert').show();
                $('.status-change-confirm').attr('data-ids', order.id);
                $('.status-change-confirm').attr('data-status', status.id);
                $('.status-change-reject').attr('data-status', status.id);
                $('.status-change-confirm').attr('data-vendor_id', order.vendor_id);
            });
            
            $(document).on('click', '.request-for-payment', function() {
                var order = $(this).data('order');
                
                $.post('orders/request-for-payment', {'id': order.id}, function(response){
                    if ( response.response_code == 200 ) {
                        window.location = "{{ url('/orders') }}";
                    }
                });
            });

            $('.status-change-confirm').click( function() {

                // trigger status-change event
                var ids = $(this).attr('data-ids');
                var status = $(this).attr('data-status');
                var vendor_id = $(this).attr('data-vendor_id');

                if ( status == 'order_cancelled' || status == 'team_enroute' ) {
                    $('.status-change-alert').hide();

                    $('#ids').val(ids);
                    $('#newStatus').val(status);
                    $('#vendor_id1').val(vendor_id);
                    $('#changeStatus').modal('show');
                    if ( status == 'order_cancelled' ) {
                        $('.reason-section').show();
                        $('.pickup_per-section').hide();
                    } else if ( status == 'team_enroute' ) {
                        $('.reason-section').hide();
                        $('.pickup_per-section').show();
                    }
                } else {
                    $.post('/orders/change-status', {'status': status, 'id': ids}, function(response){
                        if ( response.status == 200 ) {
                            window.location = "{{ url('/orders') }}";
                        }
                    });
                }
            });

            $('#changeStatus').on('show.bs.modal', function (e) {

                var vendor_id = $('#vendor_id1').val();
                
                $("#pickup_per").val("");
                $("#pickup_per").select2({
                    width: '100%',
                    ajax: {
                        url: "/vendorPickupPerson/"+vendor_id,
                        type: 'get',
                        dataType: 'json',
                        processResults: function (data, params) {
                            //console.log(data.response_data, params);
                            var results = [];
                            if ( data.response_code == 200 ) {
                                $.each(data.response_data, function(index, item){
                                    results.push({
                                        id: item.id,
                                        text: item.name
                                    });
                                });
                            }
                            return {
                                results: results
                            };
                        },
                        cache: true     
                    }
                });
            })
            
            $('.allow-change-status').click( function() {

                // trigger status-change event
                var order_id = $('#ids').val();
                var new_status = $('#newStatus').val();

                var dataPass = {'status': new_status, 'id': order_id};
                if ( new_status == 'team_enroute' ) {
                    if ( $('#pickup_per').val() == '' ) {
                        $('#pickup_per-error').show();
                        return false;
                    } else {
                        $('#pickup_per-error').hide();
                        dataPass.pickup_person_id = $('#pickup_per').val();
                    }
                } else if ( new_status == 'order_cancelled' ) {
                    dataPass.reason_for_cancellation = $('input[name=reason_for_cancellation]:checked').val();
                }

                $.post('/orders/change-status', dataPass, function(response){
                    if ( response.status == 200 ) {
                        window.location = "{{ url('/orders') }}";
                    }
                });
            });

            $('.status-change-reject').click( function() {
                $('.status-change-alert').hide();
            });

            function checkFormChanged() {
                if (window.originalFormData !== $('#vendorFrm').serialize()) {
                    $('.form-change').show(500);
                    $("#save_vendor").attr("disabled", false);
                } else {
                    $('.form-change').hide(500);
                    $("#save_vendor").attr("disabled", true);
                }
            }

            $(document).on('click', '.add-pickup_person', function() {
                if ( $('.add-pickup_person-section').hasClass('d-none') ) {
                    $('.add-pickup_person-section').removeClass('d-none');
                } else {
                    $('.add-pickup_person-section').addClass('d-none');
                    $('#name').attr('required', true);
                    $('#contact_no').attr('required', true);
                }
            });

            $(document).on('click', '.edit-pickup_person', function() {
                var pickup_person = $(this).data('pickup_person');
                var edit = $(this).data('edit');

                var html = '';
                html += '<div class="justify-content-start align-items-center mb-1 edit-pickup_person-'+pickup_person.id+'">';
                    //html += '<form id="pickup_personFrm" method="post" action="{{ URL::to("vendorPickupPerson") }}/'+pickup_person.id+'" >';
                        //html += '@csrf';
                        //html += '<input type="hidden" name="_method" value="PUT">';
                        //html += '<input type="hidden" name="vendor_id" value="{{ $vendor->id }}">';
                        html += '<input type="hidden" name="pickup_person_id" id="pickup_person_id" value="'+pickup_person.id+'">';
                        html += '<div class="row">';
                            html += '<div class="col-xl-12 col-12">';
                                html += '<label for="first-name-vertical">Name</label>';
                                html += '<div class="controls">';
                                    html += '<input type="text" placeholder="Name" name="name" id="edit_name" class="form-control name_error" value="'+pickup_person.name+'" required>';
                                    html += '<label id="name_error" class="error" for="name"></label>';
                                html += '</div>';
                            html += '</div>';
                            html += '<div class="clearfix"></div>';
                            html += '<div class="col-xl-12 col-12">';
                                html += '<label for="first-name-vertical">Contact No</label>';
                                html += '<div class="controls">';
                                    html += '<input type="text" placeholder="Contact No" name="contact_no" id="edit_contact_no" class="form-control contact_no_error" value="'+pickup_person.contact_no+'" required>';
                                    html += '<label id="contact_no_error" class="error" for="contact_no"></label>';
                                html += '</div>';
                            html += '</div>';
                            html += '<div class="clearfix"></div>';
                            html += '<div class="col-xl-6 col-6">';
                                html += '<button type="button" class="btn btn-primary btn-block mt-1 waves-effect waves-light save-pickup_person" data-mode="edit">Save</button>';
                            html += '</div>';
                            html += '<div class="col-xl-6 col-6">';
                                html += '<button type="button" class="btn btn-light btn-block mt-1 waves-effect waves-light cancel-pickup_person" data-pickup_person_id="'+pickup_person.id+'">Cancel</button>';
                            html += '</div>';
                        html += '</div>';
                    //html += '</form>';
                html += '</div>';

                if ( edit ) {
                    $('.pickup_person-item-'+pickup_person.id).after(html);
                    select2Init();
                    $('#pickup_person_model_id_'+pickup_person.id).val(pickup_person.pickup_person_model_id).trigger('change');
                    $(this).data('edit', false);
                }
            });

            $(document).on('click', '.save-pickup_person', function() {
                $('.error').html('');
                var mode = $(this).data('mode');
                if ( mode == 'edit' ) {
                    
                    var pickup_person_id = $('#pickup_person_id').val();
                    $.post("/vendorPickupPerson/"+pickup_person_id, {'_method': 'PUT', 'vendor_id': '{{ $vendor->id }}', 'name': $('#edit_name').val(), 'contact_no': $('#edit_contact_no').val()}, function(response){
                        if ( response.status == 200 ) {
                            window.location = '/vendor/{{ $vendor->id }}';
                        } else {
                            $.each( response.data, function( id, msg ) {
                                console.log(id);
                                $('.'+id).addClass('error');
                                $('#'+id).html(msg[0]);
                            });
                        }
                    });

                } else {
                    $.post("/vendorPickupPerson", {'vendor_id': '{{ $vendor->id }}', 'name': $('#name').val(), 'contact_no': $('#contact_no').val()}, function(response){
                        if ( response.status == 200 ) {
                            window.location = '/vendor/{{ $vendor->id }}';
                        } else {
                            $.each( response.data, function( id, msg ) {
                                console.log(id);
                                $('.'+id).addClass('error');
                                $('#'+id).html(msg[0]);
                            });
                        }
                    });
                }
            });

            $(document).on('click', '.cancel-pickup_person', function() {
                var pickup_person_id = $(this).data('pickup_person_id');
                $('.edit-pickup_person-'+pickup_person_id).remove();
                $('.pickup_person-'+pickup_person_id).data('edit', true);
            });

            $(document).on('click', '.inline-edit-icon d-none', function() {
                var field = $(this).data('field');
                
                if ( field == 'vendor_address' ) {
                    // List
                    loadState(true);
                }

                if ( $('.'+field+'-edit').hasClass('active') ) {
                    $('.'+field+'-edit').removeClass('active');
                    //$(this).find('i').addClass('icon-edit').removeClass('icon-save');
                } else {
                    $('.'+field+'-edit').addClass('active');
                    //$(this).find('i').removeClass('icon-edit').addClass('icon-save');
                }
            });

            $(document).on('click', '.save-address', function() {
                
                var state = $('#state_id').select2('data');
                var city = $('#city_id').select2('data');
                //console.log(state[0]['text'], city[0]['text']);

                var address = $('input[name="vendor-address_line_1"]').val() + '<br>' + $('input[name="vendor-address_line_2"]').val() + '<br>' + city[0]['text'] + ', ' + state[0]['text'] + ' - ' + $('input[name="vendor-zipcode"]').val();
                $('.label-vendor_address').html(address);
                $('.vendor_address-edit').removeClass('active');
                checkFormChanged();
            });

            $(document.body).on("keypress", ".vendor_name-input", function(e){
                if (e.keyCode == 13) {
                    $('.label-vendor_name').html(this.value + '<span class="inline-edit-icon d-none" data-field="vendor_name"><i class="feather icon-edit"></i></span>');
                    $('.vendor_name-edit').removeClass('active');
                    checkFormChanged();
                    return false;
                }
            });

            $(document.body).on("keypress", ".vendor_contact_no-input", function(e){
                if (e.keyCode == 13) {
                    $('.label-vendor_contact_no').html(this.value);
                    $('.vendor_contact_no-edit').removeClass('active');
                    checkFormChanged();
                    return false;
                }
            });

            $(document.body).on("keypress", ".vendor_GST_number-input", function(e){
                if (e.keyCode == 13) {
                    $('.label-vendor_GST_number').html(this.value);
                    $('.vendor_GST_number-edit').removeClass('active');
                    checkFormChanged();
                    return false;
                }
            });

            $(document.body).on("keypress", ".vendor_agreement_number-input", function(e){
                if (e.keyCode == 13) {
                    $('.label-vendor_agreement_number').html(this.value);
                    $('.vendor_agreement_number-edit').removeClass('active');
                    checkFormChanged();
                    return false;
                }
            });

            $(document.body).on("keypress", ".vendor_email-input", function(e){
                if (e.keyCode == 13) {
                    $('.label-vendor_email').html(this.value);
                    $('.vendor_email-edit').removeClass('active');
                    checkFormChanged();
                    return false;
                }
            });

            $(document.body).on("keypress", ".vendor_admin_name-input", function(e){
                if (e.keyCode == 13) {
                    $('.label-vendor_admin_name').html(this.value);
                    $('.vendor_admin_name-edit').removeClass('active');
                    checkFormChanged();
                    return false;
                }
            });

            $(document.body).on("keypress", ".vendor_admin_contact_no-input", function(e){
                if (e.keyCode == 13) {
                    $('.label-vendor_admin_contact_no').html(this.value);
                    $('.vendor_admin_contact_no-edit').removeClass('active');
                    checkFormChanged();
                    return false;
                }
            });

            $(document.body).on("keypress", ".vendor_admin_aadhar_card_number-input", function(e){
                if (e.keyCode == 13) {
                    $('.label-vendor_admin_aadhar_card_number').html(this.value);
                    $('.vendor_admin_aadhar_card_number-edit').removeClass('active');
                    checkFormChanged();
                    return false;
                }
            });

            $(document.body).on("keypress", ".vendor_admin_PAN_number-input", function(e){
                if (e.keyCode == 13) {
                    $('.label-vendor_admin_PAN_number').html(this.value);
                    $('.vendor_admin_PAN_number-edit').removeClass('active');
                    checkFormChanged();
                    return false;
                }
            });

            $(document).on('select2:select', '#state_id', function() {
                $("#city_id").val("");
                loadCity(false, this.value);
            });

            function loadState(selected = false) {
                var stateDrop = $("#state_id").select2({
                    width: '100%',
                    ajax: {
                        url: "/location",
                        type: 'post',
                        data: {'type': 'state', 'country_id': '1'},
                        dataType: 'json',
                        /*data: function (params) {
                            return {
                                q: params.term // search term
                            };
                        },*/
                        processResults: function (data, params) {
                            //console.log(data.response_data, params);
                            var results = [];
                            if ( data.response_code == 200 ) {
                                $.each(data.response_data, function(index, item){
                                    results.push({
                                        id: item.id,
                                        text: item.name
                                    });
                                });
                            }
                            return {
                                results: results
                            };
                        },
                        cache: true     
                    },
                    initSelection : function (element, callback) {
                        if ( selected ) {
                            var data = {id: "{{ $vendor->state_id }}", text: "{{ $vendor->state->name }}"};
                            loadCity(true, "{{ $vendor->state_id }}");
                        } else {
                            var data = {};
                        }
                        callback(data);
                    }
                });
            }

            function loadCity(selected = false, state_id) {
                $("#city_id").select2({
                    width: '100%',
                    ajax: {
                        url: "/location",
                        type: 'post',
                        data: {'type': 'city', 'country_id': '1', 'state_id': state_id},
                        dataType: 'json',
                        processResults: function (data, params) {
                            //console.log(data.response_data, params);
                            var results = [];
                            if ( data.response_code == 200 ) {
                                $.each(data.response_data, function(index, item){
                                    results.push({
                                        id: item.id,
                                        text: item.name
                                    });
                                });
                            }
                            return {
                                results: results
                            };
                        },
                        cache: true     
                    },
                    initSelection : function (element, callback) {
                        if ( selected ) {
                            var data = {id: "{{ $vendor->city_id }}", text: "{{ $vendor->city->name }}"};
                        } else {
                            var data = {};
                        }
                        callback(data);
                    }
                });
            }

        </script>
@endsection
