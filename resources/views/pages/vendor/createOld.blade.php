@extends('layouts/contentLayoutMaster')

@section('title', 'Add Vendor')

@section('vendor-style')
        {{-- vendor css files --}}
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection

@section('page-style')
        <!-- Page css files -->
        <link rel="stylesheet" href="{{ asset(mix('css/plugins/forms/wizard.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/plugins/file-uploaders/dropzone.css')) }}">
@endsection

@section('content')
    
        <!-- Form wizard with step validation section start -->
        <section id="validation">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <form method="post" id="vendorFrm" action="{{ route('vendor.store') }}" class="steps-validation wizard-circle">
                                    @csrf

                                    <!-- Step 1 -->
                                    <h6><i class="step-icon fa fa-industry"></i> Basic Information</h6>
                                    <fieldset class="mt-2">
                                        <div class="row">
                                            <div class="col-md-12 col-lg-6">
                                                <h6>Company Infomation</h6>
                                                <div class="row">
                                                    <!-- <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label for="firstName3">Code *</label>
                                                            <input type="text" class="form-control" placeholder="Vendor Code" id="code" name="code" required>
                                                        </div>
                                                    </div> -->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="lastName3">Name *</label>
                                                            <input type="text" class="form-control" placeholder="Name" id="name" name="name" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="firstName3">Contact No *</label>
                                                            <input type="text" class="form-control" placeholder="Contact No" id="contact_no" name="contact_no" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="firstName3">Emergency Contact No</label>
                                                            <input type="text" class="form-control" placeholder="Emergency Contact No" id="emergency_contact_no" name="emergency_contact_no">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="lastName3">Email</label>
                                                            <input type="text" class="form-control" placeholder="Email" id="email" name="email">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="first-name-vertical">GST Number</label>
                                                            <div class="controls">
                                                                <input type="text" class="form-control" placeholder="GST Number" id="GST_number" name="GST_number">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="first-name-vertical">Agreement Number</label>
                                                            <div class="controls">
                                                                <input type="text" class="form-control" placeholder="Agreement Number" id="agreement_number" name="agreement_number">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <h6>Address Infomation</h6>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="firstName3">Address Line 1 *</label>
                                                            <input type="text" class="form-control" placeholder="Address Line 1" name="address_line_1" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="firstName3">Address Line 2</label>
                                                            <input type="text" class="form-control" placeholder="Address Line 2" name="address_line_2">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="firstName3">State *</label>
                                                            <div class="controls">
                                                                <!-- <input type="text" class="form-control" placeholder="State" name="state" required> -->
                                                                <select name="state_id" id="state_id" class="form-control select2" required>
                                                                    <option value='' selected="selected">Select State</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="firstName3">City *</label>
                                                            <!-- <input type="text" class="form-control" placeholder="City" name="city" required> -->
                                                            <div class="controls">
                                                                <!-- <input type="text" class="form-control" placeholder="State" name="state" required> -->
                                                                <select name="city_id" id="city_id" class="form-control select2" required>
                                                                    <option value='' selected="selected">Select City</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="firstName3">Zipcode *</label>
                                                            <input type="text" class="form-control" placeholder="Zipcode" name="zipcode" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <h6>Branding</h6>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="first-name-vertical">Logo</label>
                                                            <div class="controls">
                                                                <div id="vendorLogo" class="dropzone dropzone-area" style="min-height: 220px;"><div class="dz-message">Vendor Logo</div></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-lg-6">
                                                <h6>Owner Infomation</h6>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="first-name-vertical">Owner Name*</label>
                                                            <div class="controls">
                                                                <input type="text" class="form-control" placeholder="Owner Name" id="owner_name" name="owner_name" required>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="first-name-vertical">Aadhar Card Number</label>
                                                            <div class="controls">
                                                                <input type="text" class="form-control" placeholder="Aadhar Card Number" id="aadhar_card_number" name="aadhar_card_number">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="first-name-vertical">Owner Contact No*</label>
                                                            <div class="controls">
                                                                <input type="text" class="form-control" placeholder="Owner Contact No" id="owner_contact_no" name="owner_contact_no" required>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="first-name-vertical">PAN Number</label>
                                                            <div class="controls">
                                                                <input type="text" class="form-control" placeholder="PAN Number" id="PAN_number" name="PAN_number">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <h6>Documents Infomation</h6>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="first-name-vertical">GST Certificate</label>
                                                            <div class="controls">
                                                                <div id="vendorGSTCert" class="dropzone dropzone-area" style="min-height: 220px;"><div class="dz-message">GST Certificate</div></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="first-name-vertical">Agreement</label>
                                                            <div class="controls">
                                                                <div id="vendorAgreement" class="dropzone dropzone-area" style="min-height: 220px;"><div class="dz-message">Agreement</div></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="first-name-vertical">Aadhar Card</label>
                                                            <div class="controls">
                                                                <div id="vendorAadharCard" class="dropzone dropzone-area" style="min-height: 220px;"><div class="dz-message">Aadhar Card</div></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="first-name-vertical">PAN Card</label>
                                                            <div class="controls">
                                                                <div id="vendorPAN" class="dropzone dropzone-area" style="min-height: 220px;"><div class="dz-message">PAN</div></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>

                                    <!-- Step 2 -->
                                    <h6><i class="step-icon fa fa-taxi"></i> Car Manufacturers</h6>
                                    <fieldset class="mt-2">
                                        <div class="row">
                                            @if ( isset($brands) && count($brands) > 0 )
                                                @foreach ( $brands as $brand )
                                                    <div class="col-lg-2 col-md-4 col-sm-6">
                                                        <input type="checkbox" class="custom-control-input hide-checkbox" name="brands[]" value="{{ $brand->id }}" id="brand{{ $brand->id }}">
                                                        <label class="custom-control-label d-block" for="brand{{ $brand->id }}">
                                                            <div class="card checkbox-label-section border-light text-center">
                                                                <div class="card-content cursor-pointer">
                                                                    <div class="card-body">
                                                                            <img src="{{ $brand->image_url }}" alt="element 05" width="100" class="mb-1">
                                                                            <h6 class="card-title font-medium-1 text-bold-400">{{ $brand->name }}</h6>
                                                                    </div>
                                                                </div>
                                                                <i class="fa fa-check-circle primary font-medium-3 mark-icon"></i>
                                                            </div>
                                                        </label>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </fieldset>

                                    <!-- Step 3 -->
                                    <!-- <h6><i class="step-icon feather icon-list"></i> Assign Segments</h6>
                                    <fieldset class="mt-2">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table model-table compact-table w-100">
                                                        <thead>
                                                            <tr>
                                                                <th rowspan="2">Sr</th>
                                                                <th rowspan="2">Image</th>
                                                                <th rowspan="2">Name</th>
                                                                <th rowspan="2">Model</th>
                                                                <th colspan="4" class="text-center">Segments</th>
                                                            </tr>
                                                            <tr>
                                                                <th>Small</th>
                                                                <th>Medium</th>
                                                                <th>Large</th>
                                                                <th>Premium</th>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset> -->

                                    <!-- Step 4 -->
                                    <h6><i class="step-icon fa fa-steam"></i> Car Services</h6>
                                    <fieldset class="mt-2">
                                        <div class="row">
                                            @if ( isset($services) && count($services) > 0 )
                                                @foreach ( $services as $service )
                                                    <div class="col-lg-3 col-md-6 col-sm-12">
                                                        <input type="checkbox" class="custom-control-input hide-checkbox" name="services[]" value="{{ $service->id }}" id="service{{ $service->id }}">
                                                        <label class="custom-control-label d-block" for="service{{ $service->id }}">
                                                            <div class="card checkbox-label-section border-light">
                                                                <div class="card-content cursor-pointer">
                                                                    <div class="card-body">
                                                                        <img src="{{ $service->image_url }}" alt="element 05" width="50" class="mb-1">
                                                                        <h6 class="card-title font-medium-1 text-bold-400">{{ $service->name }}</h6>
                                                                        {{-- @if ( isset($service->subservices) && count($service->subservices) > 0 )
                                                                            @foreach ( $service->subservices as $subservice )
                                                                                <p class="card-text mb-0">- {{ $subservice->name }}</p>
                                                                            @endforeach
                                                                        @endif
                                                                        --}}
                                                                    </div>
                                                                </div>
                                                                <i class="fa fa-check-circle primary font-medium-3 mark-icon"></i>
                                                            </div>
                                                        </label>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </fieldset>

                                    <!-- Step 5 -->
                                    <h6><i class="step-icon fa fa-rupee"></i> Set Service Cost</h6>
                                    <fieldset class="mt-2">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="table-responsive">
                                                    <table class="table service-table compact-table w-100">
                                                        <thead>
                                                            <tr>
                                                                <th rowspan="2">Sr</th>
                                                                <th rowspan="2">Name</th>
                                                                <th rowspan="2">Service</th>
                                                                <th colspan="4" class="text-center">Segments</th>
                                                            </tr>
                                                            <tr>
                                                                <th>Small</th>
                                                                <th>Medium</th>
                                                                <th>Large</th>
                                                                <th>Premium</th>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Form wizard with step validation section end -->

        @section('carsseva-alert')

            <!-- Alerts -->
            @foreach (['danger', 'warning', 'success', 'info'] as $key)
                @if(Session::has($key))
                    <div class="cws-alert">
                        <div class="alert alert-{{ $key }} alert-dismissible fade show" role="alert">
                            <div class="alert-text">{{ Session::get($key) }}</div>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
                            </button>
                        </div>
                    </div>
                @endif
            @endforeach

        @endsection

@endsection

@section('vendor-script')
        {{-- vendor files --}}
        <script src="{{ asset(mix('vendors/js/extensions/jquery.steps.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/extensions/dropzone.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
@endsection

@section('page-script')
        {{-- Page js files --}}
        <script type="text/javascript">
            
            Dropzone.autoDiscover = false;

            $(document).ready(function() {

                $('.select2').select2({
                    dropdownAutoWidth: true,
                    width: '100%',
                });

                $("#state_id").select2({
                    ajax: {
                        url: "/location",
                        type: 'post',
                        data: {'type': 'state', 'country_id': '1'},
                        dataType: 'json',
                        /*data: function (params) {
                            return {
                                q: params.term // search term
                            };
                        },*/
                        processResults: function (data, params) {
                            //console.log(data.response_data, params);
                            var results = [];
                            if ( data.response_code == 200 ) {
                                $.each(data.response_data, function(index, item){
                                    results.push({
                                        id: item.id,
                                        text: item.name
                                    });
                                });
                            }
                            return {
                                results: results
                            };
                        },
                        cache: true     
                    }
                }); 

                // Vendor Logo
                var myDropzone = new Dropzone('#vendorLogo', {
                    url: "{{ URL::to('upload') }}",
                    paramName: "image",
                    addRemoveLinks: true,
                    acceptedFiles: 'image/*',
                    maxFiles:1,
                    init: function () {
                        this.on("maxfilesexceeded", function(file) {
                            this.removeAllFiles();
                            this.addFile(file);
                        });
                    },
                    success: function (file, response) {
                        file.previewElement.classList.add("dz-success");
                        $('#vendorFrm').append('<input type="hidden" name="logo" id="logo" value="' + file.dataURL + '">');
                    },
                    reset: function () {
                        $('#image').val('');
                        $('#logo').val('');
                    },
                    error: function (file, response) {
                        file.previewElement.classList.add("dz-error");
                        $('#image').val('');
                        $('#logo').val('');
                    }
                });

                // Vendor GST Certificate
                var myDropzone = new Dropzone('#vendorGSTCert', {
                    url: "{{ URL::to('upload') }}",
                    paramName: "image",
                    addRemoveLinks: true,
                    acceptedFiles: 'image/*',
                    maxFiles:1,
                    init: function () {
                        this.on("maxfilesexceeded", function(file) {
                            this.removeAllFiles();
                            this.addFile(file);
                        });
                    },
                    success: function (file, response) {
                        file.previewElement.classList.add("dz-success");
                        $('#vendorFrm').append('<input type="hidden" name="GST_certificate" id="GST_certificate" value="' + file.dataURL + '">');
                    },
                    reset: function () {
                        $('#image').val('');
                        $('#GST_certificate').val('');
                    },
                    error: function (file, response) {
                        file.previewElement.classList.add("dz-error");
                        $('#image').val('');
                        $('#GST_certificate').val('');
                    }
                });

                // Vendor Agreement
                var myDropzone = new Dropzone('#vendorAgreement', {
                    url: "{{ URL::to('upload') }}",
                    paramName: "image",
                    addRemoveLinks: true,
                    acceptedFiles: 'image/*',
                    maxFiles:1,
                    init: function () {
                        this.on("maxfilesexceeded", function(file) {
                            this.removeAllFiles();
                            this.addFile(file);
                        });
                    },
                    success: function (file, response) {
                        file.previewElement.classList.add("dz-success");
                        $('#vendorFrm').append('<input type="hidden" name="agreement" id="agreement" value="' + file.dataURL + '">');
                    },
                    reset: function () {
                        $('#image').val('');
                        $('#agreement').val('');
                    },
                    error: function (file, response) {
                        file.previewElement.classList.add("dz-error");
                        $('#image').val('');
                        $('#agreement').val('');
                    }
                });

                // Vendor Aadhar Card
                var myDropzone = new Dropzone('#vendorAadharCard', {
                    url: "{{ URL::to('upload') }}",
                    paramName: "image",
                    addRemoveLinks: true,
                    acceptedFiles: 'image/*',
                    maxFiles:1,
                    init: function () {
                        this.on("maxfilesexceeded", function(file) {
                            this.removeAllFiles();
                            this.addFile(file);
                        });
                    },
                    success: function (file, response) {
                        file.previewElement.classList.add("dz-success");
                        //$('#vendorFrm').append('<input type="hidden" name="aadhar_card" id="aadhar_card" value="' + file.dataURL + '">');
                        $('#aadhar_card').val(file.dataURL);
                    },
                    reset: function () {
                        $('#image').val('');
                        $('#aadhar_card').val('');
                    },
                    error: function (file, response) {
                        file.previewElement.classList.add("dz-error");
                        $('#image').val('');
                        $('#aadhar_card').val('');
                    }
                });

                // Vendor PAN
                var myDropzone = new Dropzone('#vendorPAN', {
                    url: "{{ URL::to('upload') }}",
                    paramName: "image",
                    addRemoveLinks: true,
                    acceptedFiles: 'image/*',
                    maxFiles:1,
                    init: function () {
                        this.on("maxfilesexceeded", function(file) {
                            this.removeAllFiles();
                            this.addFile(file);
                        });
                    },
                    success: function (file, response) {
                        file.previewElement.classList.add("dz-success");
                        //$('#vendorFrm').append('<input type="hidden" name="PAN_card" id="PAN_card" value="' + file.dataURL + '">');
                        $('#PAN_card').val(file.dataURL);
                    },
                    reset: function () {
                        $('#image').val('');
                        $('#PAN_card').val('');
                    },
                    error: function (file, response) {
                        file.previewElement.classList.add("dz-error");
                        $('#image').val('');
                        $('#PAN_card').val('');
                    }
                });
            });

            // Show form
            var form = $(".steps-validation").show();

            $(".steps-validation").steps({
                headerTag: "h6",
                bodyTag: "fieldset",
                transitionEffect: "fade",
                titleTemplate: '<span class="step">#index#</span> #title#',
                labels: {
                    finish: 'Submit'
                },
                onStepChanging: function (event, currentIndex, newIndex) {
                    // Allways allow previous action even if the current form is not valid!
                    if (currentIndex > newIndex) {
                        return true;
                    }

                    // Needed in some cases if the user went back (clean up)
                    if (currentIndex < newIndex) {
                        // To remove error styles
                        form.find(".body:eq(" + newIndex + ") label.error").remove();
                        form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
                    }
                    //form.validate().settings.ignore = ":disabled,:hidden";
                    form.validate().settings.ignore = ":disabled";

                    // Car Manufacturers
                    if ( currentIndex == 1 ) {
                        var brands = [];
                        $("input[name='brands[]']:checked").each ( function() {
                            if ( this.checked ) {
                                brands.push($(this).val());
                            }
                        });

                        if ( brands <= 0 ) {
                            return false;
                        }/* else {
                            modelTableSync(brands);
                        }*/
                    }

                    // Car Services
                    if ( currentIndex == 2 ) {
                        var services = [];
                        $("input[name='services[]']:checked").each ( function() {
                            if ( this.checked ) {
                                services.push($(this).val());
                            }
                        });

                        if ( services <= 0 ) {
                            return false;
                        } else {
                            serviceTableSync(services);
                        }
                    }

                    return form.valid();
                },
                onFinishing: function (event, currentIndex) {
                    form.validate().settings.ignore = ":disabled";
                    return form.valid();
                },
                onFinished: function (event, currentIndex) {
                    form.submit();
                }
            });

            // Initialize validation
            $(".steps-validation").validate({
                rules: {
                    email: {
                        email: true
                    },
                    contact_no: {
                        minlength: 10
                    },
                    GST_number: {
                        minlength: 15,
                        maxlength: 15,
                    },
                    aadhar_card_number: {
                        minlength: 12,
                        maxlength: 12,
                    },
                    PAN_number: {
                        minlength: 10,
                        maxlength: 10,
                    },
                }
            });

            function modelTableSync(brands) {
                var groupColumn = 3;
                $('.model-table').DataTable({
                    "ajax": {
                        "url": "/cars/getModels",
                        "type": "POST",
                        "data": { 'ids': brands },
                        "dataSrc": "data"
                    },
                    "columns": [
                        { "data": "id", "visible": false, },
                        { "data": "image_url", "width": '8%' },
                        { "data": "name", "width": '170px' },
                        { "data": "parent_id" },
                        { "width": '10%'},
                        { "width": '10%'},
                        { "width": '10%'},
                        { "width": '10%'},
                    ],
                    "destroy" : true,
                    "columnDefs": [
                        { "visible": false, "targets": groupColumn },
                        { "targets": 1,
                            render: function(data) {
                                var html = '<ul class="list-unstyled users-list m-0 d-flex align-items-center">';
                                        html += '<li data-toggle="tooltip" data-popup="tooltip-custom" data-placement="bottom" class="avatar pull-up">';
                                        html += '<img class="media-object rounded-circle" src="'+data+'" alt="Avatar" height="30" width="30">';
                                    html += '</li>';
                                html += '</ul>';
                                return html
                            }
                        },
                        { "targets": 4,
                            render: function(data, type, row) {
                                var html = '<fieldset>';
                                        html += '<div class="vs-radio-con vs-radio-light">';
                                        html += '<input type="radio" '+ (( row.default_segment != null && row.default_segment.segment == 'small' ) ? 'checked="checked"' : '') +' class="modelRadio" name="model['+row.id+']" value="small">';
                                        html += '<span class="vs-radio vs-radio-sm">';
                                            html += '<span class="vs-radio--border"></span>';
                                            html += '<span class="vs-radio--circle"></span>';
                                        html += '</span>';
                                    html += '</div>';
                                html += '</fieldset>';
                                return html
                            }
                        },
                        { "targets": 5,
                            render: function(data, type, row) {
                                var html = '<fieldset>';
                                        html += '<div class="vs-radio-con vs-radio-light">';
                                        html += '<input type="radio" '+ (( row.default_segment != null && row.default_segment.segment == 'medium' ) ? 'checked="checked"' : '') +' class="modelRadio" name="model['+row.id+']" value="medium">';
                                        html += '<span class="vs-radio vs-radio-sm">';
                                            html += '<span class="vs-radio--border"></span>';
                                            html += '<span class="vs-radio--circle"></span>';
                                        html += '</span>';
                                    html += '</div>';
                                html += '</fieldset>';
                                return html
                            }
                        },
                        { "targets": 6,
                            render: function(data, type, row) {
                                var html = '<fieldset>';
                                        html += '<div class="vs-radio-con vs-radio-light">';
                                        html += '<input type="radio" '+ (( row.default_segment != null && row.default_segment.segment == 'large' ) ? 'checked="checked"' : '') +' class="modelRadio" name="model['+row.id+']" value="large">';
                                        html += '<span class="vs-radio vs-radio-sm">';
                                            html += '<span class="vs-radio--border"></span>';
                                            html += '<span class="vs-radio--circle"></span>';
                                        html += '</span>';
                                    html += '</div>';
                                html += '</fieldset>';
                                return html
                            }
                        },
                        { "targets": 7,
                            render: function(data, type, row) {
                                var html = '<fieldset>';
                                        html += '<div class="vs-radio-con vs-radio-light">';
                                        html += '<input type="radio" '+ (( row.default_segment != null && row.default_segment.segment == 'premium' ) ? 'checked="checked"' : '') +' class="modelRadio" name="model['+row.id+']" value="premium">';
                                        html += '<span class="vs-radio vs-radio-sm">';
                                            html += '<span class="vs-radio--border"></span>';
                                            html += '<span class="vs-radio--circle"></span>';
                                        html += '</span>';
                                    html += '</div>';
                                html += '</fieldset>';
                                return html
                            }
                        }
                    ],
                    "order": [[ groupColumn, 'asc' ]],
                    "drawCallback": function ( settings ) {
                        var api = this.api();
                        var rows = api.rows( {page:'current'} ).nodes();
                        var last=null;
             
                        api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                            if ( last !== group ) {
                                $(rows).eq( i ).before(
                                    '<tr class="group"><td colspan="8">'+group+'</td></tr>'
                                );
                                last = group;
                            }
                        } );
                    }
                });
            }

            function serviceTableSync(services) {

                var groupColumn = 2;
                $('.service-table').DataTable({
                    "paging": false,
                    "searching": false,
                    "bInfo" : false,
                    "ajax": {
                        "url": "/services/getSubServices",
                        "type": "POST",
                        "data": { 'ids': services },
                        "dataSrc": "data"
                    },
                    "columns": [
                        { "data": "id", "visible": false, },
                        { "data": "name", "width": '170px' },
                        { "data": "service_id" },
                        { "width": '15%'},
                        { "width": '15%'},
                        { "width": '15%'},
                        { "width": '15%'},
                    ],
                    "destroy" : true,
                    "columnDefs": [
                        { "visible": false, "targets": groupColumn },
                        { "targets": 3,
                            render: function(data, type, row) {
                                var html = '<div class="controls">';
                                    html += '<div class="input-group">';
                                        html += '<div class="input-group-prepend">';
                                            html += '<span class="input-group-text">₹</span>';
                                        html += '</div>';
                                        html += '<input type="text" name="serice['+row.id+'][small]" class="form-control" value="'+ (( row.default_segment_price != null ) ? row.default_segment_price.small : '') +'" required>';
                                    html += '</div>';
                                html += '</div>';
                                return html
                            }
                        },
                        { "targets": 4,
                            render: function(data, type, row) {
                                var html = '<div class="controls">';
                                    html += '<div class="input-group">';
                                        html += '<div class="input-group-prepend">';
                                            html += '<span class="input-group-text">₹</span>';
                                        html += '</div>';
                                        html += '<input type="text" name="serice['+row.id+'][medium]" class="form-control" value="'+ (( row.default_segment_price != null ) ? row.default_segment_price.medium : '') +'" required>';
                                    html += '</div>';
                                html += '</div>';
                                return html
                            }
                        },
                        { "targets": 5,
                            render: function(data, type, row) {
                                var html = '<div class="controls">';
                                    html += '<div class="input-group">';
                                        html += '<div class="input-group-prepend">';
                                            html += '<span class="input-group-text">₹</span>';
                                        html += '</div>';
                                        html += '<input type="text" name="serice['+row.id+'][large]" class="form-control" value="'+ (( row.default_segment_price != null ) ? row.default_segment_price.large : '') +'" required>';
                                    html += '</div>';
                                html += '</div>';
                                return html
                            }
                        },
                        { "targets": 6,
                            render: function(data, type, row) {
                                var html = '<div class="controls">';
                                    html += '<div class="input-group">';
                                        html += '<div class="input-group-prepend">';
                                            html += '<span class="input-group-text">₹</span>';
                                        html += '</div>';
                                        html += '<input type="text" name="serice['+row.id+'][premium]" class="form-control" value="'+ (( row.default_segment_price != null ) ? row.default_segment_price.premium : '') +'" required>';
                                    html += '</div>';
                                html += '</div>';
                                return html
                            }
                        },
                    ],
                    "order": [[ groupColumn, 'asc' ]],
                    "drawCallback": function ( settings ) {
                        var api = this.api();
                        var rows = api.rows( {page:'current'} ).nodes();
                        var last=null;
             
                        api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                            if ( last !== group ) {
                                $(rows).eq( i ).before(
                                    '<tr class="group"><td colspan="7">'+group+'</td></tr>'
                                );
                                last = group;
                            }
                        } );
                    }
                });
            }

            $(document).on('select2:select', '#state_id', function() {
                $("#city_id").val("");
                $("#city_id").select2({
                    ajax: {
                        url: "/location",
                        type: 'post',
                        data: {'type': 'city', 'country_id': '1', 'state_id': this.value},
                        dataType: 'json',
                        processResults: function (data, params) {
                            //console.log(data.response_data, params);
                            var results = [];
                            if ( data.response_code == 200 ) {
                                $.each(data.response_data, function(index, item){
                                    results.push({
                                        id: item.id,
                                        text: item.name
                                    });
                                });
                            }
                            return {
                                results: results
                            };
                        },
                        cache: true     
                    }
                });
            });

        </script>
@endsection

