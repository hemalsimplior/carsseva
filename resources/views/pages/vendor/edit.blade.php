@extends('layouts/contentLayoutMaster')

@section('title', $title)

@section('vendor-style')
        {{-- vendor css files --}}
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection

@section('page-style')
        <!-- Page css files -->
        <link rel="stylesheet" href="{{ asset(mix('css/plugins/file-uploaders/dropzone.css')) }}">
@endsection

@section('content')
    
        <!-- Form wizard with step validation section start -->
        <section id="validation">
            <div class="row">
                <div class="col-12">
                    @if ( $progress['per'] < 100 )
                        <div class="alert alert-warning" role="alert">
                            <h4 class="alert-heading">{{ $progress['per'] }}% Account Completed!</h4>
                            <p class="mb-0">Welcome to CarsSeva, You account has been {{ $progress['per'] }}% completed!, Please setup {{ $progress['pendingSection'] }} sections and enjoy your one order!</p>
                        </div>
                    @endif
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                
                                <ul class="nav nav-tabs form-steps" role="tablist" style="border-bottom: 3px solid #ccc;">
                                    <li class="nav-item">
                                        <a class="nav-link d-flex align-items-center {{ (isset($param['s']) ? (($param['s'] == 'basic') ? 'active' : '') : 'active') }}" data-toggle="tab" href="#basic" role="tab">
                                            <i class="fa fa-industry mr-25"></i>
                                            <span>Basic Information</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link d-flex align-items-center {{ ((isset($param['s']) && $param['s'] == 'manufacturers') ? 'active' : '') }}" data-toggle="tab" href="#carManufacturers" role="tab">
                                            <i class="fa fa-taxi mr-25"></i>
                                            <span>Car Manufacturers</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link d-flex align-items-center {{ ((isset($param['s']) && $param['s'] == 'segment') ? 'active' : '') }}" data-toggle="tab" href="#assignSegments" role="tab">
                                            <i class="feather icon-list mr-25"></i>
                                            <span>Assign Segments</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link d-flex align-items-center {{ ((isset($param['s']) && $param['s'] == 'services') ? 'active' : '') }}" data-toggle="tab" href="#carServices" role="tab">
                                            <i class="fa fa-steam mr-25"></i>
                                            <span>Manage Services</span>
                                        </a>
                                    </li>
                                    {{-- <li class="nav-item">
                                        <a class="nav-link d-flex align-items-center" data-toggle="tab" href="#changePassword" role="tab">
                                            <i class="fa fa-lock mr-25"></i>
                                            <span class="d-none d-sm-block">Change Password</span>
                                        </a>
                                    </li> --}}
                                </ul>
                                <div class="tab-content pt-1">
                                    <div class="tab-pane {{ (isset($param['s']) ? (($param['s'] == 'basic') ? 'active' : '') : 'active') }}" id="basic">
                                        <form method="post" id="vendorFrm" action="{{ URL::to('vendor/' . $vendor->id) }}" class="steps-validation wizard-circle">
                                            @csrf
                                            <input name="_method" type="hidden" value="PUT">
                                            
                                            <div class="row">
                                                <div class="col-12 d-flex justify-content-end">
                                                    <button type="submit" class="btn btn-primary glow mb-1 mb-sm-0 mr-0 waves-effect waves-light">Save</button>
                                                    @if ( isset($vendor->account_section) && $vendor->account_section['basic'] )
                                                        <button type="button" class="btn btn-secondary glow mb-1 mb-sm-0 ml-1 waves-effect waves-light next-section" data-show="carManufacturers" data-hide="basic">Next</button>
                                                    @endif
                                                </div>
                                                <div class="col-md-12 col-lg-12 col-xl-6">
                                                    <h6>Company Infomation</h6>
                                                    <div class="row">
                                                        <!-- <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="firstName3">Code *</label>
                                                                <input type="text" class="form-control" placeholder="Vendor Code" id="code" name="code" required>
                                                            </div>
                                                        </div> -->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="lastName3">Name *</label>
                                                            <div class="controls">
                                                                <input type="text" class="form-control" placeholder="Name" id="name" name="name" value="{{ $vendor->name }}" required>
                                                            </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="firstName3">Contact No *</label>
                                                            <div class="controls">
                                                                <input type="text" class="form-control @error('contact_no') error @enderror" placeholder="Contact No" id="contact_no" name="contact_no" value="{{ old('contact_no', $vendor->contact_no) }}" required>
                                                                @error('contact_no')
                                                                    <label id="contact_no-error" class="error" for="contact_no">{{ $message }}</label>
                                                                @enderror
                                                            </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="firstName3">Emergency Contact No</label>
                                                            <div class="controls">  
                                                                <input type="text" class="form-control" placeholder="Emergency Contact No" id="emergency_contact_no" name="emergency_contact_no" value="{{ $vendor->emergency_contact_no }}">
                                                            </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="lastName3">Email</label>
                                                            <div class="controls">    
                                                                <input type="text" class="form-control" placeholder="Email" id="email" name="email" value="{{ $vendor->email }}">
                                                            </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="first-name-vertical">GST Number</label>
                                                                <div class="controls">
                                                                    <input type="text" class="form-control" placeholder="GST Number" id="GST_number" name="GST_number" value="{{ $vendor->GST_number }}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="first-name-vertical">Agreement Number</label>
                                                                <div class="controls">
                                                                    <input type="text" class="form-control" placeholder="Agreement Number" id="agreement_number" name="agreement_number" value="{{ $vendor->agreement_number }}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 col-lg-12 col-xl-6">
                                                    <h6>Address Infomation</h6>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="firstName3">Address Line 1 *</label>
                                                            <div class="controls">    
                                                                <input type="text" class="form-control" placeholder="Address Line 1" name="address_line_1" id="address_line_1" value="{{ $vendor->address_line_1 }}" required>
                                                            </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="firstName3">Address Line 2</label>
                                                            <div class="controls">    
                                                                <input type="text" class="form-control" placeholder="Address Line 2" name="address_line_2" id="address_line_2" value="{{ $vendor->address_line_2 }}">
                                                            </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-4 col-lg-4 col-md-4">
                                                            <div class="form-group">
                                                                <label for="firstName3">State *</label>
                                                                <div class="controls">
                                                                    <!-- <input type="text" class="form-control" placeholder="State" name="state" required> -->
                                                                    <select name="state_id" id="state_id" class="form-control select2" required>
                                                                        <option value='{{ $vendor->state_id }}' selected="selected">{{ $vendor->state->name }}</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-3 col-lg-4 col-md-4">
                                                            <div class="form-group">
                                                                <label for="firstName3">City *</label>
                                                                <!-- <input type="text" class="form-control" placeholder="City" name="city" required> -->
                                                                <div class="controls">
                                                                    <!-- <input type="text" class="form-control" placeholder="State" name="state" required> -->
                                                                    <select name="city_id" id="city_id" class="form-control select2" required>
                                                                        <option value='{{ $vendor->city_id }}' selected="selected">{{ $vendor->city->name }}</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-3 col-lg-3 col-md-2">
                                                            <div class="form-group">
                                                                <label for="firstName3">Zipcode *</label>
                                                                <div class="controls">
                                                                <input type="text" class="form-control" placeholder="Zipcode" name="zipcode" value="{{ $vendor->zipcode }}" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-2 col-lg-1 col-md-2">
                                                            <div class="form-group">
                                                                <label class="d-none d-sm-block" for="firstName3">&nbsp;</label>
                                                                <button id="trackGeo" class="btn btn-primary btn-md waves-effect waves-light" type="button"><i class="feather icon-map-pin"></i></button>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="firstName3">Latitude</label>
                                                            <div class="controls">    
                                                                <input type="text" readonly class="form-control" placeholder="Latitude" name="latitude" id="latitude" value="{{ $vendor->latitude }}">
                                                            </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="firstName3">Longitude</label>
                                                            <div class="controls">    
                                                                <input type="text" readonly class="form-control" placeholder="Longitude" name="longitude" id="longitude" value="{{ $vendor->longitude }}">
                                                            </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="firstName3">&nbsp;</label>
                                                                <p id="fullAddress"></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <h6>Owner Infomation</h6>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="first-name-vertical">Owner Name*</label>
                                                                <div class="controls">
                                                                    <input type="text" class="form-control" placeholder="Owner Name" id="owner_name" name="owner_name"  value="{{ $vendor->admin->name }}" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="first-name-vertical">Owner Contact*</label>
                                                                <div class="controls">
                                                                    <input type="text" class="form-control @error('owner_contact_no') error @enderror" placeholder="Owner Contact No" id="owner_contact_no" name="owner_contact_no" value="{{ old('owner_contact_no', $vendor->admin->contact_no) }}" required>
                                                                    @error('owner_contact_no')
                                                                        <label id="owner_contact_no-error" class="error" for="owner_contact_no">{{ $message }}</label>
                                                                    @enderror
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="first-name-vertical">Aadhar Card No*</label>
                                                                <div class="controls">
                                                                    <input type="text" class="form-control" placeholder="Aadhar Card Number" id="aadhar_card_number" name="aadhar_card_number" value="{{ $vendor->admin->aadhar_card_number }}" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="first-name-vertical">PAN Number*</label>
                                                                <div class="controls">
                                                                    <input type="text" class="form-control" placeholder="PAN Number" id="PAN_number" name="PAN_number" value="{{ $vendor->admin->PAN_number }}" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 col-lg-12">
                                                    <h6>Branding & Documents</h6>
                                                    <div class="row">
                                                        <div class="col-xl-2 col-md-4">
                                                            <div class="form-group">
                                                                <label for="first-name-vertical">Logo</label>
                                                                <div class="controls">
                                                                    <div id="vendorLogo" class="dropzone dropzone-area" style="min-height: 220px;"><div class="dz-message">Vendor Logo</div></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-2 col-md-4">
                                                            <div class="form-group">
                                                                <label for="first-name-vertical">GST Certificate</label>
                                                                <div class="controls">
                                                                    <div id="vendorGSTCert" class="dropzone dropzone-area" style="min-height: 220px;"><div class="dz-message">GST Certificate</div></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-2 col-md-4">
                                                            <div class="form-group">
                                                                <label for="first-name-vertical">Agreement</label>
                                                                <div class="controls">
                                                                    <div id="vendorAgreement" class="dropzone dropzone-area" style="min-height: 220px;"><div class="dz-message">Agreement</div></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-2 col-md-4">
                                                            <div class="form-group">
                                                                <label for="first-name-vertical">Aadhar Card*</label>
                                                                <div class="controls">
                                                                    <div id="vendorAadharCard" class="dropzone dropzone-area" style="min-height: 220px;"><div class="dz-message">Aadhar Card</div></div>
                                                                    <input type="hidden" name="aadhar_card" id="aadhar_card" value="" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xl-2 col-md-4">
                                                            <div class="form-group">
                                                                <label for="first-name-vertical">PAN Card*</label>
                                                                <div class="controls">
                                                                    <div id="vendorPAN" class="dropzone dropzone-area" style="min-height: 220px;"><div class="dz-message">PAN</div></div>
                                                                    <input type="hidden" name="PAN_card" id="PAN_card" value="" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane {{ ((isset($param['s']) && $param['s'] == 'manufacturers') ? 'active' : '') }}" id="carManufacturers">
                                        <form method="post" action="{{ URL::to('vendor/' . $vendor->id . '/updateManufacturers') }}" class="steps-validation wizard-circle">
                                            @csrf
                                            
                                            <div class="row">
                                                <div class="col-12 d-flex justify-content-end mb-1">
                                                    <button type="submit" class="btn btn-primary glow mb-1 mb-sm-0 mr-0 waves-effect waves-light">Save</button>
                                                    @if ( isset($vendor->account_section) && $vendor->account_section['manufacturers'] )
                                                        <button type="button" class="btn btn-secondary glow mb-1 mb-sm-0 ml-1 waves-effect waves-light next-section" data-show="assignSegments" data-hide="carManufacturers">Next</button>
                                                    @endif
                                                </div>
                                                @if ( isset($brands) && count($brands) > 0 )
                                                    @foreach ( $brands as $brand )
                                                        <div class="col-lg-3 col-md-3 col-sm-6 col-6">
                                                            <input type="checkbox" class="custom-control-input hide-checkbox brand-checkbox" name="brands[]" value="{{ $brand->id }}" id="brand{{ $brand->id }}" {{ (in_array($brand->id, $vendor->brands_arr) ? 'checked="checked"' : '') }}>
                                                            <label class="custom-control-label d-block" for="brand{{ $brand->id }}">
                                                                <div class="card checkbox-label-section border-light text-center">
                                                                    <div class="card-content cursor-pointer">
                                                                        <div class="card-body">
                                                                            <img src="{{ $brand->image_url }}" alt="element 05" width="100" class="mb-1 img-fluid">
                                                                            <h6 class="card-title font-medium-1 text-bold-400">{{ $brand->name }}</h6>
                                                                        </div>
                                                                    </div>
                                                                    <i class="fa fa-check-circle primary font-medium-3 mark-icon"></i>
                                                                </div>
                                                            </label>
                                                        </div>
                                                    @endforeach
                                                @endif
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane {{ ((isset($param['s']) && $param['s'] == 'segment') ? 'active' : '') }}" id="assignSegments">
                                        <form method="post" action="{{ URL::to('vendor/' . $vendor->id . '/syncSegments') }}" class="steps-validation wizard-circle">
                                            @csrf

                                            <div class="row">
                                                <div class="col-12 d-flex justify-content-end mb-1">
                                                    @if ( Auth::user()->role == 'admin' )
                                                        <button type="button" class="btn btn-secondary glow mb-1 mb-sm-0 mr-1 waves-effect waves-light reset-segment">Reset</button>
                                                    @endif
                                                    <button type="submit" class="btn btn-primary glow mb-1 mb-sm-0 mr-0 waves-effect waves-light">Save</button>
                                                    @if ( isset($vendor->account_section) && $vendor->account_section['segment'] )
                                                        <button type="button" class="btn btn-secondary glow mb-1 mb-sm-0 ml-1 waves-effect waves-light next-section" data-show="carServices" data-hide="assignSegments">Next</button>
                                                    @endif
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="table-responsive">
                                                        <table class="table model-table compact-table w-100">
                                                            <thead>
                                                                <tr>
                                                                    <th rowspan="2">Sr</th>
                                                                    <th rowspan="2">Image</th>
                                                                    <th rowspan="2">Name</th>
                                                                    <th rowspan="2">Model</th>
                                                                    <th colspan="4" class="text-center">Segments</th>
                                                                </tr>
                                                                <tr>
                                                                    <th>Small</th>
                                                                    <th>Medium</th>
                                                                    <th>Large</th>
                                                                    <th>Premium</th>
                                                                </tr>
                                                            </thead>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane {{ ((isset($param['s']) && $param['s'] == 'services') ? 'active' : '') }}" id="carServices">
                                        <form method="post" action="{{ URL::to('vendor/' . $vendor->id . '/updateServices') }}" class="steps-validation wizard-circle">
                                            @csrf
                                            
                                            <div class="row">
                                                <div class="col-12 d-flex justify-content-end mb-1">
                                                    @if ( Auth::user()->role == 'admin' )
                                                        <button type="button" class="btn btn-secondary glow mb-1 mb-sm-0 mr-1 waves-effect waves-light reset-service">Reset</button>
                                                    @endif
                                                    <button type="submit" class="btn btn-primary glow mb-1 mb-sm-0 mr-0 waves-effect waves-light">Save</button>
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-12">
                                                    <div class="card-header">
                                                        <h4 class="card-title">Car Services</h4>
                                                    </div>
                                                    <div class="mt-1">
                                                         <div class="row">
                                                        @if ( isset($services) && count($services) > 0 )
                                                            @foreach ( $services as $service )

                                                                <div class="col-6 col-md-12">
                                                                    <input type="checkbox" class="custom-control-input hide-checkbox service-checkbox" name="services[]" value="{{ $service->id }}" id="service{{ $service->id }}" {{ (in_array($service->id, $vendor->services_arr) ? 'checked="checked"' : '') }}>
                                                                    <label class="custom-control-label d-block" for="service{{ $service->id }}">
                                                                        <div class="card checkbox-label-section border-light">
                                                                            <div class="card-content cursor-pointer">
                                                                                <div class="card-body">
                                                                                    <img src="{{ $service->image_url }}" alt="element 05" width="50" class="mb-1">
                                                                                    <h6 class="card-title font-medium-1 text-bold-400">{{ $service->name }}</h6>
                                                                                    {{-- @if ( isset($service->subservices) && count($service->subservices) > 0 )
                                                                                        @foreach ( $service->subservices as $subservice )
                                                                                            <p class="card-text mb-0">- {{ $subservice->name }}</p>
                                                                                        @endforeach
                                                                                    @endif
                                                                                    --}}
                                                                                </div>
                                                                            </div>
                                                                            <i class="fa fa-check-circle primary font-medium-3 mark-icon"></i>
                                                                        </div>
                                                                    </label>
                                                                </div>
                                                               
                                                            @endforeach
                                                        @endif
                                                    </div>
                                                     </div>
                                                </div>
                                                <div class="col-lg-9 col-md-9 col-sm-12">
                                                    <div class="card-header">
                                                        <h4 class="card-title">Set Services Cost</h4>
                                                    </div>
                                                    <div class="mt-1">
                                                        <div class="table-responsive">
                                                            <table class="table service-table compact-table w-100">
                                                                <thead>
                                                                    <tr>
                                                                        <th rowspan="2">Sr</th>
                                                                        <th rowspan="2">Name</th>
                                                                        <th rowspan="2">Service</th>
                                                                        <th colspan="4" class="text-center">Segments</th>
                                                                        <th rowspan="2">Status</th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Small</th>
                                                                        <th>Medium</th>
                                                                        <th>Large</th>
                                                                        <th>Premium</th>
                                                                    </tr>
                                                                </thead>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    {{-- <div class="tab-pane" id="changePassword">
                                        Change Password
                                    </div> --}}
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Form wizard with step validation section end -->

        @section('carsseva-alert')

            <!-- Alerts -->
            @foreach (['danger', 'warning', 'success', 'info'] as $key)
                @if(Session::has($key))
                    <div class="cws-alert">
                        <div class="alert alert-{{ $key }} alert-dismissible alert-auto-hide fade show" role="alert">
                            <div class="alert-text">{{ Session::get($key) }}</div>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
                            </button>
                        </div>
                    </div>
                @endif
            @endforeach

        @endsection

@endsection

@section('vendor-script')
        {{-- vendor files --}}
        <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/extensions/dropzone.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>

        <script type="text/javascript">
            function initMap() {
                var geocoder = new google.maps.Geocoder();
                document.getElementById('trackGeo').addEventListener('click', function() {
                    $('#latitude').val('');
                    $('#longitude').val('');
                    $('#fullAddress').html('');

                    geocodeAddress(geocoder);
                });
            }

            function geocodeAddress(geocoder) {
                
                var address_1 = $('#address_line_1').val();
                var fullAddress = address_1;

                var address_2 = $('#address_line_2').val();
                if ( address_2 != '' ) {
                    fullAddress +=  ',' + address_2;
                }

                var cityData = $('#city_id').select2('data');
                city = cityData[0].text;
                if ( city != 'Select City' ) {
                    fullAddress +=  ',' + city;
                }

                if ( fullAddress != '' ) {
                    console.log(fullAddress);
                    geocoder.geocode({'address': fullAddress}, function(results, status) {
                        if (status === 'OK') {
                            $('#latitude').val(results[0].geometry.location.lat());
                            $('#longitude').val(results[0].geometry.location.lng());
                            $('#fullAddress').html(results[0].formatted_address);
                        } else {
                            console.log('Geocode was not successful for the following reason: ' + status);
                        }
                    });
                }
            }
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCOeXxibK2_3Pr2hkvwJ_4MvE9_ykurLzI&callback=initMap">
        </script>
@endsection

@section('page-script')
        {{-- Page js files --}}
        <script type="text/javascript">
            
            Dropzone.autoDiscover = false;

            $(document).ready(function() {

                // Initialize validation
                var validator = $("#vendorFrm").validate({
                    ignore: "",
                    rules: {
                        contact_no: {
                            minlength: 10
                        },
                        GST_number: {
                            minlength: 15,
                            maxlength: 15,
                        },
                        aadhar_card_number: {
                            minlength: 12,
                            maxlength: 12,
                        },
                        PAN_number: {
                            minlength: 10,
                            maxlength: 10,
                        },
                    }
                });

                $('.select2').select2({
                    dropdownAutoWidth: true,
                    width: '100%',
                });

                $("#state_id").select2({
                    ajax: {
                        url: "/location",
                        type: 'post',
                        data: {'type': 'state', 'country_id': '1'},
                        dataType: 'json',
                        /*data: function (params) {
                            return {
                                q: params.term // search term
                            };
                        },*/
                        processResults: function (data, params) {
                            //console.log(data.response_data, params);
                            var results = [];
                            if ( data.response_code == 200 ) {
                                $.each(data.response_data, function(index, item){
                                    results.push({
                                        id: item.id,
                                        text: item.name
                                    });
                                });
                            }
                            return {
                                results: results
                            };
                        },
                        cache: true     
                    }
                });

                // Vendor Logo
                var myDropzone = new Dropzone('#vendorLogo', {
                    url: "{{ URL::to('upload') }}",
                    paramName: "image",
                    addRemoveLinks: true,
                    acceptedFiles: 'image/*',
                    maxFiles:1,
                    init: function () {

                        var mockFile = { name: '<?php echo $vendor->logo ?>' };  
                        if ( mockFile.name != '' && mockFile.name != null ) {
                            this.options.addedfile.call(this, mockFile);
                            this.options.thumbnail.call(this, mockFile, '<?php echo $vendor->logo_url ?>');
                            mockFile.previewElement.classList.add('dz-success');
                            mockFile.previewElement.classList.add('dz-complete');
                            $('#vendorFrm').append('<input type="hidden" name="logo" id="logo" value="<?php echo $vendor->logo ?>">');
                        }

                        this.on("maxfilesexceeded", function(file) {
                            this.removeAllFiles();
                            this.addFile(file);
                        });
                    },
                    success: function (file, response) {
                        file.previewElement.classList.add("dz-success");
                        $('#vendorFrm').append('<input type="hidden" name="logo" id="logo" value="' + file.dataURL + '">');
                        
                    },
                    reset: function () {
                        $('#image').val('');
                        $('#logo').val('');
                        
                    },
                    error: function (file, response) {
                        file.previewElement.classList.add("dz-error");
                        $('#image').val('');
                        $('#logo').val('');
                        
                    }
                });

                // Vendor GST Certificate
                var myDropzone = new Dropzone('#vendorGSTCert', {
                    url: "{{ URL::to('upload') }}",
                    paramName: "image",
                    addRemoveLinks: true,
                    acceptedFiles: 'image/*,application/pdf',
                    maxFiles:1,
                    init: function () {

                        this.on("addedfile", function (file) {
                            var reader = new FileReader();
                            reader.onload = function(event) {
                                // event.target.result contains base64 encoded image
                                var base64String = event.target.result;
                                var fileName = file.name
                                file.dataURL = base64String;
                            };
                            reader.readAsDataURL(file);
                        });
                        
                        var mockFile = { name: '<?php echo $vendor->GST_certificate ?>' };  
                        if ( mockFile.name != '' && mockFile.name != null ) {
                            this.options.addedfile.call(this, mockFile);
                            this.options.thumbnail.call(this, mockFile, '<?php echo $vendor->GST_certificate_url ?>');
                            mockFile.previewElement.classList.add('dz-success');
                            mockFile.previewElement.classList.add('dz-complete');
                            $('#vendorFrm').append('<input type="hidden" name="GST_certificate" id="GST_certificate" value="<?php echo $vendor->GST_certificate ?>">');
                        }

                        this.on("maxfilesexceeded", function(file) {
                            this.removeAllFiles();
                            this.addFile(file);
                        });
                    },
                    success: function (file, response) {
                        file.previewElement.classList.add("dz-success");
                        $('#vendorFrm').append('<input type="hidden" name="GST_certificate" id="GST_certificate" value="' + file.dataURL + '">');
                        
                    },
                    reset: function () {
                        $('#image').val('');
                        $('#GST_certificate').val('');
                        
                    },
                    error: function (file, response) {
                        file.previewElement.classList.add("dz-error");
                        $('#image').val('');
                        $('#GST_certificate').val('');
                        
                    }
                });

                // Vendor Agreement
                var myDropzone = new Dropzone('#vendorAgreement', {
                    url: "{{ URL::to('upload') }}",
                    paramName: "image",
                    addRemoveLinks: true,
                    acceptedFiles: 'image/*,application/pdf',
                    maxFiles:1,
                    init: function () {

                        this.on("addedfile", function (file) {
                            var reader = new FileReader();
                            reader.onload = function(event) {
                                // event.target.result contains base64 encoded image
                                var base64String = event.target.result;
                                var fileName = file.name
                                file.dataURL = base64String;
                            };
                            reader.readAsDataURL(file);
                        });

                        var mockFile = { name: '<?php echo $vendor->agreement ?>' };  
                        if ( mockFile.name != '' && mockFile.name != null ) {
                            this.options.addedfile.call(this, mockFile);
                            this.options.thumbnail.call(this, mockFile, '<?php echo $vendor->agreement_url ?>');
                            mockFile.previewElement.classList.add('dz-success');
                            mockFile.previewElement.classList.add('dz-complete');
                            $('#vendorFrm').append('<input type="hidden" name="agreement" id="agreement" value="<?php echo $vendor->agreement ?>">');
                        }

                        this.on("maxfilesexceeded", function(file) {
                            this.removeAllFiles();
                            this.addFile(file);
                        });
                    },
                    success: function (file, response) {
                        file.previewElement.classList.add("dz-success");
                        $('#vendorFrm').append('<input type="hidden" name="agreement" id="agreement" value="' + file.dataURL + '">');
                        
                    },
                    reset: function () {
                        $('#image').val('');
                        $('#agreement').val('');
                        
                    },
                    error: function (file, response) {
                        file.previewElement.classList.add("dz-error");
                        $('#image').val('');
                        $('#agreement').val('');
                        
                    }
                });

                // Vendor Aadhar Card
                var myDropzone = new Dropzone('#vendorAadharCard', {
                    url: "{{ URL::to('upload') }}",
                    paramName: "image",
                    addRemoveLinks: true,
                    acceptedFiles: 'image/*,application/pdf',
                    maxFiles:1,
                    init: function () {

                        this.on("addedfile", function (file) {
                            var reader = new FileReader();
                            reader.onload = function(event) {
                                // event.target.result contains base64 encoded image
                                var base64String = event.target.result;
                                var fileName = file.name
                                file.dataURL = base64String;
                            };
                            reader.readAsDataURL(file);
                        });


                        var mockFile = { name: '<?php echo $vendor->admin->aadhar_card ?>' };  
                        if ( mockFile.name != '' && mockFile.name != null ) {
                            this.options.addedfile.call(this, mockFile);
                            this.options.thumbnail.call(this, mockFile, '<?php echo $vendor->admin->aadhar_card_url ?>');
                            mockFile.previewElement.classList.add('dz-success');
                            mockFile.previewElement.classList.add('dz-complete');
                            //$('#vendorFrm').append('<input type="hidden" name="aadhar_card" id="aadhar_card" value="<?php echo $vendor->admin->aadhar_card ?>" required>');
                            $('#aadhar_card').val("<?php echo $vendor->admin->aadhar_card ?>");
                        }

                        this.on("maxfilesexceeded", function(file) {
                            this.removeAllFiles();
                            this.addFile(file);
                        });
                    },
                    success: function (file, response) {
                        file.previewElement.classList.add("dz-success");
                        //$('#vendorFrm').append('<input type="hidden" name="aadhar_card" id="aadhar_card" value="' + file.dataURL + '">');
                        $('#aadhar_card').val(file.dataURL);
                    },
                    reset: function () {
                        $('#image').val('');
                        $('#aadhar_card').val('');
                        
                    },
                    error: function (file, response) {
                        file.previewElement.classList.add("dz-error");
                        $('#image').val('');
                        $('#aadhar_card').val('');
                    }
                });

                // Vendor PAN
                var myDropzone = new Dropzone('#vendorPAN', {
                    url: "{{ URL::to('upload') }}",
                    paramName: "image",
                    addRemoveLinks: true,
                    acceptedFiles: 'image/*,application/pdf',
                    maxFiles:1,
                    init: function () {

                        this.on("addedfile", function (file) {
                            var reader = new FileReader();
                            reader.onload = function(event) {
                                // event.target.result contains base64 encoded image
                                var base64String = event.target.result;
                                var fileName = file.name
                                file.dataURL = base64String;
                            };
                            reader.readAsDataURL(file);
                        });

                        var mockFile = { name: '<?php echo $vendor->admin->PAN_card ?>' };  
                        if ( mockFile.name != '' && mockFile.name != null ) {
                            this.options.addedfile.call(this, mockFile);
                            this.options.thumbnail.call(this, mockFile, '<?php echo $vendor->admin->PAN_card_url ?>');
                            mockFile.previewElement.classList.add('dz-success');
                            mockFile.previewElement.classList.add('dz-complete');
                            //$('#vendorFrm').append('<input type="hidden" name="PAN_card" id="PAN_card" value="<?php echo $vendor->admin->PAN_card ?>" required>');
                            $('#PAN_card').val("<?php echo $vendor->admin->PAN_card ?>");
                        }

                        this.on("maxfilesexceeded", function(file) {
                            this.removeAllFiles();
                            this.addFile(file);
                        });
                    },
                    success: function (file, response) {
                        file.previewElement.classList.add("dz-success");
                        //$('#vendorFrm').append('<input type="hidden" name="PAN_card" id="PAN_card" value="' + file.dataURL + '" required>');
                        $('#PAN_card').val(file.dataURL);
                        
                    },
                    reset: function () {
                        $('#image').val('');
                        $('#PAN_card').val('');
                        
                    },
                    error: function (file, response) {
                        file.previewElement.classList.add("dz-error");
                        $('#image').val('');
                        $('#PAN_card').val('');
                        
                    }
                });

                modelTableSync(JSON.parse('{{ json_encode($vendor->brands_arr) }}'), false);
                serviceTableSync(JSON.parse('{{ json_encode($vendor->services_arr) }}'), false);
            });

            $(document).on('click', '.next-section', function() {
               var show = $(this).data('show');
               var hide = $(this).data('hide');

               $('a[href="#'+show+'"]').addClass('active');
               $('#'+show).addClass('active');

               $('a[href="#'+hide+'"]').removeClass('active');
               $('#'+hide).removeClass('active');
            });

            $(document).on('click', '.reset-segment', function() {
                modelTableSync(JSON.parse('{{ json_encode($vendor->brands_arr) }}'), true);
            });

            function modelTableSync(brands, is_reset = false) {

                if ( brands.length <= 0 ) {
                    brands = [''];
                } 
                var dataPass = { 'mode': 'edit', 'vendor_id': '{{ $vendor->id }}', 'ids': brands };
                if ( is_reset ) {
                    dataPass = { 'ids': brands };
                }

                var groupColumn = 3;
                $('.model-table').DataTable({
                    //"paging": false,
                    "searching": false,
                    "bInfo" : false,
                    "ajax": {
                        "url": "/cars/getModels",
                        "type": "POST",
                        "data": dataPass,
                        "dataSrc": "data"
                    },
                    "columns": [
                        { "data": "id", "visible": false, },
                        { "data": "image_url", "width": '8%' },
                        { "data": "name", "width": '170px' },
                        { "data": "parent_id" },
                        { "width": '10%'},
                        { "width": '10%'},
                        { "width": '10%'},
                        { "width": '10%'},
                    ],
                    "destroy" : true,
                    "columnDefs": [
                        { "visible": false, "targets": groupColumn },
                        { "targets": 1,
                            render: function(data) {
                                var html = '<ul class="list-unstyled users-list m-0 d-flex align-items-center">';
                                        html += '<li data-toggle="tooltip" data-popup="tooltip-custom" data-placement="bottom" class="avatar pull-up">';
                                        html += '<img class="media-object rounded-circle" src="'+data+'" alt="Avatar" height="30" width="30">';
                                    html += '</li>';
                                html += '</ul>';
                                return html
                            }
                        },
                        { "targets": 4,
                            render: function(data, type, row) {
                                var html = '<fieldset>';
                                        html += '<div class="vs-radio-con vs-radio-light">';
                                        html += '<input type="radio" '+ (( row.default_segment != null && row.default_segment.segment == 'small' ) ? 'checked="checked"' : '') +' class="modelRadio" name="model['+row.id+']" value="small">';
                                        html += '<span class="vs-radio vs-radio-sm">';
                                            html += '<span class="vs-radio--border"></span>';
                                            html += '<span class="vs-radio--circle"></span>';
                                        html += '</span>';
                                    html += '</div>';
                                html += '</fieldset>';
                                return html
                            }
                        },
                        { "targets": 5,
                            render: function(data, type, row) {
                                var html = '<fieldset>';
                                        html += '<div class="vs-radio-con vs-radio-light">';
                                        html += '<input type="radio" '+ (( row.default_segment != null && row.default_segment.segment == 'medium' ) ? 'checked="checked"' : '') +' class="modelRadio" name="model['+row.id+']" value="medium">';
                                        html += '<span class="vs-radio vs-radio-sm">';
                                            html += '<span class="vs-radio--border"></span>';
                                            html += '<span class="vs-radio--circle"></span>';
                                        html += '</span>';
                                    html += '</div>';
                                html += '</fieldset>';
                                return html
                            }
                        },
                        { "targets": 6,
                            render: function(data, type, row) {
                                var html = '<fieldset>';
                                        html += '<div class="vs-radio-con vs-radio-light">';
                                        html += '<input type="radio" '+ (( row.default_segment != null && row.default_segment.segment == 'large' ) ? 'checked="checked"' : '') +' class="modelRadio" name="model['+row.id+']" value="large">';
                                        html += '<span class="vs-radio vs-radio-sm">';
                                            html += '<span class="vs-radio--border"></span>';
                                            html += '<span class="vs-radio--circle"></span>';
                                        html += '</span>';
                                    html += '</div>';
                                html += '</fieldset>';
                                return html
                            }
                        },
                        { "targets": 7,
                            render: function(data, type, row) {
                                var html = '<fieldset>';
                                        html += '<div class="vs-radio-con vs-radio-light">';
                                        html += '<input type="radio" '+ (( row.default_segment != null && row.default_segment.segment == 'premium' ) ? 'checked="checked"' : '') +' class="modelRadio" name="model['+row.id+']" value="premium">';
                                        html += '<span class="vs-radio vs-radio-sm">';
                                            html += '<span class="vs-radio--border"></span>';
                                            html += '<span class="vs-radio--circle"></span>';
                                        html += '</span>';
                                    html += '</div>';
                                html += '</fieldset>';
                                return html
                            }
                        }
                    ],
                    "order": [[ groupColumn, 'asc' ]],
                    "drawCallback": function ( settings ) {
                        var api = this.api();
                        var rows = api.rows( {page:'current'} ).nodes();
                        var last=null;
             
                        api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                            if ( last !== group ) {
                                $(rows).eq( i ).before(
                                    '<tr class="group"><td colspan="8">'+group+'</td></tr>'
                                );
                                last = group;
                            }
                        } );
                    }
                });
            }

            $(document).on('click', '.reset-service', function() {
                var services = [];
                $("input[name='services[]']:checked").each ( function() {
                    if ( this.checked ) {
                        services.push($(this).val());
                    }
                });

                serviceTableSync(services, true);
            });

            $('.service-checkbox').change(function() {
                var services = [];
                $("input[name='services[]']:checked").each ( function() {
                    if ( this.checked ) {
                        services.push($(this).val());
                    }
                });

                serviceTableSync(services, false);
            });
            
            function serviceTableSync(services, is_reset = false) {

                if ( services.length <= 0 ) {
                    services = [''];
                } 

                var dataPass = { 'mode': 'edit', 'vendor_id': '{{ $vendor->id }}', 'ids': services };
                if ( is_reset ) {
                    dataPass = { 'ids': services };
                }

                var groupColumn = 2;
                $('.service-table').DataTable({
                    "paging": false,
                    "searching": false,
                    "bInfo" : false,
                    "ajax": {
                        "url": "/services/getSubServices",
                        "type": "POST",
                        "data": dataPass,
                        "dataSrc": "data"
                    },
                    "columns": [
                        { "data": "id", "visible": false, },
                        { "data": "name", "width": '170px' },
                        { "data": "service_id" },
                        { "width": '15%'},
                        { "width": '15%'},
                        { "width": '15%'},
                        { "width": '15%'},
                        { "width": '10%'},
                    ],
                    "destroy" : true,
                    "columnDefs": [
                        { "visible": false, "targets": groupColumn },
                        { "targets": 1,
                            render: function(data, type, row) {
                                var html = data + ' <i class="feather icon-info mr-25 desc-popover" data-toggle="popover" data-content="'+row.description.replace(/\n/g, "<br />")+'" data-trigger="hover"></i>';
                                return html;
                            }
                        },
                        { "targets": 3,
                            render: function(data, type, row) {
                                var html = '<div class="controls">';
                                    html += '<div class="input-group">';
                                        html += '<div class="input-group-prepend">';
                                            html += '<span class="input-group-text">₹</span>';
                                        html += '</div>';
                                        html += '<input type="text" name="serice['+row.id+'][small]" class="form-control" value="'+ (( row.default_segment_price != null ) ? row.default_segment_price.small : '') +'" required>';
                                    html += '</div>';
                                html += '</div>';
                                return html
                            }
                        },
                        { "targets": 4,
                            render: function(data, type, row) {
                                var html = '<div class="controls">';
                                    html += '<div class="input-group">';
                                        html += '<div class="input-group-prepend">';
                                            html += '<span class="input-group-text">₹</span>';
                                        html += '</div>';
                                        html += '<input type="text" name="serice['+row.id+'][medium]" class="form-control" value="'+ (( row.default_segment_price != null ) ? row.default_segment_price.medium : '') +'" required>';
                                    html += '</div>';
                                html += '</div>';
                                return html
                            }
                        },
                        { "targets": 5,
                            render: function(data, type, row) {
                                var html = '<div class="controls">';
                                    html += '<div class="input-group">';
                                        html += '<div class="input-group-prepend">';
                                            html += '<span class="input-group-text">₹</span>';
                                        html += '</div>';
                                        html += '<input type="text" name="serice['+row.id+'][large]" class="form-control" value="'+ (( row.default_segment_price != null ) ? row.default_segment_price.large : '') +'" required>';
                                    html += '</div>';
                                html += '</div>';
                                return html
                            }
                        },
                        { "targets": 6,
                            render: function(data, type, row) {
                                var html = '<div class="controls">';
                                    html += '<div class="input-group">';
                                        html += '<div class="input-group-prepend">';
                                            html += '<span class="input-group-text">₹</span>';
                                        html += '</div>';
                                        html += '<input type="text" name="serice['+row.id+'][premium]" class="form-control" value="'+ (( row.default_segment_price != null ) ? row.default_segment_price.premium : '') +'" required>';
                                    html += '</div>';
                                html += '</div>';
                                return html
                            }
                        },
                        { "targets": 7,
                            render: function(data, type, row) {
                                var html = '<div class="custom-control custom-switch custom-switch-success custom-control-inline">';
                                    html += '<input type="checkbox" class="custom-control-input" value="1" name="serice['+row.id+'][status]" '+((row.default_segment_price.status == '1') ? 'checked="checked"' : '')+' id="status'+row.id+'">';
                                    html += '<label class="custom-control-label" for="status'+row.id+'"></label>';
                                html += '</div>';
                                return html
                            }
                        },
                    ],
                    "order": [[ groupColumn, 'asc' ]],
                    "drawCallback": function ( settings ) {

                        $('.desc-popove').popover('dispose');
                        $(".desc-popover").popover({
                            title: function() {
                                var title = 'Description';
                                return title;
                            },
                            html: true,
                            placement: 'bottom',
                        });

                        var api = this.api();
                        var rows = api.rows( {page:'current'} ).nodes();
                        var last=null;

                        api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                            if ( last !== group ) {
                                $(rows).eq( i ).before(
                                    '<tr class="group"><td colspan="7">'+group+'</td></tr>'
                                );
                                last = group;
                            }
                        } );
                    }
                });
            }

            $(document).on('select2:select', '#state_id', function() {
                $("#city_id").val("");
                $("#city_id").select2({
                    ajax: {
                        url: "/location",
                        type: 'post',
                        data: {'type': 'city', 'country_id': '1', 'state_id': this.value},
                        dataType: 'json',
                        processResults: function (data, params) {
                            //console.log(data.response_data, params);
                            var results = [];
                            if ( data.response_code == 200 ) {
                                $.each(data.response_data, function(index, item){
                                    results.push({
                                        id: item.id,
                                        text: item.name
                                    });
                                });
                            }
                            return {
                                results: results
                            };
                        },
                        cache: true     
                    }
                });
            });

        </script>
@endsection

