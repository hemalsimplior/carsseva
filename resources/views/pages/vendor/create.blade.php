@extends('layouts/contentLayoutMaster')

@section('title', 'Add Vendor')

@section('vendor-style')
        {{-- vendor css files --}}
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection

@section('page-style')
        <!-- Page css files -->
        <link rel="stylesheet" href="{{ asset(mix('css/plugins/forms/wizard.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/plugins/file-uploaders/dropzone.css')) }}">
@endsection

@section('content')
    
        <!-- Form wizard with step validation section start -->
        <section id="validation">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <form method="post" class="steps-validation" id="vendorFrm" action="{{ route('vendor.store') }}">
                                    @csrf

                                    <div class="row">
                                        <div class="col-md-12 col-lg-6">
                                            <h6>Company Infomation</h6>
                                            <div class="row">
                                                <!-- <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="firstName3">Code *</label>
                                                        <input type="text" class="form-control" placeholder="Vendor Code" id="code" name="code" required>
                                                    </div>
                                                </div> -->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="lastName3">Name *</label>
                                                        <input type="text" class="form-control @error('name') error @enderror" placeholder="Name" id="name" name="name" value="{{ old('name') }}" required>
                                                        @error('name')
                                                            <label id="name-error" class="error" for="name">{{ $message }}</label>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="firstName3">Contact No *</label>
                                                        <input type="text" class="form-control @error('contact_no') error @enderror" placeholder="Contact No" id="contact_no" name="contact_no" value="{{ old('contact_no') }}" required>
                                                        @error('contact_no')
                                                            <label id="contact_no-error" class="error" for="contact_no">{{ $message }}</label>
                                                        @enderror
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="first-name-vertical">GST Number</label>
                                                        <div class="controls">
                                                            <input type="text" class="form-control" placeholder="GST Number" id="GST_number" name="GST_number" value="{{ old('GST_number') }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="first-name-vertical">Agreement Number</label>
                                                        <div class="controls">
                                                            <input type="text" class="form-control" placeholder="Agreement Number" id="agreement_number" name="agreement_number" value="{{ old('agreement_number') }}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <h6>Address Infomation</h6>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="firstName3">Address Line 1 *</label>
                                                    <div class="controls">    
                                                        <input type="text" class="form-control" placeholder="Address Line 1" name="address_line_1" id="address_line_1" value="{{ old('address_line_1') }}" required>
                                                        {{-- <div class="input-group">
                                                            <input type="text" class="form-control" placeholder="Address Line 1" name="address_line_1" id="address_line_1" value="{{ old('address_line_1') }}" required>
                                                            <div class="input-group-append" id="button-addon2">
                                                              <button id="trackGeo" class="btn btn-primary waves-effect waves-light" type="button"><i class="feather icon-map-pin"></i></button>
                                                            </div>
                                                        </div> --}}
                                                    </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="firstName3">Address Line 2</label>
                                                    <div class="controls">    
                                                        <input type="text" class="form-control" placeholder="Address Line 2" name="address_line_2" id="address_line_2" value="{{ old('address_line_2') }}">
                                                    </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4">
                                                    <div class="form-group">
                                                        <label for="firstName3">State *</label>
                                                        <div class="controls">
                                                            <!-- <input type="text" class="form-control" placeholder="State" name="state" required> -->
                                                            <select name="state_id" id="state_id" class="form-control select2" required>
                                                                <option value='' selected="selected">Select State</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-4">
                                                    <div class="form-group">
                                                        <label for="firstName3">City *</label>
                                                        <!-- <input type="text" class="form-control" placeholder="City" name="city" required> -->
                                                        <div class="controls">
                                                            <!-- <input type="text" class="form-control" placeholder="State" name="state" required> -->
                                                            <select name="city_id" id="city_id" class="form-control select2" required>
                                                                <option value='' selected="selected">Select City</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-md-2">
                                                    <div class="form-group">
                                                        <label for="firstName3">Zipcode *</label>
                                                    <div class="controls">
                                                        <input type="text" class="form-control" placeholder="Zipcode" name="zipcode" value="{{ old('zipcode') }}" required>
                                                    </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2 col-md-2">
                                                    <div class="form-group">
                                                        <label for="firstName3">&nbsp;</label>
                                                        <button id="trackGeo" class="btn btn-primary btn-md waves-effect waves-light" type="button"><i class="feather icon-map-pin"></i></button>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="firstName3">Latitude</label>
                                                    <div class="controls"> 
                                                        <input type="text" readonly class="form-control" placeholder="Latitude" name="latitude" id="latitude" value="{{ old('latitude') }}">
                                                    </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label for="firstName3">Longitude</label>
                                                    <div class="controls">    
                                                        <input type="text" readonly class="form-control" placeholder="Longitude" name="longitude" id="longitude" value="{{ old('longitude') }}">
                                                    </div> 
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="firstName3">&nbsp;</label>
                                                        <p id="fullAddress"></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-lg-6">
                                            <h6>Owner Infomation</h6>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="first-name-vertical">Owner Name*</label>
                                                        <div class="controls">
                                                            <input type="text" class="form-control" placeholder="Owner Name" id="owner_name" name="owner_name" value="{{ old('owner_name') }}" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="first-name-vertical">Aadhar Card Number</label>
                                                        <div class="controls">
                                                            <input type="text" class="form-control" placeholder="Aadhar Card Number" id="aadhar_card_number" name="aadhar_card_number" value="{{ old('aadhar_card_number') }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="first-name-vertical">Owner Contact No*</label>
                                                        <div class="controls">
                                                            <input type="text" class="form-control @error('owner_contact_no') error @enderror" placeholder="Owner Contact No" id="owner_contact_no" name="owner_contact_no" value="{{ old('owner_contact_no') }}" required>
                                                            @error('owner_contact_no')
                                                                <label id="owner_contact_no-error" class="error" for="owner_contact_no">{{ $message }}</label>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="first-name-vertical">PAN Number</label>
                                                        <div class="controls">
                                                            <input type="text" class="form-control" placeholder="PAN Number" id="PAN_number" name="PAN_number" value="{{ old('PAN_number') }}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Create Vendor</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Form wizard with step validation section end -->

        @section('carsseva-alert')

            <!-- Alerts -->
            @foreach (['danger', 'warning', 'success', 'info'] as $key)
                @if(Session::has($key))
                    <div class="cws-alert">
                        <div class="alert alert-{{ $key }} alert-dismissible alert-auto-hide fade show" role="alert">
                            <div class="alert-text">{{ Session::get($key) }}</div>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
                            </button>
                        </div>
                    </div>
                @endif
            @endforeach

        @endsection

@endsection

@section('vendor-script')
        {{-- vendor files --}}
        <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
@endsection

@section('page-script')
        {{-- Page js files --}}
        <script type="text/javascript">
            function initMap() {
                var geocoder = new google.maps.Geocoder();
                document.getElementById('trackGeo').addEventListener('click', function() {
                    $('#latitude').val('');
                    $('#longitude').val('');
                    $('#fullAddress').html('');

                    geocodeAddress(geocoder);
                });
            }

            function geocodeAddress(geocoder) {
                
                var address_1 = $('#address_line_1').val();
                var fullAddress = address_1;

                var address_2 = $('#address_line_2').val();
                if ( address_2 != '' ) {
                    fullAddress +=  ',' + address_2;
                }

                var cityData = $('#city_id').select2('data');
                city = cityData[0].text;
                if ( city != 'Select City' ) {
                    fullAddress +=  ',' + city;
                }

                if ( fullAddress != '' ) {
                    console.log(fullAddress);
                    geocoder.geocode({'address': fullAddress}, function(results, status) {
                        if (status === 'OK') {
                            $('#latitude').val(results[0].geometry.location.lat());
                            $('#longitude').val(results[0].geometry.location.lng());
                            $('#fullAddress').html(results[0].formatted_address);
                        } else {
                            console.log('Geocode was not successful for the following reason: ' + status);
                        }
                    });
                }
            }
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCOeXxibK2_3Pr2hkvwJ_4MvE9_ykurLzI&callback=initMap">
        </script>
        
        <script type="text/javascript">
        
            $(document).ready(function() {

                $('.select2').select2({
                    dropdownAutoWidth: true,
                    width: '100%',
                });

                $("#state_id").select2({
                    ajax: {
                        url: "/location",
                        type: 'post',
                        data: {'type': 'state', 'country_id': '1'},
                        dataType: 'json',
                        /*data: function (params) {
                            return {
                                q: params.term // search term
                            };
                        },*/
                        processResults: function (data, params) {
                            //console.log(data.response_data, params);
                            var results = [];
                            if ( data.response_code == 200 ) {
                                $.each(data.response_data, function(index, item){
                                    results.push({
                                        id: item.id,
                                        text: item.name
                                    });
                                });
                            }
                            return {
                                results: results
                            };
                        },
                        cache: true     
                    }
                }); 
            });

            // Show form
            var form = $(".steps-validation").show();

            // Initialize validation
            $(".steps-validation").validate({
                rules: {
                    contact_no: {
                        minlength: 10
                    },
                    GST_number: {
                        minlength: 15,
                        maxlength: 15,
                    },
                    aadhar_card_number: {
                        minlength: 12,
                        maxlength: 12,
                    },
                    PAN_number: {
                        minlength: 10,
                        maxlength: 10,
                    },
                }
            });

            $(document).on('select2:select', '#state_id', function() {
                $("#city_id").val("");
                $("#city_id").select2({
                    ajax: {
                        url: "/location",
                        type: 'post',
                        data: {'type': 'city', 'country_id': '1', 'state_id': this.value},
                        dataType: 'json',
                        processResults: function (data, params) {
                            //console.log(data.response_data, params);
                            var results = [];
                            if ( data.response_code == 200 ) {
                                $.each(data.response_data, function(index, item){
                                    results.push({
                                        id: item.id,
                                        text: item.name
                                    });
                                });
                            }
                            return {
                                results: results
                            };
                        },
                        cache: true     
                    }
                });
            });

        </script>
@endsection

