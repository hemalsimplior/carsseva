@extends('layouts/contentLayoutMaster')

@section('title', 'Vendors')

@section('vendor-style')
        {{-- vendor css files --}}
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
@endsection

@section('page-style')
        <!-- Page css files -->
@endsection

@section('content')
    
        @section('breadcrumbs-actions')
            <a href="{{ URL::to('vendor/create') }}" class="btn btn-outline-primary mr-1 mb-1 waves-effect waves-light text-nowrap">Add Vendor</a>
        @endsection
        
        <!-- Column selectors with Export Options and print table -->
        <section id="column-selectors">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body card-dashboard">
                                <div class="table-responsive">
                                    <table class="table vendor-table compact-table table-data-nowrap">
                                        <thead>
                                            <tr>
                                                <th>Sr</th>
                                                <th>Image</th>
                                                <th>Sequence Id</th>
                                                <th>Name</th>
                                                <th>Code</th>
                                                <th>Contact No</th>
                                                <th>Email</th>
                                                <th>Emergency No</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          @php
                                            $sr = 1;
                                          @endphp
                                          @foreach($vendors as $in => $vendor)
                                            <tr class="group">
                                                <td>{{ $sr }}</td>
                                                <td>
                                                  <ul class="list-unstyled users-list m-0 d-flex align-items-center">
                                                    <li data-toggle="tooltip" data-popup="tooltip-custom" data-placement="bottom" data-original-title="{{ $vendor->name }}" class="avatar pull-up">
                                                      <img class="media-object rounded-circle" src="{{ $vendor->logo_url }}" alt="Avatar" height="30" width="30">
                                                    </li>
                                                  </ul>
                                                </td>
                                                <td>#{{ $vendor->sequence_id }}</td>
                                                <td><a href="{{ URL::to('vendor/' . $vendor->id) }}">{{ $vendor->name }}</a></td>
                                                <td>{{ $vendor->code }}</td>
                                                <td>{{ $vendor->contact_no }}</td>
                                                <td>{{ $vendor->email }}</td>
                                                <td>{{ $vendor->emergency_contact_no }}</td>
                                                <td>
                                                    <div class="custom-control custom-switch custom-switch-success custom-control-inline">
                                                        <input type="checkbox" class="custom-control-input item-status-change" data-item="{{ $vendor }}" data-sr="status{{ $sr }}" id="status{{ $sr }}" {{ (($vendor->status == '1') ? 'checked="checked"' : '') }}>
                                                        <label class="custom-control-label" for="status{{ $sr }}"></label>
                                                    </div>
                                                </td>
                                                <td class="action-btn">
                                                  <a href="{{ URL::to('vendor/' . $vendor->id . '/edit') }}" class="btn btn-sm btn-icon btn-icon rounded-circle btn-flat-light waves-effect waves-light"><i class="feather icon-edit"></i></a>
                                                  {{-- @if ( $vendor->orders_count <= 0 )
                                                    <button type="button" class="btn btn-sm btn-icon btn-icon rounded-circle btn-flat-light mr-1 mb-1 waves-effect waves-light delete-item" data-item="{{ $vendor }}"><i class="feather icon-trash-2"></i></button>
                                                  @endif --}}
                                                </td>
                                            </tr>
                                            @php
                                                $sr += 1;
                                            @endphp
                                          @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Sr</th>
                                                <th>Image</th>
                                                <th>Sequence Id</th>
                                                <th>Name</th>
                                                <th>Code</th>
                                                <th>Contact No</th>
                                                <th>Email</th>
                                                <th>Emergency No</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Column selectors with Export Options and print table -->

        @section('carsseva-alert')

            <!-- Alerts -->
            @foreach (['danger', 'warning', 'success', 'info'] as $key)
                @if(Session::has($key))
                    <div class="cws-alert">
                        <div class="alert alert-{{ $key }} alert-dismissible alert-auto-hide fade show" role="alert">
                            <div class="alert-text">{{ Session::get($key) }}</div>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
                            </button>
                        </div>
                    </div>
                @endif
            @endforeach

            <!-- Confirmation alert msg for status change -->
            <div class="cws-alert status-change-alert" style="display: none;">
                <div class="background-overlay status-change-reject"></div>
                <div class="alert alert-dark-danger alert-dismissible fade show" role="alert">
                    <div class="alert-text">Are you sure you want to <strong class="status-change-name"></strong>? 
                        <div class="alert-btn">
                            <button class="btn btn-primary status-change-confirm">Yes</button>
                            <button class="btn btn-white status-change-reject">Cancel</button>
                        </div>
                    </div>
                    <button type="button" class="close status-change-reject">
                        <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
                    </button>
                </div>
            </div>

            <!-- Confirmation alert msg -->
            <div class="cws-alert vendor-alert" style="display: none;">
                <div class="background-overlay vendor-reject"></div>
                <div class="alert alert-dark-danger alert-dismissible fade show" role="alert">
                    <div class="alert-text">Are you sure you want to delete <strong class="vendor-name"></strong>? 
                        <div class="alert-btn">
                            <a href="#" class="btn btn-primary vendor-confirm">Yes</a>
                            <button class="btn btn-white vendor-reject">Cancel</button>
                        </div>
                    </div>
                    <button type="button" class="close vendor-reject">
                        <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
                    </button>
                </div>
            </div>

        @endsection

@endsection

@section('vendor-script')
        {{-- vendor files --}}
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
@endsection

@section('page-script')
        {{-- Page js files --}}
        <script type="text/javascript">
            $(document).ready(function() {

                // Datatable
                $('.vendor-table').DataTable({
                    "ordering": false,
                    "columnDefs": [
                        { "visible": false, "targets": 0},
                        { "width": '8%', "targets": 1},
                        { "width": '10%', "targets": 2},
                        { "width": '22%', "targets": 3},
                        { "width": '8%', "targets": 4},
                        { "width": '10%', "targets": 5},
                        { "width": '12%', "targets": 6},
                        { "width": '11%', "targets": 7},
                        { "width": '8%', "targets": 8},
                        { "width": '15%', "targets": 9},
                    ],
                    "order": [
                        [0, 'asc']
                    ]
                });

                $('.vendor-reject').click( function() {
                    $('.vendor-alert').hide();
                });

                // Status change
                $('.status-change-confirm').click( function() {
                    
                    // trigger status-change event
                    var ids = $(this).attr('data-ids');
                    var action = $(this).attr('data-action');
                    var type = $(this).attr('data-type');

                    $.post('vendor/bulk-action', {'action': action, 'ids': ids, 'type': type}, function(response){
                        if ( response.status == 200 ) {
                            window.location = "{{ url('/vendor') }}"
                        }
                    });
                });

                $('.status-change-reject').click( function() {

                    var action = $(this).attr('data-action');
                    var sr = $(this).attr('data-sr');
                    
                    if ( action == 'active' ) {
                        $("#"+sr).prop("checked", false);
                    } else {
                        $("#"+sr).prop("checked", true);
                    }

                    $('.status-change-alert').hide();
                });
            });

            // Delete Form
            $(document).on('click', '.delete-item', function() {
                var item = $(this).data('item');

                if ( item.brand_id != undefined ) {
                    item.type = 'model';
                    $('.vendor-confirm').attr('href', "{{ URL::to('vendor') }}/"+item.id+"/delete");
                    $('.vendor-name').html(item.name + ' model');
                } else {
                    item.type = 'brand';
                    $('.vendor-confirm').attr('href', "{{ URL::to('vendor') }}/"+item.id+"/delete");
                    $('.vendor-name').html(item.name + ' brand');
                }
                $('.vendor-alert').show();
            });

            $(document).on('change', '.item-status-change', function() {
                var item = $(this).data('item');
                var status = ((this.checked) ? 'active' : 'inactive');
                
                if ( item.brand_id != undefined ) {
                    $('.status-change-confirm').attr('data-ids', item.id);
                    $('.status-change-confirm').attr('data-type', 'model');
                } else {
                    $('.status-change-confirm').attr('data-ids', item.id);
                    $('.status-change-confirm').attr('data-type', 'brand');
                }

                $('.status-change-name').html(status + ' ' + item.name);
                $('.status-change-alert').show();
                $('.status-change-confirm').attr('data-action', status);
                $('.status-change-reject').attr('data-action', status);
                $('.status-change-reject').attr('data-sr', $(this).data('sr'));
            });

        </script>
@endsection

