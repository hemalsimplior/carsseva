@extends('layouts/contentLayoutMaster')

@section('title', 'Users')

@section('vendor-style')
        {{-- vendor css files --}}
        <link rel="stylesheet" href="{{ asset(mix('css/plugins/forms/validation/form-validation.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection

@section('page-style')
        <!-- Page css files -->
@endsection

@section('content')
        
        <!-- Column selectors with Export Options and print table -->
        <section id="column-selectors">
            <div class="row">
                <div class="col-12 col-lg-12 col-xl-6">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" id="card-title">Edit User</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <form class="form-horizontal" id="userModleFrm" method="post" action="{{ URL::to('users/' . $user->id) }}" novalidate enctype="multipart/form-data">
                                    @csrf
                                    <input name="_method" id="_method" type="hidden" value="PUT">
                                    <input name="role" id="role" type="hidden" value="{{ $user->role }}">
                                    <input name="call_from" type="hidden" value="profile">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="first-name-vertical">Name *</label>
                                                <div class="controls">
                                                    <input type="text" id="name" name="name" class="form-control @error('name') is-invalid @enderror" placeholder="Name" value="{{ old('name', $user->name) }}" required>

                                                    @error('name')
                                                        <span class="invalid-feedback help-block" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="first-name-vertical">Contact No *</label>
                                                <div class="controls">
                                                    <input type="text" id="contact_no" name="contact_no" class="form-control @error('contact_no') is-invalid @enderror" placeholder="Contact No" value="{{ old('contact_no', $user->contact_no) }}" required>

                                                    @error('contact_no')
                                                        <span class="invalid-feedback help-block" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="first-name-vertical">Email</label>
                                                <div class="controls">
                                                    <input type="text" id="email" name="email" class="form-control" placeholder="Email" value="{{ old('email', $user->email) }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Save</button>
                                    <button type="reset" class="btn btn-outline-warning mr-1 mb-1 waves-effect waves-light add-action">Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-12 col-xl-6">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title" id="card-title">Change Password</h4>
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <form class="form-horizontal" id="changePassword" method="post" action="{{ route('users.change-password') }}" novalidate enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" id="user_id" name="user_id" value="{{ $user->id }}">
                                    <input name="call_from" type="hidden" value="profile">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="first-name-vertical">Password *</label>
                                                <div class="controls">
                                                    <input type="password" id="password1" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password" required>
            
                                                    @error('password')
                                                        <span class="invalid-feedback help-block" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="first-name-vertical">Retype Password *</label>
                                                <div class="controls">
                                                    <input type="password" id="password_confirmation1" name="password_confirmation" class="form-control @error('password_confirmation') is-invalid @enderror" placeholder="Retype Password" required>
            
                                                    @error('password_confirmation')
                                                        <span class="invalid-feedback help-block" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <button class="btn btn-primary">Change</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Column selectors with Export Options and print table -->

        @section('carsseva-alert')

            <!-- Alerts -->
            @foreach (['danger', 'warning', 'success', 'info'] as $key)
                @if(Session::has($key))
                    <div class="cws-alert">
                        <div class="alert alert-{{ $key }} alert-dismissible alert-auto-hide fade show" role="alert">
                            <div class="alert-text">{{ Session::get($key) }}</div>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
                            </button>
                        </div>
                    </div>
                @endif
            @endforeach

        @endsection

@endsection

@section('vendor-script')
        {{-- vendor files --}}
        <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
@endsection

@section('page-script')
        {{-- Page js files --}}
        <script type="text/javascript">
            $(document).ready(function() {

                // Form
                var validator = $( "#userModleFrm" ).validate({
                    //ignore: ""
                });

                // Form
                var validatorChangePass = $( "#changePassword" ).validate({
                    ignore: "",
                    rules: {
                        password: "required",
                        password_confirmation: {
                            equalTo: "#password1"
                        }
                    }
                });
                
            });

        </script>
@endsection
