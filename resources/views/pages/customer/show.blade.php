@extends('layouts/contentLayoutMaster')

@section('title', $customer->name)

@section('vendor-style')
        {{-- vendor css files --}}
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/plugins/forms/validation/form-validation.css')) }}">
@endsection

@section('page-style')
        <!-- Page css files -->
        <link rel="stylesheet" href="{{ asset(mix('css/pages/users.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/pages/app-ecommerce-shop.css')) }}">
@endsection

@section('content')
        
        <div class="content-header row">
            <div class="content-header-left col-md-7 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <div class="breadcrumb-wrapper col-12" style="padding: 0;">
                            @if(@isset($breadcrumbs_custom))
                                <ol class="breadcrumb" style="border: none;padding-left: 0px !important;">
                                    @foreach ($breadcrumbs_custom as $breadcrumb)
                                        <li class="breadcrumb-item">
                                            @if(isset($breadcrumb['link']))
                                                <a href="/{{ $breadcrumb['link'] }}"> @endif{{$breadcrumb['name']}}@if(isset($breadcrumb['link'])) </a>
                                            @endif
                                        </li>
                                    @endforeach
                                </ol> 
                            @endisset
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xl-3 col-lg-4 col-12">
                <div class="card">
                    <div class="card-header mx-auto pb-0">
                        <div class="row m-0">
                            <div class="col-sm-12 text-center">
                                <h4>{{ $customer->name }}</h4>
                            </div>
                            <div class="col-sm-12 text-center">
                                <p class="">{{ ucfirst($customer->role) }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="card-body text-center mx-auto">
                            <div class="avatar avatar-xl">
                                <img class="img-fluid" src="{{ $customer->profile_pic_url }}" alt="img placeholder">
                            </div>
                            <div class="d-flex justify-content-between mt-2">
                                <div class="uploads">
                                    <p class="font-weight-bold font-medium-2 mb-0">{{ $customer->active_orders_count }}</p>
                                    <span class="">Active Orders</span>
                                </div>
                                <div class="followers">
                                    <p class="font-weight-bold font-medium-2 mb-0">{{ $customer->cars_count }}</p>
                                    <span class="">No of Cars</span>
                                </div>
                                <div class="following">
                                    <p class="font-weight-bold font-medium-2 mb-0">{{ $customer->completed_orders_count }}</p>
                                    <span class="">Completed Orders</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
           <div class="row">
                <!-- Manage Cars -->
            <div class="col-md-6 col-lg-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <h4>Cars</h4>
                        <i class="feather icon-more-horizontal cursor-pointer"></i>
                    </div>
                    <div class="card-body">

                        @if ( isset($customer->cars) && count($customer->cars) > 0 )
                            @foreach($customer->cars as $inc => $car)
                            <div class="d-flex justify-content-start align-items-center mb-1 car-item-{{ $car->id }}">
                                <div class="avatar mr-50">
                                    <img src="{{ $car->car_model->image_url }}" alt="avtar img holder" height="35" width="35">
                                </div>
                                <div class="user-page-info">
                                    <h6 class="mb-0">{{ $car->car_model->name }}</h6>
                                    <span class="font-small-2">{{ ucfirst($car->fuel_type) }}</span>
                                </div>
                                
                                <span class="inline-tr-edit-icon ml-auto cursor-pointer mr-25 edit-car car-{{ $car['id'] }}" data-edit="true" data-car="{{ $car }}"><i class="feather icon-edit"></i></span>
                                <span class="inline-tr-edit-icon cursor-pointer delete-car" data-item="{{ $car }}"><i class="feather icon-trash-2"></i></span>
                            </div>
                            @endforeach
                        @endif

                        <div class="justify-content-start align-items-center mb-1 add-car-section d-none">
                            <form id="carFrm" method="post" action="{{ URL::to('customerCars') }}" >
                                @csrf
                                <input type="hidden" name="user_id" value="{{ $customer->id }}">
                                <div class="row">
                                    <div class="col-xl-6 col-12">
                                        <label for="first-name-vertical">Select Model</label>
                                        <div class="controls">
                                            <select name="car_model_id" id="car_model_id" class="form-control select2 car_model_id"></select>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-12">
                                        <label for="first-name-vertical">Fuel Type</label>
                                        <div class="controls">
                                            <select name="fuel_type" id="fuel_type" class="form-control select2">
                                                <option value="petrol">Petrol</option>
                                                <option value="diesel">Diesel</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-xl-6 col-12">
                                        <label for="first-name-vertical">Gear Type</label>
                                        <div class="controls">
                                            <select name="gear_type" id="gear_type" class="form-control select2">
                                                <option value="manual">Manual</option>
                                                <option value="auto">Auto</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-12">
                                        <label for="first-name-vertical">Car Number</label>
                                        <div class="controls">
                                            <input type="text" placeholder="Car Number" name="car_number" id="car_number" class="form-control" maxlength="10" required>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-xl-6 col-12">
                                        <label for="first-name-vertical">Total Travelled</label>
                                        <div class="controls">
                                            <input type="text" placeholder="Total Travelled" name="total_travelled" id="total_travelled" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-12">
                                        <label for="first-name-vertical">Avg. Monthly Uage</label>
                                        <div class="controls">
                                            <input type="text" placeholder="Avg. Monthly Uage" id="avg_monthly_usage" name="avg_monthly_usage" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-xl-6 col-6">
                                        <button class="btn btn-primary btn-block mt-1 waves-effect waves-light save-car">Save</button>
                                    </div>
                                    <div class="col-xl-6 col-6">
                                        <button type="button" class="btn btn-light btn-block mt-1 waves-effect waves-light add-car">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        
                        <button type="button" class="btn btn-primary w-100 mt-1 waves-effect waves-light add-car"><i class="feather icon-plus mr-25"></i>Add Car</button>
                    </div>
                </div>
            </div>

                <!-- Manage Addresses -->
            <div class="col-md-6 col-lg-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <h4>Addresses</h4>
                        <i class="feather icon-more-horizontal cursor-pointer"></i>
                    </div>
                    <div class="card-body">

                        @if ( isset($customer->addresses) && count($customer->addresses) > 0 )
                            @foreach($customer->addresses as $ina => $address)
                            <div class="d-flex justify-content-start align-items-center mb-1 address-item-{{ $address->id }}">
                                <div class="user-page-info">
                                    <h6 class="mb-0">
                                        {{ $address->address_line_1 }}
                                        @if ( isset($address->is_primary) && $address->is_primary )
                                            <span class="badge badge-secondary ml-1" style="font-size: 10px;">Primary</span>
                                        @else
                                            <a href="{{ URL::to('customerAddresses/'.$address->id.'/primary') }}" class="ml-1" style="font-size: 11px;">Mark as Primary</a>
                                        @endif
                                    </h6>
                                    <span class="font-small-2">{{ $address->address_line_2 }}</span><br>
                                    <span class="font-small-2">{{ $address->state->name }},</span>
                                    <span class="font-small-2">{{ $address->city->name }} - {{ $address->zipcode }}</span>                                    
                                </div>
                                
                                <span class="inline-tr-edit-icon ml-auto cursor-pointer mr-25 edit-address address-{{ $address['id'] }}" data-edit="true" data-address="{{ $address }}"><i class="feather icon-edit"></i></span>
                                <span class="inline-tr-edit-icon cursor-pointer delete-address" data-item="{{ $address }}"><i class="feather icon-trash-2"></i></span>
                            </div>
                            @endforeach
                        @endif

                        <div class="justify-content-start align-items-center mb-1 add-address-section d-none">
                            <form id="addressFrm" method="post" action="{{ URL::to('customerAddresses') }}" >
                                @csrf
                                <input type="hidden" name="user_id" value="{{ $customer->id }}">
                                <div class="row">
                                    <div class="col-xl-12 col-lg-12 col-12">
                                        <label for="first-name-vertical">Address Line 1</label>
                                        <div class="controls">
                                            <input type="text" name="address_line_1" placeholder="Address Line 1" required class="form-control">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-xl-12 col-lg-12 col-12">
                                        <label for="first-name-vertical">Address Line 2</label>
                                        <div class="controls">
                                            <input type="text" name="address_line_2" placeholder="Address Line 2" class="form-control">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-xl-12 col-lg-12 col-12">
                                        <label for="first-name-vertical">State</label>
                                        <div class="controls">
                                            {{-- <input type="text" name="state" placeholder="State" class="form-control"> --}}
                                            <select name="state_id" id="state_id" class="form-control select2" required></select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-xl-12 col-lg-12 col-12">
                                        <label for="first-name-vertical">City</label>
                                        <div class="controls">
                                            {{-- <input type="text" placeholder="City" name="city" class="form-control" required> --}}
                                            <select name="city_id" id="city_id" class="form-control select2" required></select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-xl-12 col-lg-12 col-12">
                                        <label for="first-name-vertical">Zipcode</label>
                                        <div class="controls">
                                            <input type="text" placeholder="Zipcode" name="zipcode" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-xl-6 col-6">
                                        <button class="btn btn-primary btn-block mt-1 waves-effect waves-light save-address">Save</button>
                                    </div>
                                    <div class="col-xl-6 col-6">
                                        <button type="button" class="btn btn-light btn-block mt-1 waves-effect waves-light add-address">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        
                        <button type="button" class="btn btn-primary w-100 mt-1 waves-effect waves-light add-address"><i class="feather icon-plus mr-25"></i>Add Address</button>
                    </div>
                </div>
            </div>
        </div>
                
            </div>
            <div class="col-xl-6 col-lg-8 col-12">

                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center border-bottom pb-1">
                        <div>
                            <p class="mb-75">Total <strong>{{ $customer->orders_count }} Orders </strong></p>
                        </div>
                    </div>
                    <div class="card-content">
                        <div class="list-group analytics-list">
                            
                            @if ( isset($customer->orders) && count($customer->orders) > 0 )
                                @foreach($customer->orders as $ino => $order)

                                <div class="list-group-item d-lg-flex justify-content-between align-items-start py-1" style="border: 0;">
                                    <div class="float-left">
                                        <p class="text-bold-600 font-medium-2 mb-0 mt-25">#{{ $order->sequence_id }}</p>
                                        <small><strong>Car:</strong> {{ $order->customer_car->car_model->brand->name }} {{ $order->customer_car->car_model->name }}</small><br>
                                        <small><strong>Vendor:</strong> {{ $order->vendor->name }}</small><br>
                                        <small><strong>Order Date:</strong> {{ $order->created_at->format('d/m/Y') }}</small><br>
                                        <small><strong>Service Date:</strong> {{ $order->service_date->format('d/m/Y') }}</small><br>
                                        @if ( $order->status == 'service_completed' )
                                            <small><strong>Rated:</strong> 4</small><br>
                                        @endif
                                    </div>
                                    <div class="float-right text-center mt-2" style="width: 160px;">
                                        @if ( $order->status == 'delivered' )
                                            <button type="button" class="btn btn-sm btn-icon rounded-circle btn-outline-primary mr-50 waves-effect waves-light">
                                                <i class="feather icon-file-text"></i>
                                            </button>
                                        @endif
                                        <a href="{{ URL::to('orders/' . $order->id) }}" class="btn btn-sm btn-icon rounded-circle btn-outline-primary mr-50 waves-effect waves-light">
                                            <i class="feather icon-eye"></i>
                                        </a>
                                        <p class="mb-0 mt-25">{{ $status[$order->status] }}</p>
                                    </div>
                                </div>

                                @endforeach
                            @endif

                        </div>
                    </div>
                </div>

            </div>
            <div class="col-lg-12 col-xl-3 col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Activity Log</h4>
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            <ul class="activity-timeline timeline-left list-unstyled">
                                <li>
                                    <div class="timeline-icon bg-primary">
                                        <i class="feather icon-plus font-medium-2"></i>
                                    </div>
                                    <div class="timeline-info">
                                        <p class="font-weight-bold">Task Added</p>
                                        <span>Bonbon macaroon jelly beans gummi bears jelly lollipop apple</span>
                                    </div>
                                    <small class="">25 days ago</small>
                                </li>
                                <li>
                                    <div class="timeline-icon bg-warning">
                                        <i class="feather icon-alert-circle font-medium-2"></i>
                                    </div>
                                    <div class="timeline-info">
                                        <p class="font-weight-bold">Task Updated</p>
                                        <span>Cupcake gummi bears soufflé caramels candy</span>
                                    </div>
                                    <small class="">15 days ago</small>
                                </li>
                                <li>
                                    <div class="timeline-icon bg-success">
                                        <i class="feather icon-check font-medium-2"></i>
                                    </div>
                                    <div class="timeline-info">
                                        <p class="font-weight-bold">Task Completed</p>
                                        <span>Candy ice cream cake. Halvah gummi bears
                                        </span>
                                    </div>
                                    <small class="">20 minutes ago</small>
                                </li>
                                <li>
                                    <div class="timeline-icon bg-primary">
                                        <i class="feather icon-plus font-medium-2"></i>
                                    </div>
                                    <div class="timeline-info">
                                        <p class="font-weight-bold">Task Added</p>
                                        <span>Bonbon macaroon jelly beans gummi bears jelly lollipop apple</span>
                                    </div>
                                    <small class="">25 days ago</small>
                                </li>
                                <li>
                                    <div class="timeline-icon bg-warning">
                                        <i class="feather icon-alert-circle font-medium-2"></i>
                                    </div>
                                    <div class="timeline-info">
                                        <p class="font-weight-bold">Task Updated</p>
                                        <span>Cupcake gummi bears soufflé caramels candy</span>
                                    </div>
                                    <small class="">15 days ago</small>
                                </li>
                                <li>
                                    <div class="timeline-icon bg-success">
                                        <i class="feather icon-check font-medium-2"></i>
                                    </div>
                                    <div class="timeline-info">
                                        <p class="font-weight-bold">Task Completed</p>
                                        <span>Candy ice cream cake. Halvah gummi bears
                                        </span>
                                    </div>
                                    <small class="">20 minutes ago</small>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        @section('carsseva-alert')

            <!-- Alerts -->
            @foreach (['danger', 'warning', 'success', 'info'] as $key)
                @if(Session::has($key))
                    <div class="cws-alert">
                        <div class="alert alert-{{ $key }} alert-dismissible alert-auto-hide fade show" role="alert">
                            <div class="alert-text">{{ Session::get($key) }}</div>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
                            </button>
                        </div>
                    </div>
                @endif
            @endforeach

            <!-- Confirmation alert msg -->
            <div class="cws-alert car-alert" style="display: none;">
                <div class="background-overlay car-reject"></div>
                <div class="alert alert-dark-danger alert-dismissible fade show" role="alert">
                    <div class="alert-text">Are you sure you want to delete <strong class="car-name"></strong>? 
                        <div class="alert-btn">
                            <a href="#" class="btn btn-primary car-confirm">Yes</a>
                            <button class="btn btn-white car-reject">Cancel</button>
                        </div>
                    </div>
                    <button type="button" class="close car-reject">
                        <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
                    </button>
                </div>
            </div>

            <!-- Confirmation alert msg -->
            <div class="cws-alert address-alert" style="display: none;">
                <div class="background-overlay address-reject"></div>
                <div class="alert alert-dark-danger alert-dismissible fade show" role="alert">
                    <div class="alert-text">Are you sure you want to delete <strong class="address-name"></strong>? 
                        <div class="alert-btn">
                            <a href="#" class="btn btn-primary address-confirm">Yes</a>
                            <button class="btn btn-white address-reject">Cancel</button>
                        </div>
                    </div>
                    <button type="button" class="close address-reject">
                        <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
                    </button>
                </div>
            </div>

        @endsection

@endsection

@section('vendor-script')
        {{-- vendor files --}} 
        <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
@endsection

@section('page-script')
        {{-- Page js files --}}
        <script type="text/javascript">

            $(document).ready(function() {
                select2Init();

                // Delete Cars
                $('.delete-car').click( function() {
                    var item = $(this).data('item');

                    $('.car-name').html('customer car');
                    $('.car-confirm').attr('href', "{{ URL::to('customerCars') }}/"+item.id+"/delete");
                    $('.car-alert').show();
                });

                $('.car-reject').click( function() {
                    $('.car-alert').hide();
                });

                // Delete Addresses
                $('.delete-address').click( function() {
                    var item = $(this).data('item');

                    $('.address-name').html('customer address');
                    $('.address-confirm').attr('href', "{{ URL::to('customerAddresses') }}/"+item.id+"/delete");
                    $('.address-alert').show();
                });

                $('.address-reject').click( function() {
                    $('.address-alert').hide();
                });
            });

            function select2Init() {
                $('.select2').select2({
                    dropdownAutoWidth: true,
                    width: '100%'
                });

                $('.car_model_id').select2({
                    data: JSON.parse('<?php echo $brands; ?>'),
                    dropdownAutoWidth: true,
                    width: '100%'
                });
            }

            $(document).on('click', '.add-car', function() {
                if ( $('.add-car-section').hasClass('d-none') ) {
                    $('.add-car-section').removeClass('d-none');
                } else {
                    $('.add-car-section').addClass('d-none');
                }
            });

            $(document).on('click', '.edit-car', function() {
                var car = $(this).data('car');
                var edit = $(this).data('edit');

                var html = '';
                html += '<div class="justify-content-start align-items-center mb-1 edit-car-'+car.id+'">';
                    html += '<form id="carFrm" method="post" action="{{ URL::to("customerCars") }}/'+car.id+'" >';
                        html += '@csrf';
                        html += '<input type="hidden" name="_method" value="PUT">';
                        html += '<input type="hidden" name="user_id" value="{{ $customer->id }}">';
                        html += '<div class="row">';
                            html += '<div class="col-xl-6 col-12">';
                                html += '<label for="first-name-vertical">Select Model</label>';
                                html += '<div class="controls">';
                                    html += '<select name="car_model_id" id="car_model_id_'+car.id+'" class="form-control select2 car_model_id"></select>';
                                html += '</div>';
                            html += '</div>';
                            html += '<div class="col-xl-6 col-12">';
                                html += '<label for="first-name-vertical">Fuel Type</label>';
                                html += '<div class="controls">';
                                    html += '<select name="fuel_type" class="form-control select2">';
                                        html += '<option value="petrol" ' + ( (car.fuel_type == "petrol") ? 'selected="selected"' : '' ) + ' >Petrol</option>';
                                        html += '<option value="diesel" ' + ( (car.fuel_type == "diesel") ? 'selected="selected"' : '' ) + ' >Diesel</option>';
                                    html += '</select>';
                                html += '</div>';
                            html += '</div>';
                            html += '<div class="clearfix"></div>';
                            html += '<div class="col-xl-6 col-12">';
                                html += '<label for="first-name-vertical">Gear Type</label>';
                                html += '<div class="controls">';
                                    html += '<select name="gear_type" class="form-control select2">';
                                        html += '<option value="manual" ' + ( (car.gear_type == "manual") ? 'selected="selected"' : '' ) + ' >Manual</option>';
                                        html += '<option value="auto" ' + ( (car.gear_type == "auto") ? 'selected="selected"' : '' ) + ' >Auto</option>';
                                    html += '</select>';
                                html += '</div>';
                            html += '</div>';
                            html += '<div class="col-xl-6 col-12">';
                                html += '<label for="first-name-vertical">Car Number</label>';
                                html += '<div class="controls">';
                                    html += '<input type="text" placeholder="Car Number" name="car_number" class="form-control" value="'+car.car_number+'" maxlength="10" required>';
                                html += '</div>';
                            html += '</div>';
                            html += '<div class="clearfix"></div>';
                            html += '<div class="col-xl-6 col-12">';
                                html += '<label for="first-name-vertical">Total Travelled</label>';
                                html += '<div class="controls">';
                                    html += '<input type="text" placeholder="Total Travelled" name="total_travelled" class="form-control" value="'+car.total_travelled+'" required>';
                                html += '</div>';
                            html += '</div>';
                            html += '<div class="col-xl-6 col-12">';
                                html += '<label for="first-name-vertical">Avg. Monthly Uage</label>';
                                html += '<div class="controls">';
                                    html += '<input type="text" placeholder="Avg. Monthly Uage" name="avg_monthly_usage" class="form-control" value="'+car.avg_monthly_usage+'" required>';
                                html += '</div>';
                            html += '</div>';
                            html += '<div class="clearfix"></div>';
                            html += '<div class="col-xl-6 col-12">';
                                html += '<button class="btn btn-primary btn-block mt-1 waves-effect waves-light save-car">Save</button>';
                            html += '</div>';
                            html += '<div class="col-xl-6 col-12">';
                                html += '<button type="button" class="btn btn-light btn-block mt-1 waves-effect waves-light cancel-car" data-car_id="'+car.id+'">Cancel</button>';
                            html += '</div>';
                        html += '</div>';
                    html += '</form>';
                html += '</div>';

                if ( edit ) {
                    $('.car-item-'+car.id).after(html);
                    select2Init();
                    $('#car_model_id_'+car.id).val(car.car_model_id).trigger('change');
                    $(this).data('edit', false);
                }
            });

            $(document).on('click', '.cancel-car', function() {
                var car_id = $(this).data('car_id');
                $('.edit-car-'+car_id).remove();
                $('.car-'+car_id).data('edit', true);
            });

            $(document).on('click', '.add-address', function() {
                loadState(true);
                if ( $('.add-address-section').hasClass('d-none') ) {
                    $('.add-address-section').removeClass('d-none');
                } else {
                    $('.add-address-section').addClass('d-none');
                }
            });

            $(document).on('click', '.edit-address', function() {
                var address = $(this).data('address');
                var edit = $(this).data('edit');

                var html = '';
                html += '<div class="justify-content-start align-items-center mb-1 edit-address-'+address.id+'">';
                    html += '<form id="addressFrm" method="post" action="{{ URL::to("customerAddresses") }}/'+address.id+'" >';
                        html += '@csrf';
                        html += '<input type="hidden" name="_method" value="PUT">';
                        html += '<input type="hidden" name="user_id" value="{{ $customer->id }}">';
                        html += '<div class="row">';
                            html += '<div class="col-xl-12 col-lg-12 col-12">';
                                html += '<label for="first-name-vertical">Address Line 1</label>';
                                html += '<div class="controls">';
                                    html += '<input type="text" name="address_line_1" value="'+address.address_line_1+'" placeholder="Address Line 1" required class="form-control">';
                                html += '</div>';
                            html += '</div>';
                            html += '<div class="clearfix"></div>';
                            html += '<div class="col-xl-12 col-lg-12 col-12">';
                                html += '<label for="first-name-vertical">Address Line 2</label>';
                                html += '<div class="controls">';
                                    html += '<input type="text" name="address_line_2" value="'+address.address_line_2+'" placeholder="Address Line 2" class="form-control">';
                                html += '</div>';
                            html += '</div>';
                            html += '<div class="clearfix"></div>';
                            html += '<div class="col-xl-12 col-lg-12 col-12">';
                                html += '<label for="first-name-vertical">State</label>';
                                html += '<div class="controls">';
                                    //html += '<input type="text" name="state" value="'+address.state+'" placeholder="State" class="form-control">';
                                    html += '<select name="state_id" id="state_edit_id" class="form-control select2" required><option value="'+address.state_id+'">'+address.state.name+'</option></select>';
                                html += '</div>';
                            html += '</div>';
                            html += '<div class="clearfix"></div>';
                            html += '<div class="col-xl-6 col-12">';
                                html += '<label for="first-name-vertical">City</label>';
                                html += '<div class="controls">';
                                    //html += '<input type="text" placeholder="City" value="'+address.city+'" name="city" class="form-control" required>';
                                    html += '<select name="city_id" id="city_edit_id" class="form-control select2" required><option value="'+address.city_id+'">'+address.city.name+'</option></select>';
                                html += '</div>';
                            html += '</div>';
                            html += '<div class="col-xl-6 col-12">';
                                html += '<label for="first-name-vertical">Zipcode</label>';
                                html += '<div class="controls">';
                                    html += '<input type="text" placeholder="Zipcode" value="'+address.zipcode+'" name="zipcode" class="form-control" required>';
                                html += '</div>';
                            html += '</div>';
                            html += '<div class="clearfix"></div>';
                            html += '<div class="col-xl-6 col-6">';
                                html += '<button class="btn btn-primary btn-block mt-1 waves-effect waves-light save-address">Save</button>';
                            html += '</div>';
                            html += '<div class="col-xl-6 col-6">';
                                html += '<button type="button" class="btn btn-light btn-block mt-1 waves-effect waves-light cancel-address"  data-address_id="'+address.id+'">Cancel</button>';
                            html += '</div>';
                        html += '</div>';
                    html += '</form>';
                html += '</div>';

                if ( edit ) {
                    $('.address-item-'+address.id).after(html);
                    $(this).data('edit', false);
                    loadState(true, 'state_edit_id');
                    loadCity(true, address.state_id, 'city_edit_id');
                }
            });

            $(document).on('click', '.cancel-address', function() {
                var address_id = $(this).data('address_id');
                $('.edit-address-'+address_id).remove();
                $('.address-'+address_id).data('edit', true);
            });


            $(document).on('select2:select', '#state_id', function() {
                $("#city_id").val("");
                loadCity(false, this.value);
            });

            $(document).on('select2:select', '#state_edit_id', function() {
                $("#city_edit_id").val("");
                loadCity(false, this.value, "city_edit_id");
            });

            function loadState(selected = false, field = "state_id") {
                var stateDrop = $("#"+field).select2({
                    width: '100%',
                    ajax: {
                        url: "/location",
                        type: 'post',
                        data: {'type': 'state', 'country_id': '1'},
                        dataType: 'json',
                        /*data: function (params) {
                            return {
                                q: params.term // search term
                            };
                        },*/
                        processResults: function (data, params) {
                            //console.log(data.response_data, params);
                            var results = [];
                            if ( data.response_code == 200 ) {
                                $.each(data.response_data, function(index, item){
                                    results.push({
                                        id: item.id,
                                        text: item.name
                                    });
                                });
                            }
                            return {
                                results: results
                            };
                        },
                        cache: true     
                    }
                });
            }

            function loadCity(selected = false, state_id, field = "city_id") {
                $("#"+field).select2({
                    width: '100%',
                    ajax: {
                        url: "/location",
                        type: 'post',
                        data: {'type': 'city', 'country_id': '1', 'state_id': state_id},
                        dataType: 'json',
                        processResults: function (data, params) {
                            //console.log(data.response_data, params);
                            var results = [];
                            if ( data.response_code == 200 ) {
                                $.each(data.response_data, function(index, item){
                                    results.push({
                                        id: item.id,
                                        text: item.name
                                    });
                                });
                            }
                            return {
                                results: results
                            };
                        },
                        cache: true     
                    }
                });
            }

        </script>
@endsection
