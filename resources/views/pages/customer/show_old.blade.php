@extends('layouts/contentLayoutMaster')

@section('title', $customer->name)

@section('vendor-style')
        {{-- vendor css files --}}
@endsection

@section('page-style')
        <!-- Page css files -->
        <link rel="stylesheet" href="{{ asset(mix('css/pages/users.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/pages/app-ecommerce-shop.css')) }}">
@endsection

@section('content')
        
        <div id="user-profile">
            <div class="row">
                <div class="col-12">
                    <div class="profile-header mb-2">
                        <div class="relative">
                            <div class="cover-container">
                                <img class="img-fluid bg-cover rounded-0 w-100" style="height: 100px;" src="{{ asset('images/profile/user-uploads/cover.jpg') }}" alt="User Profile Image">
                            </div>
                            <div class="profile-img-container d-flex align-items-center justify-content-between">
                                <img src="{{ asset('images/profile/user-uploads/user-13.jpg') }}" class="rounded-circle img-border box-shadow-1" alt="Card image">
                                <div class="float-right">
                                    <button type="button" class="btn btn-icon btn-icon rounded-circle btn-primary mr-1">
                                    <i class="feather icon-edit-2"></i>
                                    </button>
                                    <button type="button" class="btn btn-icon btn-icon rounded-circle btn-primary">
                                    <i class="feather icon-settings"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-end align-items-center profile-header-nav">
                            <nav class="navbar navbar-expand-sm w-100 pr-0">
                                <button class="navbar-toggler pr-0" type="button" data-toggle="collapse" data-target="navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"><i class="feather icon-align-justify"></i></span>
                                </button>
                                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                    <ul class="nav nav-tabs w-75 ml-sm-auto" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="services-tab" data-toggle="tab" href="#services" aria-controls="services" role="tab" aria-selected="true">Services</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="cars-tab" data-toggle="tab" href="#cars" aria-controls="cars" role="tab" aria-selected="false">Cars</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="reviews-tab" data-toggle="tab" href="#reviews" aria-controls="reviews" role="tab" aria-selected="false">Reviews</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="activity-tab" data-toggle="tab" href="#activity" aria-controls="activity" role="tab" aria-selected="false">Activity Logs</a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <section id="profile-info">
                <div class="row">
                    <div class="col-xl-3 col-lg-4 col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>About</h4>
                                <i class="feather icon-more-horizontal cursor-pointer"></i>
                            </div>
                            <div class="card-body">
                                <div class="mt-1">
                                    <h6 class="mb-0">Email:</h6>
                                    <p>purvesh@mailinator.com</p>
                                </div>
                                <div class="mt-1">
                                    <h6 class="mb-0">Phone No:</h6>
                                    <p>1234567890</p>
                                </div>
                                <div class="mt-1">
                                    <h6 class="mb-0">Lives:</h6>
                                    <p>Science City, Ahmedabad, Gujarat</p>
                                </div>
                                <div class="mt-1">
                                    <h6 class="mb-0">Joined:</h6>
                                    <p>November 15, 2015</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-8 col-12">
                        <div class="tab-content">

                            {{-- Services --}}
                            <div role="tabpanel" class="tab-pane active ecommerce-application" id="services" aria-labelledby="services-tab" aria-expanded="true">
                                
                                <section id="place-order" class="list-view product-checkout mt-0" style="width: 100%;grid-template-columns: 1fr;">
                                    <div class="checkout-items">
                                        <div class="card ecommerce-card">
                                            <div class="card-content">
                                                <div class="item-img text-center">
                                                    <a href="app-ecommerce-details.html">
                                                    <img width="200" height="200" src="https://imgd.aeplcdn.com/664x374/cw/ec/38637/Tata-Hexa-Right-Front-Three-Quarter-171007.jpg" alt="img-placeholder">
                                                    </a>
                                                </div>
                                                <div class="card-body">
                                                    <div class="item-name">
                                                        <a href="app-ecommerce-details.html">Regular Services, A.C Service & Repair and Genuine OEM Parts</a>
                                                        <span></span>
                                                        <p class="item-company">By <span class="company-name">Purvesh Patel</span></p>
                                                        <p class="stock-status-in">Hyundai Creta (Diesel)</p>
                                                    </div>
                                                    <p class="delivery-date">Vendor, The Family Garage</p>
                                                    <small>Nr. Bodakdev Water Tank, B/H. Allen Care Institute, <br> West End Park, Bodakdev, <br>Ahmedabad, Gujarat 380059, India</small>
                                                </div>
                                                <div class="item-options text-center">
                                                    <div class="item-wrapper">
                                                        <div class="item-rating">
                                                            <small>3 days ago</small>
                                                        </div>
                                                        <div class="item-cost">
                                                            <h6 class="item-price">₹ 6,500.00</h6>
                                                            <p class="shipping">Waiting For Pickup</p>
                                                        </div>
                                                    </div>
                                                    <div class="wishlist remove-wishlist">More Detail</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card ecommerce-card">
                                            <div class="card-content">
                                                <div class="item-img text-center">
                                                    <a href="app-ecommerce-details.html">
                                                    <img width="200" height="200" src="https://s.aolcdn.com/dims-global/dims3/GLOB/legacy_thumbnail/640x400/quality/80/https://s.aolcdn.com/commerce/autodata/images/USC80HYC012A121001.jpg" alt="img-placeholder">
                                                    </a>
                                                </div>
                                                <div class="card-body">
                                                    <div class="item-name">
                                                        <a href="app-ecommerce-details.html">Regular Services and A.C Service & Repair</a>
                                                        <span></span>
                                                        <p class="item-company">By <span class="company-name">Purvesh Patel</span></p>
                                                        <p class="stock-status-in">Hyundai Accent (Petrol)</p>
                                                    </div>
                                                    <p class="delivery-date">Vendor, DVJ Cars LLP</p>
                                                    <small>Nr. Bodakdev Water Tank, B/H. Allen Care Institute, <br> West End Park, Bodakdev, <br>Ahmedabad, Gujarat 380059, India</small>
                                                </div>
                                                <div class="item-options text-center">
                                                    <div class="item-wrapper">
                                                        <div class="item-rating">
                                                            <small>19/12/2019</small>
                                                        </div>
                                                        <div class="item-cost">
                                                            <h6 class="item-price">₹ 5000.00</h6>
                                                            <p class="shipping success">Picked Up</p>
                                                        </div>
                                                    </div>
                                                    <div class="wishlist remove-wishlist">More Detail</div>
                                                    <!-- <div class="cart remove-wishlist">
                                                        <i class="fa fa-heart-o mr-25"></i> Wishlist
                                                    </div> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>

                            </div>

                            {{-- Cars --}}
                            <div role="tabpanel" class="tab-pane" id="cars" aria-labelledby="cars-tab" aria-expanded="true">
                                
                                <div class="row">
                                    <div class="col-xl-4 col-md-6 col-sm-12 profile-card-2">
                                        <div class="card">
                                            <div class="card-content">
                                                <div class="card-body">
                                                    <img class="card-img img-fluid mb-1" src="https://imgd.aeplcdn.com/664x374/cw/ec/38637/Tata-Hexa-Right-Front-Three-Quarter-171007.jpg" alt="Card image cap">
                                                    <h5 class="mt-1">Tata - Hexa</h5>
                                                    <p class="card-text">Manufactured on January, 2008</p>
                                                    <hr class="my-1">
                                                    <div class="d-flex justify-content-between mt-2">
                                                        <div class="float-left">
                                                            <p class="font-medium-2 mb-0">Petrol</p>
                                                            <p class="">Type</p>
                                                        </div>
                                                        <div class="float-right">
                                                            <p class="font-medium-2 mb-0">3000KM</p>
                                                            <p class="">Travelled</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-md-6 col-sm-12 profile-card-2">
                                        <div class="card">
                                            <div class="card-content">
                                                <div class="card-body">
                                                    <img class="card-img img-fluid mb-1" src="https://imgd.aeplcdn.com/664x374/cw/ec/38637/Tata-Hexa-Right-Front-Three-Quarter-171007.jpg" alt="Card image cap">
                                                    <h5 class="mt-1">Tata - Hexa</h5>
                                                    <p class="card-text">Manufactured on January, 2008</p>
                                                    <hr class="my-1">
                                                    <div class="d-flex justify-content-between mt-2">
                                                        <div class="float-left">
                                                            <p class="font-medium-2 mb-0">Petrol</p>
                                                            <p class="">Type</p>
                                                        </div>
                                                        <div class="float-right">
                                                            <p class="font-medium-2 mb-0">3000KM</p>
                                                            <p class="">Travelled</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-md-6 col-sm-12 profile-card-2">
                                        <div class="card">
                                            <div class="card-content">
                                                <div class="card-body">
                                                    <img class="card-img img-fluid mb-1" src="https://imgd.aeplcdn.com/664x374/cw/ec/38637/Tata-Hexa-Right-Front-Three-Quarter-171007.jpg" alt="Card image cap">
                                                    <h5 class="mt-1">Tata - Hexa</h5>
                                                    <p class="card-text">Manufactured on January, 2008</p>
                                                    <hr class="my-1">
                                                    <div class="d-flex justify-content-between mt-2">
                                                        <div class="float-left">
                                                            <p class="font-medium-2 mb-0">Petrol</p>
                                                            <p class="">Type</p>
                                                        </div>
                                                        <div class="float-right">
                                                            <p class="font-medium-2 mb-0">3000KM</p>
                                                            <p class="">Travelled</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>

                            {{-- Reviews --}}
                            <div role="tabpanel" class="tab-pane" id="reviews" aria-labelledby="reviews-tab" aria-expanded="true">
                                
                                <div class="row">
                                    <div class="col-4">
                                        <div class="card" style="opacity: 1;">
                                            <div class="card-header">
                                                <div class="d-flex justify-content-start align-items-center mb-1">
                                                    <div class="avatar mr-50">
                                                        <img src="http://localhost:8000/images/portrait/default.jpg" alt="avtar img holder" height="35" width="35">
                                                    </div>
                                                    <div class="user-page-info">
                                                        <p class="text-bold-600 mb-0">Hemal Kalariya</p>
                                                        <small>T&amp;T Motors</small>
                                                        <div class="badge badge-primary badge-pill badge-sm p-0">
                                                            <i class="feather icon-check font-small-1"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="heading-elements">
                                                    <ul class="list-inline mb-0">
                                                        <li><a href="javascript:void(0);" class="change-status" data-action="inactive" data-ids="2"><i class="feather icon-eye"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="card-body pt-0">
                                                <div class="twitter-feed">
                                                    <p class="mb-2">Carrot cake cake gummies I love I love tiramisu. Biscuit marzipan cookie lemon drops.
                                                        #python
                                                    </p>
                                                    <div class="item-wrapper">
                                                        <small>3 days ago</small>
                                                        <div class="item-rating pull-right">
                                                            <div class="badge badge-primary badge-md">
                                                                <span>4</span> <i class="feather icon-star"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="card" style="opacity: 1;">
                                            <div class="card-header">
                                                <div class="d-flex justify-content-start align-items-center mb-1">
                                                    <div class="avatar mr-50">
                                                        <img src="http://localhost:8000/images/portrait/default.jpg" alt="avtar img holder" height="35" width="35">
                                                    </div>
                                                    <div class="user-page-info">
                                                        <p class="text-bold-600 mb-0">Hemal Kalariya</p>
                                                        <small>T&amp;T Motors</small>
                                                        <div class="badge badge-primary badge-pill badge-sm p-0">
                                                            <i class="feather icon-check font-small-1"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="heading-elements">
                                                    <ul class="list-inline mb-0">
                                                        <li><a href="javascript:void(0);" class="change-status" data-action="inactive" data-ids="1"><i class="feather icon-eye"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="card-body pt-0">
                                                <div class="twitter-feed">
                                                    <p class="mb-2">I love cookie chupa chups sweet tart apple pie chocolate bar. Jelly-o oat cake chupa chups.
                                                        #js #vuejs
                                                    </p>
                                                    <div class="item-wrapper">
                                                        <small>2 days ago</small>
                                                        <div class="item-rating pull-right">
                                                            <div class="badge badge-primary badge-md">
                                                                <span>4.5</span> <i class="feather icon-star"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            {{-- Activity Logs --}}
                            <div role="tabpanel" class="tab-pane" id="activity" aria-labelledby="activity-tab" aria-expanded="true">

                                <div class="card">
                                    <div class="card-content">
                                        <div class="card-body">
                                            <ul class="activity-timeline timeline-left list-unstyled">
                                                <li>
                                                    <div class="timeline-icon bg-primary">
                                                        <i class="feather icon-plus font-medium-2 align-middle"></i>
                                                    </div>
                                                    <div class="timeline-info">
                                                        <p class="font-weight-bold mb-0">Client Meeting</p>
                                                        <span class="font-small-3">Bonbon macaroon jelly beans gummi bears jelly lollipop apple</span>
                                                    </div>
                                                    <small class="text-muted">25 mins ago</small>
                                                </li>
                                                <li>
                                                    <div class="timeline-icon bg-warning">
                                                        <i class="feather icon-alert-circle font-medium-2 align-middle"></i>
                                                    </div>
                                                    <div class="timeline-info">
                                                        <p class="font-weight-bold mb-0">Email Newsletter</p>
                                                        <span class="font-small-3">Cupcake gummi bears soufflé caramels candy</span>
                                                    </div>
                                                    <small class="text-muted">15 days ago</small>
                                                </li>
                                                <li>
                                                    <div class="timeline-icon bg-danger">
                                                        <i class="feather icon-check font-medium-2 align-middle"></i>
                                                    </div>
                                                    <div class="timeline-info">
                                                        <p class="font-weight-bold mb-0">Plan Webinar</p>
                                                        <span class="font-small-3">Candy ice cream cake. Halvah gummi bears</span>
                                                    </div>
                                                    <small class="text-muted">20 days ago</small>
                                                </li>
                                                <li>
                                                    <div class="timeline-icon bg-success">
                                                        <i class="feather icon-check font-medium-2 align-middle"></i>
                                                    </div>
                                                    <div class="timeline-info">
                                                        <p class="font-weight-bold mb-0">Launch Website</p>
                                                        <span class="font-small-3">Candy ice cream cake. </span>
                                                    </div>
                                                    <small class="text-muted">25 days ago</small>
                                                </li>
                                                <li>
                                                    <div class="timeline-icon bg-primary">
                                                        <i class="feather icon-check font-medium-2 align-middle"></i>
                                                    </div>
                                                    <div class="timeline-info">
                                                        <p class="font-weight-bold mb-0">Marketing</p>
                                                        <span class="font-small-3">Candy ice cream. Halvah bears Cupcake gummi bears.</span>
                                                    </div>
                                                    <small class="text-muted">28 days ago</small>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="d-none d-xl-block col-xl-2">
                        asd
                    </div>
                </div>
            </section>
        </div>

        @section('carsseva-alert')

            <!-- Alerts -->
            @foreach (['danger', 'warning', 'success', 'info'] as $key)
                @if(Session::has($key))
                    <div class="cws-alert">
                        <div class="alert alert-{{ $key }} alert-dismissible fade show" role="alert">
                            <div class="alert-text">{{ Session::get($key) }}</div>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
                            </button>
                        </div>
                    </div>
                @endif
            @endforeach

        @endsection

@endsection

@section('vendor-script')
        {{-- vendor files --}} 
@endsection

@section('page-script')
        {{-- Page js files --}}
        <script type="text/javascript">
            $(document).ready(function() {

            });
        </script>
@endsection

