@extends('layouts/contentLayoutMaster')

@section('title', 'Customers')

@section('vendor-style')
        {{-- vendor css files --}}
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
@endsection

@section('page-style')
        <!-- Page css files -->
@endsection

@section('content')
        
        <!-- Column selectors with Export Options and print table -->
        <section id="column-selectors">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body card-dashboard">
                                <div class="table-responsive">
                                    <table class="table customer-table compact-table table-data-nowrap">
                                        <thead>
                                            <tr>
                                                <th>Sr</th>
                                                <th>Image</th>
                                                <th>Name</th>
                                                <th>Contact No</th>
                                                <th>Email</th>
                                                <th>Status</th>
                                                {{-- <th>Action</th> --}}
                                            </tr>
                                        </thead>
                                        <tbody>
                                          @php
                                            $sr = 1;
                                          @endphp
                                          @foreach($customers as $in => $customer)
                                            <tr class="group">
                                                <td>{{ $sr }}</td>
                                                <td>
                                                  <ul class="list-unstyled users-list m-0 d-flex align-items-center">
                                                    <li data-toggle="tooltip" data-popup="tooltip-custom" data-placement="bottom" data-original-title="{{ $customer->name }}" class="avatar pull-up">
                                                      <img class="media-object rounded-circle" src="{{ $customer->profile_pic_url }}" alt="Avatar" height="30" width="30">
                                                    </li>
                                                  </ul>
                                                </td>
                                                <td><a href="{{ URL::to('customers/' . $customer->id) }}">{{ $customer->name }}</a></td>
                                                <td>{{ $customer->contact_no }}</td>
                                                <td>{{ $customer->email }}</td>
                                                <td>
                                                    <div class="custom-control custom-switch custom-switch-success custom-control-inline">
                                                        <input type="checkbox" class="custom-control-input item-status-change" data-item="{{ $customer }}" data-sr="status{{ $sr }}" id="status{{ $sr }}" {{ (($customer->status == '1') ? 'checked="checked"' : '') }}>
                                                        <label class="custom-control-label" for="status{{ $sr }}"></label>
                                                    </div>
                                                </td>
                                                {{-- <td class="action-btn">
                                                  <button type="button" class="btn btn-sm btn-icon btn-icon rounded-circle btn-flat-light mr-1 mb-1 waves-effect waves-light delete-item" data-item="{{ $customer }}"><i class="feather icon-trash-2"></i></button>
                                                </td> --}}
                                            </tr>
                                            @php
                                                $sr += 1;
                                            @endphp
                                          @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Sr</th>
                                                <th>Image</th>
                                                <th>Name</th>
                                                <th>Contact No</th>
                                                <th>Email</th>
                                                <th>Status</th>
                                                {{-- <th>Action</th> --}}
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Column selectors with Export Options and print table -->

        @section('carsseva-alert')

            <!-- Alerts -->
            @foreach (['danger', 'warning', 'success', 'info'] as $key)
                @if(Session::has($key))
                    <div class="cws-alert">
                        <div class="alert alert-{{ $key }} alert-dismissible alert-auto-hide fade show" role="alert">
                            <div class="alert-text">{{ Session::get($key) }}</div>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
                            </button>
                        </div>
                    </div>
                @endif
            @endforeach

            <!-- Confirmation alert msg for status change -->
            <div class="cws-alert status-change-alert" style="display: none;">
                <div class="background-overlay status-change-reject"></div>
                <div class="alert alert-dark-danger alert-dismissible fade show" role="alert">
                    <div class="alert-text">Are you sure you want to <strong class="status-change-name"></strong>? 
                        <div class="alert-btn">
                            <button class="btn btn-primary status-change-confirm">Yes</button>
                            <button class="btn btn-white status-change-reject">Cancel</button>
                        </div>
                    </div>
                    <button type="button" class="close status-change-reject">
                        <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
                    </button>
                </div>
            </div>

        @endsection

@endsection

@section('vendor-script')
        {{-- vendor files --}}
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
@endsection

@section('page-script')
        {{-- Page js files --}}
        <script type="text/javascript">
            $(document).ready(function() {

                // Datatable
                $('.customer-table').DataTable({
                    responsive: true,
                    "ordering": false,
                    "columnDefs": [
                        { "visible": false, "targets": 0},
                        { "width": '8%', "targets": 1},
                        { "width": '25%', "targets": 2},
                        { "width": '25%', "targets": 4},
                        { "width": '8%', "targets": 5},
                        //{ "width": '15%', "targets": 6},
                    ],
                    "order": [
                        [0, 'asc']
                    ]
                });

                // Status change
                $('.status-change-confirm').click( function() {
                    
                    // trigger status-change event
                    var ids = $(this).attr('data-ids');
                    var action = $(this).attr('data-action');
                    var type = $(this).attr('data-type');

                    $.post('customers/bulk-action', {'action': action, 'ids': ids, 'type': type}, function(response){
                        if ( response.status == 200 ) {
                            window.location = "{{ url('/customers') }}"
                        }
                    });
                });

                $('.status-change-reject').click( function() {

                    var action = $(this).attr('data-action');
                    var sr = $(this).attr('data-sr');
                    
                    if ( action == 'active' ) {
                        $("#"+sr).prop("checked", false);
                    } else {
                        $("#"+sr).prop("checked", true);
                    }

                    $('.status-change-alert').hide();
                });
            });

            $(document).on('change', '.item-status-change', function() {
                var item = $(this).data('item');
                var status = ((this.checked) ? 'active' : 'inactive');
                
                if ( item.brand_id != undefined ) {
                    $('.status-change-confirm').attr('data-ids', item.id);
                    $('.status-change-confirm').attr('data-type', 'model');
                } else {
                    $('.status-change-confirm').attr('data-ids', item.id);
                    $('.status-change-confirm').attr('data-type', 'brand');
                }

                $('.status-change-name').html(status + ' ' + item.name);
                $('.status-change-alert').show();
                $('.status-change-confirm').attr('data-action', status);
                $('.status-change-reject').attr('data-action', status);
                $('.status-change-reject').attr('data-sr', $(this).data('sr'));
            });
        </script>
@endsection

