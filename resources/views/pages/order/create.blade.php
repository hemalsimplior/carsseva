@extends('layouts/contentLayoutMaster')

@section('title', 'Add Order')

@section('vendor-style')
        {{-- vendor css files --}}
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection

@section('page-style')
        <!-- Page css files -->
        <link rel="stylesheet" href="{{ asset(mix('css/plugins/forms/wizard.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/plugins/file-uploaders/dropzone.css')) }}">
@endsection

@section('content')
    
        <!-- Form wizard with step validation section start -->
        <section id="validation">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <form method="post" id="orderFrm" action="{{ route('orders.store') }}">
                                    @csrf

                                    <h6>Customer Infomation</h6>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="lastName3">Select Customer *</label>
                                                <select name="customer_id" id="customer_id" class="form-control select2" required>
                                                    <option value='' selected="selected">Select Customer</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="lastName3">Select Car *</label>
                                                <select name="user_car_id" id="user_car_id" class="form-control select2" required>
                                                    <option value='' selected="selected">Select Car</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="lastName3">Select Vendor *</label>
                                                <select name="vendor_id" id="vendor_id" class="form-control select2" required>
                                                    <option value='' selected="selected">Select Vendor</option>
                                                    @foreach($vendors as $vendor)
                                                        <option value="{{ $vendor['id'] }}">{{ $vendor['name'] }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group customer_address">
                                                <label for="lastName3">Service Date *</label>
                                                <input type="text" id="service_date" name="service_date" class="form-control" placeholder="Service Date" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group customer_address">
                                                <label for="lastName3">Service Time Slot *</label>
                                                <ul class="list-unstyled mb-0">
                                                    <li class="d-inline-block mr-2">
                                                        <fieldset>
                                                            <div class="vs-radio-con">
                                                                <input type="radio" name="service_time_slot" checked value="08AM - 10AM">
                                                                <span class="vs-radio vs-radio-sm">
                                                                <span class="vs-radio--border"></span>
                                                                <span class="vs-radio--circle"></span>
                                                                </span>
                                                                <span class="">08AM - 10AM</span>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2">
                                                        <fieldset>
                                                            <div class="vs-radio-con">
                                                                <input type="radio" name="service_time_slot" value="10AM - 12PM">
                                                                <span class="vs-radio vs-radio-sm">
                                                                <span class="vs-radio--border"></span>
                                                                <span class="vs-radio--circle"></span>
                                                                </span>
                                                                <span class="">10AM - 12PM</span>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2">
                                                        <fieldset>
                                                            <div class="vs-radio-con">
                                                                <input type="radio" name="service_time_slot" value="12PM - 02PM">
                                                                <span class="vs-radio vs-radio-sm">
                                                                <span class="vs-radio--border"></span>
                                                                <span class="vs-radio--circle"></span>
                                                                </span>
                                                                <span class="">12PM - 02PM</span>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2">
                                                        <fieldset>
                                                            <div class="vs-radio-con">
                                                                <input type="radio" name="service_time_slot" value="02PM - 04PM">
                                                                <span class="vs-radio vs-radio-sm">
                                                                <span class="vs-radio--border"></span>
                                                                <span class="vs-radio--circle"></span>
                                                                </span>
                                                                <span class="">02PM - 04PM</span>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2">
                                                        <fieldset>
                                                            <div class="vs-radio-con">
                                                                <input type="radio" name="service_time_slot" value="04PM - 06PM">
                                                                <span class="vs-radio vs-radio-sm">
                                                                <span class="vs-radio--border"></span>
                                                                <span class="vs-radio--circle"></span>
                                                                </span>
                                                                <span class="">04PM - 06PM</span>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2">
                                                        <fieldset>
                                                            <div class="vs-radio-con">
                                                                <input type="radio" name="service_time_slot" value="06PM - 08PM">
                                                                <span class="vs-radio vs-radio-sm">
                                                                <span class="vs-radio--border"></span>
                                                                <span class="vs-radio--circle"></span>
                                                                </span>
                                                                <span class="">06PM - 08PM</span>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="lastName3">Pickup Type *</label>
                                                <ul class="list-unstyled mb-0">
                                                    <li class="d-inline-block mr-2">
                                                        <fieldset>
                                                            <div class="vs-radio-con">
                                                                <input type="radio" name="pickup_type" checked value="pickup">
                                                                <span class="vs-radio">
                                                                <span class="vs-radio--border"></span>
                                                                <span class="vs-radio--circle"></span>
                                                                </span>
                                                                <span class="">Pickup</span>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2">
                                                        <fieldset>
                                                            <div class="vs-radio-con">
                                                                <input type="radio" name="pickup_type" value="self_drop">
                                                                <span class="vs-radio">
                                                                <span class="vs-radio--border"></span>
                                                                <span class="vs-radio--circle"></span>
                                                                </span>
                                                                <span class="">Self Drop</span>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group customer_address">
                                                <label for="lastName3">Select Customer Address *</label>
                                                <select name="user_address_id" id="user_address_id" class="form-control select2" required>
                                                    <option value='' selected="selected">Select Address</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <h6>Services Detail</h6>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="lastName3">Select Services *</label>
                                                <select name="service_id[]" id="service_id" multiple="multiple" class="form-control select2" required></select>
                                            </div>
                                        </div>
                                    </div>

                                    <button type="submit" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Create Order</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Form wizard with step validation section end -->

        @section('carsseva-alert')

            <!-- Alerts -->
            @foreach (['danger', 'warning', 'success', 'info'] as $key)
                @if(Session::has($key))
                    <div class="cws-alert">
                        <div class="alert alert-{{ $key }} alert-dismissible alert-auto-hide fade show" role="alert">
                            <div class="alert-text">{{ Session::get($key) }}</div>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
                            </button>
                        </div>
                    </div>
                @endif
            @endforeach

        @endsection

@endsection

@section('vendor-script')
        {{-- vendor files --}}
        <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
@endsection

@section('page-script')
        {{-- Page js files --}}
        <script type="text/javascript">
        
            $(document).ready(function() {

                // Form
                var validator = $( "#orderFrm" ).validate({
                    //ignore: ""
                });

                $('#service_date').daterangepicker({
                    singleDatePicker: true,
                    timePicker: false,
                    startDate: moment().format('DD/MM/YYYY'),
                    locale: {
                      format: 'DD/MM/YYYY'
                    }
                });

                $('.select2').select2({
                    dropdownAutoWidth: true,
                    width: '100%',
                });

                $("#customer_id").select2({
                    ajax: {
                        url: "/customerList",
                        type: 'post',
                        //data: {'type': 'state', 'country_id': '1'},
                        data: function (params) {
                            var query = {'s': params.term, 'type': 'state', 'country_id': '1'};
                            return query;
                        },
                        dataType: 'json',
                        processResults: function (data, params) {
                            //console.log(data.response_data, params);
                            var results = [];
                            if ( data.response_code == 200 ) {
                                $.each(data.response_data, function(index, item){
                                    results.push({
                                        id: item.id,
                                        text: item.name
                                    });
                                });
                            }
                            return {
                                results: results
                            };
                        },
                        cache: true     
                    }
                });
            });

            $(document).on('select2:select', '#customer_id', function() {
                $("#user_car_id").val("");
                $("#user_address_id").val("");
                
                initCar();
                initAddress();
            });

            $(document).on('select2:select', '#vendor_id', function() {
                initServices();
            });

            function initCar() {
                $("#user_car_id").select2({
                    ajax: {
                        url: "/carList",
                        type: 'post',
                        data: {'user_id': $('#customer_id').val()},
                        dataType: 'json',
                        processResults: function (data, params) {
                            //console.log(data.response_data, params);
                            var results = [];
                            if ( data.response_code == 200 ) {
                                $.each(data.response_data, function(index, item){
                                    console.log(item);
                                    results.push({
                                        id: item.id,
                                        text: item.car_model.brand.name +' ('+item.car_model.name+') - '+item.car_number
                                    });
                                });
                            }
                            return {
                                results: results
                            };
                        },
                        cache: true     
                    }
                });
            }

            function initAddress() {
                $("#user_address_id").select2({
                    ajax: {
                        url: "/customerAddressList",
                        type: 'post',
                        data: {'user_id': $('#customer_id').val()},
                        dataType: 'json',
                        processResults: function (data, params) {
                            //console.log(data.response_data, params);
                            var results = [];
                            if ( data.response_code == 200 ) {
                                $.each(data.response_data, function(index, item){
                                    results.push({
                                        id: item.id,
                                        text: item.address_line_1 + ', ' + item.city.name + ', ' + item.state.name + ' - ' + item.zipcode
                                    });
                                });
                            }
                            return {
                                results: results
                            };
                        },
                        cache: true     
                    }
                });
            }
            
            function initServices() {
                $("#service_id").select2({
                    ajax: {
                        url: "/serviceList",
                        type: 'post',
                        //data: {'vendor_id': $('#vendor_id').val()},
                        data: function (params) {
                            var query = {'s': params.term, 'vendor_id': $('#vendor_id').val()};
                            return query;
                        },
                        dataType: 'json',
                        processResults: function (data, params) {
                            //console.log(data.response_data, params);
                            var results = [];
                            if ( data.response_code == 200 ) {
                                results = data.response_data;
                                /* $.each(data.response_data, function(index, item){
                                    results.push({
                                        id: item.id,
                                        text: item.name
                                    });
                                }); */
                            }
                            return {
                                results: results
                            };
                        },
                        cache: true     
                    }
                });
            }

        </script>
@endsection

