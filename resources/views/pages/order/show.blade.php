@extends('layouts/contentLayoutMaster')

@section('title', '#' . $order->sequence_id)

@section('vendor-style')
        {{-- vendor css files --}}
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/plugins/forms/validation/form-validation.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/file-uploaders/dropzone.css')) }}">
@endsection

@section('page-style')
        <!-- Page css files -->
        <link rel="stylesheet" href="{{ asset(mix('css/pages/users.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/pages/app-ecommerce-shop.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/pages/invoice.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/plugins/file-uploaders/dropzone.css')) }}">
@endsection

@section('content')

		<form id="orderFrm" method="POST" action="{{ URL::to('orders/') }}/{{ (( $order->is_awaiting_for_approval == '1' ) ? $order->order_id : $order->id ) }}">
            @csrf
            <input name="_method" type="hidden" value="PUT">
            
            <div class="content-header row">
			    <div class="content-header-left col-md-7 col-12 mb-2">
			        <div class="row breadcrumbs-top">
			            <div class="col-12">
			                <div class="breadcrumb-wrapper col-12" style="padding: 0;">
			                    @if(@isset($breadcrumbs_custom))
			                        <ol class="breadcrumb" style="border: none;padding-left: 0px !important;">
			                            @foreach ($breadcrumbs_custom as $breadcrumb)
			                                <li class="breadcrumb-item">
			                                    @if(isset($breadcrumb['link']))
			                                        <a href="/{{ $breadcrumb['link'] }}"> @endif{{$breadcrumb['name']}}@if(isset($breadcrumb['link'])) </a>
			                                    @endif
			                                </li>
			                            @endforeach
			                        </ol> 
			                    @endisset
			                </div>
			            </div>
			        </div>
				</div>
				@if ($order->status != 'order_cancelled')
					<div class="content-header-right text-md-right col-md-5 col-12 d-md-block d-none">
						<button type="submit" id="save_order" disabled="disabled" class="btn btn-primary mr-1 mb-1 waves-effect waves-light">Update Order</button>
					</div>
				@endif
			</div>
			
			<div class="row order-view-section {{ (($mode == 'edit') ? 'edit-mode' : '') }}">
	            <div class="col-xl-3 col-lg-4 col-12">
	            <div class="row">
	            <div class="col-md-6 col-lg-12">
	            	<div class="card">
					    <div class="card-content">
					        <div class="card-body">
					            <h4>
					            	{{ $order->customer->name }}
					            	<div class="badge badge-secondary pull-right" style="font-size: 17px;">{{ $status[$order->status] }}</div>
					            </h4>
					            <p class="">{{ ucfirst($order->customer->role) }}</p>
					            <hr class="my-2">
					            <dl>
					                <dt>Phone No</dt>
					                <dd><small>+91 {{ $order->customer->contact_no }}</small></dd>
					                <dt>Email</dt>
					                <dd><small>{{ $order->customer->email }}</small></dd>
					                <dt>Car<span class="inline-edit-icon {{ in_array($order->status, array('order_received', 'order_confirmed', 'team_enroute', 'car_picked_up')) ? '' : 'd-none' }}" data-field="order_car"><i class="feather icon-edit"></i></span></dt>
					                <dd>
					                	<div class="inline-edit-section order_car-edit">
					                		<small class="label-control label-order_car">{{ $order->customer_car->car_model->name }} ({{ ucfirst($order->customer_car->fuel_type) }})</small>
						                	<div class="controls edit-control">
		                                        <select name="order-user_car_id" class="form-control select2 order_car-select">
		                                            @foreach ($order->customer->cars as $car)
		                                            	<option value="{{ $car->id }}" {{ (($car->id == $order->user_car_id) ? 'selected="selected"' : '') }}>{{ $car->car_model->name }} ({{ ucfirst($car->fuel_type) }})</option>
		                                            @endforeach
		                                        </select>
		                                    </div>
					                	</div>
					                </dd>
					                <dt>Booking Date</dt>
					                <dd><small>{{ $order->created_at->format('d/m/Y') }}</small></dd>
									<dt>Service Date<span class="inline-edit-icon {{ in_array($order->status, array('order_received', 'order_confirmed')) ? '' : 'd-none' }}" data-field="order_service_date"><i class="feather icon-edit"></i></span></dt>
					                <dd>
					                	<div class="inline-edit-section order_service_date-edit">
					                		<small class="label-control label-order_service_date">{{ $order->service_date->format('d/m/Y') }}</small>
						                	<div class="controls edit-control">
						                		<input type="text" name="order-service_date" value="{{ $order->service_date->format('d/m/Y') }}" class="form-control order_service_date-input" required>
		                                    </div>
					                	</div>
					                </dd>
					                <dt>Pickup Address<span class="inline-edit-icon {{ in_array($order->status, array('order_received', 'order_confirmed')) ? '' : 'd-none' }}" data-field="order_address"><i class="feather icon-edit"></i></span></dt>
					                <dd>
					                	<div class="inline-edit-section order_address-edit">
					                		<small class="label-control label-order_address">{{ $order->customer_address->address_line_1 }} <br> {{ $order->customer_address->address_line_2 }}, <br>{{ $order->customer_address->city->name }}, {{ $order->customer_address->state->name }} - {{ $order->customer_address->zipcode }}</small>
						                	<div class="controls edit-control">
		                                        <select name="order-user_address_id" class="form-control select2 order_address-select">
		                                            @foreach ($order->customer->addresses as $address)
		                                            	<option value="{{ $address->id }}" {{ (($address->id == $order->user_address_id) ? 'selected="selected"' : '') }}>{{ $address->address_line_1 }}, <br> {{ $address->address_line_2 }}, <br>{{ $address->city->name }}, {{ $address->state->name }} - {{ $address->zipcode }}</option>
		                                            @endforeach
		                                        </select>
		                                    </div>
					                	</div>
									</dd>
									@if ( $order->pickup_type == 'pickup' && isset($order->pickup_person_id) && $order->pickup_person_id != '' )
										<dt>Pickup Person</dt>
										<dd><small>{{ $order->pickup_person->name }} (+91 {{ $order->pickup_person->contact_no }})</small></dd>
									@endif
					            </dl>
					        </div>
					    </div>
					</div>
				</div>
				<div class="col-md-6 col-lg-12">
					<div class="card">
					    <div class="card-header d-flex justify-content-between align-items-center border-bottom pb-1">
					        <div>
					            <p class="mb-75"><strong>Documents</strong></p>
					        </div>
					    </div>
					    <div class="card-body">
					    	<ul class="nav nav-tabs nav-justified" role="tablist">
							    <li class="nav-item">
							        <a class="nav-link active" data-toggle="tab" href="#before" role="tab">Before Service</a>
							    </li>
							    <li class="nav-item">
							        <a class="nav-link" data-toggle="tab" href="#after" role="tab">After Service</a>
							    </li>
							</ul>
							<div class="tab-content pt-1">
							    <div class="tab-pane active" id="before">

							        <div class="row">
							        	<div class="col-md-12 col-12 mb-50">
                                            <div class="controls">
                                                <div id="beforeImage" class="dropzone dropzone-area" style="min-height: 220px;"><div class="dz-message">Drop Files Here To Upload</div></div>
                                            </div>
                                        </div>

                                        @if ( isset($order->before_service_documents) && count($order->before_service_documents) > 0 )
                                        	@foreach ($order->before_service_documents as $doc)
									            <div class="col-md-3 col-3 user-latest-img">
									                <img src="{{ $doc->document_url }}" class="img-fluid mb-1 rounded-sm" alt="{{ $doc->document }}">
									            </div>
								            @endforeach
							            @endif
							        </div>
							    </div>
							    <div class="tab-pane" id="after">
							        <div class="row">
							        	<div class="col-md-12 col-12 mb-50">
                                            <div class="controls">
                                                <div id="afterImage" class="dropzone dropzone-area" style="min-height: 220px;"><div class="dz-message">Drop Files Here To Upload</div></div>
                                            </div>
                                        </div>

							            @if ( isset($order->after_service_documents) && count($order->after_service_documents) > 0 )
                                        	@foreach ($order->after_service_documents as $doc)
									            <div class="col-md-3 col-3 user-latest-img">
									                <img src="{{ $doc->document_url }}" class="img-fluid mb-1 rounded-sm" alt="{{ $doc->document }}">
									            </div>
								            @endforeach
							            @endif
							        </div>
							    </div>
							</div>

					    </div>
					</div>
				</div>	
	            </div>
	        </div>
	            <div class="col-xl-6 col-lg-8 col-12">

	            	<section class="card invoice-page" style="padding: 0;">
					    <div id="invoice-template" class="card-body">
					        <!-- Invoice Company Details -->
					        <div id="invoice-company-details" class="row">
					            <div class="col-sm-6 col-12 text-left pt-1">
					                <div class="media pt-1">
					                    <img src="{{ asset('images/logo/carsseva-logo.png') }}" width="100" alt="company logo">
					                </div>
					            </div>
					            <div class="col-sm-6 col-12 text-right">
					                <h1>@if ($order->status == 'service_completed' || $order->status == 'delivered') Invoice @else Order @endif</h1>
					                <div class="invoice-details mt-2">
					                    <h6>@if ($order->status == 'service_completed' || $order->status == 'delivered') INVOICE @else ORDER @endif NO.</h6>
					                    <p>#{{ $order->sequence_id }}</p>
					                    <h6 class="mt-2">SERVICE DATE</h6>
					                    <p>{{ $order->service_date->format('d/m/Y') }}</p>
					                </div>
					            </div>
					        </div>
					        <!--/ Invoice Company Details -->
					        <!-- Invoice Recipient Details -->
					        <div id="invoice-customer-details" class="row pt-2">
					            <div class="col-sm-6 col-12 text-left">
									@if ( $currentUser['role'] == 'admin' || ($currentUser['role'] == 'vendor_admin' && $order->status != 'order_received' ) )
										<h5>Customer</h5>
										<div class="recipient-info my-2">
											<p>{{ $order->customer->name }}</p>
											<p>{{ $order->customer->primary_address->address_line_1 }}</p>
											<p>{{ $order->customer->primary_address->address_line_2 }}</p>
											<p>{{ $order->customer->primary_address->city->name }}, {{ $order->customer->primary_address->state->name }}</p>
											<p>{{ $order->customer->primary_address->zipcode }}</p>
										</div>
										<div class="recipient-contact pb-2">
											<p>
												<i class="feather icon-mail"></i>
												{{ $order->customer->email }}
											</p>
											<p>
												<i class="feather icon-phone"></i>
												+91 {{ $order->customer->contact_no }}
											</p>
										</div>
									@endif
					            </div>
					            <div class="col-sm-6 col-12 text-right">
					                <h5>Vendor</h5>
					                <div class="company-info my-2">
					                	<p>{{ $order->vendor->name }}</p>
					                    <p>{{ $order->vendor->address_line_1 }}</p>
					                    <p>{{ $order->vendor->address_line_2 }}</p>
					                    <p>{{ $order->vendor->city->name }}, {{ $order->vendor->state->name }}</p>
					                    <p>{{ $order->vendor->zipcode }}</p>
					                </div>
					                <div class="company-contact">
					                    <p>
					                        <i class="feather icon-mail"></i>
					                        {{ $order->vendor->email }}
					                    </p>
					                    @if ( isset($order->vendor->contact_no) && $order->vendor->contact_no != '' )
					                    <p>
					                        <i class="feather icon-phone"></i>
					                        +91 {{ $order->vendor->contact_no }}
					                    </p>
					                    @endif
					                </div>
					            </div>
					        </div>
					        <!--/ Invoice Recipient Details -->
					        <!-- Invoice Items Details -->
					        <div id="invoice-items-details" class="pt-1 invoice-items-table order-items-table">
					            <div class="row">
					                <div class="table-responsive col-12">
					                    <table class="table table-borderless">
					                        <thead>
					                            <tr>
					                                <th>TASK DESCRIPTION</th>
					                                <th class="text-right">COST</th>
					                                <th class="text-right">TAX</th>
					                                <th class="text-right">AMOUNT</th>
					                                <th class="{{ in_array($order->status, array('service_completed', 'delivered', 'order_cancelled')) ? 'd-none' : '' }}" width="20px">&nbsp;</th>
					                            </tr>
					                        </thead>
					                        <tbody>
					                        	@if ( isset($order->items) && count($order->items) > 0 )
	                            					@foreach($order->items as $ini => $ser)
	                            						<tr class="service-tr">
							                                <td colspan="5">
							                                	<p class="text-bold-600 mb-0 mt-25">{{ $ser['name'] }}</p>
							                                </td>
							                            </tr>
							                            @foreach($ser['items'] as $insi => $item)
								                            <tr class="subservice-tr order_item_{{ $item->id }}">
								                                <td>
																	@if ( $item->is_custom == 1 )
																		<small>- {{ $item->service_name }} ({{ (($item->cost_type == 'service_cost') ? "Service Cost" : "Part Cost") }})</small><br>
																	@else
																		<small>- {{ $item->service_name }}</small><span class="font-small-1 pl-50"> x 1</span><br>
																	@endif
								                                </td>
								                                <td class="text-right">
								                                	@if ( $item->is_custom == 1 )
									                                	<div class="inline-edit-section order_item_{{ $item->id }}_cost-edit">
													                		<div class="label-control label-order_item_{{ $item->id }}_cost">₹ {{ $item->cost }}</div>
														                	<div class="controls edit-control">
														                		<div class="input-group" style="width: 100px;float: right;">
				                                                                    <div class="input-group-prepend">
				                                                                        <span class="input-group-text">₹</span>
				                                                                    </div>
				                                                                    <input type="text" name="items[{{ $item->id }}][cost]" class="form-control order_item_{{ $item->id }}_cost-input" value="{{ $item->cost }}" required>
				                                                                </div>
										                                    </div>
													                	</div>
													                @else
													                	₹ {{ $item->cost }}
													                	<input type="text" name="items[{{ $item->id }}][cost]" class="form-control d-none" value="{{ $item->cost }}" required>
												                	@endif
								                                </td>
								                                <td class="text-right">
								                                	@if ( $item->is_custom == 1 )
									                                	<div class="inline-edit-section order_item_{{ $item->id }}_tax-edit">
													                		<div class="label-control label-order_item_{{ $item->id }}_tax">{{ $item->tax }}%</div>
														                	<div class="controls edit-control">
														                		<div class="input-group" style="width: 70px;float: right;">
				                                                                    <input type="text" name="items[{{ $item->id }}][tax]" class="form-control order_item_{{ $item->id }}_tax-input" value="{{ $item->tax }}" required>
				                                                                    <div class="input-group-append">
				                                                                        <span class="input-group-text">%</span>
				                                                                    </div>
																				</div>
																				<input type="hidden" name="items[{{ $item->id }}][cost_type]" class="form-control d-none" value="{{ $item->cost_type }}" required>
										                                    </div>
													                	</div>
												                	@else
													                	{{ $item->tax }}%
																		<input type="text" name="items[{{ $item->id }}][tax]" class="form-control d-none" value="{{ $item->tax }}" required>
																		<input type="hidden" name="items[{{ $item->id }}][cost_type]" class="form-control d-none" value="{{ $item->cost_type }}" required>
												                	@endif
								                                </td>
								                                <td class="text-right order_item_{{ $item->id }}_total">₹ {{ $item->total }}</td>
								                                <td class="{{ in_array($order->status, array('service_completed', 'delivered', 'order_cancelled')) ? 'd-none' : '' }}" width="75px">
								                                	<div>
								                                		@if ( $item->is_custom )
								                                			<span class="inline-tr-edit-icon cursor-pointer" data-field="order_item_{{ $item->id }}"><i class="feather icon-{{ (($mode == 'edit') ? 'save' : 'edit') }}"></i></span>
								                                		@endif
								                                		<span class="inline-tr-delete-icon cursor-pointer" data-field="order_item_{{ $item->id }}"><i class="feather icon-trash-2"></i></span>
								                                	</div>
								                                </td>
								                            </tr>
							                            @endforeach

					                            	@endforeach
	                        					@endif
	                        					<tr class="add-new-tr {{ in_array($order->status, array('service_completed', 'delivered', 'order_cancelled')) ? 'd-none' : '' }}">
					                                <td colspan="5">
					                                	<p class="text-bold-600 font-medium-2 mb-0 mt-25 cursor-pointer add-new-service" data-addItems="1"><i class="users-delete-icon feather icon-plus"></i>Add New Service</p>
					                                </td>
					                            </tr>
					                        </tbody>
					                    </table>
					                </div>
					            </div>
					        </div>
					        <div id="invoice-total-details" class="invoice-total-table">
					            <div class="row">
					                <div class="col-12 col-sm-9 offset-sm-3">
					                    <div class="table-responsive table-load">
					                    	<div class="text-center loader">
								              	<div class="spinner-border" role="status">
								                	<span class="sr-only">Loading...</span>
								              	</div>
								            </div>
					                        <table class="table table-borderless">
					                            <tbody>
					                                <tr>
					                                    <th>SUB TOTAL</th>
					                                    <td class="text-right" id="sub_total"></td>
														<td class="{{ in_array($order->status, array('service_completed', 'delivered', 'order_cancelled')) ? 'd-none' : '' }}" width="75px">&nbsp;</td>
														<input type="hidden" name="main_sub_total" id="main_sub_total" class="form-control" value="">
					                                </tr>
													<tr>
					                                    <th>DISCOUNT
					                                    	<div class="inline-edit-section order_discount-edit">
															    <div class="label-control label-order_discount">
																	@if ( isset($order->promo_code['sign_off_type']) )
																		<p class="text-success">Promo Code {{ $order->promo_code['code'] }} applied!</p>
																	@endif
																</div>
															    <div class="controls edit-control">
															        <div class="input-group">
																	<input type="text" id="order-discount" name="order-discount" class="form-control order_discount-input" value="{{ ((isset($order->promo_code['sign_off_type'])) ? $order->promo_code['code'] : ((isset($order->discount) && $order->discount != '') ? $order->discount : 0)) }}" data-promodetail="{{ json_encode($order->promo_code) }}" required>
															            <div class="input-group-append">
															                <span class="input-group-text">
															                	<select name="discount_type" id="discount_type">
															                		<option value="%">%</option>
																					<option value="₹" {{ (isset($order->discount) && $order->discount != '') ? 'selected="selected"' : '' }}>₹</option>
																					<option value="promo" {{ isset($order->promo_code['sign_off_type']) ? 'selected="selected"' : '' }}>Promo Code</option>
															                	</select>
															                </span>
															            </div>
															        </div>
															    </div>
															</div>
					                                    </th>
					                                    <td class="text-right" id="discount"></td>
					                                    <td class="{{ in_array($order->status, array('service_completed', 'delivered', 'order_cancelled')) ? 'd-none' : '' }}">
					                                    	<div>
						                                		<span class="inline-edit-icon cursor-pointer" data-field="order_discount"><i class="feather icon-edit"></i></span>
						                                	</div>
														</td>
														<input type="hidden" name="main_discount" id="main_discount" class="form-control" value="">
													</tr>
													<tr>
					                                    <th>AFTER DISCOUNT</th>
					                                    <td class="text-right" id="after_discount"></td>
					                                    <td class="{{ in_array($order->status, array('service_completed', 'delivered', 'order_cancelled')) ? 'd-none' : '' }}">&nbsp;</td>
					                                </tr>
													<tr>
					                                    <th>TAX <i class="feather icon-info mr-25 tax-popover" data-toggle="popover" data-placement="top" data-trigger="hover"></i></th>
					                                    <td class="text-right" id="total_tax"></td>
														<td class="{{ in_array($order->status, array('service_completed', 'delivered', 'order_cancelled')) ? 'd-none' : '' }}">&nbsp;</td>
														<input type="hidden" name="main_total_tax" id="main_total_tax" class="form-control" value="">
													</tr>
													{{-- <tr>
					                                    <th>TOTAL AMOUNT</th>
					                                    <td class="text-right" id="total_amount"></td>
					                                    <td>&nbsp;</td>
													</tr> --}}
													<input type="hidden" name="main_total_amount" id="main_total_amount" class="form-control" value="">
					                                <tr>
					                                    <th>ROUND OFF</th>
					                                    <td class="text-right" id="round_off"></td>
					                                    <td class="{{ in_array($order->status, array('service_completed', 'delivered', 'order_cancelled')) ? 'd-none' : '' }}">&nbsp;</td>
					                                </tr>
					                                <tr>
					                                    <th>NET PAY</th>
					                                    <td class="text-right" id="net_pay"></td>
					                                    <td class="{{ in_array($order->status, array('service_completed', 'delivered', 'order_cancelled')) ? 'd-none' : '' }}">&nbsp;</td>
					                                </tr>
					                            </tbody>
					                        </table>
					                    </div>
					                </div>
					            </div>
					        </div>
					        <!-- Invoice Footer -->
					        <div id="invoice-footer" class="text-right pt-3">
					            <p>Thank you for your business!</p> 
					            <p class="bank-details mb-0">
					                <span><strong>Terms & Conditions:</strong> Please pay the Invoice before the due date.</span>
					            </p>
					        </div>
					        <!--/ Invoice Footer -->
					    </div>
					</section>

	            </div>
	            <div class="col-xl-3 col-lg-12 col-sm-12">
					@if ($order->status != 'order_cancelled')
						<div class="card">
							<div class="card-header">
								<h4 class="card-title">Order Status</h4>
							</div>
							<div class="card-content">
								<div class="card-body">
									@if($order->is_awaiting_for_approval == '1')
										<div class="alert alert-warning" role="alert">
											<h4 class="alert-heading">Awaiting for Customer Approval</h4>
											<p class="mb-0">Your last changes are under approval so for the time being you can't change order status.</p>
											<p class="mb-0"><a href="{{ URL::to('orders/' . $order->id . '/discard') }}" class="warning" style="border-bottom: 1px dotted;">Click here</a>, If you want to <strong>discard</strong> changes.</p>
										</div>
									@endif

									<div class="form-group">
										<div class="w-100">
											<select class="form-control select2" id="order_status">
												@foreach ($allow_status[$order->status] as $status)
													<option value="{{ $status['id'] }}" {{ (($status['id'] == $order->status) ? 'selected="selected"' : '') }}>{{ $status['name'] }}</option>
												@endforeach
											</select>
										</div>
									</div>

									<div class="form-group pickup_per-section" style="display: none;">
										<label for="first-name-vertical">Pickup Person *</label>
										<div class="w-100">
											<select class="form-control select2" id="pickup_per">
												<option value="">Select Pickup Person</option>
												@foreach ($pickup_persons as $person)
													<option value="{{ $person['id'] }}">{{ $person['name'] }}</option>
												@endforeach
											</select>
											<label id="pickup_per-error" class="error" for="pickup_per" style="display: none;">This field is required.</label>
										</div>
									</div>

									<div class="form-group reason-section" style="display: none;">
										<label for="first-name-vertical">Reason for Cancellation *</label>
										<div class="w-100">
											<fieldset>
												<div class="vs-radio-con">
													<input type="radio" name="reason_for_cancellation" checked="" value="Reason 1">
													<span class="vs-radio">
													<span class="vs-radio--border"></span>
													<span class="vs-radio--circle"></span>
													</span>
													<span class="">Reason 1</span>
												</div>
											</fieldset>
											<fieldset>
												<div class="vs-radio-con">
													<input type="radio" name="reason_for_cancellation" value="Reason 2">
													<span class="vs-radio">
													<span class="vs-radio--border"></span>
													<span class="vs-radio--circle"></span>
													</span>
													<span class="">Reason 2</span>
												</div>
											</fieldset>
											<fieldset>
												<div class="vs-radio-con">
													<input type="radio" name="reason_for_cancellation" value="Reason 3">
													<span class="vs-radio">
													<span class="vs-radio--border"></span>
													<span class="vs-radio--circle"></span>
													</span>
													<span class="">Reason 3</span>
												</div>
											</fieldset>
										</div>
									</div>

									@if($order->is_awaiting_for_approval != '1')
										<div class="text-nowrap pull-left mb-2">
											<button type="button" class="btn btn-primary waves-effect waves-light pull-right change-status" data-status="{{ $order->status }}" data-order_id="{{ $order->id }}">Update Status</button>
										</div>
									@endif
								</div>
							</div>
						</div>
					@endif
	            	<div class="card">
				        <div class="card-header">
				            <h4 class="card-title">Activity Log</h4>
				        </div>
				        <div class="card-content">
				            <div class="card-body">
								@if ( $logs )
									<ul class="activity-timeline timeline-left list-unstyled">
										@foreach ($logs as $log)
											<li>
												<div class="timeline-icon bg-warning">
													<i class="feather icon-plus font-medium-2"></i>
												</div>
												<div class="timeline-info">
													<p class="font-weight-bold">{{ $log['title'] }}</p>
													<span>{{ $log['description'] }}</span>
												</div>
												<small class="">{{ $log['created_at']->diffForHumans() }} by {{ $log['action_by']['name'] }}</small>
											</li>
										@endforeach
										{{-- <li>
											<div class="timeline-icon bg-primary">
												<i class="feather icon-plus font-medium-2"></i>
											</div>
											<div class="timeline-info">
												<p class="font-weight-bold">Task Added</p>
												<span>Bonbon macaroon jelly beans gummi bears jelly lollipop apple</span>
											</div>
											<small class="">25 days ago</small>
										</li>
										<li>
											<div class="timeline-icon bg-warning">
												<i class="feather icon-alert-circle font-medium-2"></i>
											</div>
											<div class="timeline-info">
												<p class="font-weight-bold">Task Updated</p>
												<span>Cupcake gummi bears soufflé caramels candy</span>
											</div>
											<small class="">15 days ago</small>
										</li>
										<li>
											<div class="timeline-icon bg-success">
												<i class="feather icon-check font-medium-2"></i>
											</div>
											<div class="timeline-info">
												<p class="font-weight-bold">Task Completed</p>
												<span>Candy ice cream cake. Halvah gummi bears
												</span>
											</div>
											<small class="">20 minutes ago</small>
										</li> --}}
									</ul>
								@endif
				            </div>
				        </div>
				    </div>
				</div>
        	</div>
		</form>
		
		@section('carsseva-alert')

            <!-- Alerts -->
            @foreach (['danger', 'warning', 'success', 'info'] as $key)
                @if(Session::has($key))
                    <div class="cws-alert">
                        <div class="alert alert-{{ $key }} alert-dismissible alert-auto-hide fade show" role="alert">
                            <div class="alert-text">{{ Session::get($key) }}</div>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
                            </button>
                        </div>
                    </div>
                @endif
            @endforeach

            <div class="cws-alert form-change" style="display: none;">
                <div class="alert alert-warning fade show" role="alert" style="padding: 10px 27px;background: #ff9f43 !important;">
                    <div class="alert-text">Order information have changed, you should save them!</div>
                </div>
            </div>

            <div class="cws-alert order-status-alert" style="display: none;">
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <div class="alert-text">Please change order status first.</div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
                    </button>
                </div>
            </div>

        @endsection

@endsection

@section('vendor-script')
        {{-- vendor files --}} 
        <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/forms/validation/jquery.validate.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/extensions/dropzone.js')) }}"></script>
@endsection

@section('page-script')
        {{-- Page js files --}}
        <script type="text/javascript">

        	Dropzone.autoDiscover = false;
        	var originalFormData = $('#orderFrm').serialize();

            $(document).ready(function() {

				/* $(".tax-popover").popover({
					content: function() {
						var content = '<div class="table-responsive">';
							content += '<table class="table table-bordered mb-0">';
								content += '<tbody>';
									content += '<tr><th scope="row">Service Tax</th><td>1200</td></tr>';
									content += '<tr><th scope="row">Parts Tax</th><td>200</td></tr>';
								content += '</tbody>';
							content += '</table>';
						content += '</div>';
						return $(content);
					},
					title: function() {
						var title = 'Tax Calculation ';
						return title;
					},
					html: true,
					placement: 'bottom',
				}); */

            	if ( $('.order-view-section').hasClass('edit-mode') ) {
            		$('.inline-edit-section').addClass('active');
            	}

            	// Form
                var validator = $( "#orderFrm" ).validate({
                    ignore: ""
                });
            	
            	$('.select2').select2({
                    dropdownAutoWidth: true,
                    width: '100%'
                });

                // File Upload
                var myDropzone = new Dropzone('#beforeImage', {
                    url: "{{ URL::to('upload') }}",
                    addRemoveLinks: true,
                    acceptedFiles: 'image/*',
                    maxFiles: 10,
                    init: function () {
                        this.on("maxfilesexceeded", function(file) {
                            this.removeAllFiles();
                            this.addFile(file);
                        });

                        this.on("removedfile", function (file) {
                            $('input[value="'+file.dataURL+'"]').remove();
                        });
                    },
                    success: function (file, response) {
                        file.previewElement.classList.add("dz-success");
                        $('#orderFrm').append('<input type="hidden" name="before_images[]" id="before_images" value="' + file.dataURL + '">');
                        checkFormChanged();
                    },
                    reset: function () {
                        $('#image').val('');
                        $('#before_images').val('');
                        checkFormChanged();
                    },
                    error: function (file, response) {
                        file.previewElement.classList.add("dz-error");
                        $('#image').val('');
                        $('#before_images').val('');
                        checkFormChanged();
                    }
                });

                // File Upload
                var myDropzone = new Dropzone('#afterImage', {
                    url: "{{ URL::to('upload') }}",
                    addRemoveLinks: true,
                    acceptedFiles: 'image/*',
                    maxFiles: 10,
                    init: function () {
                        this.on("maxfilesexceeded", function(file) {
                            this.removeAllFiles();
                            this.addFile(file);
                        });

                        this.on("removedfile", function (file) {
                            $('input[value="'+file.dataURL+'"]').remove();
                        });
                    },
                    success: function (file, response) {
                        file.previewElement.classList.add("dz-success");
                        $('#orderFrm').append('<input type="hidden" name="after_images[]" id="after_images" value="' + file.dataURL + '">');
                        checkFormChanged();
                    },
                    reset: function () {
                        $('#image').val('');
                        $('#after_images').val('');
                        checkFormChanged();
                    },
                    error: function (file, response) {
                        file.previewElement.classList.add("dz-error");
                        $('#image').val('');
                        $('#after_images').val('');
                        checkFormChanged();
                    }
                });

                total_amounts();
            });

            function convertToInt(val){
		      	//console.log(val);
		      	if ( val != undefined ) {
		        	return Number(val);
		      	} else {
		        	return 0;
		      	}
		    }

		    function convertToParseInt(val){
		      	if ( val != undefined ) {
		        	if ( isNaN(val) ) {
		          		return 0;
		        	} else {
		         	 	return val.toFixed(2);
		        	}
		      	} else {
		        	return 0;
		      	}
		    }

            function arrayColumn(array, columnName) {
			    return array.map(function(value,index) {
			        return value[columnName];
			    })
			}

            function total_amounts() {
            	$('.table-load').addClass('loading');

            	var valueArr = $('#orderFrm').serializeArray();
            	var names = arrayColumn(valueArr, 'name');
            	//console.log(names, valueArr);
            	            		
            	var amounts = {
            		'sub_total': 0,
					'service_cost': 0,
            		'parts_cost': 0,
            		'total_tax': 0,
            		'service_tax': 0,
            		'parts_tax': 0,
            	};
            	var i;
            	$.each( valueArr, function( key, field ) {

            		// Edit Mode
				  	var index = field['name'].indexOf("][cost]");
            		if (index != -1) {
					  	
					  	amounts['sub_total'] = convertToInt(amounts['sub_total']) + convertToInt(field['value']);
					  	
					  	var pre = field['name'].replace("[cost]", "[tax]");
					  	var indexT = names.indexOf(pre);
	            		if (indexT != -1) {
						  	
						  	amounts['total_tax'] = convertToInt(amounts['total_tax']) + convertToInt(field['value'] * (valueArr[indexT]['value']/100));

							// check cost_type
							var preCT = field['name'].replace("[cost]", "[cost_type]");
							var indexCT = names.indexOf(preCT);
							if (indexCT != -1) {
								if ( valueArr[indexCT]['value'] == 'service_cost' ) {
									amounts['service_cost'] = convertToInt(amounts['service_cost']) + convertToInt(field['value']);
									amounts['service_tax'] = convertToInt(amounts['service_tax']) + convertToInt(field['value'] * (valueArr[indexT]['value']/100));
								} else {
									amounts['parts_cost'] = convertToInt(amounts['parts_cost']) + convertToInt(field['value']);
									amounts['parts_tax'] = convertToInt(amounts['parts_tax']) + convertToInt(field['value'] * (valueArr[indexT]['value']/100));
								}
							}
						}
					}

					// New Services - Parts and Labour Cost
					var index_new = field['name'].indexOf("][parts_cost]");
            		if (index_new != -1) {
					  	
					  	amounts['sub_total'] = convertToInt(amounts['sub_total']) + convertToInt(field['value']);
						amounts['parts_cost'] = convertToInt(amounts['parts_cost']) + convertToInt(field['value']);

					  	var pre_new = field['name'].replace("[parts_cost]", "[parts_tax]");
					  	var indexT_new = names.indexOf(pre_new);
	            		if (indexT_new != -1) {
						  	
						  	amounts['total_tax'] = convertToInt(amounts['total_tax']) + convertToInt(field['value'] * (valueArr[indexT_new]['value']/100));
							amounts['parts_tax'] = convertToInt(amounts['parts_tax']) + convertToInt(field['value'] * (valueArr[indexT_new]['value']/100));
						}
					}

					var index_new_lab = field['name'].indexOf("][labour_cost]");
            		if (index_new_lab != -1) {
					  	
					  	amounts['sub_total'] = convertToInt(amounts['sub_total']) + convertToInt(field['value']);
						amounts['service_cost'] = convertToInt(amounts['service_cost']) + convertToInt(field['value']);

					  	var pre_new_lab = field['name'].replace("[labour_cost]", "[labour_tax]");
					  	var indexT_new_lab = names.indexOf(pre_new_lab);
	            		if (indexT_new_lab != -1) {
						  	
						  	amounts['total_tax'] = convertToInt(amounts['total_tax']) + convertToInt(field['value'] * (valueArr[indexT_new_lab]['value']/100));
						}
					}

					var index_new_in_lab = field['name'].indexOf("][inbuilt_labour_cost]");
            		if (index_new_in_lab != -1) {
					  	
					  	amounts['sub_total'] = convertToInt(amounts['sub_total']) + convertToInt(field['value']);
						amounts['service_cost'] = convertToInt(amounts['service_cost']) + convertToInt(field['value']);
						
						var pre_new_in_lab = field['name'].replace("[inbuilt_labour_cost]", "[inbuilt_labour_tax]");
					  	var indexT_new_in_lab = names.indexOf(pre_new_in_lab);
	            		if (indexT_new_in_lab != -1) {
						  	
						  	amounts['total_tax'] = convertToInt(amounts['total_tax']) + convertToInt(field['value'] * (valueArr[indexT_new_in_lab]['value']/100));
						}
					}
				});

            	for (i = 1; i < valueArr.length; i++) {
            		
					/*// New Services - Parts and Labour Cost
					var index_new = names.indexOf("items[new]["+i+"][parts_cost]");
            		if (index_new != -1) {
					  	
					  	amounts['sub_total'] = convertToInt(amounts['sub_total']) + convertToInt(valueArr[index_new]['value']);

					  	var indexT_new = names.indexOf("items[new]["+i+"][parts_tax]");
	            		if (indexT_new != -1) {
						  	
						  	amounts['total_tax'] = convertToInt(amounts['total_tax']) + convertToInt(valueArr[index_new]['value'] * (valueArr[indexT_new]['value']/100));
						}
					}

					var index_new_lab = names.indexOf("items[new]["+i+"][labour_cost]");
            		if (index_new_lab != -1) {
					  	
					  	amounts['sub_total'] = convertToInt(amounts['sub_total']) + convertToInt(valueArr[index_new_lab]['value']);

					  	var indexT_new_lab = names.indexOf("items[new]["+i+"][labour_tax]");
	            		if (indexT_new_lab != -1) {
						  	
						  	amounts['total_tax'] = convertToInt(amounts['total_tax']) + convertToInt(valueArr[index_new_lab]['value'] * (valueArr[indexT_new_lab]['value']/100));
						}
					}

					var index_new_in_lab = names.indexOf("items[new]["+i+"][inbuilt_labour_cost]");
            		if (index_new_in_lab != -1) {
					  	console.log(valueArr[index_new_in_lab]);
					  	amounts['sub_total'] = convertToInt(amounts['sub_total']) + convertToInt(valueArr[index_new_in_lab]['value']);
						
					  	var indexT_new_in_lab = names.indexOf("items[new]["+i+"][inbuilt_labour_tax]");
	            		if (indexT_new_in_lab != -1) {
						  	
						  	amounts['total_tax'] = convertToInt(amounts['total_tax']) + convertToInt(valueArr[index_new_in_lab]['value'] * (valueArr[indexT_new_in_lab]['value']/100));
						}
					}*/
				}

				var DTIndex = names.indexOf("discount_type");
        		if (DTIndex != -1) {
        			var discount_type = valueArr[DTIndex]['value'];

        			var ODIndex = names.indexOf("order-discount");
	        		if (ODIndex != -1) {
	        			var discount = valueArr[ODIndex]['value'];

						if ( discount_type == '%' ) {
        					amounts['discount_amount'] = convertToParseInt(convertToInt(amounts['service_cost']) * (discount/100));
        				} else if ( discount_type == '₹' ) {
        					amounts['discount_amount'] = convertToParseInt(convertToInt(discount));
        				} else {
							
							var promo = JSON.parse($('.order_discount-input').attr('data-promodetail'));
							console.log(promo);
							if ( promo.sign_off_type == 'percentage' ) {
								amounts['discount_amount'] = convertToParseInt(convertToInt(amounts['service_cost']) * (promo.amount/100));
							} else {
								amounts['discount_amount'] = convertToParseInt(convertToInt(promo.amount));
							}
						}
	        		}

	        		//amounts['total_amount'] = convertToInt(amounts['after_discount']) - convertToInt(amounts['discount_amount']);
        		}

				amounts['after_discount'] = convertToInt(amounts['sub_total']) - convertToInt(amounts['discount_amount']);

				amounts['service_tax'] = convertToParseInt((convertToInt(amounts['service_cost']) - convertToInt(amounts['discount_amount'])) * 0.18);
				amounts['total_tax'] = convertToInt(amounts['service_tax']) + convertToInt(amounts['parts_tax']);
				amounts['total_amount'] = convertToInt(amounts['after_discount']) + convertToInt(amounts['total_tax']);
				
				//console.log(amounts);
				$('#sub_total').html('₹ ' + convertToParseInt(convertToInt(amounts['sub_total'])));
				$('#main_sub_total').val(convertToParseInt(convertToInt(amounts['sub_total'])));

				$('#total_tax').html('₹ ' + convertToParseInt(convertToInt(amounts['total_tax'])));
				$('#main_total_tax').val(convertToParseInt(convertToInt(amounts['total_tax'])));

				$('#after_discount').html('₹ ' + convertToParseInt(convertToInt(amounts['after_discount'])));
				
				$('#discount').html('₹ ' + convertToParseInt(convertToInt(amounts['discount_amount'])));
				$('#main_discount').val(convertToParseInt(convertToInt(amounts['discount_amount'])));

				//$('#total_amount').html('₹ ' + convertToParseInt(Math.round(convertToInt(amounts['total_amount']))));
				$('#main_total_amount').val(convertToParseInt(Math.round(convertToInt(amounts['total_amount']))));

				$('#round_off').html('₹ ' + convertToParseInt(Math.round(convertToInt(amounts['total_amount'])) - convertToInt(amounts['total_amount'])));
				$('#net_pay').html('₹ ' + convertToParseInt(Math.round(convertToInt(amounts['total_amount']))));

				$('.tax-popover').popover('dispose');
				$(".tax-popover").popover({
					content: function() {
						var content = '<div class="table-responsive">';
							content += '<table class="table table-bordered mb-0">';
								content += '<tbody>';
									content += '<tr><th scope="row">Service Tax</th><td>₹ '+amounts['service_tax']+'<input type="hidden" name="service_tax" class="form-control d-none" value="'+amounts['service_tax']+'"></td></tr>';
									content += '<tr><th scope="row">Parts Tax</th><td>₹ '+amounts['parts_tax']+'<input type="hidden" name="parts_tax" class="form-control d-none" value="'+amounts['parts_tax']+'"></td></tr>';
								content += '</tbody>';
							content += '</table>';
						content += '</div>';
						return $(content);
					},
					title: function() {
						var title = 'Tax Calculation ';
						return title;
					},
					html: true,
					placement: 'bottom',
				});

				$('.table-load').removeClass('loading');
            }

            function checkFormChanged() {
			    if (window.originalFormData !== $('#orderFrm').serialize()) {
			        $('.form-change').show(500);
			        $("#save_order").attr("disabled", false);
			    } else {
			    	$('.form-change').hide(500);
			    	$("#save_order").attr("disabled", true);
			    }
			    total_amounts();
			}

            $(document).on('click', '.inline-edit-icon', function() {
                var field = $(this).data('field');
                if ( $('.'+field+'-edit').hasClass('active') ) {
                	$('.'+field+'-edit').removeClass('active');

                	if ( field == 'order_discount' ) {

						if ( $('#discount_type').val() == 'promo' ) {
							
							$('.table-load').addClass('loading');
							$.post('/orders/apply-promo-code', {'promo_code': $('.'+field+'-input').val(), 'vendor_id': '{!! $order->vendor_id !!}'}, function(response){
								if ( response.response_code == 200 ) {
									
									$('#discount_type').val('promo');
									$('.'+field+'-input').val(response.response_data.amount);
									$('.'+field+'-input').attr('data-promodetail', JSON.stringify(response.response_data));
									$('.label-'+field).html('<p class="text-success">Promo Code '+response.response_data.code+' applied!</p>');
								} else {
									$('.label-'+field).html('<p class="text-danger">Invalide Promo Code</p>');
								}
								$('.table-load').removeClass('loading');
								$('.'+field+'-edit').removeClass('active');
								$(this).find('i').addClass('icon-edit').removeClass('icon-save');
								checkFormChanged();
							});
							
						} else {	
							$('.label-'+field).html($('.'+field+'-input').val() + ' ' + $('#discount_type').val());
							$('.'+field+'-edit').removeClass('active');
							$(this).find('i').addClass('icon-edit').removeClass('icon-save');
							checkFormChanged();
						}
                	}

                	//$(this).find('i').addClass('icon-edit').removeClass('icon-save');
                } else {
                	$('.'+field+'-edit').addClass('active');
                	//$(this).find('i').removeClass('icon-edit').addClass('icon-save');
                }
            });

            $(document.body).on("change", ".order_car-select", function(e){
            	var text = $(".order_car-select option:selected").text();
            	$('.label-order_car').html(text);
			 	$('.order_car-edit').removeClass('active');
			 	checkFormChanged();
			});

			$(document.body).on("change", ".order_address-select", function(e){
            	var text = $(".order_address-select option:selected").text();
            	$('.label-order_address').html(text);
			 	$('.order_address-edit').removeClass('active');
			 	checkFormChanged();
			});

			$(document.body).on("keypress", ".order_created_at-input", function(e){
				if (e.keyCode == 13) {
		            $('.label-order_created_at').html(this.value);
			 		$('.order_created_at-edit').removeClass('active');
		            checkFormChanged();
		            return false;
		        }
			});

			$(document.body).on("keypress", ".order_service_date-input", function(e){
				if (e.keyCode == 13) {
		            $('.label-order_service_date').html(this.value);
			 		$('.order_service_date-edit').removeClass('active');
		            checkFormChanged();
		            return false;
		        }
			});

			$(document).on('click', '.inline-tr-edit-icon', function() {
                var field = $(this).data('field');

                // Edit Cost Input
                if ( $('.'+field+'_cost-edit').hasClass('active') ) {
                	
                	// save
                	$('.label-'+field+'_cost').html('₹ ' + $('.'+field+'_cost-input').val());
                	
                	$('.'+field+'_cost-edit').removeClass('active');
                	$(this).find('i').addClass('icon-edit').removeClass('icon-save');
                	checkFormChanged();
                } else {
                	$('.'+field+'_cost-edit').addClass('active');
                	$(this).find('i').removeClass('icon-edit').addClass('icon-save');
                }

                // Edit Tax Input
                if ( $('.'+field+'_tax-edit').hasClass('active') ) {
                	
                	// save
                	$('.label-'+field+'_tax').html($('.'+field+'_tax-input').val() + '%');
                	
                	$('.'+field+'_tax-edit').removeClass('active');
                	$(this).find('i').addClass('icon-edit').removeClass('icon-save');
                	checkFormChanged();
                } else {
                	$('.'+field+'_tax-edit').addClass('active');
                	$(this).find('i').removeClass('icon-edit').addClass('icon-save');
                }

                var total = convertToInt($('.'+field+'_cost-input').val()) + convertToInt(($('.'+field+'_cost-input').val() * ($('.'+field+'_tax-input').val() / 100)));
                $('.'+field+'_total').html('₹ ' + total);

            });

            $(document).on('click', '.inline-tr-delete-icon', function() {
                var field = $(this).data('field');
                $('.'+field).remove();
                checkFormChanged();
            });

            $(document).on('click', '.add-new-service', function() {
                var current = $(this).attr('data-addItems');
                var next = convertToInt(current) + convertToInt(1);

                var html = '';
                	html += '<tr class="add-services add-service-'+current+'" data-item="'+current+'">';
                        html += '<td class="align-top">';
                        	html += '<div class="mr-2">';
                        		html += '<label for="first-name-vertical">Select Service</label>';
                            	html += '<div class="controls">';
                            		html += '<select name="items[new]['+current+'][service_id]" id="service_id_'+current+'" data-item="'+current+'" class="form-control select2 service_id">';
                            		html += '</select>';
                                html += '</div>';
                        	html += '</div>';
                        	/*html += '<div class="type-section'+current+'" style="display: inline-block;">';
                        		html += '<label for="first-name-vertical">Type</label>';
                            	html += '<div class="controls">';
                            		html += '<select name="items[new]['+current+'][type]" id="type_'+current+'" class="form-control select2">';
                                    	html += '<option value="service_cost">Service Cost</option>';
                                    	html += '<option value="part_cost">Part Cost</option>';
                                    html += '</select>';
                                html += '</div>';
                        	html += '</div>';*/
                        	html += '<div class="mt-25 name-section'+current+'">';
                        		html += '<label for="first-name-vertical">Service Name</label>';
                            	html += '<div class="controls">';
                            		html += '<input type="text" id="service_name_'+current+'" name="items[new]['+current+'][service_name]" placeholder="Custom Service" class="form-control">';
                                html += '</div>';
                        	html += '</div>';
                        html += '</td>';

                        // Custom Service Cost
                        html += '<td class="pull-right custom-service-cost'+current+'" style="padding-top: 87px;">';
                        	html += '<label for="first-name-vertical mt-5">&nbsp;</label>';
                        	html += '<div class="controls edit-control align-right">';
		                		html += '<div class="input-group" style="width: 120px;">';
                                    html += '<div class="input-group-prepend">';
                                        html += '<span class="input-group-text">₹</span>';
                                    html += '</div>';
                                    html += '<input type="text" placeholder="Parts Cost" id="parts_cost'+current+'" name="items[new]['+current+'][parts_cost]" class="form-control cost-input" data-item="'+current+'" data-type="parts">';
                                html += '</div>';
                            html += '</div>';
                            html += '<div class="controls edit-control align-right mt-50">';
		                		html += '<div class="input-group" style="width: 120px;">';
                                    html += '<div class="input-group-prepend">';
                                        html += '<span class="input-group-text">₹</span>';
                                    html += '</div>';
                                    html += '<input type="text" placeholder="Labour Cost" id="labour_cost'+current+'" name="items[new]['+current+'][labour_cost]" class="form-control cost-input" data-item="'+current+'" data-type="labour">';
                                html += '</div>';
                            html += '</div>';
                        html += '</td>';

                        // Inbuilt Service Cost
                        html += '<td class="text-right inbuilt-service-cost'+current+'" style="display: none;"><div class="cost_labour_'+current+'"></div><input type="text" id="labour_cost_'+current+'" name="items[new]['+current+'][inbuilt_labour_cost]" class="form-control d-none"></td>';

                        // Custom Service Tax
                        html += '<td class="custom-service-tax'+current+'" style="padding-top: 87px;">';
                        	html += '<label for="first-name-vertical">&nbsp;</label>';
                        	html += '<div class="controls edit-control">';
		                		html += '<div class="input-group" style="width: 70px;">';
                                    html += '<input type="text" id="parts_tax_'+current+'" value="18" name="items[new]['+current+'][parts_tax]" class="form-control tax-input" data-item="'+current+'" data-type="parts">';
                                    html += '<div class="input-group-append">';
                                        html += '<span class="input-group-text">%</span>';
                                    html += '</div>';
                                html += '</div>';
                            html += '</div>';
                            html += '<div class="controls edit-control mt-50">';
		                		html += '<div class="input-group" style="width: 70px;">';
                                    html += '<input type="text" id="labour_tax_'+current+'" value="18" name="items[new]['+current+'][labour_tax]" class="form-control tax-input" data-item="'+current+'" data-type="labour">';
                                    html += '<div class="input-group-append">';
                                        html += '<span class="input-group-text">%</span>';
                                    html += '</div>';
                                html += '</div>';
                            html += '</div>';
                        html += '</td>';

                        // Inbuilt Service Tax
                        html += '<td class="text-right inbuilt-service-tax'+current+'" style="display: none;"><div class="tax_labour_'+current+'"></div><input type="text" id="labour_tax_'+current+'" name="items[new]['+current+'][inbuilt_labour_tax]" class="form-control d-none"></td>';

                        html += '<td class="text-right" style="padding-top: 87px;"><div class="total_parts_'+current+'"></div><br><div class="total_labour_'+current+'"></div></td>';
                        html += '<td style="padding-top: 45px;">';
                        	html += '<span class="inline-tr-delete-icon cursor-pointer" data-field="add-service-'+current+'"><i class="feather icon-trash-2"></i></span>';
                        html += '</td>';
                    html += '</tr>';

                $(this).attr('data-addItems', next);
                $('.add-new-tr').before(html);

                $('.service_id').select2({
                	data: JSON.parse('<?php echo $services; ?>'),
                    dropdownAutoWidth: true,
                    width: '100%'
                });

                $('.select2').select2({
                    dropdownAutoWidth: true,
                    width: '100%'
                });
            });

            $(document.body).on("change", ".service_id", function(e){
            	var text = $(".service_id option:selected").text();
            	var item = $(this).data('item');

            	if ( this.value != 'custom' ) {
            		$('.name-section'+item).hide();
            		$('#service_name_'+item).val(text);
            		$("#service_name_"+item).attr("disabled", true);

            		$('.inbuilt-service-cost'+item).show();
            		$('.inbuilt-service-tax'+item).show();
            		$('.custom-service-cost'+item).hide();
            		$('.custom-service-tax'+item).hide();
            		
            		$.post('/getServiceCost', {'car_id': $('.order_car-select').val(), 'service_id': this.value, 'vendor_id': '{!! $order->vendor_id !!}'}, function(response){
		                if ( response.status == 200 ) {
		                    $('.cost_labour_'+item).html('₹ ' + response.data.cost);
		                    $('#labour_cost_'+item).attr('value', response.data.cost);

		                    $('.tax_labour_'+item).html('18%');
		                    $('#labour_tax_'+item).attr('value', 18);

		                    var total = convertToInt(response.data.cost) + convertToInt((response.data.cost * 0.18));
		                    $('.total_labour_'+item).html('₹ ' + total);

							$('.total_labour_'+item).parent().css('padding-top','0px');
							$('.total_labour_'+item).parent().next().css('padding-top','0px');
		                    checkFormChanged();
		                }
		            });
            	} else {
            		$('.name-section'+item).show();
            		$('#service_name_'+item).val('');
            		$("#service_name_"+item).attr("disabled", false);

            		$('.custom-service-cost'+item).show();
            		$('.custom-service-tax'+item).show();
            		$('.inbuilt-service-cost'+item).hide();
            		$('.inbuilt-service-tax'+item).hide();
            		checkFormChanged();
            	}
			});

			$(document.body).on("keyup", ".cost-input", function(e){
				var item = $(this).data('item');
				var type = $(this).data('type');
				var tax = $('#'+type+'_tax_'+item).val();
				var total =  convertToInt(this.value) + convertToInt(this.value * (tax/100)); 

				$('.total_'+type+'_'+item).html('₹ ' + total);
				checkFormChanged();
			});

			$(document.body).on("keyup", ".tax-input", function(e){
				var item = $(this).data('item');
				var type = $(this).data('type');
				//var cost = $('#cost_'+item).val();
				var cost = $('#'+type+'_cost'+item).val();
				var total =  convertToInt(cost) + convertToInt(cost * (this.value/100)); 

				//$('.total_'+item).html('₹ ' + total);
				$('.total_'+type+'_'+item).html('₹ ' + total);
				checkFormChanged();
			});

			$(document.body).on("change", "#order_status", function(e){
				$('.pickup_per-section').hide();
				$('.reason-section').hide();

				if ( this.value == 'team_enroute' ) {
					$('.pickup_per-section').show();
				} else if ( this.value == 'order_cancelled' ) {
					$('.reason-section').show();
				}
			});

			$(document.body).on("click", ".change-status", function(e){
            	var status = $(this).data('status');
            	var order_id = $(this).data('order_id');
            	var new_status = $('#order_status').val();
            	
            	//console.log(status, new_status);

            	if ( new_status != status ) {
            		
            		var dataPass = {'status': new_status, 'id': order_id};
            		if ( new_status == 'team_enroute' ) {
            			if ( $('#pickup_per').val() == '' ) {
            				$('#pickup_per-error').show();
            				return false;
            			} else {
            				$('#pickup_per-error').hide();
            				dataPass.pickup_person_id = $('#pickup_per').val();
            			}
            		} else if ( new_status == 'order_cancelled' ) {
            			dataPass.reason_for_cancellation = $('input[name=reason_for_cancellation]:checked').val();
            		}

            		$('.order-status-alert').hide();
            		$.post('/orders/change-status', dataPass, function(response){
		                if ( response.status == 200 ) {
		                    window.location = "{{ url('/orders') }}/" + order_id;
		                }
		            });
            	} else {
            		$('.order-status-alert').show();
            	}
			});

        </script>
@endsection
