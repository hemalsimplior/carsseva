@extends('layouts/contentLayoutMaster')

@section('title', '#' . $order->sequence_id)

@section('vendor-style')
        {{-- vendor css files --}}
@endsection

@section('page-style')
        <!-- Page css files -->
        <link rel="stylesheet" href="{{ asset(mix('css/pages/users.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('css/pages/app-ecommerce-shop.css')) }}">
@endsection

@section('content')

		<div class="row">
            <div class="col-xl-3 col-lg-4 col-12">
            	<div class="card">
				    <div class="card-header mx-auto pb-0">
				        <div class="row m-0">
				            <div class="col-sm-12 text-center">
				                <h4>Purvesh Patel</h4>
				            </div>
				            <div class="col-sm-12 text-center">
				                <p class="">Customer</p>
				            </div>
				        </div>
				    </div>
				    <div class="card-content">
				        <div class="card-body text-center mx-auto">
				            <div class="avatar avatar-xl">
				                <img class="img-fluid" src="http://127.0.0.1:8000/images/portrait/default.jpg" alt="img placeholder">
				            </div>
				            <div class="d-flex justify-content-between mt-2">
				                <div class="uploads">
				                    <p class="font-weight-bold font-medium-2 mb-0">10</p>
				                    <span class="">Current Orders</span>
				                </div>
				                <div class="followers">
				                    <p class="font-weight-bold font-medium-2 mb-0">5</p>
				                    <span class="">No of Cars</span>
				                </div>
				                <div class="following">
				                    <p class="font-weight-bold font-medium-2 mb-0">45</p>
				                    <span class="">No of Orders</span>
				                </div>
				            </div>
				            <button class="btn gradient-light-primary btn-block mt-2 waves-effect waves-light">Customer Detail</button>
				        </div>
				    </div>
				</div>
				<div class="card">
				    <div class="card-content">
				        <div class="card-body">
				            <h4>The Family Garage</h4>
				            <p class="">Vendor</p>
				            <hr class="my-2">
				            <dl>
				                <dt>Phone No</dt>
				                <dd><small>+91 9879745593</small></dd>
				                <dt>Email</dt>
				                <dd><small>the.family.garage.v@gmail.com</small></dd>
				                <dt>Address</dt>
				                <dd><small>Nr. Bodakdev Water Tank, B/H. Allen Care Institute, <br> West End Park, Bodakdev, <br>Ahmedabad, Gujarat 380059, India</small></dd>
				                <dt>Pickup Person</dt>
				                <dd><small>Carissa Dolle (+91 9879879875)</small></dd>
				            </dl>
				        </div>
				    </div>
				</div>
                <div class="card">
                    <div class="card-header">
                        <h4>Order Detail</h4>
                    </div>
                    <div class="card-body">
                    	<div class="mt-1">
                            <h6 class="mb-0">Pickup Address:</h6>
                            <small>Nr. Bodakdev Water Tank, B/H. Allen Care Institute, <br> West End Park, Bodakdev, <br>Ahmedabad, Gujarat 380059, India</small>
                        </div>
                        <hr>
                        <div class="mt-1">
                            <h6 class="mb-0">Company:</h6>
                            <p>Hyundai</p>
                        </div>
                        <div class="mt-1">
                            <h6 class="mb-0">Model:</h6>
                            <p>Creta</p>
                        </div>
                        <div class="mt-1">
                            <h6 class="mb-0">Fuel Type:</h6>
                            <p>Diesel</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-8 col-12">

            	<div class="card">
				    <div class="card-header d-flex justify-content-between align-items-center border-bottom pb-1">
				        <div>
				            <p class="mb-75">Total <strong>5 Services </strong></p>
				        </div>
				        <p>Sat, 16, Feb</p>
				    </div>
				    <div class="card-content">
				        <div class="list-group analytics-list">
				            <div class="list-group-item d-lg-flex justify-content-between align-items-start py-1" style="border: 0;">
				                <div class="float-left">
				                    <p class="text-bold-600 font-medium-2 mb-0 mt-25">Genuine OEM Parts</p>
				                    <small>Tyre Replace</small><br>
				                    <small>Wheel Alignment , Wheel Balancing</small><br>
				                    <small>Battery Fitting</small>
				                </div>
				                <div class="float-right">
				                    <p class="text-bold-600 font-medium-2 mb-0 mt-25">₹5000</p>
				                </div>
				            </div>
				            <div class="list-group-item d-lg-flex justify-content-between align-items-start py-1" style="border: 0;">
				                <div class="float-left">
				                    <p class="text-bold-600 font-medium-2 mb-0 mt-25">A.C Service & Repair</p>
				                    <small>AC/Heater Repair</small><br>
				                    <small>AC/Heater Service</small><br>
				                    <small>AC Gas Topup</small><br>
				                </div>
				                <div class="float-right">
				                    <p class="text-bold-600 font-medium-2 mb-0 mt-25">₹300</p>
				                </div>
				            </div>
				            <div class="list-group-item d-lg-flex justify-content-between align-items-start py-1" style="border: 0;">
				                <div class="float-left">
				                    <p class="text-bold-600 font-medium-2 mb-0 mt-25">Car Spa</p>
				                    <small>Express Full Spa</small><br>
				                    <small>Exterior Spa</small><br>
				                    <small>Interior Spa</small><br>
				                </div>
				                <div class="float-right">
				                    <p class="text-bold-600 font-medium-2 mb-0 mt-25">₹3300</p>
				                </div>
				            </div>
				            <div class="list-group-item d-lg-flex justify-content-between align-items-start py-1" style="border: 0;">
				                <div class="float-left">
				                    <p class="text-bold-600 font-medium-2 mb-0 mt-25">Custom Services</p>
				                    <small>Change Gear Handle</small><br>
				                    <small>Change Steering</small>
				                </div>
				                <div class="float-right">
				                    <p class="text-bold-600 font-medium-2 mb-0 mt-25">₹600</p>
				                </div>
				            </div>
				        </div>
				    </div>
				</div>

				<div class="card">
				    <div class="card-header d-flex justify-content-between align-items-center border-bottom pb-1">
				        <div>
				            <p class="mb-75"><strong>Documents</strong></p>
				        </div>
				    </div>
				    <div class="card-body">
				        <div class="row">
				            <div class="col-md-3 col-3 user-latest-img">
				                <img src="https://design.cwsweblab.com/carsseva/app/images/car-photos1.jpg" class="img-fluid mb-1 rounded-sm" alt="avtar img holder">
				            </div>
				            <div class="col-md-3 col-3 user-latest-img">
				                <img src="https://design.cwsweblab.com/carsseva/app/images/car-photos1.jpg" class="img-fluid mb-1 rounded-sm" alt="avtar img holder">
				            </div>
				            <div class="col-md-3 col-3 user-latest-img">
				                <img src="https://design.cwsweblab.com/carsseva/app/images/car-photos1.jpg" class="img-fluid mb-1 rounded-sm" alt="avtar img holder">
				            </div>
				            <div class="col-md-3 col-3 user-latest-img">
				                <img src="https://design.cwsweblab.com/carsseva/app/images/car-photos1.jpg" class="img-fluid mb-1 rounded-sm" alt="avtar img holder">
				            </div>
				            <div class="col-md-3 col-3 user-latest-img">
				                <img src="https://design.cwsweblab.com/carsseva/app/images/car-photos1.jpg" class="img-fluid mb-1 rounded-sm" alt="avtar img holder">
				            </div>
				        </div>
				    </div>
				</div>

            </div>
            <div class="col-lg-3 col-sm-12">
            	<div class="card">
				    <div class="card-body d-flex justify-content-between align-items-start flex-column">
				        <div>
				            <h1 class="mb-0">
				                <sup class="font-medium-3">₹</sup>
				                8600
				            </h1>
				            <small><span class="text-muted">Promotion: </span>₹600</small>
				            <h5 class="mt-1">
				                <span class="text-success">+5.2% ($956)</span>
				            </h5>
				        </div>
				    </div>
				</div>
            	<div class="card">
			        <div class="card-header">
			            <h4 class="card-title">Activity Log</h4>
			        </div>
			        <div class="card-content">
			            <div class="card-body">
			                <ul class="activity-timeline timeline-left list-unstyled">
			                    <li>
			                        <div class="timeline-icon bg-primary">
			                            <i class="feather icon-plus font-medium-2"></i>
			                        </div>
			                        <div class="timeline-info">
			                            <p class="font-weight-bold">Task Added</p>
			                            <span>Bonbon macaroon jelly beans gummi bears jelly lollipop apple</span>
			                        </div>
			                        <small class="">25 days ago</small>
			                    </li>
			                    <li>
			                        <div class="timeline-icon bg-warning">
			                            <i class="feather icon-alert-circle font-medium-2"></i>
			                        </div>
			                        <div class="timeline-info">
			                            <p class="font-weight-bold">Task Updated</p>
			                            <span>Cupcake gummi bears soufflé caramels candy</span>
			                        </div>
			                        <small class="">15 days ago</small>
			                    </li>
			                    <li>
			                        <div class="timeline-icon bg-success">
			                            <i class="feather icon-check font-medium-2"></i>
			                        </div>
			                        <div class="timeline-info">
			                            <p class="font-weight-bold">Task Completed</p>
			                            <span>Candy ice cream cake. Halvah gummi bears
			                            </span>
			                        </div>
			                        <small class="">20 minutes ago</small>
			                    </li>
			                    <li>
			                        <div class="timeline-icon bg-primary">
			                            <i class="feather icon-plus font-medium-2"></i>
			                        </div>
			                        <div class="timeline-info">
			                            <p class="font-weight-bold">Task Added</p>
			                            <span>Bonbon macaroon jelly beans gummi bears jelly lollipop apple</span>
			                        </div>
			                        <small class="">25 days ago</small>
			                    </li>
			                    <li>
			                        <div class="timeline-icon bg-warning">
			                            <i class="feather icon-alert-circle font-medium-2"></i>
			                        </div>
			                        <div class="timeline-info">
			                            <p class="font-weight-bold">Task Updated</p>
			                            <span>Cupcake gummi bears soufflé caramels candy</span>
			                        </div>
			                        <small class="">15 days ago</small>
			                    </li>
			                    <li>
			                        <div class="timeline-icon bg-success">
			                            <i class="feather icon-check font-medium-2"></i>
			                        </div>
			                        <div class="timeline-info">
			                            <p class="font-weight-bold">Task Completed</p>
			                            <span>Candy ice cream cake. Halvah gummi bears
			                            </span>
			                        </div>
			                        <small class="">20 minutes ago</small>
			                    </li>
			                </ul>
			            </div>
			        </div>
			    </div>
			</div>
        </div>
		
		@section('carsseva-alert')

            <!-- Alerts -->
            @foreach (['danger', 'warning', 'success', 'info'] as $key)
                @if(Session::has($key))
                    <div class="cws-alert">
                        <div class="alert alert-{{ $key }} alert-dismissible fade show" role="alert">
                            <div class="alert-text">{{ Session::get($key) }}</div>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
                            </button>
                        </div>
                    </div>
                @endif
            @endforeach

        @endsection

@endsection

@section('vendor-script')
        {{-- vendor files --}} 
@endsection

@section('page-script')
        {{-- Page js files --}}
        <script type="text/javascript">
            $(document).ready(function() {

            });
        </script>
@endsection
