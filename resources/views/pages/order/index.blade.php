@extends('layouts/contentLayoutMaster')

@section('title', 'Orders')

@section('vendor-style')
        {{-- vendor css files --}}
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/datatables.min.css')) }}">
        <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection

@section('page-style')
        <!-- Page css files -->
@endsection

@section('content')

        @section('breadcrumbs-actions')
            @if ( $currentUser['role'] == 'admin' )
                <a href="{{ URL::to('orders/create') }}" class="btn btn-outline-primary mr-1 mb-1 waves-effect waves-light text-nowrap">Add Order</a>
            @endif
        @endsection
        
        <!-- Column selectors with Export Options and print table -->
        <section id="column-selectors">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body card-dashboard">
                                <div class="table-responsive">
                                    <table class="table order-table compact-table table-data-nowrap">
                                        <thead>
                                            <tr>
                                                <th>Sr</th>
                                                <th>Order No</th>
                                                <th>Status</th>
                                                <th>Order Date</th>
                                                <th>Customer Name</th>
                                                <th>Vendor Name</th>
                                                <th>Amount</th>
                                                <th>Payment Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          @php
                                            $sr = 1;
                                          @endphp
                                          @foreach($orders as $in => $order)
                                            <tr class="group">
                                                <td>{{ $sr }}</td>
                                                <td><a href="{{ URL::to('orders/' . $order->order_id) }}">#{{ $order->sequence_id }}</a></td>
                                                <td><div class="badge badge-secondary">{{ $status[$order->status] }}</div></td>
                                                <td>{{ $order->created_at->format('d/m/Y') }}</td>
                                                <td>{{ $order->customer->name }}</td>
                                                <td>{{ $order->vendor->name }}</td>
                                                <td>₹{{ $order->total }}</td>
                                                <td>
                                                    @if ( isset($order->payment_status) && $order->payment_status != 'pending' )
                                                        {{ ucwords(str_replace('_', ' ', $order->payment_status)) }}
                                                    @else - @endif
                                                </td>
                                                <td>
                                                    @if ( !$order->is_awaiting_for_approval && $order->status != 'order_cancelled' )
                                                        <div class="dropdown">
                                                            <button type="button" class="btn btn-primary btn-sm dropdown-toggle waves-effect waves-light" data-toggle="dropdown">Action</button>
                                                            <div class="dropdown-menu">
                                                                
                                                                @foreach ($allow_status[$order->status] as $status1)
                                                                    @if ( $status1['id'] != $order->status )
                                                                        <a class="dropdown-item status-change" data-status="{{ json_encode($status1) }}" data-order="{{ json_encode(array('id' => $order->order_id, 'sequence_id' => $order->sequence_id, 'vendor_id' => $order->vendor_id)) }}" href="javascript:void(0)">{{ $status1['name'] }}</a>
                                                                    @endif
                                                                @endforeach
                                                                <div class="dropdown-divider"></div>
                                                                
                                                                @if ( $order->status == 'service_completed' )
                                                                    <a class="dropdown-item request-for-payment" data-order="{{ json_encode(array('id' => $order->order_id, 'sequence_id' => $order->sequence_id, 'vendor_id' => $order->vendor_id)) }}" href="javascript:void(0)">Request for Payment</a>
                                                                @endif
                                                                
                                                                <a class="dropdown-item" href="{{ URL::to('orders/' . $order->order_id . '/edit') }}">Edit</a>
                                                            </div>
                                                        </div>
                                                    @else - @endif
                                                </td>
                                            </tr>
                                          @endforeach
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Sr</th>
                                                <th>Order No</th>
                                                <th>Status</th>
                                                <th>Order Date</th>
                                                <th>Customer Name</th>
                                                <th>Vendor Name</th>
                                                <th>Amount</th>
                                                <th>Payment Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Column selectors with Export Options and print table -->

        @section('carsseva-alert')

            <!-- Alerts -->
            @foreach (['danger', 'warning', 'success', 'info'] as $key)
                @if(Session::has($key))
                    <div class="cws-alert">
                        <div class="alert alert-{{ $key }} alert-dismissible alert-auto-hide fade show" role="alert">
                            <div class="alert-text">{{ Session::get($key) }}</div>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
                            </button>
                        </div>
                    </div>
                @endif
            @endforeach

            <!-- Confirmation alert msg for status change -->
            <div class="cws-alert status-change-alert" style="display: none;">
                <div class="background-overlay status-change-reject"></div>
                <div class="alert alert-dark-danger alert-dismissible fade show" role="alert">
                    <div class="alert-text">Are you sure you want to <strong class="status-change-name"></strong>? 
                        <div class="alert-btn">
                            <button class="btn btn-primary status-change-confirm">Yes</button>
                            <button class="btn btn-white status-change-reject">Cancel</button>
                        </div>
                    </div>
                    <button type="button" class="close status-change-reject">
                        <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
                    </button>
                </div>
            </div>

            {{-- Modal --}}
            <div class="modal fade text-left" id="changeStatus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                    <h4 class="modal-title">Change Status</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <input type="hidden" name="ids" id="ids">
                                <input type="hidden" name="newStatus" id="newStatus">
                                <input type="hidden" name="vendor_id" id="vendor_id1">

                                <div class="form-group pickup_per-section" style="display: none;">
                                    <label for="first-name-vertical">Pickup Person *</label>
                                    <div class="w-100">
                                        <select class="form-control select2" id="pickup_per">
                                            <option value="">Select Pickup Person</option>
                                            {{-- @foreach ($pickup_persons as $person)
                                                <option value="{{ $person['id'] }}">{{ $person['name'] }}</option>
                                            @endforeach --}}
                                        </select>
                                        <label id="pickup_per-error" class="error" for="pickup_per" style="display: none;">This field is required.</label>
                                    </div>
                                </div>

                                <div class="form-group reason-section" style="display: none;">
                                    <label for="first-name-vertical">Reason for Cancellation *</label>
                                    <div class="w-100">
                                        <fieldset>
                                            <div class="vs-radio-con">
                                                <input type="radio" name="reason_for_cancellation" checked="" value="Reason 1">
                                                <span class="vs-radio">
                                                <span class="vs-radio--border"></span>
                                                <span class="vs-radio--circle"></span>
                                                </span>
                                                <span class="">Reason 1</span>
                                            </div>
                                        </fieldset>
                                        <fieldset>
                                            <div class="vs-radio-con">
                                                <input type="radio" name="reason_for_cancellation" value="Reason 2">
                                                <span class="vs-radio">
                                                <span class="vs-radio--border"></span>
                                                <span class="vs-radio--circle"></span>
                                                </span>
                                                <span class="">Reason 2</span>
                                            </div>
                                        </fieldset>
                                        <fieldset>
                                            <div class="vs-radio-con">
                                                <input type="radio" name="reason_for_cancellation" value="Reason 3">
                                                <span class="vs-radio">
                                                <span class="vs-radio--border"></span>
                                                <span class="vs-radio--circle"></span>
                                                </span>
                                                <span class="">Reason 3</span>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary allow-change-status">Change</button>
                    </div>
                </div>
                </div>
            </div>

        @endsection

@endsection

@section('vendor-script')
        {{-- vendor files --}}
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.bootstrap4.min.js')) }}"></script>
        <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
@endsection

@section('page-script')
        {{-- Page js files --}}
        <script type="text/javascript">
            $(document).ready(function() {

                $('.select2').select2({
                    dropdownAutoWidth: true,
                    width: '100%'
                });

                // Datatable
                var table = $('.order-table').DataTable({
                    "ordering": false,
                    "columnDefs": [
                        { "visible": false, "targets": 0},
                        { "width": '10%', "targets": 1},
                        { "width": '10%', "targets": 2},
                        { "width": '10%', "targets": 3},
                        { "width": '15%', "targets": 4},
                        { "width": '15%', "targets": 5},
                        { "width": '10%', "targets": 6},
                        { "width": '10%', "targets": 7},
                    ],
                    "order": [
                        [0, 'asc']
                    ]
                });
                
            });

            $(document).on('click', '.status-change', function() {
                var order = $(this).data('order');
                var status = $(this).data('status');

                $('.status-change-name').html(status.name + ' ' + order.sequence_id);
                $('.status-change-alert').show();
                $('.status-change-confirm').attr('data-ids', order.id);
                $('.status-change-confirm').attr('data-status', status.id);
                $('.status-change-reject').attr('data-status', status.id);
                $('.status-change-confirm').attr('data-vendor_id', order.vendor_id);
            });
            
            $(document).on('click', '.request-for-payment', function() {
                var order = $(this).data('order');
                
                $.post('orders/request-for-payment', {'id': order.id}, function(response){
                    if ( response.response_code == 200 ) {
                        window.location = "{{ url('/orders') }}";
                    }
                });
            });

            $('.status-change-confirm').click( function() {

                // trigger status-change event
                var ids = $(this).attr('data-ids');
                var status = $(this).attr('data-status');
                var vendor_id = $(this).attr('data-vendor_id');

                if ( status == 'order_cancelled' || status == 'team_enroute' ) {
                    $('.status-change-alert').hide();

                    $('#ids').val(ids);
                    $('#newStatus').val(status);
                    $('#vendor_id1').val(vendor_id);
                    $('#changeStatus').modal('show');
                    if ( status == 'order_cancelled' ) {
                        $('.reason-section').show();
                        $('.pickup_per-section').hide();
                    } else if ( status == 'team_enroute' ) {
                        $('.reason-section').hide();
                        $('.pickup_per-section').show();
                    }
                } else {
                    $.post('orders/change-status', {'status': status, 'id': ids}, function(response){
                        if ( response.status == 200 ) {
                            window.location = "{{ url('/orders') }}";
                        }
                    });
                }
            });

            $('#changeStatus').on('show.bs.modal', function (e) {

                var vendor_id = $('#vendor_id1').val();
                
                $("#pickup_per").val("");
                $("#pickup_per").select2({
                    width: '100%',
                    ajax: {
                        url: "/vendorPickupPerson/"+vendor_id,
                        type: 'get',
                        dataType: 'json',
                        processResults: function (data, params) {
                            //console.log(data.response_data, params);
                            var results = [];
                            if ( data.response_code == 200 ) {
                                $.each(data.response_data, function(index, item){
                                    results.push({
                                        id: item.id,
                                        text: item.name
                                    });
                                });
                            }
                            return {
                                results: results
                            };
                        },
                        cache: true     
                    }
                });
            })
            
            $('.allow-change-status').click( function() {

                // trigger status-change event
                var order_id = $('#ids').val();
                var new_status = $('#newStatus').val();

                var dataPass = {'status': new_status, 'id': order_id};
                if ( new_status == 'team_enroute' ) {
                    if ( $('#pickup_per').val() == '' ) {
                        $('#pickup_per-error').show();
                        return false;
                    } else {
                        $('#pickup_per-error').hide();
                        dataPass.pickup_person_id = $('#pickup_per').val();
                    }
                } else if ( new_status == 'order_cancelled' ) {
                    dataPass.reason_for_cancellation = $('input[name=reason_for_cancellation]:checked').val();
                }

                $.post('orders/change-status', dataPass, function(response){
                    if ( response.status == 200 ) {
                        window.location = "{{ url('/orders') }}";
                    }
                });
            });

            $('.status-change-reject').click( function() {
                $('.status-change-alert').hide();
            });

        </script>
@endsection

