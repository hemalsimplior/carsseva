@extends('layouts/contentLayoutMaster')

@section('title', 'Reviews')

@section('page-style')
        {{-- Page Css files --}}
@endsection

@section('content')
  <div id="user-profile">
    <div class="row">
        
        @foreach($reviews as $inr => $review)
        <div class="col-3">
          <div class="card" style="opacity: {{ (($review->status == '1') ? 1 : 0.5) }};">
            <div class="card-header">
              <div class="d-flex justify-content-start align-items-center mb-1">
                <div class="avatar mr-50">
                  <img src="{{ $review->user->profile_pic_url }}" alt="avtar img holder" height="35" width="35">
                </div>
                <div class="user-page-info">
                  <p class="text-bold-600 mb-0">{{ $review->user->name }}</p>
                  <small>{{ $review->vendor->name }}</small>
                  <div class="badge badge-primary badge-pill badge-sm p-0">
                    <i class="feather icon-check font-small-1"></i>
                  </div>
                </div>
              </div>
              <div class="heading-elements">
                <ul class="list-inline mb-0">
                  <li><a href="javascript:void(0);" class="change-status" data-action="{{ (($review->status == '1') ? 'inactive' : 'active') }}" data-ids="{{ $review->id }}"><i class="feather icon-eye{{ (($review->status == '1') ? '' : '-off') }}"></i></a></li>
                </ul>
              </div>
            </div>
            <div class="card-body pt-0">
              <div class="twitter-feed">
                <p class="mb-2">{{ $review->description }}</p>
                <div class="item-wrapper">
                  <small>{{ $review->created_at->diffForHumans() }}</small>
                  <div class="item-rating pull-right">
                    <div class="badge badge-primary badge-md">
                      <span>{{ $review->rating }}</span> <i class="feather icon-star"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        @endforeach

    </div>
  </div>

  @section('carsseva-alert')
    <!-- Alerts -->
    @foreach (['danger', 'warning', 'success', 'info'] as $key)
        @if(Session::has($key))
            <div class="cws-alert">
                <div class="alert alert-{{ $key }} alert-dismissible alert-auto-hide fade show" role="alert">
                    <div class="alert-text">{{ Session::get($key) }}</div>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
                    </button>
                </div>
            </div>
        @endif
    @endforeach
  @endsection

@endsection
@section('page-script')
        {{-- Page js files --}}
        <script type="text/javascript">
          
          $('.change-status').click( function(){

            // trigger status-change event
            var ids = $(this).attr('data-ids');
            var action = $(this).attr('data-action');

            $.post('reviews/bulk-action', {'action': action, 'ids': ids}, function(response){
                if ( response.status == 200 ) {
                    window.location = "{{ url('/reviews') }}"
                }
            });
          });
        </script>
@endsection
