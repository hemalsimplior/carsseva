
@extends('layouts/contentLayoutMaster')

@section('title', 'Dashboard')

@section('vendor-style')
        <!-- vendor css files -->
@endsection

@section('page-style')
        <!-- Page css files -->
  @endsection

  @section('content')

    {{-- Dashboard Analytics Start --}}
    <section id="dashboard-analytics">
      	<div class="row">
          	<div class="col-xl-2 col-md-4 col-sm-6">
		      <div class="card text-center">
		        <div class="card-content">
		          <div class="card-body">
		            <div class="avatar bg-rgba-danger p-50 m-0 mb-1">
		              <div class="avatar-content">
		                <i class="feather icon-layers text-danger font-medium-5"></i>
		              </div>
		            </div>
					<h2 class="text-bold-700">{{ $statistics['order_received'] ?? 0 }}</h2>
		            <p class="mb-0 line-ellipsis">Pending for Approval</p>
		          </div>
		        </div>
		      </div>
		    </div>
		    <div class="col-xl-2 col-md-4 col-sm-6">
		      <div class="card text-center">
		        <div class="card-content">
		          <div class="card-body">
		            <div class="avatar bg-rgba-success p-50 m-0 mb-1">
		              <div class="avatar-content">
		                <i class="feather icon-layers text-success font-medium-5"></i>
		              </div>
		            </div>
		            <h2 class="text-bold-700">{{ $statistics['today_orders'] ?? 0 }}</h2>
		            <p class="mb-0 line-ellipsis">Today's Orders</p>
		          </div>
		        </div>
		      </div>
		    </div>
		    <div class="col-xl-2 col-md-4 col-sm-6">
		      <div class="card text-center">
		        <div class="card-content">
		          <div class="card-body">
		            <div class="avatar bg-rgba-warning p-50 m-0 mb-1">
		              <div class="avatar-content">
		                <i class="feather icon-layers text-warning font-medium-5"></i>
		              </div>
		            </div>
		            <h2 class="text-bold-700">{{ $statistics['upcoming_orders'] ?? 0 }}</h2>
		            <p class="mb-0 line-ellipsis">Upcoming Orders</p>
		          </div>
		        </div>
		      </div>
		    </div>
		    <div class="col-xl-2 col-md-4 col-sm-6">
		      <div class="card text-center">
		        <div class="card-content">
		          <div class="card-body">
		            <div class="avatar bg-rgba-info p-50 m-0 mb-1">
		              <div class="avatar-content">
		                <i class="feather icon-credit-card text-info font-medium-5"></i>
		              </div>
		            </div>
					<h2 class="text-bold-700"><i class="fa fa-rupee"></i>{{ isset($statistics['total_revenue']) ?  number_format(round($statistics['total_revenue']->total_amount, 2), 2, '.', '') : 0 }}</h2>
		            <p class="mb-0 line-ellipsis">Total Revenue</p>
		          </div>
		        </div>
		      </div>
			</div>
			<div class="col-xl-4 col-md-4 col-sm-6">
				<div class="card">
				  	<div class="card-header">
						<h4 class="card-title">Status Statistics</h4>
				  	</div>
				  	<div class="card-content">
						<div class="card-body">
							<div id="pie-chart" class="mx-auto"></div>
						</div>
				  	</div>
				</div>
			</div>
			{{-- <div class="col-xl-2 col-md-4 col-sm-6">
				<div class="card text-center">
					<div class="card-content">
					<div class="card-body">
						<div class="avatar bg-rgba-info p-50 m-0 mb-1">
						<div class="avatar-content">
							<i class="feather icon-credit-card text-info font-medium-5"></i>
						</div>
						</div>
						<h2 class="text-bold-700"><i class="fa fa-rupee"></i> 75</h2>
						<p class="mb-0 line-ellipsis">Available Balance</p>
					</div>
					</div>
				</div>
			</div> --}}
      	</div>
    </section>
  	<!-- Dashboard Analytics end -->

  @endsection

@section('vendor-script')
		<!-- vendor files -->
		<script src="{{ asset(mix('vendors/js/charts/apexcharts.min.js')) }}"></script>
@endsection

@section('page-script')
		<!-- Page js files -->
		
		<script type="text/javascript">
			
			$(document).ready(function () {
				
				var themeColors = ['#7367F0', '#28C76F', '#EA5455', '#FF9F43', '#00cfe8', '#A5C351', '#E14A84', '#3A5794'];

				// Pie Chart
				/* var pieChartOptions = {
					chart: {
						type: 'pie',
						height: 350
					},
					colors: themeColors,
					labels: {!! json_encode($statistics['status_chart']['name']) !!},
					series: {!! json_encode($statistics['status_chart']['value']) !!},
					legend: {
						itemMargin: {
							horizontal: 2
						},
					},
					responsive: [{
						breakpoint: 480,
						options: {
							chart: {
								width: 350
							},
							legend: {
								position: 'bottom'
							}
						}
					}]
				}
				var pieChart = new ApexCharts(
					document.querySelector("#pie-chart"),
					pieChartOptions
				);
				pieChart.render(); */

				// Bar Chart
				var barChartOptions = {
					chart: {
						height: 350,
						type: 'bar',
					},
					colors: themeColors,
					plotOptions: {
						bar: {
							horizontal: true,
						}
					},
					dataLabels: {
						enabled: false
					},
					series: [{
						data: {!! json_encode($statistics['status_chart']['value']) !!}
					}],
					xaxis: {
						categories: {!! json_encode($statistics['status_chart']['name']) !!},
						tickAmount: 5
					},
					yaxis: {
						opposite: false
					}
				}
				var barChart = new ApexCharts(
					document.querySelector("#pie-chart"),
					barChartOptions
				);
				barChart.render();

			});

		</script>
@endsection
