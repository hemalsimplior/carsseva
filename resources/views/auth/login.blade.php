@extends('layouts/fullLayoutMaster')

@section('title', 'Login Page')

@section('page-style')
        {{-- Page Css files --}}
        <link rel="stylesheet" href="{{ asset(mix('css/pages/authentication.css')) }}">
@endsection

@section('page-script')
  
@endsection

@section('content')
<section class="row flexbox-container">
  <div class="col-xl-8 col-11 d-flex justify-content-center">
      <div class="card rounded-0 mb-0">
          <div class="row m-0">
              <div class="col-lg-6 d-lg-block d-none text-center align-self-center px-1 py-0">
                  <img src="{{ asset('images/pages/login.png') }}" alt="branding logo">
              </div>
              <div class="col-lg-6 col-12 p-0">
                  <div class="card rounded-0 mb-0 px-2">
                      <div class="card-header pb-1">
                          <div class="card-title">
                              <h4 class="mb-0">Login</h4>
                          </div>
                      </div>
                      <p class="px-2">Welcome back, please login to your account.</p>
                      <div class="card-content">
                          <div class="card-body pt-1">

                            <form method="POST" action="{{ route('login') }}">
                              @csrf
                              <fieldset class="form-label-group form-group position-relative has-icon-left">

                                  <input id="contact_no" type="number" class="form-control @error('contact_no') is-invalid @enderror" name="contact_no" placeholder="Contact No" value="{{ old('contact_no') }}" required autocomplete="contact_no" autofocus>

                                  <div class="form-control-position">
                                      <i class="feather icon-phone"></i>
                                  </div>
                                  <label for="contact_no">Contact No</label>
                                  @error('contact_no')
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $message }}</strong>
                                      </span>
                                  @enderror
                              </fieldset>

                              <fieldset class="form-label-group position-relative has-icon-left">

                                  <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password"required autocomplete="current-password">

                                  <div class="form-control-position">
                                      <i class="feather icon-lock"></i>
                                  </div>
                                  <label for="password">Password</label>
                                  @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                  @enderror
                              </fieldset>
                              <div class="form-group d-flex justify-content-between align-items-center">
                                  <div class="text-left">
                                      <fieldset class="checkbox">
                                        <div class="vs-checkbox-con vs-checkbox-primary">
                                          <input type="checkbox" {{ old('remember') ? 'checked' : '' }} name="remember">
                                          <span class="vs-checkbox">
                                            <span class="vs-checkbox--check">
                                              <i class="vs-icon feather icon-check"></i>
                                            </span>
                                          </span>
                                          <span class="">Remember me</span>
                                        </div>
                                      </fieldset>
                                  </div>
                                  @if (Route::has('password.request'))
                                    <div class="text-right"><a class="card-link" href="{{ URL::to('password/reset') }}">
                                        Forgot Password?
                                      </a></div>
                                  @endif

                              </div>
                              <button type="submit" class="btn btn-primary float-right btn-inline">Login</button>
                            </form>
                          </div>
                      </div>
                      <div class="login-footer">
                        <div class="divider">
                          {{-- <div class="divider-text">OR</div> --}}
                        </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>

  @section('carsseva-alert')

      @foreach (['danger', 'warning', 'success', 'info'] as $key)
          @if(Session::has($key))
              <div class="cws-alert">
                  <div class="alert alert-{{ $key }} alert-dismissible fade show" role="alert">
                      <p class="mb-0">{{ Session::get($key) }}</p>
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true"><i class="feather icon-x-circle"></i></span>
                      </button>
                  </div>
              </div>
          @endif
      @endforeach

  @endsection
  
</section>
@endsection
