<?php

namespace App\Http\Middleware;

use Closure;

class CheckApplication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $mode)
    {
        if ( $request->header('Accept-Mode') != null && $request->header('Accept-Mode') != '' ) {
            if ( $request->header('Accept-Mode') == $mode ) {
                return $next($request);
            } else {
                $response = array(
                    'response_code' => 401,
                    'response_message' => 'Unauthenticated',
                );
                return response()->json($response, $response['response_code']);
            }
        } else {
            $response = array(
                'response_code' => 401,
                'response_message' => 'Unauthenticated',
            );
            return response()->json($response, $response['response_code']);
        }
    }
}
