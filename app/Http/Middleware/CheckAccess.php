<?php

namespace App\Http\Middleware;

use Closure, Auth;

class CheckAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $roles)
    {
        $roles = explode("|", $roles);
        $currentUser = Auth::user();
        
        if ( in_array($currentUser->role, $roles) ) {
            return $next($request);
        } else {
            abort(403, 'Unauthorized action.');
        }
    }
}
