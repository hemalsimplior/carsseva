<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Service, App\Models\Vendor, App\Models\VendorCarModelSetting, App\Models\VendorServiceSetting, App\Models\Country, App\Models\State, App\Models\City, App\Models\Promotion, App\Models\Order, App\Models\TempOrder, App\Models\ServiceVendor, App\Models\UserAddress, App\Models\OrderItem, App\User;
use Validator, Auth, Carbon\Carbon;

class ListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function services(Request $request)
    {
        $data = $request->all();
        $_mode = $request->header('Accept-Mode');
        $validator = Validator::make($data, array(
            //'order_id' => 'required|integer',
            'parent_id' => 'required|integer',
            'car_model_id' => 'required_unless:parent_id,0|integer',
        ));
        
        // process the login
        if ($validator->fails()) {

            $validatorString = implode(", ", $validator->messages()->all());
            $validatorArray = array_combine(
                array_map(function($key){ return $key.'_error'; }, array_keys($validator->errors()->toArray())),
                $validator->errors()->toArray()
            );

            $response = array(
                'response_code' => 400,
                'response_message' => $validatorString,
                'response_data' => $validatorArray
            );
            return response()->json($response, 200);
        }

        $currentUser = Auth::user();
        if ( $data['parent_id'] == 0 ) {
            
            $services = Service::select('id', 'name', 'description', 'image')
                ->where('parent_id', $data['parent_id']);

            if ( $_mode == 'vendor' ) {
                $vendorServices = ServiceVendor::where('vendor_id', $currentUser['vendor_id'])->pluck('service_id');
                if ( count($vendorServices) > 0 ) {
                    $services = $services->whereIn('id', $vendorServices);
                }
            }
            
            $services = $services->where('status', '1')
                ->where('is_deleted', '0')
                ->orderBy('order_number', 'asc')
                ->get();
            
            if ( $services ) {
                
                if ( $_mode == 'vendor' ) {
                    $order_service_id = OrderItem::where('order_id', $data['order_id'])
                        ->where('is_custom', '0')
                        ->pluck('service_id');
                }

                foreach ($services as $Skey => $service) {

                    $sub_services = Service::select('id', 'name', 'description', 'image');
                    if ( $_mode == 'vendor' ) {
                        $sub_services = $sub_services->whereNotIn('id', $order_service_id);
                    }
                    $sub_services = $sub_services->where('parent_id', $service->id)
                        ->where('status', '1')
                        ->where('is_deleted', '0')
                        ->orderBy('order_number', 'asc');

                    $sub_services = $sub_services->get()->map(function ($sub_service) use($data, $_mode, $currentUser) {

                        $car_model = VendorCarModelSetting::select('segment')
                            ->where('car_model_id', $data['car_model_id'])
                            ->where('vendor_id', 0)
                            ->first();

                        if ( $_mode == 'vendor' ) {
                            
                            $checkVendorServices = VendorServiceSetting::select('id', $car_model->segment)
                                ->where('vendor_id', $currentUser['vendor_id'])
                                ->where('service_id', $sub_service->id)
                                ->where('status', '1')
                                ->first();
                            
                            $sub_service->cost = ($checkVendorServices == null ? 0 : $checkVendorServices[$car_model->segment]);
                            $sub_service->tax = 18;

                        } else {
                            $vendorService = VendorServiceSetting::selectRaw('MIN(`'.$car_model->segment.'`) AS cost')
                                ->where('service_id', $sub_service->id)
                                ->where('status', '1')
                                ->first();
                            $sub_service->min_cost = (isset($vendorService->cost) ? $vendorService->cost : 0);
                        }
                        
                        return $sub_service;
                    });

                    $service['sub_services'] = $sub_services;
                }
            }

        } else {

            $services = Service::select('id', 'name', 'description', 'image');
                
            if ( $_mode == 'vendor' ) {
                $order_service_id = OrderItem::where('order_id', $data['order_id'])
                        ->where('is_custom', '0')
                        ->pluck('service_id');
                $services = $services->whereNotIn('id', $order_service_id);
            }

            $services = $services->where('parent_id', $data['parent_id'])
                ->where('status', '1')
                ->where('is_deleted', '0')
                ->orderBy('order_number', 'asc');

            $services = $services->get()->map(function ($service) use($data, $_mode, $currentUser) {

                $car_model = VendorCarModelSetting::select('segment')
                    ->where('car_model_id', $data['car_model_id'])
                    ->where('vendor_id', 0)
                    ->first();

                if ( $_mode == 'vendor' ) {
                    
                    $checkVendorServices = VendorServiceSetting::select('id', $car_model->segment)
                        ->where('vendor_id', $currentUser['vendor_id'])
                        ->where('service_id', $service->id)
                        ->where('status', '1')
                        ->first();
                    
                    $service->cost = ($checkVendorServices == null ? 0 : $checkVendorServices[$car_model->segment]);
                    $service->tax = 18;

                } else {
                    $vendorService = VendorServiceSetting::selectRaw('MIN(`'.$car_model->segment.'`) AS cost')
                        ->where('service_id', $service->id)
                        ->where('status', '1')
                        ->first();
                    $service->min_cost = (isset($vendorService->cost) ? $vendorService->cost : 0);
                }
                
                return $service;
            });
        }

        $response = array(
            'response_code' => 200,
            'response_message' => '',
            'response_data' => $services
        );

        return response()->json($response, 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function vendors(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, array(
            'user_address_id' => 'required|integer',
            'car_model_id' => 'required|integer',
            'services' => 'required',
        ));

        // process the login
        if ($validator->fails()) {

            $validatorString = implode(", ", $validator->messages()->all());
            $validatorArray = array_combine(
                array_map(function($key){ return $key.'_error'; }, array_keys($validator->errors()->toArray())),
                $validator->errors()->toArray()
            );

            $response = array(
                'response_code' => 400,
                'response_message' => $validatorString,
                'response_data' => $validatorArray
            );
            return response()->json($response, 200);
        }

        $currentUser = Auth::user();

        $data['services'] = explode(",", $data['services']);

        $addressData = UserAddress::select('id', 'latitude', 'longitude')->find($data['user_address_id']);
        $parentServices = Service::whereIn('id', $data['services'])->with('vendors')->pluck('parent_id');

        $serviceVenArr = array();
        foreach ($parentServices as $parent_service) {
            $serviceVenArr[] = ServiceVendor::where('service_id', $parent_service)->pluck('vendor_id')->toArray();
        }
        if ( count($serviceVenArr) > 1 ) {
            $parentServicesAllowVendor = call_user_func_array('array_intersect', $serviceVenArr);
        } else {
            $parentServicesAllowVendor = $serviceVenArr[0];
        }
        //$parentServicesAllowVendor = ServiceVendor::whereIn('service_id', $parentServices)->pluck('vendor_id');
        
        $car_model = VendorCarModelSetting::select('segment')
            ->where('car_model_id', $data['car_model_id'])
            ->where('vendor_id', 0)
            ->first();

        $vendors = Vendor::select('id', 'latitude', 'longitude')
            ->whereIn('id', $parentServicesAllowVendor)
            ->where('is_deleted', '0')
            ->where('status', '1');

        if ( isset($data['string']) && $data['string'] != '' ) {
            $vendors = $vendors->where(function ($query) use($data) {
                $query->where('name', 'like', '%'.$data['string'].'%')
                      ->orWhere('code', 'like', '%'.$data['string'].'%');
            });
        }

        $vendors = $vendors->get();

        if ( $vendors ) {
            $allowVendorsWithRange = array();
            $allowVendors = array();
            foreach ($vendors as $k => $vendor) {
                
                $vendorData = array(
                    'id' => $vendor->id,
                    'total_cost' => 0,
                );
                $available = true;
                foreach ($data['services'] as $service) {
                    
                    if ( $available ) {
                        $checkVendorServices = VendorServiceSetting::select('id', $car_model->segment)
                            ->where('vendor_id', $vendor->id)
                            ->where('service_id', $service)
                            ->where('status', '1')
                            ->first();
                        
                        if ( $checkVendorServices == null ) {
                            $available = false;
                        } else {
                            $vendorData['total_cost'] += $checkVendorServices[$car_model->segment];
                        }
                    }
                }

                if ( $available ) {
                    
                    if ( isset($vendor->latitude) && $vendor->latitude != '' && $vendor->latitude != null && isset($vendor->longitude) && $vendor->longitude != '' && $vendor->longitude != null && isset($addressData['latitude']) && $addressData['latitude'] != '' && $addressData['latitude'] != null && isset($addressData['longitude']) && $addressData['longitude'] != '' && $addressData['longitude'] != null ) {
                        $distance = $this->checkDistance($addressData['latitude'], $vendor->latitude, $addressData['longitude'], $vendor->longitude);
                        if ( $distance != '' ) {
                            $unit = explode(" ", $distance);
                            $vendorData['distance'] = $distance;
                            $vendorData['distance_check'] = (($unit[1] == 'km') ? $unit[0]*1000 : $unit[0]*1);

                            if ( $vendorData['distance_check'] <= 5000 ) {
                                $allowVendors[$vendor->id] = $vendorData;
                            } else {
                                $allowVendorsWithRange[$vendor->id] = $vendorData;
                            }
                        } 
                    }/* else {
                        $allowVendors[$vendor->id] = $vendorData;
                    }*/
                }
            }
        }

        //dd($allowVendorsWithRange);

        //array_multisort( array_column($vendorData, "distance_check"), SORT_ASC, $vendorData );
        uasort($allowVendors, function($a, $b) {
            return $a['distance_check'] <=> $b['distance_check'];
        });

        $ids_ordered = implode(',', array_column($allowVendors, 'id'));
        $finalVendors = Vendor::select('id', 'code', 'name', 'contact_no', 'address_line_1', 'address_line_2', 'city_id', 'state_id', 'latitude', 'longitude', 'zipcode', 'logo')
            ->with('reviews')
            ->withCount('reviews')
            ->with(array('state' => function($query){
                $query->select('id', 'country_id', 'name')->with('country:id,name');
            }))
            ->with('city:id,name')
            ->whereIn('id', array_column($allowVendors, 'id'))
            ->where('is_deleted', '0')
            ->where('status', '1');

        if ( isset($allowVendors) && count($allowVendors) > 0 ) {
            $finalVendors = $finalVendors->orderByRaw("FIELD(id, $ids_ordered)");
        }

        $finalVendors = $finalVendors->get()->map(function ($vendor) use($allowVendors) {

            $vendor->is_verified = true;
            $vendor->is_free_pickup_drop = true;
            $vendor->total_cost = $allowVendors[$vendor->id]['total_cost'];
            if ( isset($allowVendors[$vendor->id]['distance']) ) {
                $vendor->distance = $allowVendors[$vendor->id]['distance'];
                $vendor->distance_check = $allowVendors[$vendor->id]['distance_check'];
            }

            if ( $vendor->reviews_count > 0 ) {
                $vendor->avg_reviews = array_sum(array_column($vendor->reviews->toArray(), 'rating')) / $vendor->reviews_count;
            } else {
                $vendor->avg_reviews = 0;
            }

            // Get Promotions
            $promotions = Promotion::select('id', 'name', 'code', 'description', 'sign_off_type', 'amount')
                ->whereRaw('(JSON_CONTAINS(vendors, "true", "$.'.$vendor->id.'") OR JSON_CONTAINS(vendors, "true", "$.all"))')
                ->whereDate('start_date', '<=', Carbon::now())
                ->whereDate('end_date', '>', Carbon::now())
                ->where('status', '1')
                ->where('is_deleted', '0')
                ->orderBy('id', 'desc')
                ->get();
            if ( isset($promotions[0]) ) {
                $vendor->promotions = array($promotions[0]);
            } else {
                $vendor->promotions = array();
            }

            unset($vendor->reviews);

            return $vendor;
        });

        $response = array(
            'response_code' => 200,
            'response_message' => '',
            'response_data' => $finalVendors
        );

        return response()->json($response, 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function servicesCost(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, array(
            'vendor_id' => 'required|integer',
            'car_model_id' => 'required|integer',
            'services' => 'required',
        ));

        // process the login
        if ($validator->fails()) {

            $validatorString = implode(", ", $validator->messages()->all());
            $validatorArray = array_combine(
                array_map(function($key){ return $key.'_error'; }, array_keys($validator->errors()->toArray())),
                $validator->errors()->toArray()
            );

            $response = array(
                'response_code' => 400,
                'response_message' => $validatorString,
                'response_data' => $validatorArray
            );
            return response()->json($response, 200);
        }

        $currentUser = Auth::user();
        $data['services'] = explode(",", $data['services']);

        $car_model = VendorCarModelSetting::select('segment')
            ->where('car_model_id', $data['car_model_id'])
            ->where('vendor_id', 0)
            ->first();

        $available = true;
        $vendorData = array();
        foreach ($data['services'] as $service) {
            
            if ( $available ) {
                $checkVendorServices = VendorServiceSetting::select('id', $car_model->segment)
                    ->where('vendor_id', $data['vendor_id'])
                    ->where('service_id', $service)
                    ->where('status', '1')
                    ->first();
                
                if ( $checkVendorServices == null ) {
                    array_push($vendorData, array(
                        'id' => $service,
                        'cost' => 0,
                        'tax' => 18
                    ));
                } else {
                    array_push($vendorData, array(
                        'id' => $service,
                        'cost' => $checkVendorServices[$car_model->segment],
                        'tax' => 18
                    ));
                }
            }
        }

        $response = array(
            'response_code' => 200,
            'response_message' => '',
            'response_data' => $vendorData
        );

        return response()->json($response, 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addresses(Request $request)
    {
        $currentUser = Auth::user();

        $response = array(
            'response_code' => 200,
            'response_message' => '',
            'response_data' => $currentUser->addresses
        );

        return response()->json($response, 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function reasons(Request $request)
    {
        $reasons = array(
            array(
                'id' => 'Reason 1',
                'name' => 'Reason 1'
            ),
            array(
                'id' => 'Reason 2',
                'name' => 'Reason 2'
            ),
            array(
                'id' => 'Reason 3',
                'name' => 'Reason 3'
            ),
        );

        $response = array(
            'response_code' => 200,
            'response_message' => '',
            'response_data' => $reasons
        );

        return response()->json($response, 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pickupPersons(Request $request)
    {
        $currentUser = Auth::user();

        $persons = User::select('id', 'name', 'contact_no')
            ->where('vendor_id', $currentUser['vendor_id'])
            ->where('role', 'pickup_person')
            ->where('status', '1')
            ->where('is_deleted', '0')
            ->get();
        
        $response = array(
            'response_code' => 200,
            'response_message' => '',
            'response_data' => $persons
        );

        return response()->json($response, 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function location(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, array(
            'type' => 'required',
            'country_id' => 'required_unless:type,country|integer',
            'state_id' => 'required_if:type,city|integer',
        ));

        // process the login
        if ($validator->fails()) {

            $validatorString = implode(", ", $validator->messages()->all());
            $validatorArray = array_combine(
                array_map(function($key){ return $key.'_error'; }, array_keys($validator->errors()->toArray())),
                $validator->errors()->toArray()
            );

            $response = array(
                'response_code' => 400,
                'response_message' => $validatorString,
                'response_data' => $validatorArray
            );
            return response()->json($response, 200);
        }
        
        if ( $data['type'] == 'country' ) {
            $location = Country::select('id', 'name')
                ->get();
        } else if ( $data['type'] == 'state' ) {
            $location = State::select('id', 'name')
                ->where('country_id', $data['country_id'])
                ->get();
        } else {
            $location = City::select('id', 'name')
                ->where('country_id', $data['country_id'])
                ->where('state_id', $data['state_id'])
                ->get();
        }

        $response = array(
            'response_code' => 200,
            'response_message' => '',
            'response_data' => $location
        );

        return response()->json($response, 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function homePage(Request $request)
    {
        $user = Auth::user();

        // Get Main Services
        $mainServices = Service::select('id', 'parent_id', 'name', 'description', 'image')
            ->where('parent_id', '0')
            ->where('status', '1')
            ->where('is_deleted', '0')
            ->orderBy('order_number', 'asc')
            ->get();

        // Get Promotions
        $promotions = Promotion::select('id', 'name', 'code', 'description', 'sign_off_type', 'amount')
            ->whereDate('start_date', '<=', Carbon::now())
            ->whereDate('end_date', '>', Carbon::now())
            ->where('status', '1')
            ->where('is_deleted', '0')
            ->orderBy('id', 'desc')
            ->get();

        $cars = array();
        if ( count($user->cars) > 0 ) {
            foreach ($user->cars as $k => $car) {
                $cars[$k] = array(
                    'id' => $car['id'],
                    'car_model_id' => $car['car_model_id'],
                    'car_number' => $car['car_number'],
                    'car_model' => array(
                        'id' => $car->car_model->id,
                        'brand_id' => $car->car_model->brand_id,
                        'name' => $car->car_model->name,
                        'image_url' => $car->car_model->image_url,
                        'brand' => array(
                            'id' => $car->car_model->brand->id,
                            'name' => $car->car_model->brand->name,
                            'image_url' => $car->car_model->brand->image_url,
                        )
                    )
                );
            }
        }

        // Recomm. Services
        $recommendation = TempOrder::select('id', 'order_id', 'customer_id')
            ->where('customer_id', $user->id)
            ->where('is_awaiting_for_approval', '1')
            ->with(array('items' => function($query) {
                $query->select('id', 'temp_order_id', 'parent_service_id', 'service_id', 'service_name')
                    ->with('parent_service:id,name');
            }))
            ->orderBy('id', 'desc')
            ->get();

        $response = array(
            'response_code' => 200,
            'response_message' => '',
            'response_data' => array(
                'cars' => $cars,
                'recommendation' => $recommendation,
                'main_services' => $mainServices,
                'promotions' => $promotions,
            )
        );

        return response()->json($response, 200);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function homePageVendor(Request $request)
    {
        $user = Auth::user();

        // Get Promotions
        $promotions = Promotion::select('id', 'name', 'code', 'description', 'sign_off_type', 'amount')
            ->whereDate('start_date', '<=', Carbon::now())
            ->whereDate('end_date', '>', Carbon::now())
            ->whereRaw('(JSON_CONTAINS(vendors, "true", "$.'.$user['vendor_id'].'") OR JSON_CONTAINS(vendors, "true", "$.all"))')
            ->where('status', '1')
            ->where('is_deleted', '0')
            ->orderBy('id', 'desc')
            ->get();

        // Order Count Based on Status
        $order_count = array(
            'order_received' => 0,
            'order_confirmed' => 0,
            'order_cancelled' => 0,
            'team_enroute' => 0,
            'car_picked_up' => 0,
            'service_started' => 0,
            'service_completed' => 0,
            'delivered' => 0,
        );
        $orders = Order::selectRaw('COUNT(id) as count, status')
            ->where('vendor_id', $user['vendor_id'])
            ->groupBy('status')
            ->get();

        if ( $orders ) {
            foreach ($orders as $order) {
                $order_count[$order['status']] = $order['count'];
            }
        }

        // List of pending orders
        $pendingOrders = Order::where('vendor_id', $user['vendor_id'])
            ->where('status', 'order_received')
            ->pluck('sequence_id');

        $response = array(
            'response_code' => 200,
            'response_message' => '',
            'response_data' => array(
                'pending_orders' => array(
                    'count' => $order_count['order_received'],
                    'orders' => $pendingOrders
                ),
                'order_count' => $order_count,
                'promotions' => $promotions,
                'total_money' => 10000,
                'pending_money' => 1000,
            )
        );

        return response()->json($response, 200);
    }
}
