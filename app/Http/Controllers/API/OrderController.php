<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Validator, Auth, Config, Carbon\Carbon, File, DB;
use App\User, App\Models\Order, App\Models\OrderItem, App\Models\OrderDocument, App\Models\Service, App\Models\UserCar, App\Models\Vendor, App\Models\Promotion, App\Models\TempOrder, App\Models\TempOrderItem, App\Models\Log, App\Models\Payment, App\Models\UserFirebaseToken;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currentUser = Auth::user();
        
        $orders = Order::select('id', 'vendor_id', 'sequence_id', 'service_date', 'status', 'created_at as booking_date')
            ->with(array('vendor' => function($query){
                $query->select('id', 'name', 'logo');
            }))
            //->with('payments')
            ->where('customer_id', $currentUser->id)
            ->where('is_deleted', '0')
            ->orderBy('id', 'desc')
            ->get();

        $response = array(
            'response_code' => 200,
            'response_message' => '',
            'response_data' => $orders
        );

        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, array(
            'vendor_id' => 'required',
            'user_car_id' => 'required',
            'pickup_type' => 'required',
            'user_address_id' => 'required_if:pickup_type,pickup',
            'service_date' => 'required',
            'services' => 'required',
        ));

        // process the login
        if ($validator->fails()) {

            $validatorString = implode(", ", $validator->messages()->all());
            $validatorArray = array_combine(
                array_map(function($key){ return $key.'_error'; }, array_keys($validator->errors()->toArray())),
                $validator->errors()->toArray()
            );

            $response = array(
                'response_code' => 400,
                'response_message' => $validatorString,
                'response_data' => $validatorArray
            );
            return response()->json($response, 200);
        }

        $currentUser = Auth::user();
        $data['service_date'] = Carbon::parse($data['service_date'])->format('Y-m-d');
        $checkOrderExist = Order::where('user_car_id', $data['user_car_id'])
            ->whereDate('service_date', $data['service_date'])
            ->whereNotIn('status', array('order_cancelled', 'delivered'))
            ->count();
        if ( $checkOrderExist <= 0 ) {
            
            $userCar = UserCar::select('car_model_id')->find($data['user_car_id']);

            $services = explode(",", $data['services']);
            $data['customer_id'] = $currentUser->id;
            $data['status'] = 'order_received';

            // find seq id
            $prefix = Config::get('custom.carsseva.prefix.order');
            $vendor = Vendor::select('code')->find($data['vendor_id']);
            $lastOrder = Order::select('sequence_id')->where('vendor_id', $data['vendor_id'])->orderBy('ID', 'desc')->first();
            if ( isset($lastOrder->sequence_id) ) {
                $data['sequence_id'] = $this->increaseSeqNo($lastOrder->sequence_id, $vendor->code.$prefix);
            } else {
                $data['sequence_id'] = $vendor->code.$prefix.'0001';
            }

            if ( isset($data['promo_code_id']) && $data['promo_code_id'] != '' ) {
                $promoDetail = Promotion::select('id', 'code', 'sign_off_type', 'amount')->find($data['promo_code_id']);
                if ( $promoDetail ) {
                    $promoDetail = $promoDetail->toArray();
                    //$data['promo_code'] = serialize($promoDetail);
                    $data['promo_code'] = json_encode($promoDetail);
                }
            }
            
            $order = Order::create($data);
            if( $order ){ 

                $order_amount = array(
                    'sub_total' => 0,
                    'discount' => 0,
                    'tax' => 0
                );
                foreach ($services as $service) {

                    $serviceData = Service::find($service);
                    $cost = $this->serviceCost($service, $userCar->car_model_id, $data['vendor_id']);
                    $orderItemData = array(
                        'order_id' => $order->id,
                        'service_id' => $service,
                        'parent_service_id' => $serviceData->parent_id,
                        'service_name' => $serviceData->name,
                        'cost_type' => 'service_cost',
                        'cost' => $cost,
                        'tax' => '18',
                        'total' => $cost + ($cost * 0.18),
                        'status' => 'confirmed',
                    );

                    $orderItem = OrderItem::create($orderItemData);
                    if ( $orderItem ) {
                        $order_amount['sub_total'] += $cost;
                        //$order_amount['tax'] += $cost * 0.18;
                    }
                }

                if ( $order->promo_code != '' && $order->promo_code != NULL ) {
                    //$promoData = unserialize($order->promo_code);
                    $promoData = json_decode($order->promo_code, true);
                    if ( $promoData['sign_off_type'] == 'fixed_amount' ) {
                        $order_amount['discount'] = $promoData['amount'];
                    } else {
                        $order_amount['discount'] = ($order_amount['sub_total']*($promoData['amount']/100));
                    }
                }

                $temp_total = ($order_amount['sub_total'] - $order_amount['discount']);
                $order_amount['tax'] = $temp_total * 0.18;
                $order_amount['total'] = $temp_total + $order_amount['tax'];
                Order::find($order->id)->update($order_amount);

                $orders = Order::select('id', 'vendor_id', 'sequence_id', 'service_date', 'status')
                    ->with(array('vendor' => function($query){
                        $query->select('id', 'name', 'logo');
                    }))
                    ->where('customer_id', $currentUser->id)
                    ->where('is_deleted', '0')
                    ->orderBy('id', 'desc')
                    ->get();

                /* log */
                $vendorAdmin = User::select('id')
                    ->where('vendor_id', $order['vendor_id'])
                    ->where('role', 'vendor_admin')
                    ->first();
                $logData = array('common_id' => $order['id'], 'action_type' => 'order', 'action' => 'add', 'description' => '', 'action_by' => $currentUser['id'], 'user_ip' => $request->ip(), 'is_read' => json_encode(array($currentUser['id'] => false, $vendorAdmin['id'] => false)));
                $logId = Log::create($logData);
                /* log */

                // Send Notification to Customer
                $tokens = UserFirebaseToken::where('user_id', $currentUser['id'])->pluck('firebase_token')->toArray();
                $notification = array(
                    'title' => 'Order Placed',
                    'body' => 'Your order #'.$order['sequence_id'].' has been placed!',
                    'data' => array(
                        'order_id' => $order['id'],
                        'mode' => 'order_add',
                        'destination_screen' => 'order_status',
                        'notification_id' => $logId['id'],
                    )
                );
                $msg = '';
                try {
                    $msg = $this->sendNotification($notification, $tokens);
                } catch (\Throwable $th) {
                    
                }

                // Send Notification to Vendor
                $tokens = UserFirebaseToken::where('user_id', $vendorAdmin['id'])->pluck('firebase_token')->toArray();
                $notification = array(
                    'title' => 'Order Recived',
                    'body' => 'New order #'.$order['sequence_id'].' has been recived!',
                    'data' => array(
                        'order_id' => $order['id'],
                        'mode' => 'order_list',
                        'destination_screen' => 'order_list',
                        'notification_id' => $logId['id'],
                    )
                );
                try {
                    $this->sendNotification($notification, $tokens);
                } catch (\Throwable $th) {
                    
                }

                $response = array(
                    'response_code' => 200,
                    'response_message' => 'Order Recived!',
                    'response_data' => $orders
                );
            } else {
                $response = array(
                    'response_code' => 500,
                    'response_message' => "Opps, Something went wrong please try again later!"
                );
            }

        } else {
            $response = array(
                'response_code' => 500,
                'response_message' => "You are already placed order on this date!"
            );
        }
        return response()->json($response, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $_mode = $request->header('Accept-Mode');
        
        $response_message = '';
        
        $order_mode = 'order';
        $originalOrder = Order::select('temp_order_id', 'is_awaiting_for_approval')->find($id)->toArray();
        if ( $originalOrder['is_awaiting_for_approval'] == 1 ) {
            $order = new TempOrder;
            $findId = $originalOrder['temp_order_id'];
            $order_col = 'temp_order_id';
            $ref_col = 'order_id';
        } else {
            $order = Order::where('is_deleted', 0);
            $findId = $id;
            $order_col = 'order_id';
            $ref_col = 'temp_order_id';
        }
        //dd($order, $findId);
        
        $order = $order->select('id', $ref_col, 'vendor_id', 'customer_id', 'user_car_id', 'user_address_id', 'sequence_id', 'pickup_type', 'pickup_person_id', 'sub_total', 'discount', 'tax', 'total', 'service_date', 'service_time_slot', 'status', 'is_awaiting_for_approval', 'created_at as booking_date', 'payment_type', 'promo_code')
            ->with(array('items' => function($query) use($order_col) {
                $query->select('id', $order_col, 'associate_id', 'parent_service_id', 'service_id', 'service_name', 'cost_type', 'cost', 'tax', 'total', 'status', 'is_custom')
                    ->with('parent_service:id,name')
                    ->with('service:id,description');
            }))
            ->with(array('customer_car' => function($query){
                $query->select('id', 'car_model_id', 'car_number', 'fuel_type', 'gear_type')->with(array('car_model' => function($query){
                    $query->select('id', 'brand_id', 'name', 'image')->with('brand:id,name');
                }));
            }))
            ->with(array('customer_address' => function($query){
                $query->select('id', 'address_line_1', 'address_line_2', 'state_id', 'city_id', 'latitude', 'longitude', 'zipcode')
                    ->with(array('state' => function($query){
                        $query->select('id', 'country_id', 'name')->with('country:id,name');
                    }))
                    ->with('city:id,name');
            }));

        if ( $_mode == 'customer' ) {
            $order = $order->with(array('vendor' => function($query){
                    $query->select('id', 'name', 'contact_no', 'address_line_1', 'address_line_2', 'state_id', 'city_id', 'latitude', 'longitude', 'zipcode', 'logo')
                        ->with(array('state' => function($query){
                            $query->select('id', 'country_id', 'name')->with('country:id,name');
                        }))
                        ->with('city:id,name');
                }));
        } else {
            $order = $order->with('customer:id,name,contact_no')
                ->with('pickup_person:id,name,contact_no')
                ->with('before_service_documents:id,order_id,type,document');
        }

        $order = $order->with('payments')->find($findId);
        
        if ( $order ) {
            
            //$order['promo_code'] = unserialize($order['promo_code']);
            $order['promo_code'] = json_decode($order['promo_code'], true);
            $order['service_date'] = Carbon::parse($order['service_date'])->format('Y-m-d h:m:s');

            $temp_items = $order['items'];
            unset($order['items']);

            $itemsNew = array();
            foreach ($temp_items as $ord => $item) {
                if ( $item['is_custom'] ) {
                    if ( $item['associate_id'] != '' && $item['associate_id'] != null ) {
                        
                        $associateArr = array_column($itemsNew, 'id');
                        $associate_key = array_search($item['associate_id'], $associateArr);

                        $item['tax_amount'] = $item['cost']*($item['tax']/100);
                        $itemsNew[$associate_key]['associate_item'] = $item;
                        $itemsNew[$associate_key]['total_custom_service_amount'] = $item['cost']+$itemsNew[$associate_key]['cost'];
                        $itemsNew[$associate_key]['total_custom_service_tax'] = $item['tax_amount']+$itemsNew[$associate_key]['tax_amount'];

                    } else {
                        $item['tax_amount'] = $item['cost']*($item['tax']/100);
                        $itemsNew[] = $item;
                    }
                } else {
                    $item['tax_amount'] = $item['cost']*($item['tax']/100);
                    $itemsNew[] = $item;
                }
            }
            $order['items'] = $itemsNew;
            
        } else {
            $response_message = 'Order not available in our portal.';
            $order = array();
        }

        $response = array(
            'response_code' => 200,
            'response_message' => $response_message,
            'response_data' => $order
        );

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, array(
            'action' => 'required',
            'items' => 'required_if:action,accept'
        ));

        // process the login
        if ($validator->fails()) {

            $validatorString = implode(", ", $validator->messages()->all());
            $validatorArray = array_combine(
                array_map(function($key){ return $key.'_error'; }, array_keys($validator->errors()->toArray())),
                $validator->errors()->toArray()
            );

            $response = array(
                'response_code' => 400,
                'response_message' => $validatorString,
                'response_data' => $validatorArray
            );
            return response()->json($response, 200);
        }

        $originalOrder = Order::select('*')->find($id)->toArray();
        if ( $originalOrder['is_awaiting_for_approval'] == 1 ) {

            if ( $data['action'] == 'accept' ) {
                $data['items'] = explode(",", $data['items']);
                foreach ($data['items'] as $item) {
                    $itemData = TempOrderItem::find($item);
                    if ( $itemData ) {
                        $itemData = $itemData->toArray();

                        $itemData['order_id'] = $id;
                        $itemData['status'] = 'confirmed';
                        $itemData['is_deleted'] = '0';

                        OrderItem::create($itemData);
                    }
                }
            }

            // Update Order
            Order::find($id)->update(array('temp_order_id' => null, 'is_awaiting_for_approval' => '0'));

            // Remove Temp Order and Temp Order Items
            $deleteTempOrder = TempOrder::find($originalOrder['temp_order_id'])->delete();
            if ( $deleteTempOrder ) {
                TempOrderItem::where('temp_order_id', $originalOrder['temp_order_id'])->delete();
            }

            // Send Notification to Vendor
            $vendorAdmin = User::select('id')
                ->where('vendor_id', $originalOrder['vendor_id'])
                ->where('role', 'vendor_admin')
                ->first();
            $tokens = UserFirebaseToken::where('user_id', $vendorAdmin['id'])->pluck('firebase_token')->toArray();
            $notification = array(
                'title' => "Recommendation " . (($data['action'] == 'decline') ? 'Declined' : 'Accepted'),
                'body' => "Customer ".(($data['action'] == 'decline') ? 'declined' : 'accepted')." your recommendation for order #".$originalOrder['sequence_id'],
                'data' => array(
                    'order_id' => $originalOrder['id'],
                    'mode' => 'service_recommended',
                    'destination_screen' => 'order_job_card',
                )
            );
            $msg = '';
            try {
                $msg = $this->sendNotification($notification, $tokens);
            } catch (\Throwable $th) {
                
            }
            

            $response = array(
                'response_code' => 200,
                'response_message' => 'Updated!'
            );
            
        } else {
            $response = array(
                'response_code' => 500,
                'response_message' => "Invalid Order!"
            );
        }

        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cancel($id)
    {
        $currentUser = Auth::user();
        $orderData = Order::find($id);
        if ( $orderData['customer_id'] == $currentUser['id'] ) {
            if ( $orderData['status'] == 'order_received' || $orderData['status'] == 'order_confirmed' ) {

                $updateData = array(
                    'status' => 'order_cancelled'
                );

                $order = Order::find($id);
                if ( $order->update($updateData) ) {
                    
                    /* log */
                    $logData = array('common_id' => $order['id'], 'action_type' => 'order', 'action' => 'change_status', 'old' => $orderData['status'], 'new' => 'order_cancelled', 'description' => '', 'action_by' => $currentUser['id'], 'user_ip' => '', 'is_read' => json_encode(array($order['customer_id'] => false)));
                    $logId = Log::create($logData);
                    /* log */

                    // Send Notification to Customer
                    $body = "#".$order['sequence_id']." order's status changed to ".str_replace('_', ' ', 'order_cancelled').".";
                    $tokens = UserFirebaseToken::where('user_id', $order['customer_id'])->pluck('firebase_token')->toArray();
                    $notification = array(
                        'title' => ucwords(str_replace('_', ' ', 'order_cancelled')),
                        'body' => $body,
                        'data' => array(
                            'order_id' => $order['id'],
                            'mode' => 'order_status_change',
                            'destination_screen' => 'order_status',
                            'notification_id' => $logId['id'],
                        )
                    );
                    $msg = '';
                    try {
                        $msg = $this->sendNotification($notification, $tokens);
                    } catch (\Throwable $th) {
                        
                    }
                    
                    $response = array(
                        'response_code' => 200,
                        'response_message' => 'Order Cancelled!'
                    );
                } else {
                    $response = array(
                        'response_code' => 500,
                        'response_message' => "Opps, Something went wrong please try again later!"
                    );
                }

            } else {
                $response = array(
                    'response_code' => 500,
                    'response_message' => "Order's process was started so you are not able to cancel it!"
                );
            }

        } else {
            $response = array(
                'response_code' => 500,
                'response_message' => "You are not able to cancel other customer's order!"
            );
        }
        
        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function checkPromoCode(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, array(
            'promo_code' => 'required'
        ));

        // process the login
        if ($validator->fails()) {

            $validatorString = implode(", ", $validator->messages()->all());
            $validatorArray = array_combine(
                array_map(function($key){ return $key.'_error'; }, array_keys($validator->errors()->toArray())),
                $validator->errors()->toArray()
            );

            $response = array(
                'response_code' => 400,
                'response_message' => $validatorString,
                'response_data' => $validatorArray
            );
            return response()->json($response, 200);
        }

        $currentUser = Auth::user();
        $promo = Promotion::select('id', 'name', 'code', 'description', 'sign_off_type', 'amount', 'limit_per_coupon', 'limit_per_customer')
            ->where('code', $data['promo_code'])
            ->whereDate('start_date', '<=', Carbon::now())
            ->whereDate('end_date', '>', Carbon::now());
            
        if ( isset($data['vendor_id']) && $data['vendor_id'] != '' && $data['vendor_id'] != null ) {
            $promo = $promo->whereRaw('(JSON_CONTAINS(vendors, "true", "$.'.$data['vendor_id'].'") OR JSON_CONTAINS(vendors, "true", "$.all"))');
        } else {
            $promo = $promo->whereRaw('JSON_CONTAINS(vendors, "true", "$.all")');
        }

        $promo = $promo->where('status', '1')
            ->where('is_deleted', '0')
            ->first();
        
        if ( $promo ) {

            $allowAccess = true;
            $couponCountInit = DB::table('coupon_count')
                ->selectRaw('SUM(total_orders) as total_orders')
                ->where('coupon_code', $data['promo_code']);
            $couponCount = $couponCountInit->get()->toArray();
            if ( isset($couponCount[0]) ) {
                if ( isset($promo['limit_per_coupon']) && $promo['limit_per_coupon'] != '' && $promo['limit_per_coupon'] != null ) {
                    if ( (int) $couponCount[0]->total_orders < $promo['limit_per_coupon'] ) {
                        if ( isset($promo['limit_per_customer']) && $promo['limit_per_customer'] != '' && $promo['limit_per_customer'] != null && $promo['limit_per_customer'] != 0 && isset($currentUser['id']) && $currentUser['id'] != '' && $currentUser['id'] != null ) {

                            $couponCountDealer = $couponCountInit->where('customer_id', $currentUser['id'])->get()->toArray();
                            if ( isset($couponCountDealer[0]) ) {
                                if ( (int) $couponCountDealer[0]->total_orders >= $promo['limit_per_customer'] ) {
                                    $allowAccess = false;
                                    $response = array(
                                        'response_code' => 500,
                                        'response_message' => 'Coupon code limit has expired for this customer.',
                                    );
                                }
                            }
                        }

                    } else {
                        $allowAccess = false;
                        $response = array(
                            'response_code' => 500,
                            'response_message' => 'Coupon code limit has expired.',
                        );
                    }   
                }
            }

            if ( $allowAccess ) {
                $response = array(
                    'response_code' => 200,
                    'response_message' => 'Promo Code Applied!',
                    'response_data' => $promo,
                );
            }
            
        } else {
            $response = array(
                'response_code' => 500,
                'response_message' => 'Invalid Promo Code',
            );
        }

        return response()->json($response, 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function photos($id)
    {
        $currentUser = Auth::user();
        
        $photos = Order::select('id')
            ->with(array('before_service_documents' => function($query){
                $query->select('id', 'order_id', 'type', 'document');
            }))
            ->with(array('after_service_documents' => function($query){
                $query->select('id', 'order_id', 'type', 'document');
            }))
            ->find($id);

        $response = array(
            'response_code' => 200,
            'response_message' => '',
            'response_data' => $photos
        );

        return response()->json($response, 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function status($id)
    {
        $currentUser = Auth::user();
        $orderStatusTimeline = array(
            array(
                'status' => 'order_recived',
                'title' => 'Order Received',
                'is_filled' => false
            ),
            array(
                'status' => 'order_confirmed',
                'title' => 'Order Confirmed',
                'is_filled' => false
            ),
            array(
                'status' => 'order_cancelled',
                'title' => 'Order Cancelled',
                'is_filled' => false
            ),
            array(
                'status' => 'team_enroute',
                'title' => 'Team Enroute',
                'is_filled' => false
            ),
            array(
                'status' => 'car_picked_up',
                'title' => 'Picked Up',
                'is_filled' => false
            ),
            array(
                'status' => 'service_started',
                'title' => 'Service Started',
                'is_filled' => false
            ),
            array(
                'status' => 'service_completed',
                'title' => 'Service Completed',
                'is_filled' => false
            ),
            array(
                'status' => 'delivered',
                'title' => 'Delivered',
                'is_filled' => false
            )
        );
        
        $logs = Log::select('id', 'common_id', 'action_type', 'action', 'description', 'new', 'action_by', 'created_at')
            /*->with(array('action_by' => function($query){
                $query->select('id', 'name');
            }))*/
            ->where('common_id', $id)
            ->where('action_type', 'order')
            ->whereIn('action', array('add', 'change_status'))
            ->orderBy('id', 'desc');

        $logs = $logs->get()->map(function ($log) {

            //$log = $log->toArray();
            
            $title = '';
            if ( $log['action'] == 'add' ) {
                $title = 'Received';
                $log['status'] = 'order_recived';
            } else if ( $log['action'] == 'change_status' ) {
                $title = ucwords(str_replace('_', ' ', $log['new']));
                $log['status'] = $log['new'];
                if ( isset($log[$log['action_type']]) && $log['new'] == 'team_enroute' ) {
                    //if ( $log[$log['action_type']]['pickup_type'] == 'pickup' ) {
                        $pickup_person = $log[$log['action_type']]->pickup_person;
                        $log['pickup_person'] = array(
                            'name' => $pickup_person->name,
                            'contact_no' => $pickup_person->contact_no,
                        );
                    //}
                }
            }
            $log['title'] = $title;
            $log['is_filled'] = true;

            unset($log['description']);
            unset($log[$log['action_type']]);
            return $log;
        });

        $onlyStatus = array_column($logs->toArray(), 'new');
        if ( in_array('order_confirmed', $onlyStatus) ) {
            unset($orderStatusTimeline[2]);
        }
        if ( in_array('order_cancelled', $onlyStatus) ) {
            unset($orderStatusTimeline[1]);
        }
        
        $orderStatusTimeline = array_values($orderStatusTimeline);
        foreach ($orderStatusTimeline as $k => $order) {
            foreach ($logs as $log) {
                if ( $order['status'] == 'order_recived' ) {
                    if ( $log['action'] == 'add' ) {
                        $orderStatusTimeline[$k] = $log;
                    }
                } else {
                    if ( $order['status'] == $log['new'] ) {
                        $orderStatusTimeline[$k] = $log;
                    }
                }
            }
        }
        
        $response = array(
            'response_code' => 200,
            'response_message' => '',
            'response_data' => $orderStatusTimeline
        );

        return response()->json($response, 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function vendorOrder(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, array(
            'type' => 'required',
            'filter' => 'required_if:type,waiting_for_pickup',
        ));

        // process the login
        if ($validator->fails()) {

            $validatorString = implode(", ", $validator->messages()->all());
            $validatorArray = array_combine(
                array_map(function($key){ return $key.'_error'; }, array_keys($validator->errors()->toArray())),
                $validator->errors()->toArray()
            );

            $response = array(
                'response_code' => 400,
                'response_message' => $validatorString,
                'response_data' => $validatorArray
            );
            return response()->json($response, 200);
        }
        
        $currentUser = Auth::user();
        if ( $data['type'] == "pending_for_approval" ) {
        
            $status = "order_received";
            $select = ['id', 'vendor_id', 'user_car_id', 'sequence_id', 'service_date', 'status', 'created_at as booking_date'];
        
        } else if ( $data['type'] == "waiting_for_pickup" ) {
        
            $status = "order_confirmed";
            $select = ['id', 'vendor_id', 'user_car_id', 'sequence_id'];
        
        } else if ( $data['type'] == "picked_up" ) {
        
            $status = "car_picked_up";
            $select = ['id', 'vendor_id', 'user_car_id', 'sequence_id', 'pickup_person_id'];
        
        } else if ( $data['type'] == "service_started" || $data['type'] == "service_completed" ) {
        
            $status = $data['type'];
            $select = ['id', 'vendor_id', 'user_car_id', 'sequence_id'];
        
        } else if ( $data['type'] == "drop" ) {
        
            $status = 'delivered';
            $select = ['id', 'vendor_id', 'user_car_id', 'sequence_id', 'payment_status'];
        
        }

        $orders = Order::select($select)
            ->where('vendor_id', $currentUser['vendor_id'])
            ->where('status', $status)
            ->with(array('customer_car' => function($query){
                $query->select('id', 'car_model_id', 'car_number')->with(array('car_model' => function($query){
                    $query->select('id', 'brand_id', 'name', 'image')->with('brand:id,name');
                }));
            }));

        if ( $data['type'] == "waiting_for_pickup" ) {
            if ( $data['filter'] == 'today' ) {
                $orders = $orders->whereDate('service_date', '<=', Carbon::now());
            } else {
                $orders = $orders->whereDate('service_date', '>', Carbon::now());
            }
        } else if ( $data['type'] == "picked_up" ) {
            $orders = $orders->with('pickup_person:id,name,contact_no');
        }

        $orders = $orders->where('is_deleted', 0)
            ->get();

        $response = array(
            'response_code' => 200,
            'response_message' => '',
            'response_data' => $orders
        );

        return response()->json($response, 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function changeStatus(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, array(
            'order_id' => 'required',
            'action' => 'required',
            'reason_for_cancellation' => 'required_if:action,decline',
            'additional_comments' => 'required_if:action,decline',
            'pickup_person_id' => 'required_if:action,team_enroute',
        ));

        // process the login
        if ($validator->fails()) {

            $validatorString = implode(", ", $validator->messages()->all());
            $validatorArray = array_combine(
                array_map(function($key){ return $key.'_error'; }, array_keys($validator->errors()->toArray())),
                $validator->errors()->toArray()
            );

            $response = array(
                'response_code' => 400,
                'response_message' => $validatorString,
                'response_data' => $validatorArray
            );
            return response()->json($response, 200);
        }
        
        $updateData = array();
        $currentUser = Auth::user();

        if ( $data['action'] == "approved" ) {
            $updateData['status'] = "order_confirmed";
        } else if ( $data['action'] == "decline" ) {
            $updateData['status'] = "order_cancelled";
            $updateData['reason_for_cancellation'] = $data['reason_for_cancellation'];
        } else if ( $data['action'] == "team_enroute" ) {
            $updateData['status'] = "team_enroute";
            $updateData['pickup_person_id'] = $data['pickup_person_id'];
        } else if ( $data['action'] == "service_start" ) {
            $updateData['status'] = "service_started";
        } else if ( $data['action'] == "service_complete" ) {
            $updateData['status'] = "service_completed";
        }

        $order = Order::find($data['order_id']);
        if ( $order->update($updateData) ) {
            
            $response = array(
                'response_code' => 200,
                'response_message' => 'Updated!'
            );
        } else {
            $response = array(
                'response_code' => 500,
                'response_message' => "Opps, Something went wrong please try again later!"
            );
        }

        return response()->json($response, 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function uploadDocuments(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, array(
            'order_id' => 'required',
            'type' => 'required',
            'documents' => 'required',
            'with_change_status' => 'required',
        ));

        // process the login
        if ($validator->fails()) {

            $validatorString = implode(", ", $validator->messages()->all());
            $validatorArray = array_combine(
                array_map(function($key){ return $key.'_error'; }, array_keys($validator->errors()->toArray())),
                $validator->errors()->toArray()
            );

            $response = array(
                'response_code' => 400,
                'response_message' => $validatorString,
                'response_data' => $validatorArray
            );
            return response()->json($response, 200);
        }
        
        $updateData = array();
        $currentUser = Auth::user();

        $subpath = "public/uploads/orders/".$data['order_id']."/".$data['type']."/";
        foreach ($data['documents'] as $k => $document) {
            
            $documentData = array(
                'order_id' => $data['order_id'],
                'type' => $data['type'],
            );

            $fileStoreName = Carbon::now()->timestamp . $k .'.' . $document->getClientOriginalExtension();
            $path = storage_path('app') . "/" . $subpath;
            if( !File::isDirectory($path) ){
                File::makeDirectory($path, 0755, true, true);
            }
            
            if ( $document->move($path, $fileStoreName) ) {
                $documentData['document'] = $fileStoreName;
            }

            $upload = OrderDocument::create($documentData);
            if ( $upload ) {
                array_push($updateData, $k);
            }
        }
        
        if ( count($data['documents']) == count($updateData) ) {

            if ( isset($data['with_change_status']) && $data['with_change_status'] == '1' ) {
                if ( isset($data['type']) && $data['type'] == 'before_service' ) {
                    $orderDataUpdate['status'] = 'car_picked_up';
                } else {
                    $orderDataUpdate['status'] = 'delivered';
                }

                Order::find($data['order_id'])->update($orderDataUpdate);
            }
            
            $response = array(
                'response_code' => 200,
                'response_message' => 'Documents Uploaded!'
            );
        } else {
            $response = array(
                'response_code' => 500,
                'response_message' => "Opps, Something went wrong please try again later!"
            );
        }

        return response()->json($response, 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getServiceCost(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, array(
            'sub_service_id' => 'required',
            'car_model_id' => 'required'
        ));

        // process the login
        if ($validator->fails()) {

            $validatorString = implode(", ", $validator->messages()->all());
            $validatorArray = array_combine(
                array_map(function($key){ return $key.'_error'; }, array_keys($validator->errors()->toArray())),
                $validator->errors()->toArray()
            );

            $response = array(
                'response_code' => 400,
                'response_message' => $validatorString,
                'response_data' => $validatorArray
            );
            return response()->json($response, 200);
        }
        
        $currentUser = Auth::user();
        $serviceCost = $this->serviceCost($data['sub_service_id'], $data['car_model_id'], $currentUser['vendor_id']);
        
        $response = array(
            'response_code' => 200,
            'response_message' => '',
            'response_data' => array(
                'cost' => $serviceCost
            )
        );

        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function recommendationStore(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, array(
            'custom_services' => 'required',
        ));

        // process the login
        if ($validator->fails()) {

            $validatorString = implode(", ", $validator->messages()->all());
            $validatorArray = array_combine(
                array_map(function($key){ return $key.'_error'; }, array_keys($validator->errors()->toArray())),
                $validator->errors()->toArray()
            );

            $response = array(
                'response_code' => 400,
                'response_message' => $validatorString,
                'response_data' => $validatorArray
            );
            return response()->json($response, 200);
        }

        $currentUser = Auth::user();
        //dd($data);

        $mode = 'order';
        $originalOrder = Order::with('items')->find($id)->toArray();
        if ( $originalOrder['is_awaiting_for_approval'] == 1 ) {
            $mode = 'temp_order';
            $originalOrder = TempOrder::with('items')->find($originalOrder['temp_order_id'])->toArray();
        }
        //dd($data, $mode, $originalOrder);

        $tempOrderData = $originalOrder;
        $tempOrderData['is_awaiting_for_approval'] = '1';
        if ( $originalOrder['payment_status'] != 'pending' ) {
            $tempOrderData['payment_status'] = 'partially_paid';
        }

        $tempOrder = TempOrder::updateOrCreate(
            ['order_id' => $id],
            $tempOrderData
        );

        if ( $tempOrder ) {
            
            // Update Original Order Data
            $originalOrderUpdateData = ['temp_order_id' => $tempOrder->id, 'is_awaiting_for_approval' => '1'];
            if ( $originalOrder['payment_status'] != 'pending' ) {
                $originalOrderUpdateData['payment_status'] = 'partially_paid';
            }
            Order::find($id)->update($originalOrderUpdateData);

            // Sync Order Items
            TempOrderItem::where('temp_order_id', $tempOrder->id)->delete();
            $orderItems = OrderItem::where('order_id', $id)->get();
            foreach ($orderItems as $ori_item) {

                $insert = true;
                if ( $mode == 'temp_order' ) {
                    if ( !$ori_item['is_custom'] ) {
                        $checkItem = TempOrderItem::where('temp_order_id', $tempOrder['id'])->where('service_id', $ori_item['service_id'])->first();
                    } else {
                        $checkItem = TempOrderItem::where('temp_order_id', $tempOrder['id'])->where('service_name', $ori_item['service_name'])->where('cost_type', $ori_item['cost_type'])->first();
                    }
                    if ( $checkItem ) {
                        $insert = false;
                    }
                }

                if ( $insert ) {
                    $tempItemData1 = $ori_item;
                    $tempItemData1['temp_order_id'] = $tempOrder['id'];
                    TempOrderItem::create($tempItemData1->toArray());
                }
            }
            /*if ( isset($data['main_services']) && count($data['main_services']) > 0 ) {
                foreach ($data['main_services'] as $k => $ms) {

                    if ( $mode == 'temp_order' ) {
                        $checkItem = TempOrderItem::where('temp_order_id', $tempOrder['id'])->where('service_id', $ms)->first();
                        if ( !$checkItem ) {
                            $orderItem = OrderItem::where('order_id', $tempOrder['order_id'])->where('service_id', $ms)->first()->toArray();
                            $orderItem['temp_order_id'] = $tempOrder['id'];
                            TempOrderItem::create($orderItem);
                        }
                    } else {
                        $orderItem = OrderItem::where('order_id', $id)->where('service_id', $ms)->first()->toArray();
                        $orderItem['temp_order_id'] = $tempOrder['id'];
                        TempOrderItem::create($orderItem);
                    }
                }
            }*/

            if ( isset($data['custom_services']) && count($data['custom_services']) > 0 ) {
                foreach ($data['custom_services'] as $index => $service) {

                    if ( isset($service['is_custom']) ) {
                        
                        if ( !$service['is_custom'] ) {

                            $tempItemData = array(
                                'temp_order_id' => $tempOrder->id
                            );
                            $tempItemData['cost_type'] = 'service_cost';
                            $tempItemData['cost'] = $service['labour_cost'];
                            $tempItemData['tax'] = ((isset($service['labour_tax']) && $service['labour_tax'] != '') ? $service['labour_tax'] : 18);
                            $tempItemData['total'] = $tempItemData['cost'] + ($tempItemData['cost'] * ($tempItemData['tax']/100));

                            $tempItemData['service_id'] = $service['service_id'];
                            $parent_service_id = Service::select('parent_id', 'name')->find($tempItemData['service_id']);
                            $tempItemData['service_name'] = $parent_service_id['name'];
                            $tempItemData['parent_service_id'] = $parent_service_id['parent_id'];

                            TempOrderItem::updateOrCreate(
                                ['temp_order_id' => $tempOrder->id, 'service_id' => $service['service_id']],
                                $tempItemData
                            );

                        } else {

                            if ( isset($service['service_name']) && $service['service_name'] != '' ) {

                                $associate_id = '';
                                if ( isset($service['parts_cost']) && $service['parts_cost'] != '' && $service['parts_cost'] > 0 ) {

                                    $tempItemData = array(
                                        'temp_order_id' => $tempOrder->id
                                    );
                                    $tempItemData['cost_type'] = 'part_cost';
                                    $tempItemData['cost'] = $service['parts_cost'];
                                    $tempItemData['tax'] = $service['parts_tax'];
                                    $tempItemData['total'] = $tempItemData['cost'] + ($tempItemData['cost'] * ($tempItemData['tax']/100));
                                    $tempItemData['service_name'] = $service['service_name'];
                                    $tempItemData['is_custom'] = 1;

                                    $partsCostData = TempOrderItem::updateOrCreate(
                                        ['temp_order_id' => $tempOrder->id, 'service_name' => $service['service_name'], 'cost_type' => 'part_cost'],
                                        $tempItemData
                                    );
                                    if ($partsCostData) {
                                        $associate_id = $partsCostData['id'];
                                    }
                                }

                                if ( isset($service['labour_cost']) && $service['labour_cost'] != '' && $service['labour_cost'] > 0 ) {

                                    $tempItemData = array(
                                        'temp_order_id' => $tempOrder->id
                                    );
                                    $tempItemData['cost_type'] = 'service_cost';
                                    $tempItemData['cost'] = $service['labour_cost'];
                                    $tempItemData['tax'] = ((isset($service['labour_tax']) && $service['labour_tax'] != '') ? $service['labour_tax'] : 18);
                                    $tempItemData['total'] = $tempItemData['cost'] + ($tempItemData['cost'] * ($tempItemData['tax']/100));
                                    $tempItemData['service_name'] = $service['service_name'];
                                    $tempItemData['is_custom'] = 1;
                                    $tempItemData['associate_id'] = $associate_id;
                                    //TempOrderItem::create($tempItemData);
                                    TempOrderItem::updateOrCreate(
                                        ['temp_order_id' => $tempOrder->id, 'service_name' => $service['service_name'], 'cost_type' => 'service_cost'],
                                        $tempItemData
                                    );
                                }
                            }
                        }
                    }
                }
            }

            // Send Notification to Customer
            $tokens = UserFirebaseToken::where('user_id', $originalOrder['customer_id'])->pluck('firebase_token')->toArray();
            $notification = array(
                'title' => "Service Recommended",
                'body' => "Vendor recommended few services for your order #".$originalOrder['sequence_id'].", Please Accept/Decline recommendation",
                'data' => array(
                    'order_id' => $originalOrder['id'],
                    'mode' => 'order_recommendation_feedback',
                    'destination_screen' => 'order_job_card',
                )
            );
            $msg = '';
            try {
                $msg = $this->sendNotification($notification, $tokens);
            } catch (\Throwable $th) {
                
            }

            $response = array(
                'response_code' => 200,
                'response_message' => 'Order changes successfully updated!, All changes under approval.'
            );
        } else {
            $response = array(
                'response_code' => 500,
                'response_message' => "Opps, Something went wrong please try again later!"
            );
        }

        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function paymentReceived(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, array(
            'payment_type' => 'required',
            'vendor_OTP' => 'required',
            //'razorpay_payment_id' => 'required_unless:payment_type,cash_on_delivery'
        ));

        // process the login
        if ($validator->fails()) {

            $validatorString = implode(", ", $validator->messages()->all());
            $validatorArray = array_combine(
                array_map(function($key){ return $key.'_error'; }, array_keys($validator->errors()->toArray())),
                $validator->errors()->toArray()
            );

            $response = array(
                'response_code' => 400,
                'response_message' => $validatorString,
                'response_data' => $validatorArray
            );
            return response()->json($response, 200);
        }

        $currentUser = Auth::user();
        $originalOrder = Order::find($id);
        if ( $originalOrder ) {
            $originalOrder = $originalOrder->toArray();

            $paymentData = array(
                'order_id' => $id,
                'vendor_id' => $originalOrder['vendor_id'],
                'customer_id' => $originalOrder['customer_id'],
                'payment_type' => 'credited',
                'amount' => $originalOrder['total'],
                'payment_via' => $data['payment_type'],
                'payment_details' => serialize($data),
                'status' => 'settled',
            );

            // find seq id
            $prefix = Config::get('custom.carsseva.prefix.transaction');
            $vendor = Vendor::select('code')->find($originalOrder['vendor_id']);
            $lastOrder = Payment::select('transaction_sequence_id')->where('vendor_id', $originalOrder['vendor_id'])->orderBy('ID', 'desc')->first();
            if ( isset($lastOrder->transaction_sequence_id) ) {
                $paymentData['transaction_sequence_id'] = $this->increaseSeqNo($lastOrder->transaction_sequence_id, $vendor->code.$prefix);
            } else {
                $paymentData['transaction_sequence_id'] = $vendor->code.$prefix.'0001';
            }
            
            $payment = Payment::create($paymentData);
            if ( $payment ) {
                Order::find($id)->update(array('payment_status' => 'paid'));

                /* $logData = array('common_id' => $id, 'action_type' => 'order', 'action' => 'change_status', 'old' => $originalOrder['status'], 'new' => 'delivered', 'description' => '', 'action_by' => $currentUser['id'], 'user_ip' => $request->ip(), 'is_read' => json_encode(array($currentUser['id'] => false)));
                $logId = Log::create($logData); */

                // Send Notification to Vendor
                $vendorAdmin = User::select('id')
                    ->where('vendor_id', $originalOrder['vendor_id'])
                    ->where('role', 'vendor_admin')
                    ->first();
                $tokens = UserFirebaseToken::where('user_id', $vendorAdmin['id'])->pluck('firebase_token')->toArray();
                $notification = array(
                    'title' => "Payment Received",
                    'body' => "#".$originalOrder['sequence_id']." order's payment received via COD",
                    'data' => array(
                        'order_id' => $originalOrder['id'],
                        'mode' => 'payment_received',
                        'destination_screen' => 'payment_received',
                    )
                );
                $msg = '';
                try {
                    $this->sendNotification($notification, $tokens);
                } catch (\Throwable $th) {
    
                }                

                $response = array(
                    'response_code' => 200,
                    'response_message' => 'Order payment successfully received!',
                    'response_data' => array(
                        'transaction_sequence_id' => $payment['transaction_sequence_id'],
                        'status' => $payment['status']
                    )
                );

            } else {
                $response = array(
                    'response_code' => 500,
                    'response_message' => "Opps, Something went wrong please try again later!"
                );
            }

        } else {
            $response = array(
                'response_code' => 500,
                'response_message' => "Invalid Order!"
            );
        }

        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function razorpayPaymentReceived(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, array(
            'payment_status' => 'required',
            'razorpay_payment_id' => 'required',
        ));

        // process the login
        if ($validator->fails()) {

            $validatorString = implode(", ", $validator->messages()->all());
            $validatorArray = array_combine(
                array_map(function($key){ return $key.'_error'; }, array_keys($validator->errors()->toArray())),
                $validator->errors()->toArray()
            );

            $response = array(
                'response_code' => 400,
                'response_message' => $validatorString,
                'response_data' => $validatorArray
            );
            return response()->json($response, 200);
        }

        $currentUser = Auth::user();
        $originalOrder = Order::find($id);
        if ( $originalOrder ) {
            $originalOrder = $originalOrder->toArray();
            
            if ( isset($data['payment_status']) && (/* $data['payment_status'] ||  */$data['payment_status'] == 'true') ) {
                
                $paymentData = array(
                    'order_id' => $id,
                    'vendor_id' => $originalOrder['vendor_id'],
                    'customer_id' => $originalOrder['customer_id'],
                    'payment_type' => 'credited',
                    'amount' => $originalOrder['total'],
                    'payment_via' => 'card',
                    'payment_details' => serialize($data),
                    'status' => 'settled',
                );
    
                // find seq id
                $prefix = Config::get('custom.carsseva.prefix.transaction');
                $vendor = Vendor::select('code')->find($originalOrder['vendor_id']);
                $lastOrder = Payment::select('transaction_sequence_id')->where('vendor_id', $originalOrder['vendor_id'])->orderBy('ID', 'desc')->first();
                if ( isset($lastOrder->transaction_sequence_id) ) {
                    $paymentData['transaction_sequence_id'] = $this->increaseSeqNo($lastOrder->transaction_sequence_id, $vendor->code.$prefix);
                } else {
                    $paymentData['transaction_sequence_id'] = $vendor->code.$prefix.'0001';
                }
                
                $payment = Payment::create($paymentData);
                if ( $payment ) {
                    // 'status' => 'delivered', 
                    Order::find($id)->update(array('payment_type' => 'card', 'payment_status' => 'paid'));
    
                    /* $logData = array('common_id' => $id, 'action_type' => 'order', 'action' => 'change_status', 'old' => $originalOrder['status'], 'new' => 'delivered', 'description' => '', 'action_by' => $currentUser['id'], 'user_ip' => $request->ip(), 'is_read' => json_encode(array($currentUser['id'] => false)));
                    $logId = Log::create($logData); */
                    
                    // Send Notification to Vendor
                    $vendorAdmin = User::select('id')
                        ->where('vendor_id', $originalOrder['vendor_id'])
                        ->where('role', 'vendor_admin')
                        ->first();
                    $tokens = UserFirebaseToken::where('user_id', $vendorAdmin['id'])->pluck('firebase_token')->toArray();
                    $notification = array(
                        'title' => "Payment Received",
                        'body' => "#".$originalOrder['sequence_id']." order's payment received via COD",
                        'data' => array(
                            'order_id' => $originalOrder['id'],
                            'mode' => 'payment_received',
                            'destination_screen' => 'payment_received',
                        )
                    );
                    $msg = '';
                    try {
                        $this->sendNotification($notification, $tokens);
                    } catch (\Throwable $th) {
        
                    }                
    
                    $response = array(
                        'response_code' => 200,
                        'response_message' => 'Order payment successfully received!',
                        'response_data' => array(
                            'transaction_sequence_id' => $payment['transaction_sequence_id'],
                            'status' => $payment['status']
                        )
                    );
    
                } else {
                    $response = array(
                        'response_code' => 500,
                        'response_message' => "Opps, Something went wrong please try again later!"
                    );
                }

            } else {
                /* log */
                $logData = array('common_id' => $originalOrder['id'], 'action_type' => 'order', 'action' => 'payment_failed', 'old' => '', 'new' => '', 'description' => serialize($data), 'action_by' => $originalOrder['customer_id'], 'user_ip' => $request->ip(), 'is_read' => json_encode(array($originalOrder['customer_id'] => false)));
                $logId = Log::create($logData);
                /* log */

                $response = array(
                    'response_code' => 200,
                    'response_message' => 'Order payment failed status tracked!',
                    'response_data' => array()
                );
            }

        } else {
            $response = array(
                'response_code' => 500,
                'response_message' => "Invalid Order!"
            );
        }

        return response()->json($response, 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function makeHash(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, array(
            'key' => 'required',
            'txnid' => 'required',
            'amount' => 'required',
            'productinfo' => 'required',
            'firstname' => 'required',
            'email' => 'required'
        ));

        // process the login
        if ($validator->fails()) {

            $validatorString = implode(", ", $validator->messages()->all());
            $validatorArray = array_combine(
                array_map(function($key){ return $key.'_error'; }, array_keys($validator->errors()->toArray())),
                $validator->errors()->toArray()
            );

            $response = array(
                'response_code' => 400,
                'response_message' => $validatorString,
                'response_data' => $validatorArray
            );
            return response()->json($response, 200);
        }

        $salt = "4MWtkeP7p"; //Please change the value with the live salt for production environment
        $payhash_str = $data['key'] . '|' . $this->checkNull($data['txnid']) . '|' . $this->checkNull($data['amount']) . '|' . $this->checkNull($data['productinfo']) . '|' . $this->checkNull($data['firstname']) . '|' .$this-> checkNull($data['email']) . '|||||||||||' . $salt;
        $hash = strtolower(hash('sha512', $payhash_str));

        $response = array(
            'response_code' => 200,
            'response_message' => '',
            'response_data' => array(
                'hash' => $hash
            )
        );

        return response()->json($response, 200);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function payNow(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, array(
            'order_id' => 'required'
        ));

        // process the login
        if ($validator->fails()) {

            $validatorString = implode(", ", $validator->messages()->all());
            $validatorArray = array_combine(
                array_map(function($key){ return $key.'_error'; }, array_keys($validator->errors()->toArray())),
                $validator->errors()->toArray()
            );

            $response = array(
                'response_code' => 400,
                'response_message' => $validatorString,
                'response_data' => $validatorArray
            );
            return response()->json($response, 200);
        }

        $order = Order::find($data['order_id']);
        if ( $order ) {
            $amountInPaisa = number_format(round($order['total']*100, 2), 2, '.', '');
            $response_data = array(
                'description' => 'Credits towards services',
                'image' => 'https://cdn.razorpay.com/logos/EIeHUaHxUM9gFd_medium.png',
                'currency' => 'INR',
                'amount' => $amountInPaisa,
                'prefill' => array(
                    'email' => $order->customer->email,
                    'contact' => $order->customer->contact_no,
                    'name' => $order->customer->name,
                ),
                'theme' => array(
                    'color' => '#ff0000'
                ),
            );
            
            if(config('app.env') == 'prod'){
                $response_data['key'] = 'rzp_live_lEiQCKTM89fYFl';
                $response_data['name'] = 'Carseva Autocare Pvt Ltd';
            } else {
                $response_data['key'] = 'rzp_test_9hvOPrimeW3OC1';
                $response_data['name'] = 'Carseva Autocare Pvt Ltd (Beta)';
            }

            $response = array(
                'response_code' => 200,
                'response_message' => '',
                'response_data' => $response_data
            );

        } else {
            $response = array(
                'response_code' => 500,
                'response_message' => "Order not available in our portal."
            );
        }
        
        return response()->json($response, 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function checkNull($value)
    {
        if ($value == null) {
            return '';
        } else {
            return $value;
        }
    }
}
