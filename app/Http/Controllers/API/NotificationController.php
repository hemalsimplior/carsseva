<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Log;
use Auth, Validator, DB;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currentUser = Auth::user();
        
        $logs = Log::select('id', 'common_id', 'action_type', 'action', 'new', 'action', 'description', 'is_read', 'created_at')
            ->where('action_type', 'order')
            ->whereIn('action', array('add', 'change_status', 'payment_failed'))
            ->whereRaw('JSON_CONTAINS(is_read, "false", "$.'.$currentUser['id'].'") OR JSON_CONTAINS(is_read, "true", "$.'.$currentUser['id'].'")')
            ->orderBy('id', 'desc');

        $logs = $logs->get()->map(function ($log) use($currentUser) {

            $isReadArr = json_decode($log->is_read, true);
            $log = $log->toArray();
            $log['mode'] = $log['action_type'].'_'.$log['action'];
            
            if ( isset($log['order']['payments']) ) {
                $payment = $log['order']['payments'];
            } else {
                $payment = null;
            }
            $log['destination_screen'] = $this->destinationScreenApp($log['action_type'], $log['action'], $log['new'], $payment);

            $log['is_read'] = $isReadArr[$currentUser['id']];

            unset($log[$log['action_type']]);
            unset($log['action_type']);
            unset($log['new']);
            unset($log['action']);
            return $log;
        });

        $response = array(
            'response_code' => 200,
            'response_data' => $logs
        );

        return response()->json($response, 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCount()
    {
        $currentUser = Auth::user();
        //$_mode = $request->header('Accept-Mode');

        $logs = Log::select('id', 'is_read')
            ->whereRaw('JSON_CONTAINS(is_read, "false", "$.'.$currentUser['id'].'")')
            ->where('action_type', 'order')
            ->whereIn('action', array('add', 'change_status', 'payment_failed'))
            ->count();
        
        $response = array(
            'response_code' => 200,
            'response_data' => array(
                'unread_count' => $logs
            )
        );

        return response()->json($response, 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function markRead(Request $request)
    {
        $data = $request->all();
        $_mode = $request->header('Accept-Mode');

        $validator = Validator::make($data, array(
            'id' => 'required'
        ));

        // process the login
        if ($validator->fails()) {

            $validatorString = implode(", ", $validator->messages()->all());
            $validatorArray = array_combine(
                array_map(function($key){ return $key.'_error'; }, array_keys($validator->errors()->toArray())),
                $validator->errors()->toArray()
            );

            $response = array(
                'response_code' => 400,
                'response_message' => $validatorString,
                'response_data' => $validatorArray
            );
            return response()->json($response, 200);
        }
        
        $currentUser = Auth::user();
        $log = Log::find($data['id']);
        if ( $log ) {
            
            $update_data = array();
            $isReadArr = json_decode($log['is_read'], true);
            if ( array_key_exists($currentUser['id'], $isReadArr) ) {
                $update_data['is_read->'.$currentUser['id']] = true;
            }

            // Update is_read flag
            $upd = Log::where('id', $data['id'])->update($update_data);
            if ( $upd ) {
                $response = array(
                    'response_code' => 200,
                    'response_message' => 'Notification has been read!'
                );
            } else {
                $response = array(
                    'response_code' => 500,
                    'response_message' => "Opps, Something went wrong please try again later!"
                );
            }
            
        } else {
            $response = array(
                'response_code' => 200,
                'response_message' => 'Notification not available in our portal.'
            );
        }

        return response()->json($response, 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function clear()
    {
        $currentUser = Auth::user();
        //$_mode = $request->header('Accept-Mode');
        $update_data = array(
            'is_read' => DB::raw('JSON_REMOVE(is_read, "$.'.$currentUser['id'].'")')
        );

        $logs = Log::select('id', 'common_id', 'action_type', 'action', 'new', 'action', 'description', 'is_read', 'created_at')
            ->whereRaw('JSON_CONTAINS(is_read, "false", "$.'.$currentUser['id'].'") OR JSON_CONTAINS(is_read, "true", "$.'.$currentUser['id'].'")')
            ->where('action_type', 'order')
            ->whereIn('action', array('add', 'change_status', 'payment_failed'));

        // Update is_read flag
        $upd = $logs->update($update_data);
        if ( $upd ) {
            $response = array(
                'response_code' => 200,
                'response_message' => 'Notification has been cleared!'
            );
        } else {
            $response = array(
                'response_code' => 500,
                'response_message' => "Opps, Something went wrong please try again later!"
            );
        }

        return response()->json($response, 200);
    }
}
