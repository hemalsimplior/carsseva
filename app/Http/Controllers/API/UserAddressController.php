<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\UserAddress;
use Auth, Validator;

class UserAddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currentUser = Auth::user();
        $addresses = UserAddress::select('id', 'friendly_name', 'address_type', 'address_line_1', 'address_line_2', 'state_id', 'city_id', 'latitude', 'longitude', 'zipcode', 'is_primary')
            ->with(array('state' => function($query){
                $query->select('id', 'country_id', 'name')->with('country:id,name');
            }))
            ->with('city:id,name')
            ->where('user_id', $currentUser->id)
            ->where('is_deleted', '0')
            ->get();

        $response = array(
            'response_code' => 200,
            'response_data' => array(
                'tax_amount' => 18,
                'addresses' => $addresses
            )
        );

        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, array(
            'friendly_name' => 'required',
            'address_type' => 'required',
            'address_line_1' => 'required',
            'state_id' => 'required|integer',
            'city_id' => 'required|integer',
            'zipcode' => 'required|size:6',
            'latitude' => 'required',
            'longitude' => 'required',
        ));

        // process the login
        if ($validator->fails()) {

            $validatorString = implode(", ", $validator->messages()->all());
            $validatorArray = array_combine(
                array_map(function($key){ return $key.'_error'; }, array_keys($validator->errors()->toArray())),
                $validator->errors()->toArray()
            );

            $response = array(
                'response_code' => 400,
                'response_message' => $validatorString,
                'response_data' => $validatorArray
            );
            return response()->json($response, 200);
        }

        $currentUser = Auth::user();
        if ( $currentUser->primary_address == null ) {
            $data['is_primary'] = 1;
        }
        $data['user_id'] = $currentUser->id;
        
        $mycar = UserAddress::create($data);
        if( $mycar ){ 
            
            $address = UserAddress::select('id', 'friendly_name', 'address_type', 'address_line_1', 'address_line_2', 'state_id', 'city_id', 'latitude', 'longitude', 'zipcode', 'is_primary')
                ->with(array('state' => function($query){
                    $query->select('id', 'country_id', 'name')->with('country:id,name');
                }))
                ->with('city:id,name')
                ->find($mycar->id);
            
            $response = array(
                'response_code' => 200,
                'response_message' => 'Stored!',
                'response_data' => $address
            );
        } else {
            $response = array(
                'response_code' => 500,
                'response_message' => "Opps, Something went wrong please try again later!"
            );
        }

        return response()->json($response, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $myAddress = UserAddress::where('is_deleted', 0)->find($id);
        if ( $myAddress ) {
            
            $data = $request->all();
            $validator = Validator::make($data, array(
                'friendly_name' => 'required',
                'address_type' => 'required',
                'address_line_1' => 'required',
                'state_id' => 'required|integer',
                'city_id' => 'required|integer',
                'zipcode' => 'required|size:6',
                'latitude' => 'required',
                'longitude' => 'required',
            ));
    
            // process the login
            if ($validator->fails()) {
    
                $validatorString = implode(", ", $validator->messages()->all());
                $validatorArray = array_combine(
                    array_map(function($key){ return $key.'_error'; }, array_keys($validator->errors()->toArray())),
                    $validator->errors()->toArray()
                );
    
                $response = array(
                    'response_code' => 400,
                    'response_message' => $validatorString,
                    'response_data' => $validatorArray
                );
                return response()->json($response, 200);
            }
    
            $currentUser = Auth::user();
            
            if( $myAddress->update($data) ){ 
                
                $address = UserAddress::select('id', 'friendly_name', 'address_type', 'address_line_1', 'address_line_2', 'state_id', 'city_id', 'latitude', 'longitude', 'zipcode', 'is_primary')
                    ->with(array('state' => function($query){
                        $query->select('id', 'country_id', 'name')->with('country:id,name');
                    }))
                    ->with('city:id,name')
                    ->find($id);

                $response = array(
                    'response_code' => 200,
                    'response_message' => 'Updated!',
                    'response_data' => $address
                );
            } else {
                $response = array(
                    'response_code' => 500,
                    'response_message' => "Opps, Something went wrong please try again later!"
                );
            }
            
        } else {
            $response = array(
                'response_code' => 500,
                'response_message' => "Address not available in our portal."
            );
        }

        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = array(
            'is_deleted' => 1
        );

        $myAddress = UserAddress::find($id);
        if( $myAddress->update($data) ){ 

            $response = array(
                'response_code' => 200,
                'response_message' => 'Deleted!'
            );
        } else {
            $response = array(
                'response_code' => 500,
                'response_message' => "Opps, Something went wrong please try again later!"
            );
        }

        return response()->json($response, 200);
    }
}
