<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Auth;
use App\Models\VendorServiceSetting, App\Models\VendorCarModelSetting, App\Models\Vendor;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function artisan($command)
    {
        $command = str_replace('@', ':', $command);
        $exitCode = \Artisan::call($command);
        $output = \Artisan::output();
        dd($output);
    }
    
    public function initials($str, $reCheck = false, $reStr = '') {
        $ret = '';

        $str = trim($str);
        foreach (explode(' ', $str) as $word)
            $ret .= strtoupper($word[0]);
        
        if ( $reCheck ) {
            $num = str_replace(trim($reStr), '', trim($str));
            $num = (int)$num + 1;
            $ret = $ret . $num;
        }
        $check = Vendor::where('code', $ret)->count();
        if ( $check > 0 ) {
            return $this->initials($str, true, $ret);
        } else {
            return $ret;
        }
    }
    
    public function increaseSeqNo($string, $prefixStr = '') {
        
        $num = str_replace(trim($prefixStr), '', trim($string));
        $num = (int)$num + 1;
        $num = str_pad($num, 4, '0', STR_PAD_LEFT);

        if ( $prefixStr != '' ) {
            return $prefixStr . $num;
        } else {
            return $num;
        }
    }

    public function serviceCost($service_id, $car_model_id, $vendor_id = '') {
        $currentUser = Auth::user();
        if ( $vendor_id == '' ) {
            $vendor_id = $currentUser->vendor_id;
        }

        // Get Cost
        $serviceSettings = VendorServiceSetting::where('service_id', $service_id)
            ->where('vendor_id', $vendor_id)
            ->where('status', '1')
            ->first();
        if ( !$serviceSettings ) {
            $serviceSettings = VendorServiceSetting::where('service_id', $service_id)
                ->where('vendor_id', 0)
                ->first();
        }

        // Get Segment
        $modelSettings = VendorCarModelSetting::select('segment')
            ->where('car_model_id', $car_model_id)
            ->where('vendor_id', $vendor_id)
            ->first();
        if ( !$modelSettings ) {
            $modelSettings = VendorCarModelSetting::select('segment')
                ->where('car_model_id', $car_model_id)
                ->where('vendor_id', 0)
                ->first();
        }

        $cost = 0;
        if ( isset($serviceSettings[$modelSettings['segment']]) ) {
            $cost = $serviceSettings[$modelSettings['segment']];
        }
        return $cost;
    }

    public function sendNotification($notificationData, $token)
    {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);

        $notificationBuilder = new PayloadNotificationBuilder($notificationData['title']);
        $notificationBuilder->setBody($notificationData['body'])->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData($notificationData['data']);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        //$token = "e9hlXDr3R_0:APA91bEyZkSDPLJihkD2-su8aHkiL-ob5fwX5qHkm6iZmVb7o2THPI1DRI9ccFKsSTRSjdMUr4lr2UT3bKt-N50R9Gj8NSFUDkiPyUcDJtj56D93CKy9yavPXU_kYXxPVrKdmDCAY3KB";
        $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
        
        return array(
            'numberSuccess' => $downstreamResponse->numberSuccess(),
            'numberFailure' => $downstreamResponse->numberFailure(),
            'numberModification' => $downstreamResponse->numberModification(),
        );
    }

    public function checkDistance($latitudeFrom, $latitudeTo, $longitudeFrom, $longitudeTo)
    {
        $distance = '';
        $outputFrom = '';
        $geocodeFrom = file_get_contents('https://maps.googleapis.com/maps/api/distancematrix/json?origins='.$latitudeFrom.','.$longitudeFrom.'&destinations='.$latitudeTo.','.$longitudeTo.'&key=AIzaSyBNcEJms1i-QoRa03JIqzgDNhQf8hv1H9M');
        $outputFrom = json_decode($geocodeFrom, true);
        if ( $outputFrom['status'] === 'OK' ) {
            $distance = $outputFrom['rows'][0]['elements'][0]['distance']['text'];
        }

        return $distance;
    }

    public function destinationScreenApp($action_type, $action, $status, $payment)
    {
        $destination_screen = '';
        if ( $action_type == 'order' ) {
            if ( $action == 'add' ) {
                $destination_screen = 'order_summary';
            } else if ( $action == 'payment_failed' ) {
                $destination_screen = 'order_summary';
            } else if ( $action == 'change_status' ) {
                if ( in_array($status, array('service_completed', 'delivered')) ) {
                    if ( $status == 'service_completed' ) {
                        if ( isset($payment) && $payment == null ) {
                            $destination_screen = 'order_summary';
                        } else {
                            $destination_screen = 'order_job_card';
                        }
                    } else if ( $status == 'delivered' ) {
                        $destination_screen = 'order_summary';
                    }
                } else {
                    $destination_screen = 'order_status';
                }
            }
        }

        return $destination_screen;
    }
}
