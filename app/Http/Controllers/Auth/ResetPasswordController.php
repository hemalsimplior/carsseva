<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\User;
use Validator, View, DB, Hash, Redirect, Session;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showResetForm(Request $request, $token = null)
    {
        $pageConfigs = [
            'bodyClass' => "bg-full-screen-image",
            'blankPage' => true
        ];
        
        return view('auth.passwords.reset')->with(
            ['token' => $token, 'contact_no' => $request->contact_no, 'pageConfigs' => $pageConfigs]
        );
    }

    public function reset(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, array(
            'contact_no' => ['required'],
            'password' => ['required', 'string', 'confirmed'],
        ));
        
        // process the login
        if ($validator->fails()) {
            return Redirect::back()
            ->withErrors($validator)
            ->withInput();
        }
        
        $reminder = DB::table('password_resets')
            ->where('contact_no', '=', $data['contact_no'])
            ->first();
        
        if (! $reminder or $data['token'] != $reminder->token) {
            // redirect
            Session::flash('danger', 'Invalide Token!');
            return Redirect::back();
        }
        
        $updateData = array(
            'password' => Hash::make($data['password'])
        );
        $user = User::where('contact_no', $reminder->contact_no)
            ->whereIn('role', array('admin', 'vendor_admin'))
            ->where('is_deleted', 0);
        if ( $user->update($updateData) ) {

            // remove token
            DB::table('password_resets')
                ->where('contact_no', '=', $data['contact_no'])
                ->delete();
            
            Session::flash('success', 'Your password successfully changed!');
            return Redirect::to('login');
        } else {
            // redirect
            Session::flash('danger', 'Some thing is wrong. Please try again');
            return Redirect::back();
        }
    }
}
