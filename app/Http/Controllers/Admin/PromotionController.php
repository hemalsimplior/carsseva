<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Promotion, App\User, App\Models\Service, App\Models\Vendor;
use Session, Redirect, Carbon\Carbon, Auth, Validator;

class PromotionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currentUser = Auth::user();

        $promotions = Promotion::where('is_deleted', '0');
        if ( isset($currentUser['role']) && $currentUser['role'] != 'admin' ) {
            $promotions = $promotions->whereRaw('(JSON_CONTAINS(vendors, "true", "$.'.$currentUser['vendor_id'].'") OR JSON_CONTAINS(vendors, "true", "$.all"))');
        }
        $promotions = $promotions->orderBy('id', 'desc')
            ->get();

        $vendors = Vendor::where('status', '1')
            ->where('is_deleted', '0')
            ->orderBy('id', 'desc')
            ->get();

        $services = Service::where('parent_id', 0)
            ->where('is_deleted', '0')
            ->orderBy('order_number', 'asc')
            ->get();
        
        $breadcrumbs = [
            ['link' => "dashboard", 'name' => "Home"],
            ['name' => "Promotions"]
        ];
        
        return view('pages.promotion.index', [
            'breadcrumbs' => $breadcrumbs,
            'promotions' => $promotions,
            'vendors' => $vendors,
            'services' => $services,
            'currentUser' => $currentUser,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $currentUser = Auth::user();
        if ( isset($currentUser['role']) && $currentUser['role'] != "admin") {
            $data['vendors'] = array($currentUser['vendor_id']);
        }

        $validator = Validator::make($data, array(
            'name' => 'required',
            'code' => 'required|unique:promotions',
            'range' => 'required',
            'vendors' => 'required',
        ));

        // process the login
        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }

        $vendorsArr = array();
        if ( isset($data['vendors']) && count($data['vendors']) > 0 ) {
            foreach ($data['vendors'] as $vendor) {
                $vendorsArr[$vendor] = true;
            }
        }
        $data['vendors'] = json_encode($vendorsArr);

        $servicesArr = array();
        if ( isset($data['services']) && count($data['services']) > 0 ) {
            foreach ($data['services'] as $vendor) {
                $servicesArr[$vendor] = true;
            }
        }
        $data['services'] = json_encode($servicesArr);
        
        list($data['start_date'], $data['end_date']) = explode(' - ', $data['range']);
        $data['start_date'] = Carbon::parse(str_replace('/', '-', $data['start_date']))->format('Y-m-d');
        $data['end_date'] = Carbon::parse(str_replace('/', '-', $data['end_date']))->format('Y-m-d');
        
        $promotion = Promotion::create($data);
        if ( $promotion ) {

            // redirect
            Session::flash('success', 'Promotion '.$data['name'].' successfully created!');
            return Redirect::to('promotions');
        } else {
            // redirect
            Session::flash('danger', 'Some thing is wrong. Please try again');
            return Redirect::to('promotions');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $currentUser = Auth::user();
        if ( isset($currentUser['role']) && $currentUser['role'] != "admin") {
            $data['vendors'] = array($currentUser['vendor_id']);
        }

        $validator = Validator::make($data, array(
            'name' => 'required',
            'code' => 'required|unique:promotions,code,'.$id,
            'range' => 'required',
            'vendors' => 'required',
        ));

        // process the login
        if ($validator->fails()) {
            /* return Redirect::back()
                ->withErrors($validator)
                ->withInput(); */
            
            $validatorString = implode(", ", $validator->messages()->all());
            Session::flash('danger', $validatorString);
            return Redirect::back();
        }
        $vendorsArr = array();
        if ( isset($data['vendors']) && count($data['vendors']) > 0 ) {
            foreach ($data['vendors'] as $vendor) {
                $vendorsArr[$vendor] = true;
            }
        }
        $data['vendors'] = json_encode($vendorsArr);

        $servicesArr = array();
        if ( isset($data['services']) && count($data['services']) > 0 ) {
            foreach ($data['services'] as $vendor) {
                $servicesArr[$vendor] = true;
            }
        }
        $data['services'] = json_encode($servicesArr);
        
        list($data['start_date'], $data['end_date']) = explode(' - ', $data['range']);
        $data['start_date'] = Carbon::parse(str_replace('/', '-', $data['start_date']))->format('Y-m-d');
        $data['end_date'] = Carbon::parse(str_replace('/', '-', $data['end_date']))->format('Y-m-d');
        
        $promotion = Promotion::find($id);
        if ( $promotion->update($data) ) {

            // redirect
            Session::flash('success', 'Promotion '.$data['name'].' successfully updated!');
            return Redirect::to('promotions');
        } else {
            // redirect
            Session::flash('danger', 'Some thing is wrong. Please try again');
            return Redirect::to('promotions');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $currentUser = Auth::user();
        $data['is_deleted'] = '1';

        $itme = Promotion::find($id);
        if ( $itme->update($data) ) {

            // redirect
            Session::flash('success', 'Promotion successfully deleted!');
            return Redirect::to('promotions');
        } else {

            // redirect
            Session::flash('danger', 'Some thing is wrong. Please try again');
            return Redirect::to('promotions');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function bulkAction(Request $request)
    {
        $data = $request->all();
        $currentUser = Auth::user();
        $validator = Validator::make($data, array(
            'action' => 'required',
            'ids' => 'required',
        ));

        // process the login
        if ($validator->fails()) {

            $response = array(
                'status' => 500,
                'message' => 'Invalid data',
                'data' => $validator->errors()
            );
            return response()->json($response, $response['status']);
        }

        $dataUpdate = array();
        if ( isset($data['action']) && $data['action'] == 'active' ) {
            $dataUpdate['status'] = '1';
        } else if ( isset($data['action']) && $data['action'] == 'inactive' ) {
            $dataUpdate['status'] = '0';
        } else if ( isset($data['action']) && $data['action'] == 'delete' ) {
            $dataUpdate['is_deleted'] = '1';
        }

        $item = Promotion::find($data['ids']);
        if ( $item->update($dataUpdate) ) {
            
            Session::flash('success', 'All selected items successfully '.$data['action'].'d!');
            $response = array(
                'status' => 200,
                'message' => ''
            );
        } else {
            
            Session::flash('danger', 'Some thing is wrong. Please try again');
            $response = array(
                'status' => 500,
                'message' => ''
            );
        }

        return response()->json($response, $response['status']);
    }
}
