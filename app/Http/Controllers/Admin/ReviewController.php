<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Review;
use Session, Redirect, Carbon\Carbon, Auth, Validator;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reviews = Review::orderBy('id', 'desc')
            ->get();

        $breadcrumbs = [
            ['link' => "dashboard", 'name' => "Home"],
            ['name' => "Reviews"]
        ];
        
        return view('pages.review.index', [
            'breadcrumbs' => $breadcrumbs,
            'reviews' => $reviews
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function bulkAction(Request $request)
    {
        $data = $request->all();
        $currentUser = Auth::user();
        $validator = Validator::make($data, array(
            'action' => 'required',
            'ids' => 'required',
        ));

        // process the login
        if ($validator->fails()) {

            $response = array(
                'status' => 500,
                'message' => 'Invalid data',
                'data' => $validator->errors()
            );
            return response()->json($response, $response['status']);
        }

        $dataUpdate = array();
        if ( isset($data['action']) && $data['action'] == 'active' ) {
            $dataUpdate['status'] = '1';
        } else if ( isset($data['action']) && $data['action'] == 'inactive' ) {
            $dataUpdate['status'] = '0';
        } else if ( isset($data['action']) && $data['action'] == 'delete' ) {
            $dataUpdate['is_deleted'] = '1';
        }

        $item = Review::find($data['ids']);
        if ( $item->update($dataUpdate) ) {
            
            Session::flash('success', 'Review successfully '.$data['action'].'d!');
            $response = array(
                'status' => 200,
                'message' => ''
            );
        } else {
            
            Session::flash('danger', 'Some thing is wrong. Please try again');
            $response = array(
                'status' => 500,
                'message' => ''
            );
        }

        return response()->json($response, $response['status']);
    }
}
