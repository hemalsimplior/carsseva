<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Order, App\Models\Vendor, App\Models\Promotion, App\User;
use Auth, Carbon\Carbon, DB, Config;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        $statistics = array();
        $currentUser = Auth::user();

        if ( isset($currentUser['role']) && $currentUser['role'] == 'vendor_admin' ) {
            $statistics['order_received'] = Order::where('vendor_id', $currentUser['vendor_id'])
                ->where('status', 'order_received')
                ->count();

            $statistics['today_orders'] = Order::where('vendor_id', $currentUser['vendor_id'])
                ->where('status', 'order_confirmed')
                ->whereDate('service_date', '<=', Carbon::now())
                ->count();
            
            $statistics['upcoming_orders'] = Order::where('vendor_id', $currentUser['vendor_id'])
                ->where('status', 'order_confirmed')
                ->whereDate('service_date', '>', Carbon::now())
                ->count();
                
            $statistics['total_revenue'] = DB::table('vendor_revenue')
                ->where('vendor_id', $currentUser['vendor_id'])
                ->first();

            
            $status = array();
            foreach (Config::get('custom.carsseva.all_order_status') as $s) {
                $chart_status = Order::where('vendor_id', $currentUser['vendor_id'])
                    ->where('status', $s['id'])
                    ->count();
                $lab = $s['name'].' ('.$chart_status.')';
                $status[$lab] = $chart_status;
            }
            
            $statistics['status_chart'] = array(
                'name' => array_keys($status),
                'value' => array_values($status)
            );

        } else {
            $statistics['customers'] = User::where('status', '1')
                ->where('is_deleted', '0')
                ->where('role', 'customer')
                ->count();
            
            $statistics['vendors'] = Vendor::where('status', '1')
                ->where('is_deleted', '0')
                ->count();

            $statistics['promotions'] = Promotion::where('status', '1')
                ->where('is_deleted', '0')
                ->count();

            $statistics['order_received'] = Order::where('status', 'order_received')
                ->count();

            $statistics['completed_orders'] = Order::whereIn('status', array('service_completed', 'delivered'))
                ->count();

            $statistics['orders'] = Order::count();

            $status = array();
            foreach (Config::get('custom.carsseva.all_order_status') as $s) {
                $chart_status = Order::where('status', $s['id'])
                    ->count();
                $lab = $s['name'].' ('.$chart_status.')';
                $status[$lab] = $chart_status;
            }
            
            $statistics['status_chart'] = array(
                'name' => array_keys($status),
                'value' => array_values($status)
            );
        }
        
        $breadcrumbs = [
            ['name' => 'Dashboard']
        ];

        $pageConfigs = [
            'pageHeader' => true
        ];

        return view('pages.dashboard.'.$currentUser['role'], [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs,
            'statistics' => $statistics
        ]);
    }
}

