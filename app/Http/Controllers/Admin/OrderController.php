<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User, App\Models\Order, App\Models\OrderItem, App\Models\OrderDocument, App\Models\TempOrder, App\Models\TempOrderItem, App\Models\Service, App\Models\Log, App\Models\Promotion, App\Models\UserFirebaseToken, App\Models\Vendor, App\Models\UserCar;
use Session, Redirect, Carbon\Carbon, Auth, Validator, Config, File, Storage;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currentUser = Auth::user();
        
        $orders = Order::where('is_deleted', '0')
            //->where('id', '18')
            //->with('amount_credited')
            ->with('items');
        
        if ( isset($currentUser['role']) && $currentUser['role'] != 'admin' ) {
            $orders = $orders->where('vendor_id', $currentUser['vendor_id']);
        }

        $orders = $orders->orderBy('id', 'desc')->get()->map(function ($order) {
            if ( $order->is_awaiting_for_approval && $order->temp_order_id != '' && $order->temp_order_id != null ) {
                $order = TempOrder::with('items')/* ->with('amount_credited') */->find($order->temp_order_id);
            } else {
                $order->order_id = $order->id;
            }
            $order->amount_credited = $order->amount_credited->sum('amount');
            
            return $order;
        });
        
        $breadcrumbs = [
            ['link' => "dashboard", 'name' => "Home"],
            ['name' => "Orders"]
        ];

        $status = array();
        foreach (Config::get('custom.carsseva.all_order_status') as $s) {
            $status[$s['id']] = $s['name'];
        }
        
        return view('pages.order.index', [
            'breadcrumbs' => $breadcrumbs,
            'orders' => $orders,
            'allow_status' => Config::get('custom.carsseva.allow_status'),
            'status' => $status,
            'currentUser' => $currentUser,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vendors = Vendor::select('id', 'name')
            ->where('status', '1')
            ->where('is_deleted', '0')
            ->orderBy('id', 'desc')
            ->get();

        $breadcrumbs = [
            ['link' => "dashboard", 'name' => "Home"],
            ['link' => "orders", 'name' => "Orders"],
            ['name' => "Add Order"]
        ];
        
        return view('pages.order.create', [
            'breadcrumbs' => $breadcrumbs,
            'vendors' => $vendors,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $currentUser = Auth::user();
        $data = $request->all();

        if ( $currentUser['role'] == 'admin' ) {
            
            $validator = Validator::make($data, array(
                'customer_id' => 'required',
                'vendor_id' => 'required',
                'user_car_id' => 'required',
                'pickup_type' => 'required',
                'user_address_id' => 'required_if:pickup_type,pickup',
                'service_date' => 'required',
                'service_id' => 'required',
            ));

            // process the login
            if ($validator->fails()) {
                return Redirect::back()
                    ->withErrors($validator)
                    ->withInput();
            }

            $checkOrderExist = Order::where('user_car_id', $data['user_car_id'])
                ->whereDate('service_date', $data['service_date'])
                ->whereNotIn('status', array('order_cancelled', 'delivered'))
                ->count();
            if ( $checkOrderExist <= 0 ) {
                
                $userCar = UserCar::select('car_model_id')->find($data['user_car_id']);

                $services = $data['service_id'];
                $data['status'] = 'order_received';

                if ( isset($data['service_date']) && $data['service_date'] != '' ) {
                    $data['service_date'] = Carbon::parse(str_replace('/', '-', $data['service_date']))->toDateTimeString();
                }

                // find seq id
                $prefix = Config::get('custom.carsseva.prefix.order');
                $vendor = Vendor::select('code')->find($data['vendor_id']);
                $lastOrder = Order::select('sequence_id')->where('vendor_id', $data['vendor_id'])->orderBy('ID', 'desc')->first();
                if ( isset($lastOrder->sequence_id) ) {
                    $data['sequence_id'] = $this->increaseSeqNo($lastOrder->sequence_id, $vendor->code.$prefix);
                } else {
                    $data['sequence_id'] = $vendor->code.$prefix.'0001';
                }

                $order = Order::create($data);
                if( $order ){

                    $order_amount = array(
                        'sub_total' => 0,
                        'discount' => 0,
                        'tax' => 0
                    );
                    foreach ($services as $service) {
    
                        $serviceData = Service::find($service);
                        $cost = $this->serviceCost($service, $userCar->car_model_id, $data['vendor_id']);
                        $orderItemData = array(
                            'order_id' => $order->id,
                            'service_id' => $service,
                            'parent_service_id' => $serviceData->parent_id,
                            'service_name' => $serviceData->name,
                            'cost_type' => 'service_cost',
                            'cost' => $cost,
                            'tax' => '18',
                            'total' => $cost + ($cost * 0.18),
                            'status' => 'confirmed',
                        );
    
                        $orderItem = OrderItem::create($orderItemData);
                        if ( $orderItem ) {
                            $order_amount['sub_total'] += $cost;
                            //$order_amount['tax'] += $cost * 0.18;
                        }
                    }

                    $temp_total = ($order_amount['sub_total'] - $order_amount['discount']);
                    $order_amount['tax'] = $temp_total * 0.18;
                    $order_amount['total'] = $temp_total + $order_amount['tax'];
                    Order::find($order->id)->update($order_amount);

                    /* log */
                    $vendorAdmin = User::select('id')
                        ->where('vendor_id', $order['vendor_id'])
                        ->where('role', 'vendor_admin')
                        ->first();
                    $logData = array('common_id' => $order['id'], 'action_type' => 'order', 'action' => 'add', 'description' => '', 'action_by' => $data['customer_id'], 'user_ip' => $request->ip(), 'is_read' => json_encode(array($data['customer_id'] => false, $vendorAdmin['id'] => false)));
                    $logId = Log::create($logData);
                    /* log */

                    // Send Notification to Customer
                    $tokens = UserFirebaseToken::where('user_id', $data['customer_id'])->pluck('firebase_token')->toArray();
                    $notification = array(
                        'title' => 'Order Placed',
                        'body' => 'Your order #'.$order['sequence_id'].' has been placed!',
                        'data' => array(
                            'order_id' => $order['id'],
                            'mode' => 'order_add',
                            'destination_screen' => 'order_status',
                            'notification_id' => $logId['id'],
                        )
                    );
                    $msg = '';
                    try {
                        $msg = $this->sendNotification($notification, $tokens);
                    } catch (\Throwable $th) {
                        
                    }

                    // Send Notification to Vendor
                    $tokens = UserFirebaseToken::where('user_id', $vendorAdmin['id'])->pluck('firebase_token')->toArray();
                    $notification = array(
                        'title' => 'Order Recived',
                        'body' => 'New order #'.$order['sequence_id'].' has been recived!',
                        'data' => array(
                            'order_id' => $order['id'],
                            'mode' => 'order_list',
                            'destination_screen' => 'order_list',
                            'notification_id' => $logId['id'],
                        )
                    );
                    try {
                        $this->sendNotification($notification, $tokens);
                    } catch (\Throwable $th) {
                        
                    }

                    // redirect
                    Session::flash('success', 'Order #'.$order['sequence_id'].' successfully placed!');
                    return Redirect::to('orders/create');

                } else {
                    // redirect
                    Session::flash('danger', 'Opps, Something went wrong please try again later!');
                    return Redirect::to('orders/create');
                }

            } else {
                // redirect
                Session::flash('danger', 'Already placed order on this date!');
                return Redirect::to('orders/create');
            }

        } else {
            return response( 'Permissions insuffisantes !', 403 );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $mode = '')
    {
        $currentUser = Auth::user();
        $order = Order::with(['items', 'before_service_documents', 'after_service_documents'])->find($id);
        if ( isset($order->temp_order_id) && $order->temp_order_id != NULL && $order->is_awaiting_for_approval == '1' ) {
            $order = TempOrder::with(['items', 'before_service_documents', 'after_service_documents'])->find($order->temp_order_id);
        }
        
        if ( $currentUser['role'] == 'admin' || ($currentUser['role'] != 'admin' && $currentUser['vendor_id'] == $order['vendor_id']) ) {
            
            $sortedItems = array();
            foreach ($order->items as $item) {

                $key = $item->parent_service_id;
                if ( isset($item->is_custom) && $item->is_custom ) {
                    $key = 'custom';
                }

                if ( !array_key_exists($key, $sortedItems) ) {
                    $sortedItems[$key] = array(
                        'id' => $item->parent_service_id,
                        'name' => ((isset($item->parent_service->name) && $item->parent_service->name) ? $item->parent_service->name : 'Custom Services'),
                        'items' => array()
                    );
                }

                array_push($sortedItems[$key]['items'], $item);
            }
            $order->items = $sortedItems;

            $vendor = Vendor::select('id')->find($order['vendor_id']);
            $services = Service::whereIn('id', $vendor->services->pluck('id')->toArray())
                ->where('parent_id', 0)
                ->where('is_deleted', '0')
                ->with('subServices')
                ->orderBy('order_number', 'asc')
                ->get();
            
            if ( $services ) {
                $servicesData = array();
                $services = $services->toArray();
                foreach ($services as $k => $ser) {
                    $servicesData[$k] = array(
                        'id' => 0,
                        'text' => $ser['name'],
                    );
                    if ( $ser['sub_services'] && count($ser['sub_services']) > 0 ) {
                        $servicesData[$k]['children'] = array();
                        foreach ($ser['sub_services'] as $sk => $sub_ser) {
                            $servicesData[$k]['children'][$sk] = array(
                                'id' => $sub_ser['id'],
                                'text' => $sub_ser['name'],
                            );
                        }
                    }
                }

                array_unshift($servicesData, array(
                    'id' => 'custom',
                    'text' => 'Custom Service',
                ));
                $services = json_encode($servicesData);
            }

            $status = array();
            foreach (Config::get('custom.carsseva.all_order_status') as $s) {
                $status[$s['id']] = $s['name'];
            }

            $pickup_persons = User::where('role', 'pickup_person')
                ->where('vendor_id', $order->vendor_id)
                ->where('status', '1')
                ->where('is_deleted', '0')
                ->get();
            
            // Activity Log
            $logs = Log::select('id', 'common_id', 'action_type', 'action', 'description', 'new', 'action_by', 'created_at')
                ->with(array('action_by' => function($query){
                    $query->select('id', 'name');
                }))
                ->where('common_id', $id)
                ->where('action_type', 'order')
                ->whereIn('action', array('add', 'change_status', 'payment_failed'))
                ->orderBy('id', 'desc');
    
            $logs = $logs->get()->map(function ($log) {
    
                $log = $log->toArray();
    
                $title = '';
                if ( $log['action'] == 'add' ) {
                    $title = 'Received';
                } else if ( $log['action'] == 'change_status' ) {
                    $title = ucwords(str_replace('_', ' ', $log['new']));
                }
                $log['title'] = $title;
                $log['created_at'] = Carbon::parse($log['created_at']);
    
                unset($log[$log['action_type']]);
                return $log;
            });
            
            //$order->promo_code = unserialize($order->promo_code);
            $order->promo_code = json_decode($order->promo_code, true);
            if ( !$order->promo_code ) {
                $order->promo_code = array();
            }

            $breadcrumbs = [
                ['link' => "dashboard", 'name' => "Home"],
                ['link' => "orders", 'name' => "Orders"],
                ['name' => "#".$order->sequence_id]
            ];
            
            return view('pages.order.show', [
                'hide_page_header' => false,
                'breadcrumbs_custom' => $breadcrumbs,
                'order' => $order,
                'services' => $services,
                'status' => $status,
                'mode' => $mode,
                'pickup_persons' => $pickup_persons,
                'logs' => $logs,
                'currentUser' => $currentUser,
                'allow_status' => Config::get('custom.carsseva.allow_status')
            ]);
            
        } else {
            return response( 'Permissions insuffisantes !', 403 );
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $currentUser = Auth::user();
        
        $data = $request->all();
        $mode = 'order';
        $originalOrder = Order::with('items')->find($id)->toArray();
        if ( $originalOrder['is_awaiting_for_approval'] == 1 ) {
            $mode = 'temp_order';
            $originalOrder = TempOrder::with('items')->find($originalOrder['temp_order_id'])->toArray();
        }

        if ( $currentUser['role'] == 'admin' || ($currentUser['role'] == 'vendor_admin' && $currentUser['vendor_id'] == $originalOrder['vendor_id']) ) {
            
            // Update Car
            $originalOrder['user_car_id'] = $data['order-user_car_id'];

            // Update Dates
            if ( isset($data['order-created_at']) && $data['order-created_at'] != '' ) {
                $originalOrder['created_at'] = Carbon::parse(str_replace('/', '-', $data['order-created_at']))->toDateTimeString();
            }
            if ( isset($data['order-service_date']) && $data['order-service_date'] != '' ) {
                $originalOrder['service_date'] = Carbon::parse(str_replace('/', '-', $data['order-service_date']))->toDateTimeString();
            }

            // Update Address
            $originalOrder['user_address_id'] = $data['order-user_address_id'];
            
            $tempOrderData = $originalOrder;
            $tempOrderData['is_awaiting_for_approval'] = '1';
            if ( $originalOrder['payment_status'] != 'pending' ) {
                $tempOrderData['payment_status'] = 'partially_paid';
            }

            $tempOrder = TempOrder::updateOrCreate(
                ['order_id' => $id],
                $tempOrderData
            );

            if ( $tempOrder ) {

                // Update Original Order Data
                $originalOrderUpdateData = ['temp_order_id' => $tempOrder->id, 'is_awaiting_for_approval' => '1'];
                if ( $originalOrder['payment_status'] != 'pending' ) {
                    $originalOrderUpdateData['payment_status'] = 'partially_paid';
                }
                Order::find($id)->update($originalOrderUpdateData);

                // Sync Order Items
                if ( isset($data['items']) && count($data['items']) > 0 ) {
                
                    $itemIdsArr = array_column($originalOrder['items'], 'id');
                    TempOrderItem::where('temp_order_id', $tempOrder->id)->delete();
                    
                    foreach ($data['items'] as $item_id => $item) {
                        if ( $item_id != 'new' ) {
                            $index = array_search($item_id, $itemIdsArr);
                            if ( is_int($index) ) {
                                $tempItemData = $originalOrder['items'][$index];

                                // Update Item
                                $tempItemData['temp_order_id'] = $tempOrder->id;
                                $tempItemData['cost'] = (float) $item['cost'];
                                $tempItemData['tax'] = (float) $item['tax'];
                                $tempItemData['total'] = $tempItemData['cost'] + ($tempItemData['cost'] * ($tempItemData['tax']/100));

                                TempOrderItem::create($tempItemData);
                            }
                        } else {
                            foreach ($data['items']['new'] as $new_service) {
                                
                                if ( $new_service['service_id'] != 'custom' ) {

                                    $tempItemData = array(
                                        'temp_order_id' => $tempOrder->id
                                    );
                                    $tempItemData['cost_type'] = 'service_cost';
                                    $tempItemData['cost'] = $new_service['inbuilt_labour_cost'];
                                    $tempItemData['tax'] = 18;
                                    $tempItemData['total'] = $tempItemData['cost'] + ($tempItemData['cost'] * ($tempItemData['tax']/100));

                                    $tempItemData['service_id'] = $new_service['service_id'];
                                    $parent_service_id = Service::select('parent_id', 'name')->find($tempItemData['service_id']);
                                    $tempItemData['service_name'] = $parent_service_id['name'];
                                    $tempItemData['parent_service_id'] = $parent_service_id['parent_id'];

                                    TempOrderItem::create($tempItemData);
                                } else {

                                    $associate_id = '';
                                    if ( isset($new_service['parts_cost']) && $new_service['parts_cost'] != '' && $new_service['parts_cost'] > 0 ) {

                                        $tempItemData = array(
                                            'temp_order_id' => $tempOrder->id
                                        );
                                        $tempItemData['cost_type'] = 'part_cost';
                                        $tempItemData['cost'] = $new_service['parts_cost'];
                                        $tempItemData['tax'] = $new_service['parts_tax'];
                                        $tempItemData['total'] = $tempItemData['cost'] + ($tempItemData['cost'] * ($tempItemData['tax']/100));
                                        $tempItemData['service_name'] = $new_service['service_name'];
                                        $tempItemData['is_custom'] = 1;
                                        $partsCostData = TempOrderItem::create($tempItemData);
                                        if ($partsCostData) {
                                            $associate_id = $partsCostData['id'];
                                        }
                                    }

                                    if ( isset($new_service['labour_cost']) && $new_service['labour_cost'] != '' && $new_service['labour_cost'] > 0 ) {

                                        $tempItemData = array(
                                            'temp_order_id' => $tempOrder->id
                                        );
                                        $tempItemData['cost_type'] = 'service_cost';
                                        $tempItemData['cost'] = $new_service['labour_cost'];
                                        $tempItemData['tax'] = $new_service['labour_tax'];
                                        $tempItemData['total'] = $tempItemData['cost'] + ($tempItemData['cost'] * ($tempItemData['tax']/100));
                                        $tempItemData['service_name'] = $new_service['service_name'];
                                        $tempItemData['is_custom'] = 1;
                                        $tempItemData['associate_id'] = $associate_id;
                                        TempOrderItem::create($tempItemData);
                                    }

                                }
                            }
                        }
                    }

                    $order_amount = array(
                        'sub_total' => (isset($data['main_sub_total']) ? $data['main_sub_total'] : $tempOrder['sub_total']),
                        'discount' => (isset($data['main_discount']) ? $data['main_discount'] : $tempOrder['discount']),
                        'tax' => (isset($data['main_total_tax']) ? $data['main_total_tax'] : $tempOrder['tax']),
                        'total' => (isset($data['main_total_amount']) ? $data['main_total_amount'] : $tempOrder['total'])
                    );
                    TempOrder::find($tempOrder->id)->update($order_amount);
                }

                // Before Order Documents
                if ( isset($data['before_images']) && count($data['before_images']) > 0 ) {
                    foreach ($data['before_images'] as $ky => $image) {
                        $orderDocData = array(
                            'order_id' => $tempOrder->order_id,
                            'type' => 'before_service'
                        );

                        $img = explode(',', $image);
                        $ini =substr($img[0], 11);
                        $type = explode(';', $ini);
                        $extention = str_replace('+xml', '', $type[0]);
                        $document = Carbon::now()->timestamp . $ky . '.' .$extention;

                        $subpath = "public/uploads/orders/".$tempOrder->order_id."/before_service/";
                        $path = storage_path('app') . "/" . $subpath;

                        if( !File::isDirectory($path) ){
                            File::makeDirectory($path, 0755, true, true);
                        }

                        $image = substr($image, strpos($image, ",")+1);
                        $data1 = base64_decode($image);
                        
                        if ( Storage::put($subpath . $document, $data1) ) {
                            $orderDocData['document'] = $document;
                            OrderDocument::create($orderDocData);
                        }
                    }
                }

                // After Order Documents
                if ( isset($data['after_images']) && count($data['after_images']) > 0 ) {
                    foreach ($data['after_images'] as $ky => $after_image) {
                        $orderDocData = array(
                            'order_id' => $tempOrder->order_id,
                            'type' => 'after_service'
                        );

                        $img = explode(',', $after_image);
                        $ini =substr($img[0], 11);
                        $type = explode(';', $ini);
                        $extention = str_replace('+xml', '', $type[0]);
                        $document = Carbon::now()->timestamp . $ky . '.' .$extention;

                        $subpath = "public/uploads/orders/".$tempOrder->order_id."/after_service/";
                        $path = storage_path('app') . "/" . $subpath;

                        if( !File::isDirectory($path) ){
                            File::makeDirectory($path, 0755, true, true);
                        }

                        $after_image = substr($after_image, strpos($after_image, ",")+1);
                        $data1 = base64_decode($after_image);
                        
                        if ( Storage::put($subpath . $document, $data1) ) {
                            $orderDocData['document'] = $document;
                            OrderDocument::create($orderDocData);
                        }
                    }
                }

                // Send Notification to Customer
                $tokens = UserFirebaseToken::where('user_id', $originalOrder['customer_id'])->pluck('firebase_token')->toArray();
                $notification = array(
                    'title' => "Service Recommended",
                    //'body' => "Vendor recommended few services for your order #".$originalOrder['sequence_id'].", Please Accept/Decline recommendation",
                    'body' => "Vendor added additional services to your order #".$originalOrder['sequence_id'].".",
                    'data' => array(
                        'order_id' => $originalOrder['id'],
                        'mode' => 'service_recommended',
                        'destination_screen' => 'order_job_card',
                    ),
                );
                $msg = '';
                try {
                    $msg = $this->sendNotification($notification, $tokens);
                } catch (\Throwable $th) {
                    
                }


                // Auto Accept
                if ( 'accept' == 'accept' ) {
                    OrderItem::where('order_id', $id)->delete();

                    $tempOrd = TempOrder::find($tempOrder->id);
                    $tempOrdTemp = TempOrderItem::where('temp_order_id', $tempOrder->id)
                        //->where('status', 'waiting_for_confirmation')
                        ->get();
                    foreach ($tempOrdTemp as $item) {
                        $item = $item->toArray();
                        $item['order_id'] = $id;
                        $item['status'] = 'confirmed';
                        $item['is_deleted'] = '0';
                        OrderItem::create($item);
                    }

                    // Update Order
                    Order::find($id)->update(array(
                        'user_car_id' => $tempOrd->user_car_id, 
                        'user_address_id' => $tempOrd->user_address_id, 
                        'pickup_type' => $tempOrd->pickup_type, 
                        'pickup_person_id' => $tempOrd->pickup_person_id, 
                        'sub_total' => $tempOrd->sub_total, 
                        'discount' => $tempOrd->discount, 
                        'tax' => $tempOrd->tax, 
                        'total' => $tempOrd->total, 
                        'promo_code' => $tempOrd->promo_code, 
                        'temp_order_id' => null, 
                        'is_awaiting_for_approval' => '0'
                    ));

                    // Remove Temp Order and Temp Order Items
                    $deleteTempOrder = TempOrder::find($tempOrder->id)->delete();
                    if ( $deleteTempOrder ) {
                        TempOrderItem::where('temp_order_id', $tempOrder->id)->delete();
                    }
                }
                // Auto Accept - End


                Session::flash('success', 'Order changes successfully updated!, All changes under approval.');
                return Redirect::to('orders/'.$id);
            } else {
                Session::flash('danger', 'Some thing is wrong. Please try again');
                return Redirect::to('orders/'.$id);
            }
            
        } else {
            return response( 'Permissions insuffisantes !', 403 );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function discardChanges(Request $request, $id)
    {
        $currentUser = Auth::user();
        $tempOrder = TempOrder::find($id);
        if ( $currentUser['role'] == 'admin' || ($currentUser['role'] == 'vendor_admin' && $currentUser['vendor_id'] == $tempOrder['vendor_id']) ) {
            
            $revertBackOriginalOrder = Order::find($tempOrder->order_id);
            if ( $revertBackOriginalOrder->update(array('is_awaiting_for_approval' => '0', 'temp_order_id' => NULL)) ) {
                if ( $tempOrder->delete() ) {
                    TempOrderItem::where('temp_order_id', $id)->delete();

                    Session::flash('success', 'Order changes successfully discarded!');
                    return Redirect::to('orders/'.$tempOrder->order_id);
                }
            } else {
                Session::flash('danger', 'Some thing is wrong. Please try again');
                return Redirect::to('orders/'.$tempOrder->order_id);
            }

        } else {
            return response( 'Permissions insuffisantes !', 403 );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function changeStatus(Request $request)
    {
        $currentUser = Auth::user();
        $data = $request->all();
        
        // Order Detail
        $order = Order::find($data['id']);
        if ( $currentUser['role'] == 'admin' || ($currentUser['role'] == 'vendor_admin' && $currentUser['vendor_id'] == $order['vendor_id']) ) {
            
            $old_status = $order['status'];
            if ( $order->update($data) ) {

                /* log */
                $logData = array('common_id' => $data['id'], 'action_type' => 'order', 'action' => 'change_status', 'old' => $old_status, 'new' => $data['status'], 'description' => '', 'action_by' => $currentUser['id'], 'user_ip' => $request->ip(), 'is_read' => json_encode(array($order['customer_id'] => false)));
                $logId = Log::create($logData);
                /* log */

                // Send Notification to Customer
                if ( $data['status'] == 'team_enroute' ) {
                    $body = $order->pickup_person->name." assigned as pickup person of your order #".$order['sequence_id'].". Pickup person's contact no is +91".$order->pickup_person->contact_no;
                } else {
                    $body = "#".$order['sequence_id']." order's status changed to ".str_replace('_', ' ', $data['status']).".";
                }
                if ( isset($logId['order']['payments']) ) {
                    $payment = $logId['order']['payments'];
                } else {
                    $payment = null;
                }
                $tokens = UserFirebaseToken::where('user_id', $order['customer_id'])->pluck('firebase_token')->toArray();
                $notification = array(
                    'title' => ucwords(str_replace('_', ' ', $data['status'])),
                    'body' => $body,
                    'data' => array(
                        'order_id' => $order['id'],
                        'mode' => 'order_status_change',
                        'destination_screen' => $this->destinationScreenApp('order', 'change_status', $data['status'], $payment),
                        'notification_id' => $logId['id'],
                    )
                );
                $msg = '';
                try {
                    $msg = $this->sendNotification($notification, $tokens);
                } catch (\Throwable $th) {
                    
                }

                Session::flash('success', 'Order changes successfully changed!');
                $response = array(
                    'status' => 200,
                    'message' => ''
                );
            } else {

                Session::flash('danger', 'Some thing is wrong. Please try again');
                $response = array(
                    'status' => 500,
                    'message' => ''
                );
            }

        } else {
            Session::flash('danger', 'Permissions insuffisantes!');
            $response = array(
                'status' => 500,
                'message' => ''
            );
        }

        return response()->json($response, $response['status']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function checkPromoCode(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, array(
            'promo_code' => 'required'
        ));

        // process the login
        if ($validator->fails()) {

            $validatorString = implode(", ", $validator->messages()->all());
            $validatorArray = array_combine(
                array_map(function($key){ return $key.'_error'; }, array_keys($validator->errors()->toArray())),
                $validator->errors()->toArray()
            );

            $response = array(
                'response_code' => 400,
                'response_message' => $validatorString,
                'response_data' => $validatorArray
            );
            return response()->json($response, 200);
        }

        $promo = Promotion::select('id', 'name', 'code', 'description', 'sign_off_type', 'amount')
            ->where('code', $data['promo_code'])
            ->whereDate('start_date', '<=', Carbon::now())
            ->whereDate('end_date', '>', Carbon::now());
            
        if ( isset($data['vendor_id']) && $data['vendor_id'] != '' && $data['vendor_id'] != null ) {
            $promo = $promo->whereRaw('(JSON_CONTAINS(vendors, "true", "$.'.$data['vendor_id'].'") OR JSON_CONTAINS(vendors, "true", "$.all"))');
        } else {
            $promo = $promo->whereRaw('JSON_CONTAINS(vendors, "true", "$.all")');
        }

        $promo = $promo->where('status', '1')
            ->where('is_deleted', '0')
            ->first();
        
        if ( $promo ) {
            
            $response = array(
                'response_code' => 200,
                'response_message' => 'Promo Code Applied!',
                'response_data' => $promo,
            );

        } else {
            $response = array(
                'response_code' => 500,
                'response_message' => 'Invalid Promo Code',
            );
        }

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function requestForPayment(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, array(
            'id' => 'required'
        ));

        // process the login
        if ($validator->fails()) {

            $validatorString = implode(", ", $validator->messages()->all());
            $validatorArray = array_combine(
                array_map(function($key){ return $key.'_error'; }, array_keys($validator->errors()->toArray())),
                $validator->errors()->toArray()
            );

            $response = array(
                'response_code' => 400,
                'response_message' => $validatorString,
                'response_data' => $validatorArray
            );
            return response()->json($response, 200);
        }

        $order = Order::find($data['id']);
        if ( $order ) {

            // Send Notification to Customer
            $tokens = UserFirebaseToken::where('user_id', $order['customer_id'])->pluck('firebase_token')->toArray();
            $notification = array(
                'title' => 'Payment Request',
                'body' => "This is just a reminder that the order #".$order['sequence_id']." payment is now overdue. Please check order summary and Pay Now",
                'data' => array(
                    'order_id' => $order['id'],
                    'mode' => 'payment_request',
                    'destination_screen' => 'order_summary',
                )
            );

            $msg = '';
            try {
                $msg = $this->sendNotification($notification, $tokens);
            } catch (\Throwable $th) {
                
            }
            
            if ( $msg != '' ) {
                Session::flash('success', 'Payment request sent to customer!');
            } else {
                Session::flash('danger', 'Some thing is wrong. Please try again');    
            }

        } else {
            Session::flash('danger', 'Invalid Order');
        }

        $response = array(
            'response_code' => 200,
            'response_message' => '',
            'response_data' => array(),
        );

        return response()->json($response, 200);
    }
}
