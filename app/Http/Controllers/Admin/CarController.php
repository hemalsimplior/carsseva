<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Brand, App\Models\CarModel, App\Models\VendorCarModelSetting, App\Models\UserCar;
use Session, Redirect, Carbon\Carbon, Validator, Auth, File, Storage;

class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = Brand::where('is_deleted', '0')
            ->with('models.segment')
            ->orderBy('id', 'asc')
            ->get();

        $models = CarModel::where('is_deleted', '0')
            ->with('brand')
            ->orderBy('id', 'asc')
            ->get();

        $models_name = array();
        if ( $models ) {
            $models = $models->toArray();
            $models_name = array_map('strtolower', array_column($models, 'name'));
        }

        $breadcrumbs = [
            ['link' => "dashboard", 'name' => "Home"],
            ['name' => "Manage Cars"]
        ];
        
        return view('pages.car.index', [
            'breadcrumbs' => $breadcrumbs,
            'brands' => $brands,
            'models' => $models,
            'models_name' => json_encode($models_name),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $currentUser = Auth::user();
        
        $validatorArr = array(
            'type' => 'required'
        );

        if ( isset($data['type']) && $data['type'] != '' ) {
            if ( $data['type'] == 'brand' ) {
                $validatorArr['name'] = 'required|unique:brands';
            } else {
                $validatorArr['model_name'] = 'required';
                //$validatorArr['model_image'] = 'required';
                $validatorArr['brand_id'] = 'required';
            }
        }

        $validator = Validator::make($data, $validatorArr);

        // process the login
        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }
        
        // Create Brand
        if ( $data['type'] == 'brand' ) {

            $image = (isset($data['image_file']) ? $data['image_file']: '');
            unset($data['image_file']);

            $brand = Brand::create($data);
            if ( $brand ) {

                if ( isset($image) && $image != '' ) {

                    //$profile_image_name = "user-".time().".pdf";
                    $img = explode(',', $image);
                    $ini =substr($img[0], 11);
                    $type = explode(';', $ini);
                    $extention = str_replace('+xml', '', $type[0]);
                    $profile_image_name = Carbon::now()->timestamp . '.' .$extention;

                    $subpath = "public/uploads/brands/";
                    $path = storage_path('app') . "/" . $subpath;

                    if( !File::isDirectory($path) ){
                        File::makeDirectory($path, 0755, true, true);
                    }

                    $image = substr($image, strpos($image, ",")+1);
                    $data1 = base64_decode($image);
                    
                    // delete old file
                    if( $brand->image != '' && Storage::exists($brand->image) ){
                        Storage::delete($brand->image);
                    }

                    if ( Storage::put($subpath . $profile_image_name, $data1) ) {
                        Brand::where('id', $brand->id)->update(array('image' => $profile_image_name));
                    }
                }

                // redirect
                Session::flash('success', 'Brand '.$data['name'].' successfully created!');
                return Redirect::to('cars');
            } else {
                // redirect
                Session::flash('danger', 'Some thing is wrong. Please try again');
                return Redirect::to('cars');
            }
        } else {

            $result = array();
            $files = $request->file('model_image');
            foreach ($data['model_name'] as $index => $name) {

                $modelData = array(
                    'brand_id' => $data['brand_id'],
                    'name' => $name
                );

                $model = CarModel::create($modelData);
                if ( $model ) {
                    array_push($result, $model->name);
                    if ( isset($data['model_image'][$index]) && $data['model_image'][$index] != '' ) {
                        
                        $file = $files[$index];
                        $fileStoreName = Carbon::now()->timestamp . '.' . $file->getClientOriginalExtension();
                        $subpath = "public/uploads/models/";
                        $path = storage_path('app') . "/" . $subpath;
                        if( !File::isDirectory($path) ){
                            File::makeDirectory($path, 0755, true, true);
                        }
                        
                        if ( $file->move($path, $fileStoreName) ) {
                            CarModel::where('id', $model->id)->update(array('image' => $fileStoreName));
                        }
                    }
                }
            }

            if ( count($result) > 0 ) {
                // redirect
                Session::flash('success', implode(', ', $result).' these car models are successfully created!');
                return Redirect::to('cars');
            } else {
                // redirect
                Session::flash('danger', 'Some thing is wrong. Please try again');
                return Redirect::to('cars');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $currentUser = Auth::user();
        
        $validatorArr = array(
            'type' => 'required'
        );

        if ( isset($data['type']) && $data['type'] != '' ) {
            if ( $data['type'] == 'brand' ) {
                $validatorArr['name'] = 'required|unique:brands,name,'.$id;
            } else {
                $validatorArr['model_name'] = 'required';
                //$validatorArr['model_image'] = 'required';
                $validatorArr['brand_id'] = 'required';
            }
        }

        $validator = Validator::make($data, $validatorArr);

        // process the login
        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }

        // Create Brand
        if ( $data['type'] == 'brand' ) {
            
            $image = (isset($data['image_file']) ? $data['image_file']: '');
            unset($data['image_file']);

            $brand = Brand::find($id);
            $oldImage = $brand->image;
            if ( $brand->update($data) ) {
                
                if ( $image != $oldImage ) {

                    $subpath = "public/uploads/brands/";
                    if ( isset($image) && $image != '' ) {
                        
                        $img = explode(',', $image);
                        $ini =substr($img[0], 11);
                        $type = explode(';', $ini);
                        $extention = str_replace('+xml', '', $type[0]);
                        $profile_image_name = Carbon::now()->timestamp . '.' .$extention;

                        $path = storage_path('app') . "/" . $subpath;
                        if( !File::isDirectory($path) ){
                            File::makeDirectory($path, 0755, true, true);
                        }

                        $image = substr($image, strpos($image, ",")+1);
                        $data1 = base64_decode($image);
                        
                        // delete old file
                        if( $oldImage != '' && Storage::exists($subpath . $oldImage) ){
                            Storage::delete($subpath . $oldImage);
                        }

                        if ( Storage::put($subpath . $profile_image_name, $data1) ) {
                            Brand::find($id)->update(array('image' => $profile_image_name));
                        }

                    } else {
                        if( $oldImage != '' && Storage::exists($subpath . $oldImage) ){
                            Storage::delete($subpath . $oldImage);
                            Brand::find($id)->update(array('image' => ''));
                        }
                    }
                }

                Session::flash('success', 'Brand '.$data['name'].' successfully updated!');
                return Redirect::to('cars');
            } else {
                // redirect
                Session::flash('danger', 'Some thing is wrong. Please try again');
                return Redirect::to('cars');
            }
        } else {
            
            $file = '';
            $image = (isset($data['image_file']) ? $data['image_file']: '');
            $files = $request->file('model_image');
            if ( isset($files[0]) ) {
                $file = $files[0];
                $image = $file->getClientOriginalName();
            }

            $data['name'] = $data['model_name'][0];
            
            $model = CarModel::find($id);
            $oldImage = $model->image;
            if ( $model->update($data) ) {
                
                if ( $image != $oldImage ) {

                    $subpath = "public/uploads/models/";
                    if ( isset($image) && $image != '' ) {
                        
                        $fileStoreName = Carbon::now()->timestamp . '.' . $file->getClientOriginalExtension();
                        $subpath = "public/uploads/models/";
                        $path = storage_path('app') . "/" . $subpath;
                        if( !File::isDirectory($path) ){
                            File::makeDirectory($path, 0755, true, true);
                        }

                        // delete old file
                        if( $oldImage != '' && Storage::exists($subpath . $oldImage) ){
                            Storage::delete($subpath . $oldImage);
                        }
                        
                        if ( $file->move($path, $fileStoreName) ) {
                            CarModel::find($id)->update(array('image' => $fileStoreName));
                        }

                    } else {
                        if( $oldImage != '' && Storage::exists($subpath . $oldImage) ){
                            Storage::delete($subpath . $oldImage);
                            CarModel::find($id)->update(array('image' => ''));
                        }
                    }
                }

                Session::flash('success', 'Model '.$data['name'].' successfully updated!');
                return Redirect::to('cars');
            } else {
                // redirect
                Session::flash('danger', 'Some thing is wrong. Please try again');
                return Redirect::to('cars');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($type, $id, Request $request)
    {
        $currentUser = Auth::user();
        $data['is_deleted'] = '1';

        if ( $type == 'brand' ) {
            $itme = Brand::find($id);
        } else {
            $itme = CarModel::find($id);
        }
        
        if ( $itme->update($data) ) {

            $itmeName = 'Model';
            if ( $type == 'brand' ) {
                $itmeName = 'Brand';
                $itme = CarModel::where('brand_id', $id)->update($data);
            }

            // redirect
            Session::flash('success', $itmeName.' successfully deleted!');
            return Redirect::to('cars');
        } else {

            // redirect
            Session::flash('danger', 'Some thing is wrong. Please try again');
            return Redirect::to('cars');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function bulkAction(Request $request)
    {
        $data = $request->all();
        $currentUser = Auth::user();
        $validator = Validator::make($data, array(
            'action' => 'required',
            'ids' => 'required',
            'type' => 'required',
        ));

        // process the login
        if ($validator->fails()) {

            $response = array(
                'status' => 500,
                'message' => 'Invalid data',
                'data' => $validator->errors()
            );
            return response()->json($response, $response['status']);
        }

        $dataUpdate = array();
        if ( isset($data['action']) && $data['action'] == 'active' ) {
            $dataUpdate['status'] = '1';
        } else if ( isset($data['action']) && $data['action'] == 'inactive' ) {
            $dataUpdate['status'] = '0';
        } else if ( isset($data['action']) && $data['action'] == 'delete' ) {
            $dataUpdate['is_deleted'] = '1';
        }

        if ( $data['type'] == 'brand' ) {
            $item = Brand::find($data['ids']);
        } else {
            $item = CarModel::find($data['ids']);
        }

        if ( $item->update($dataUpdate) ) {
            
            if ( $data['type'] == 'brand' ) {
                $itme = CarModel::where('brand_id', $data['ids'])->update($dataUpdate);
            } else {
                if ( isset($data['action']) && $data['action'] == 'active' ) {
                    $itme = Brand::find($item['brand_id'])->update($dataUpdate);
                }
            }

            Session::flash('success', 'All selected items successfully '.$data['action'].'d!');
            $response = array(
                'status' => 200,
                'message' => ''
            );
        } else {
            
            Session::flash('danger', 'Some thing is wrong. Please try again');
            $response = array(
                'status' => 500,
                'message' => ''
            );
        }

        return response()->json($response, $response['status']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function syncSegment(Request $request)
    {
        $data = $request->all();
        $currentUser = Auth::user();
        $validator = Validator::make($data, array(
            'car_model_id' => 'required',
            'segment' => 'required'
        ));

        // process the login
        if ($validator->fails()) {

            $response = array(
                'status' => 500,
                'message' => 'Invalid data',
                'data' => $validator->errors()
            );
            return response()->json($response, $response['status']);
        }

        if ( $currentUser['role'] == 'admin' ) {
            $data['vendor_id'] = 0;
        } else {
            $data['vendor_id'] = $currentUser['id'];
        }
        
        $setting = VendorCarModelSetting::updateOrCreate(
            ['vendor_id' => $data['vendor_id'], 'car_model_id' => $data['car_model_id']],
            $data
        );

        if ( $setting ) {

            //Session::flash('success', 'Segment sync successfully');
            $response = array(
                'status' => 200,
                'message' => ''
            );
        } else {
            
            //Session::flash('danger', 'Some thing is wrong. Please try again');
            $response = array(
                'status' => 500,
                'message' => ''
            );
        }

        return response()->json($response, $response['status']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getModelsFromBrands(Request $request)
    {
        $data = $request->all();
        $currentUser = Auth::user();
        $validator = Validator::make($data, array(
            'ids' => 'required'
        ));

        // process the login
        if ($validator->fails()) {

            $response = array(
                'status' => 500,
                'message' => 'Invalid data',
                'data' => $validator->errors()
            );
            return response()->json($response, $response['status']);
        }

        $brands = Brand::whereIn('id', $data['ids'])
            ->with('models.default_segment')
            ->where('status', '1')
            ->orderBy('id', 'desc')
            ->get();

        $i = 0;
        $allData = array();
        foreach ($brands as $brand) {
            
            if ( isset($brand->models) && count($brand->models) > 0 ) {
                foreach ($brand->models as $model) {
                    $allData[$i] = $model;
                    $allData[$i]['parent_id'] = $brand->name;

                    if ( isset($data['mode']) && $data['mode'] == 'edit' ) {
                        $checkVendorCarModelSetting = VendorCarModelSetting::where('vendor_id', $data['vendor_id'])
                            ->where('car_model_id', $model->id)
                            ->first();
                        if ( $checkVendorCarModelSetting ) {
                            unset($allData[$i]['default_segment']);
                            $allData[$i]['default_segment'] = $checkVendorCarModelSetting;
                        }
                    }

                    $i++;
                }
            }
        }

        $response = array(
            'status' => 200,
            'message' => '',
            'data' => $allData
        );

        return response()->json($response, $response['status']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function carList(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, array(
            'user_id' => 'required'
        ));

        // process the login
        if ($validator->fails()) {

            $validatorString = implode(", ", $validator->messages()->all());
            $validatorArray = array_combine(
                array_map(function($key){ return $key.'_error'; }, array_keys($validator->errors()->toArray())),
                $validator->errors()->toArray()
            );

            $response = array(
                'response_code' => 400,
                'response_message' => $validatorString,
                'response_data' => $validatorArray
            );
            return response()->json($response, 200);
        }

        $cars = UserCar::select('id', 'car_model_id', 'car_number')
            ->with(array('car_model' => function($query){
                $query->select('id', 'brand_id', 'name', 'image')->with('brand:id,name');
            }))
            ->where('user_id', $data['user_id'])
            ->where('is_deleted', '0')
            ->orderBy('id', 'desc')
            ->get();

        $response = array(
            'response_code' => 200,
            'response_message' => '',
            'response_data' => $cars
        );

        return response()->json($response, 200);
    }
}
