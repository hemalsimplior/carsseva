<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User, App\Models\Brand, App\Models\UserCar, App\Models\UserAddress, App\Models\State, App\Models\City, App\Models\VerifyUserEmail;
use Session, Redirect, Carbon\Carbon, Auth, Validator, Config;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers = User::where('is_deleted', '0')
            ->where('role', 'customer')
            ->orderBy('id', 'desc')
            ->get();
        
        $breadcrumbs = [
            ['link' => "dashboard", 'name' => "Home"],
            ['name' => "Customers"]
        ];
        
        return view('pages.customer.index', [
            'breadcrumbs' => $breadcrumbs,
            'customers' => $customers
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $currentUser = Auth::user();
        if ( $currentUser['role'] == 'admin' ) {
            
            $customer = User::withCount(['cars', 'addresses', 'orders', 'active_orders', 'completed_orders'])
                ->with(['cars'])
                ->find($id);

            $brands = Brand::where('is_deleted', '0')
                ->with('models.segment')
                ->orderBy('id', 'desc')
                ->get();
            if ( $brands ) {
                $brandsData = array();
                $brands = $brands->toArray();
                foreach ($brands as $k => $brand) {
                    $brandsData[$k] = array(
                        'id' => 0,
                        'text' => $brand['name'],
                    );
                    if ( $brand['models'] && count($brand['models']) > 0 ) {
                        $brandsData[$k]['children'] = array();
                        foreach ($brand['models'] as $mk => $model) {
                            $brandsData[$k]['children'][$mk] = array(
                                'id' => $model['id'],
                                'text' => $model['name'],
                            );
                        }
                    }
                }
                $brands = json_encode($brandsData);
            }

            $status = array();
            foreach (Config::get('custom.carsseva.all_order_status') as $s) {
                $status[$s['id']] = $s['name'];
            }

            $breadcrumbs = [
                ['link' => "dashboard", 'name' => "Home"],
                ['link' => "customers", 'name' => "Customers"],
                ['name' => "#".$customer->sequence_id]
            ];
            
            return view('pages.customer.show', [
                'hide_page_header' => false,
                'breadcrumbs_custom' => $breadcrumbs,
                'brands' => $brands,
                'status' => $status,
                'customer' => $customer
            ]);
            
        } else {
            return response( 'Permissions insuffisantes !', 403 );
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function bulkAction(Request $request)
    {
        $data = $request->all();
        $currentUser = Auth::user();
        $validator = Validator::make($data, array(
            'action' => 'required',
            'ids' => 'required',
            'type' => 'required',
        ));

        // process the login
        if ($validator->fails()) {

            $response = array(
                'status' => 500,
                'message' => 'Invalid data',
                'data' => $validator->errors()
            );
            return response()->json($response, $response['status']);
        }

        $dataUpdate = array();
        if ( isset($data['action']) && $data['action'] == 'active' ) {
            $dataUpdate['status'] = '1';
        } else if ( isset($data['action']) && $data['action'] == 'inactive' ) {
            $dataUpdate['status'] = '0';
        } else if ( isset($data['action']) && $data['action'] == 'delete' ) {
            $dataUpdate['is_deleted'] = '1';
        }

        $item = User::find($data['ids']);
        if ( $item->update($dataUpdate) ) {
            
            Session::flash('success', 'All selected items successfully '.$data['action'].'d!');
            $response = array(
                'status' => 200,
                'message' => ''
            );
        } else {
            
            Session::flash('danger', 'Some thing is wrong. Please try again');
            $response = array(
                'status' => 500,
                'message' => ''
            );
        }

        return response()->json($response, $response['status']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeCar(Request $request)
    {
        $data = $request->all();
        $currentUser = Auth::user();
        
        $validator = Validator::make($data, array(
            'user_id' => 'required',
            'car_model_id' => 'required',
            'fuel_type' => 'required',
            'gear_type' => 'required',
            'total_travelled' => 'required',
            'avg_monthly_usage' => 'required',
        ));

        // process the login
        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }

        //str_replace('/', '-', $data['end_date'])
        $data['manufacturing_year'] = Carbon::now()->format('Y-m-d');
        
        $userCar = UserCar::create($data);
        if ( $userCar ) {

            // redirect
            Session::flash('success', 'User car successfully created!');
            return Redirect::to('customers/'.$data['user_id']);
        } else {
            // redirect
            Session::flash('danger', 'Some thing is wrong. Please try again');
            return Redirect::to('customers/'.$data['user_id']);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateCar(Request $request, $id)
    {
        $data = $request->all();
        $currentUser = Auth::user();
        
        $validator = Validator::make($data, array(
            'user_id' => 'required',
            'car_model_id' => 'required',
            'fuel_type' => 'required',
            'gear_type' => 'required',
            'total_travelled' => 'required',
            'avg_monthly_usage' => 'required',
        ));

        // process the login
        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }

        //str_replace('/', '-', $data['end_date'])
        $data['manufacturing_year'] = Carbon::now()->format('Y-m-d');
        
        $userCar = UserCar::find($id);
        if ( $userCar->update($data) ) {

            // redirect
            Session::flash('success', 'User car successfully updated!');
            return Redirect::to('customers/'.$data['user_id']);
        } else {
            // redirect
            Session::flash('danger', 'Some thing is wrong. Please try again');
            return Redirect::to('customers/'.$data['user_id']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyCar($id, Request $request)
    {
        $currentUser = Auth::user();
        $data['is_deleted'] = '1';

        $item = UserCar::find($id);
        if ( $item->update($data) ) {

            // redirect
            Session::flash('success', 'Customer car successfully deleted!');
            return Redirect::to('customers/'.$item['user_id']);
        } else {

            // redirect
            Session::flash('danger', 'Some thing is wrong. Please try again');
            return Redirect::to('customers/'.$item['user_id']);
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeAddress(Request $request)
    {
        $data = $request->all();
        $currentUser = Auth::user();
        
        $validator = Validator::make($data, array(
            'user_id' => 'required',
            'address_line_1' => 'required',
            'state_id' => 'required|integer',
            'city_id' => 'required|integer',
            'zipcode' => 'required'
        ));

        // process the login
        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }

        $userAddress = UserAddress::create($data);
        if ( $userAddress ) {

            // redirect
            Session::flash('success', 'User address successfully created!');
            return Redirect::to('customers/'.$data['user_id']);
        } else {
            // redirect
            Session::flash('danger', 'Some thing is wrong. Please try again');
            return Redirect::to('customers/'.$data['user_id']);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateAddress(Request $request, $id)
    {
        $data = $request->all();
        $currentUser = Auth::user();
        
        $validator = Validator::make($data, array(
            'user_id' => 'required',
            'address_line_1' => 'required',
            'state_id' => 'required|integer',
            'city_id' => 'required|integer',
            'zipcode' => 'required'
        ));

        // process the login
        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }

        $userAddress = UserAddress::find($id);
        if ( $userAddress->update($data) ) {

            // redirect
            Session::flash('success', 'User address successfully updated!');
            return Redirect::to('customers/'.$data['user_id']);
        } else {
            // redirect
            Session::flash('danger', 'Some thing is wrong. Please try again');
            return Redirect::to('customers/'.$data['user_id']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyAddress($id, Request $request)
    {
        $currentUser = Auth::user();
        $data['is_deleted'] = '1';

        $item = UserAddress::find($id);
        if ( $item->update($data) ) {

            // redirect
            Session::flash('success', 'Customer address successfully deleted!');
            return Redirect::to('customers/'.$item['user_id']);
        } else {

            // redirect
            Session::flash('danger', 'Some thing is wrong. Please try again');
            return Redirect::to('customers/'.$item['user_id']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function primaryAddress($id, Request $request)
    {
        $data['is_primary'] = '1';
        $item = UserAddress::find($id);
        UserAddress::where('user_id', $item->user_id)->update(array('is_primary' => '0'));
        if ( $item->update($data) ) {

            // redirect
            Session::flash('success', 'Customer address successfully updated!');
            return Redirect::to('customers/'.$item['user_id']);
        } else {

            // redirect
            Session::flash('danger', 'Some thing is wrong. Please try again');
            return Redirect::to('customers/'.$item['user_id']);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function location(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, array(
            'type' => 'required',
            'country_id' => 'required_unless:type,country|integer',
            'state_id' => 'required_if:type,city|integer',
        ));

        // process the login
        if ($validator->fails()) {

            $validatorString = implode(", ", $validator->messages()->all());
            $validatorArray = array_combine(
                array_map(function($key){ return $key.'_error'; }, array_keys($validator->errors()->toArray())),
                $validator->errors()->toArray()
            );

            $response = array(
                'response_code' => 400,
                'response_message' => $validatorString,
                'response_data' => $validatorArray
            );
            return response()->json($response, 200);
        }
        
        if ( $data['type'] == 'state' ) {
            $location = State::select('id', 'name')
                ->where('country_id', $data['country_id'])
                ->get();
        } else {
            $location = City::select('id', 'name')
                ->where('country_id', $data['country_id'])
                ->where('state_id', $data['state_id'])
                ->get();
        }

        $response = array(
            'response_code' => 200,
            'response_message' => '',
            'response_data' => $location
        );

        return response()->json($response, 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function verifyUser(Request $request, $token)
    {
        $VerifyUserEmail = VerifyUserEmail::where('token', $token)->first();
        if ( $VerifyUserEmail ) {
            $userData = User::find($VerifyUserEmail['user_id']);
            if ( $userData->update(array('email' => $VerifyUserEmail['email_id'], 'is_email_verified' => '1')) ) {
                VerifyUserEmail::where('token', $token)->delete();
                $msg = "Your email address successfully verified!";
            }
        } else {
            $msg = "Your token is invalid, Please try again!";
        }

        $pageConfigs = [
            'bodyClass' => "bg-full-screen-image",
            'blankPage' => true
        ];
  
        return view('/pages/customer/verification', [
            'msg' => $msg,
            'pageConfigs' => $pageConfigs
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function customerList(Request $request)
    {
        $data = $request->all();
        $customers = User::select('id', 'name')
            ->where('role', 'customer')
            ->where('status', '1')
            ->where('is_deleted', '0');

        if ( isset($data['s']) && $data['s'] != '' ) {
            $customers = $customers->where('name', 'like', '%'.$data['s'].'%');
        }
        $customers = $customers->get();

        $response = array(
            'response_code' => 200,
            'response_message' => '',
            'response_data' => $customers
        );

        return response()->json($response, 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function customerAddressList(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, array(
            'user_id' => 'required'
        ));

        // process the login
        if ($validator->fails()) {

            $validatorString = implode(", ", $validator->messages()->all());
            $validatorArray = array_combine(
                array_map(function($key){ return $key.'_error'; }, array_keys($validator->errors()->toArray())),
                $validator->errors()->toArray()
            );

            $response = array(
                'response_code' => 400,
                'response_message' => $validatorString,
                'response_data' => $validatorArray
            );
            return response()->json($response, 200);
        }

        $addresses = UserAddress::select('id', 'address_line_1', 'address_line_2', 'state_id', 'city_id', 'latitude', 'longitude', 'zipcode', 'is_primary')
            ->with(array('state' => function($query){
                $query->select('id', 'country_id', 'name')->with('country:id,name');
            }))
            ->with('city:id,name')
            ->where('user_id', $data['user_id'])
            ->where('is_deleted', '0')
            ->get();

        $response = array(
            'response_code' => 200,
            'response_message' => '',
            'response_data' => $addresses
        );

        return response()->json($response, 200);
    }
}
