<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Excel, App\Imports\BrandsImport;

class MigrationController extends Controller
{

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        set_time_limit(0);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function brands()
    {
        dd('exit');
        
        $latitudeFrom = '23.0668884';
        $longitudeFrom = '72.5086395';
        $latitudeTo = '22.2857183';
        $longitudeTo = '70.768798';

        $geocodeFrom = file_get_contents('https://maps.googleapis.com/maps/api/distancematrix/json?origins='.$latitudeFrom.','.$longitudeFrom.'&destinations='.$latitudeTo.','.$longitudeTo.'&key=AIzaSyBNcEJms1i-QoRa03JIqzgDNhQf8hv1H9M');
        $outputFrom = json_decode($geocodeFrom, true);
        if ( $outputFrom['status'] === 'OK' ) {
            dump('Dis: '.$outputFrom['rows'][0]['elements'][0]['distance']['text']);
        }
        dd($outputFrom);

        /* //$dist = $this->getDistance('23.0668884', '23.0599758', '72.5086395', '72.5457505', 'K');
        $dist = $this->getDistance('23.0668884', '22.2857183', '72.5086395', '70.768798', 'K');
        dd($dist); */

        //$brands = Excel::import(storage_path('app/public') . '/glr-dealer.xls', null, \Maatwebsite\Excel\Excel::XLS);
        //$sheetData = Excel::loadView(storage_path('app/public') . '/glr-dealer.xls')->get();
        //$sheetData = Excel::load(storage_path('app/public') . '/glr-dealer.xls')->get();
        Excel::import(new BrandsImport, storage_path('app/public') . '/car-brands-models.xls');
        dd('as');
    }

    function GetDrivingDistance($lat1, $lat2, $long1, $long2) {
        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat1.",".$long1."&destinations=".$lat2.",".$long2."&key=AIzaSyCOeXxibK2_3Pr2hkvwJ_4MvE9_ykurLzI";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $response_a = json_decode($response, true);
        
        /* $dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
        $time = $response_a['rows'][0]['elements'][0]['duration']['text']; */

        return $response_a;
    }

    /**
     * @function getDistance()
     * Calculates the distance between two address
     * 
     * @params
     * $addressFrom - Starting point
     * $addressTo - End point
     * $unit - Unit type
     * 
     * @author CodexWorld
     * @url https://www.codexworld.com
     *
     */
    function getDistance($latitudeFrom, $latitudeTo, $longitudeFrom, $longitudeTo, $unit){
        
        // Calculate distance between latitude and longitude
        $theta    = $longitudeFrom - $longitudeTo;
        $dist    = sin(deg2rad($latitudeFrom)) * sin(deg2rad($latitudeTo)) +  cos(deg2rad($latitudeFrom)) * cos(deg2rad($latitudeTo)) * cos(deg2rad($theta));
        $dist    = acos($dist);
        $dist    = rad2deg($dist);
        $miles    = $dist * 60 * 1.1515;
        
        // Convert unit and return distance
        $unit = strtoupper($unit);
        if($unit == "K"){
            return round($miles * 1.609344, 2).' km';
        } elseif ($unit == "M") {
            return round($miles * 1609.344, 2).' meters';
        } else {
            return round($miles, 2).' miles';
        }
    }
}
