<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use App\Models\Service, App\Models\VendorServiceSetting, App\Models\UserCar, App\Models\Vendor;
use Session, Redirect, Carbon\Carbon, Validator, Auth, File, Storage;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::where('parent_id', 0)
            ->where('is_deleted', '0')
            ->with('subServices.segment_price')
            ->orderBy('order_number', 'asc')
            ->get();

        $subservices = Service::where('parent_id', '!=', 0)
            ->where('is_deleted', '0')
            ->orderBy('order_number', 'asc')
            ->get();

        $subservices_name = array();
        if ( $subservices ) {
            $subservices = $subservices->toArray();
            $subservices_name = array_map('strtolower', array_column($subservices, 'name'));
        }
        
        $breadcrumbs = [
            ['link' => "dashboard", 'name' => "Home"],
            ['name' => "Manage Services"]
        ];
        
        return view('pages.service.index', [
            'breadcrumbs' => $breadcrumbs,
            'services' => $services,
            'subservices_name' => json_encode($subservices_name)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $currentUser = Auth::user();
        
        $validatorArr = array(
            'type' => 'required'
        );

        if ( isset($data['type']) && $data['type'] != '' ) {
            if ( $data['type'] == 'service' ) {
                //$validatorArr['name'] = 'required|unique:services';
                $validatorArr['name'] = ['required',
                    Rule::unique('services', 'name')->where(function ($query) {
                        return $query->where('parent_id', '0');
                    })
                ];
            } else {
                $validatorArr['model_name'] = 'required';
                $validatorArr['parent_id'] = 'required';
            }
        }

        $validator = Validator::make($data, $validatorArr);

        // process the login
        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }
        
        // Create Service
        if ( $data['type'] == 'service' ) {

            $data['parent_id'] = 0;
            
            $image = (isset($data['image_file']) ? $data['image_file']: '');
            unset($data['image_file']);

            $service = Service::create($data);
            if ( $service ) {

                if ( isset($image) && $image != '' ) {

                    //$profile_image_name = "user-".time().".pdf";
                    $img = explode(',', $image);
                    $ini =substr($img[0], 11);
                    $type = explode(';', $ini);
                    $extention = str_replace('+xml', '', $type[0]);
                    $profile_image_name = Carbon::now()->timestamp . '.' .$extention;

                    $subpath = "public/uploads/services/";
                    $path = storage_path('app') . "/" . $subpath;

                    if( !File::isDirectory($path) ){
                        File::makeDirectory($path, 0755, true, true);
                    }

                    $image = substr($image, strpos($image, ",")+1);
                    $data1 = base64_decode($image);
                    
                    // delete old file
                    if( $service->image != '' && Storage::exists($service->image) ){
                        Storage::delete($service->image);
                    }

                    if ( Storage::put($subpath . $profile_image_name, $data1) ) {
                        Service::where('id', $service->id)->update(array('image' => $profile_image_name));
                    }
                }

                // redirect
                Session::flash('success', 'Service '.$data['name'].' successfully created!');
                return Redirect::to('services');
            } else {
                // redirect
                Session::flash('danger', 'Some thing is wrong. Please try again');
                return Redirect::to('services');
            }
        } else {

            $result = array();
            foreach ($data['model_name'] as $index => $name) {

                $subserviceData = array(
                    'parent_id' => $data['parent_id'],
                    'description' => $data['model_description'][$index],
                    'name' => $name
                );

                $subservice = Service::create($subserviceData);
                if ( $subservice ) {

                    $segmentSetting = array(
                        'service_id' => $subservice['id'],
                        'small' => $data['small'][$index],
                        'medium' => $data['medium'][$index],
                        'large' => $data['large'][$index],
                        'premium' => $data['premium'][$index],
                    );

                    if ( $currentUser['role'] == 'admin' ) {
                        $segmentSetting['vendor_id'] = 0;
                    } else {
                        $segmentSetting['vendor_id'] = $currentUser['id'];
                    }
                    
                    $storeVendorServiceSetting = VendorServiceSetting::create($segmentSetting);
                    if ( $storeVendorServiceSetting ) {
                        array_push($result, $subservice->name);
                    }
                }
            }

            if ( count($result) > 0 ) {
                // redirect
                Session::flash('success', implode(', ', $result).' these sub service are successfully created!');
                return Redirect::to('services');
            } else {
                // redirect
                Session::flash('danger', 'Some thing is wrong. Please try again');
                return Redirect::to('services');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $currentUser = Auth::user();
        
        $validatorArr = array(
            'type' => 'required'
        );

        if ( isset($data['type']) && $data['type'] != '' ) {
            if ( $data['type'] == 'service' ) {
                //$validatorArr['name'] = 'required|unique:services,name,'.$id;
                $validatorArr['name'] = ['required',
                    Rule::unique('services', 'name')->where(function ($query) use($id) {
                        return $query->where('id', '!=', $id)
                            ->where('parent_id', '0');
                    })
                ];
            } else {
                $validatorArr['model_name'] = 'required';
                $validatorArr['parent_id'] = 'required';
            }
        }

        $validator = Validator::make($data, $validatorArr);

        // process the login
        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }
        
        // Create Service
        if ( $data['type'] == 'service' ) {

            $data['parent_id'] = 0;
            
            $image = (isset($data['image_file']) ? $data['image_file']: '');
            unset($data['image_file']);

            $service = Service::find($id);
            $oldImage = $service->image;
            if ( $service->update($data) ) {

                if ( $image != $oldImage ) {

                    $subpath = "public/uploads/services/";
                    if ( isset($image) && $image != '' ) {

                        //$profile_image_name = "user-".time().".pdf";
                        $img = explode(',', $image);
                        $ini =substr($img[0], 11);
                        $type = explode(';', $ini);
                        $extention = str_replace('+xml', '', $type[0]);
                        $profile_image_name = Carbon::now()->timestamp . '.' .$extention;

                        $path = storage_path('app') . "/" . $subpath;

                        if( !File::isDirectory($path) ){
                            File::makeDirectory($path, 0755, true, true);
                        }

                        $image = substr($image, strpos($image, ",")+1);
                        $data1 = base64_decode($image);
                        
                        // delete old file
                        if( $oldImage != '' && Storage::exists($subpath . $oldImage) ){
                            Storage::delete($subpath . $oldImage);
                        }

                        if ( Storage::put($subpath . $profile_image_name, $data1) ) {
                            Service::where('id', $service->id)->update(array('image' => $profile_image_name));
                        }
                    } else {
                        if( $oldImage != '' && Storage::exists($subpath . $oldImage) ){
                            Storage::delete($subpath . $oldImage);
                            Service::find($id)->update(array('image' => ''));
                        }
                    }

                }

                // redirect
                Session::flash('success', 'Service '.$data['name'].' successfully updated!');
                return Redirect::to('services');
            } else {
                // redirect
                Session::flash('danger', 'Some thing is wrong. Please try again');
                return Redirect::to('services');
            }
        } else {

            $data['name'] = $data['model_name'][0];
            $data['description'] = $data['model_description'][0];
            $data['order_number'] = $data['model_order_number'][0];
            
            $model = Service::find($id);
            $oldImage = $model->image;
            if ( $model->update($data) ) {

                // Sync service cost
                $segmentSetting = array(
                    'small' => $data['small'][0],
                    'medium' => $data['medium'][0],
                    'large' => $data['large'][0],
                    'premium' => $data['premium'][0],
                );

                $sett = VendorServiceSetting::where('service_id', $id);
                if ( $currentUser['role'] == 'admin' ) {
                    $sett = $sett->where('vendor_id', 0);
                } else {
                    $sett = $sett->where('vendor_id', $currentUser['vendor_id']);
                }

                if ( $sett->update($segmentSetting) ) {
                    
                    // redirect
                    Session::flash('success', 'Sub service '.$data['name'].' successfully updated!');
                    return Redirect::to('services');
                } else {

                    // redirect
                    Session::flash('danger', 'Some thing is wrong. Please try again');
                    return Redirect::to('services');
                }

            } else {

                // redirect
                Session::flash('danger', 'Some thing is wrong. Please try again');
                return Redirect::to('services');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $currentUser = Auth::user();
        $data['is_deleted'] = '1';

        $itme = Service::find($id);
        if ( $itme->update($data) ) {

            $itmeName = 'Subservice';
            if ( isset($itme->parent_id) && $itme->parent_id == 0 ) {
                $itmeName = 'Service';
                $itme = Service::where('parent_id', $id)->update($data);
            }

            // redirect
            Session::flash('success', $itmeName.' successfully deleted!');
            return Redirect::to('services');
        } else {

            // redirect
            Session::flash('danger', 'Some thing is wrong. Please try again');
            return Redirect::to('services');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function bulkAction(Request $request)
    {
        $data = $request->all();
        $currentUser = Auth::user();
        $validator = Validator::make($data, array(
            'action' => 'required',
            'ids' => 'required',
            'type' => 'required',
        ));

        // process the login
        if ($validator->fails()) {

            $response = array(
                'status' => 500,
                'message' => 'Invalid data',
                'data' => $validator->errors()
            );
            return response()->json($response, $response['status']);
        }

        $dataUpdate = array();
        if ( isset($data['action']) && $data['action'] == 'active' ) {
            $dataUpdate['status'] = '1';
        } else if ( isset($data['action']) && $data['action'] == 'inactive' ) {
            $dataUpdate['status'] = '0';
        } else if ( isset($data['action']) && $data['action'] == 'delete' ) {
            $dataUpdate['is_deleted'] = '1';
        }

        $item = Service::find($data['ids']);
        if ( $item->update($dataUpdate) ) {
            
            if ( isset($item['parent_id']) && $item['parent_id'] == 0 ) {
                Service::where('parent_id', $data['ids'])->update($dataUpdate);
            } else {
                if ( isset($data['action']) && $data['action'] == 'active' ) {
                    Service::find($item['parent_id'])->update($dataUpdate);
                }
            }

            Session::flash('success', 'All selected items successfully '.$data['action'].'d!');
            $response = array(
                'status' => 200,
                'message' => ''
            );
        } else {
            
            Session::flash('danger', 'Some thing is wrong. Please try again');
            $response = array(
                'status' => 500,
                'message' => ''
            );
        }

        return response()->json($response, $response['status']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getSubServicesFromServices(Request $request)
    {
        $data = $request->all();
        $currentUser = Auth::user();
        $validator = Validator::make($data, array(
            'ids' => 'required'
        ));

        // process the login
        if ($validator->fails()) {

            $response = array(
                'status' => 500,
                'message' => 'Invalid data',
                'data' => $validator->errors()
            );
            return response()->json($response, $response['status']);
        }

        $services = Service::where('parent_id', 0);

        if ( isset($data['ids']) && $data['ids'] != 'all' ) {
            $services = $services->whereIn('id', $data['ids']);
        }

        $services = $services->where('is_deleted', '0')
            ->where('status', '1')
            ->with('subServices.default_segment_price')
            ->orderBy('order_number', 'asc')
            ->get();

        $i = 0;
        $allData = array();
        foreach ($services as $service) {
            
            /*$allData[$i] = $service;
            $allData[$i]['service_id'] = $service->name;
            $i++;*/

            if ( isset($service->subServices) && count($service->subServices) > 0 ) {
                foreach ($service->subServices as $subservice) {
                    if ( $subservice->status == '1' ) {
                        $allData[$i] = $subservice;
                        $allData[$i]['service_id'] = $service->name;

                        if ( isset($data['mode']) && $data['mode'] == 'edit' ) {
                            $checkVendorServiceSetting = VendorServiceSetting::where('vendor_id', $data['vendor_id'])
                                ->where('service_id', $subservice->id)
                                ->first();
                            if ( $checkVendorServiceSetting ) {
                                unset($allData[$i]['default_segment_price']);
                                $allData[$i]['default_segment_price'] = $checkVendorServiceSetting;
                            }
                        }
                        $i++;
                    }
                }
            }
        }

        $response = array(
            'status' => 200,
            'message' => '',
            'data' => $allData
        );

        return response()->json($response, $response['status']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getServiceCost(Request $request)
    {
        $data = $request->all();
        $currentUser = Auth::user();
        $validator = Validator::make($data, array(
            'car_id' => 'required',
            'service_id' => 'required'
        ));

        // process the login
        if ($validator->fails()) {

            $response = array(
                'status' => 500,
                'message' => 'Invalid data',
                'data' => $validator->errors()
            );
            return response()->json($response, $response['status']);
        }

        $userCar = UserCar::find($data['car_id']);
        $serviceCost = $this->serviceCost($data['service_id'], $userCar['car_model_id'], $data['vendor_id']);
        
        $response = array(
            'status' => 200,
            'message' => '',
            'data' => array(
                'cost' => $serviceCost
            )
        );

        return response()->json($response, $response['status']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function serviceList(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, array(
            'vendor_id' => 'required'
        ));

        // process the login
        if ($validator->fails()) {

            $validatorString = implode(", ", $validator->messages()->all());
            $validatorArray = array_combine(
                array_map(function($key){ return $key.'_error'; }, array_keys($validator->errors()->toArray())),
                $validator->errors()->toArray()
            );

            $response = array(
                'response_code' => 400,
                'response_message' => $validatorString,
                'response_data' => $validatorArray
            );
            return response()->json($response, 200);
        }

        $vendor = Vendor::select('id')->find($data['vendor_id']);
        $services = Service::whereIn('id', $vendor->services->pluck('id')->toArray())
            ->where('parent_id', 0)
            ->where('is_deleted', '0')
            ->with('subServices');
        
        if ( isset($data['s']) && $data['s'] != '' ) {
            $services = $services->where('name', 'like', '%'.$data['s'].'%');
        }
        $services = $services->orderBy('order_number', 'asc')
            ->get();
        
        if ( $services ) {
            $servicesData = array();
            $services = $services->toArray();
            foreach ($services as $k => $ser) {
                $servicesData[$k] = array(
                    'id' => 0,
                    'text' => $ser['name'],
                );
                if ( $ser['sub_services'] && count($ser['sub_services']) > 0 ) {
                    $servicesData[$k]['children'] = array();
                    foreach ($ser['sub_services'] as $sk => $sub_ser) {
                        $servicesData[$k]['children'][$sk] = array(
                            'id' => $sub_ser['id'],
                            'text' => $sub_ser['name'],
                        );
                    }
                }
            }
        }

        $response = array(
            'response_code' => 200,
            'response_message' => '',
            'response_data' => $servicesData
        );

        return response()->json($response, 200);
    }
}
