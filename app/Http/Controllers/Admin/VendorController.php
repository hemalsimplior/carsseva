<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;

use App\Models\Vendor, App\Models\Brand, App\Models\Service, App\Models\VendorServiceSetting, App\Models\VendorCarModelSetting, App\Models\Order, App\Models\TempOrder, App\User;
use Session, Redirect, Carbon\Carbon, Auth, Validator, Config, File, Storage, LaravelMsg91, DB;

class VendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /* $msg = "Welcome to CarsSeva,\n Congratulations for being partner with us. Kindly setup your account by logining into ".url('/')." \n User: 7600004204 \n Password: ASASASAS";
        $result = LaravelMsg91::message(917600004204, $msg);
        dd($result); */
        $vendors = Vendor::where('is_deleted', '0')
            ->withCount('orders')
            ->orderBy('id', 'desc')
            ->get();
        
            $breadcrumbs = [
            ['link' => "dashboard", 'name' => "Home"],
            ['name' => "Vendors"]
        ];
        
        return view('pages.vendor.index', [
            'breadcrumbs' => $breadcrumbs,
            'vendors' => $vendors
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brands = Brand::where('is_deleted', '0')
            ->with('models.segment')
            ->orderBy('id', 'desc')
            ->get();

        $services = Service::where('parent_id', 0)
            ->where('is_deleted', '0')
            ->with('subServices.segment_price')
            ->orderBy('order_number', 'asc')
            ->get();

        $breadcrumbs = [
            ['link' => "dashboard", 'name' => "Home"],
            ['link' => "vendor", 'name' => "Vendor"],
            ['name' => "Add Vendor"]
        ];
        
        return view('pages.vendor.create', [
            'breadcrumbs' => $breadcrumbs,
            'brands' => $brands,
            'services' => $services,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        //dd($data);
        $currentUser = Auth::user();
        $validator = Validator::make($data, array(
            //'code' => 'required',
            'name' => 'required|unique:vendors',
            //'owner_contact_no' => 'required',
            'owner_contact_no' => [ 'required',
                Rule::unique('users', 'contact_no')->where(function ($query) {
                    return $query->whereIn('role', array('vendor_admin','manager','pickup_person'))
                        ->where('is_deleted', 0);
                })
            ],
            'contact_no' => 'required|unique:vendors',
            'address_line_1' => 'required',
            'city_id' => 'required',
            'state_id' => 'required',
            'zipcode' => 'required',
            //'brands' => 'required',
            //'serice' => 'required',
        ));

        // process the login
        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        }

        // find seq id
        $prefix = Config::get('custom.carsseva.prefix.vendor');
        $lastVendor = Vendor::select('sequence_id')->orderBy('ID', 'desc')->first();
        if ( isset($lastVendor->sequence_id) ) {
            $data['sequence_id'] = $this->increaseSeqNo($lastVendor->sequence_id, $prefix);
        } else {
            $data['sequence_id'] = $prefix.'0001';
        }

        $data['code'] = $this->initials($data['name']);

        /* $logo = (isset($data['logo']) ? $data['logo']: '');
        unset($data['logo']);

        $agreement = (isset($data['agreement']) ? $data['agreement']: '');
        unset($data['agreement']);

        $GST_certificate = (isset($data['GST_certificate']) ? $data['GST_certificate']: '');
        unset($data['GST_certificate']);

        $aadhar_card = (isset($data['aadhar_card']) ? $data['aadhar_card']: '');
        unset($data['aadhar_card']);

        $PAN_card = (isset($data['PAN_card']) ? $data['PAN_card']: '');
        unset($data['PAN_card']); */

        $data['account_section'] = serialize(array(
            'basic' => false,
            'manufacturers' => false,
            'segment' => false,
            'services' => false,
        ));
        
        $vendor = Vendor::create($data);
        if ( $vendor ) {

            // Create Vendor Admin
            $originalPass = Str::random(9);
            $vendorAdmin = array(
                'role' => 'vendor_admin',
                'vendor_id' => $vendor->id,
                'name' => $data['owner_name'],
                'aadhar_card_number' => ((isset($data['aadhar_card_number']) && $data['aadhar_card_number'] != '') ? $data['aadhar_card_number'] : ''),
                'PAN_number' => ((isset($data['PAN_number']) && $data['PAN_number'] != '') ? $data['PAN_number'] : ''),
                'contact_no' => $data['owner_contact_no'],
                'password' => Hash::make($originalPass),
            );
            $prefix = Config::get('custom.carsseva.prefix.admin');
            $lastVendorAdmin = User::select('sequence_id')->where('role', 'vendor_admin')->orderBy('ID', 'desc')->first();
            if ( isset($lastVendorAdmin->sequence_id) ) {
                $vendorAdmin['sequence_id'] = $this->increaseSeqNo($lastVendorAdmin->sequence_id, $vendor->code.$prefix);
            } else {
                $vendorAdmin['sequence_id'] = $vendor->code.$prefix.'0001';
            }
            $createAdmin = User::create($vendorAdmin);
            if ( $createAdmin ) {

                $subpath1 = "public/uploads/users/".$createAdmin->id."/";
                
                /* // Admin Aadhar Card
                if ( isset($aadhar_card) && $aadhar_card != '' ) {

                    $img = explode(',', $aadhar_card);
                    $ini =substr($img[0], 11);
                    $type = explode(';', $ini);
                    $extention = str_replace('+xml', '', $type[0]);
                    $aadhar_card_name = Carbon::now()->timestamp . 'a.' .$extention;

                    $path = storage_path('app') . "/" . $subpath1;

                    if( !File::isDirectory($path) ){
                        File::makeDirectory($path, 0755, true, true);
                    }

                    $aadhar_card = substr($aadhar_card, strpos($aadhar_card, ",")+1);
                    $data1 = base64_decode($aadhar_card);
                    
                    if ( Storage::put($subpath1 . $aadhar_card_name, $data1) ) {
                        User::where('id', $createAdmin->id)->update(array('aadhar_card' => $aadhar_card_name));
                    }
                }

                // Admin PAN Card
                if ( isset($PAN_card) && $PAN_card != '' ) {

                    $img = explode(',', $PAN_card);
                    $ini =substr($img[0], 11);
                    $type = explode(';', $ini);
                    $extention = str_replace('+xml', '', $type[0]);
                    $PAN_card_name = Carbon::now()->timestamp . 'p.' .$extention;

                    $path = storage_path('app') . "/" . $subpath1;

                    if( !File::isDirectory($path) ){
                        File::makeDirectory($path, 0755, true, true);
                    }

                    $PAN_card = substr($PAN_card, strpos($PAN_card, ",")+1);
                    $data1 = base64_decode($PAN_card);
                    
                    if ( Storage::put($subpath1 . $PAN_card_name, $data1) ) {
                        User::where('id', $createAdmin->id)->update(array('PAN_card' => $PAN_card_name));
                    }
                } */

                // Send Welcome SMS with password
                //$msg = "Hello ".$createAdmin['name'].", Thank you for signing up with us. Your new account has been setup and you can now login to our portal area using your contact numer and password. ".$originalPass." is your portal's passwprd.";
                $msg = "Welcome to CarsSeva,\nCongratulations for being partner with us. Kindly setup your account by logining into ".url('/login')." \nUser: ".$createAdmin['contact_no']." \nPassword: ".$originalPass;
                
                try {
                    $result = LaravelMsg91::message('91'.$createAdmin['contact_no'], $msg);
                } catch (\Throwable $th) {
                    $result = '';
                }
            }


            /* $subpath = "public/uploads/vendors/".$vendor->id."/";
            // Vendor Logo
            if ( isset($logo) && $logo != '' ) {

                $img = explode(',', $logo);
                $ini =substr($img[0], 11);
                $type = explode(';', $ini);
                $extention = str_replace('+xml', '', $type[0]);
                $logo_name = Carbon::now()->timestamp . 'l.' .$extention;

                $path = storage_path('app') . "/" . $subpath;

                if( !File::isDirectory($path) ){
                    File::makeDirectory($path, 0755, true, true);
                }

                $logo = substr($logo, strpos($logo, ",")+1);
                $data1 = base64_decode($logo);
                
                if ( Storage::put($subpath . $logo_name, $data1) ) {
                    Vendor::where('id', $vendor->id)->update(array('logo' => $logo_name));
                }
            }

            // Vendor Agreement
            if ( isset($agreement) && $agreement != '' ) {

                $img = explode(',', $agreement);
                $ini =substr($img[0], 11);
                $type = explode(';', $ini);
                $extention = str_replace('+xml', '', $type[0]);
                $agreement_name = Carbon::now()->timestamp . 'a.' .$extention;

                $path = storage_path('app') . "/" . $subpath;

                if( !File::isDirectory($path) ){
                    File::makeDirectory($path, 0755, true, true);
                }

                $agreement = substr($agreement, strpos($agreement, ",")+1);
                $data1 = base64_decode($agreement);
                
                if ( Storage::put($subpath . $agreement_name, $data1) ) {
                    Vendor::where('id', $vendor->id)->update(array('agreement' => $agreement_name));
                }
            }

            // Vendor GST Certificate
            if ( isset($GST_certificate) && $GST_certificate != '' ) {

                $img = explode(',', $GST_certificate);
                $ini =substr($img[0], 11);
                $type = explode(';', $ini);
                $extention = str_replace('+xml', '', $type[0]);
                $GST_certificate_name = Carbon::now()->timestamp . 'g.' .$extention;

                $path = storage_path('app') . "/" . $subpath;

                if( !File::isDirectory($path) ){
                    File::makeDirectory($path, 0755, true, true);
                }

                $GST_certificate = substr($GST_certificate, strpos($GST_certificate, ",")+1);
                $data1 = base64_decode($GST_certificate);
                
                if ( Storage::put($subpath . $GST_certificate_name, $data1) ) {
                    Vendor::where('id', $vendor->id)->update(array('GST_certificate' => $GST_certificate_name));
                }
            }

            // sync brands
            $vendor->brands()->sync($data['brands']);

            // sync services
            $vendor->services()->sync($data['services']);

            // sync sub services cost
            if ( isset($data['serice']) && count($data['serice']) ) {
                foreach ($data['serice'] as $service_id => $cost) {
                    
                    $segmentSetting = array(
                        'vendor_id' => $vendor->id,
                        'service_id' => $service_id,
                        'small' => $cost['small'],
                        'medium' => $cost['medium'],
                        'large' => $cost['large'],
                        'premium' => $cost['premium'],
                    );
                    $storeVendorServiceSetting = VendorServiceSetting::create($segmentSetting);
                }
            } */

            // redirect
            Session::flash('success', 'Vendor '.$data['name'].' successfully created!');
            return Redirect::to('vendor');
        } else {
            // redirect
            Session::flash('danger', 'Some thing is wrong. Please try again');
            return Redirect::to('vendor');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $currentUser = Auth::user();
        if ( $currentUser['role'] == 'admin' || ($currentUser['role'] == 'vendor_admin' && $currentUser['vendor_id'] == $id) ) {
            
            $vendor = Vendor::withCount(['orders', 'active_orders', 'completed_orders'])
                ->find($id);

            $status = array();
            foreach (Config::get('custom.carsseva.all_order_status') as $s) {
                $status[$s['id']] = $s['name'];
            }

            $orders = Order::where('is_deleted', '0')
                ->where('vendor_id', $id)
                ->with('items');
            
            $orders = $orders->orderBy('id', 'desc')->get()->map(function ($order) {
                if ( $order->is_awaiting_for_approval && $order->temp_order_id != '' && $order->temp_order_id != null ) {
                    $order = TempOrder::with('items')->find($order->temp_order_id);
                } else {
                    $order->order_id = $order->id;
                }
                return $order;
            });

            $breadcrumbs = [
                ['link' => "dashboard", 'name' => "Home"],
                ['link' => "vendor", 'name' => "Vendors"],
                ['name' => "#".$vendor->sequence_id]
            ];

            if ( $currentUser['role'] == 'vendor_admin' ) {
                unset($breadcrumbs[1]);
            }

            $vendor['total_revenue'] = DB::table('vendor_revenue')
                ->where('vendor_id', $id)
                ->first();
            
            return view('pages.vendor.show', [
                'hide_page_header' => false,
                'breadcrumbs_custom' => $breadcrumbs,
                'status' => $status,
                'vendor' => $vendor,
                'orders' => $orders,
                'allow_status' => Config::get('custom.carsseva.allow_status')
            ]);
            
        } else {
            return response( 'Permissions insuffisantes !', 403 );
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {   
        $data = $request->all();
        $currentUser = Auth::user();
        if ( $currentUser['role'] == 'admin' || ($currentUser['role'] == 'vendor_admin' && $currentUser['vendor_id'] == $id) ) {

            $vendor = Vendor::find($id);
            $vendor['brands_arr'] = $vendor->brands->pluck('id')->toArray();
            $vendor['services_arr'] = $vendor->services->pluck('id')->toArray();
            $vendor['account_section'] = unserialize($vendor->account_section);

            $progress = array(
                'per' => 15,
                'pendingSection' => ''
            );
            foreach ($vendor['account_section'] as $section => $val) {
                if ( $section == 'basic' ) {
                    if ($val) {
                        $progress['per'] += 10;
                    } else {
                        $progress['pendingSection'] .= 'Basic Infomation, ';
                    }   
                } else if ( $section == 'manufacturers' ) {
                    if ( $val ) {
                        $progress['per'] += 25;
                    } else {
                        $progress['pendingSection'] .= 'Car Manufacturers, ';
                    }
                } else if ( $section == 'segment' ) {
                    if ( $val ) {
                        $progress['per'] += 25;
                    } else {
                        $progress['pendingSection'] .= 'Assign Segments, ';
                    }
                } else if ( $section == 'services' ) {
                    if ( $val ) {
                        $progress['per'] += 25;
                    } else {
                        $progress['pendingSection'] .= 'Manage Services, ';
                    }
                }
            }
            $progress['pendingSection'] = rtrim($progress['pendingSection'], ', ');
            
            $brands = Brand::where('is_deleted', '0')
                ->with('models.segment')
                ->where('status', '1')
                ->orderBy('id', 'asc')
                ->get();

            $services = Service::where('parent_id', 0)
                ->where('status', '1')
                ->where('is_deleted', '0')
                ->with('subServices.segment_price')
                //->with('subServicesActive.segment_price')
                ->orderBy('order_number', 'asc')
                ->get();

            $breadcrumbs = [
                ['link' => "dashboard", 'name' => "Home"],
                ['link' => "vendor", 'name' => "Vendor"],
                ['name' => "Edit Vendor"]
            ];

            $title = "Edit Vendor";
            if ( $currentUser['role'] == 'vendor_admin' ) {
                $title = "Vendor Settings";
                $breadcrumbs[1]['link'] = "vendor/".$id;
                $breadcrumbs[2]['name'] = "Vendor Settings";
            }
            
            return view('pages.vendor.edit', [
                'breadcrumbs' => $breadcrumbs,
                'vendor' => $vendor,
                'brands' => $brands,
                'services' => $services,
                'title' => $title,
                'progress' => $progress,
                'param' => $data
            ]);

        } else {
            return response( 'Permissions insuffisantes !', 403 );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $currentUser = Auth::user();
        $vendor = Vendor::find($id);
        if ( $currentUser['role'] == 'admin' || ($currentUser['role'] == 'vendor_admin' && $currentUser['vendor_id'] == $id) ) {
            
            $data = $request->all();
            $validator = Validator::make($data, array(
                'name' => 'required',
                'contact_no' => 'required|unique:vendors,contact_no,'.$id,
                'owner_contact_no' => [ 'required',
                    Rule::unique('users', 'contact_no')->where(function ($query) use($vendor) {
                        return $query->where('id', '!=', $vendor->admin->id)
                            ->whereIn('role', array('vendor_admin','manager','pickup_person'))
                            ->where('is_deleted', 0);
                    })
                ],
                'owner_name' => 'required',
                'aadhar_card_number' => 'required',
                'PAN_number' => 'required',
                'address_line_1' => 'required',
                'city_id' => 'required',
                'state_id' => 'required',
                'zipcode' => 'required',
            ));
    
            // process the login
            if ($validator->fails()) {
                return Redirect::back()
                    ->withErrors($validator)
                    ->withInput();
            }
            
            $vendor->name = $data['name'];
            $vendor->contact_no = $data['contact_no'];
            $vendor->emergency_contact_no = $data['emergency_contact_no'];
            $vendor->email = $data['email'];
            $vendor->address_line_1 = $data['address_line_1'];
            $vendor->address_line_2 = $data['address_line_2'];
            $vendor->city_id = $data['city_id'];
            $vendor->state_id = $data['state_id'];
            $vendor->zipcode = $data['zipcode'];
            $vendor->latitude = $data['latitude'];
            $vendor->longitude = $data['longitude'];
            $vendor->GST_number = $data['GST_number'];
            $vendor->agreement_number = $data['agreement_number'];

            $account_section = unserialize($vendor->account_section);
            $account_section['basic'] = true;
            $vendor->account_section = serialize($account_section);

            $oldLogo = $vendor->logo;
            $oldGST_certificate = $vendor->GST_certificate;
            $oldagreement = $vendor->agreement;
            
            if ( $vendor->save() ) {

                $subpath = "public/uploads/vendors/".$id."/";

                // Vendor Logo
                if ( isset($data['logo']) && $data['logo'] != '' ) {
                    if ( $data['logo'] != $oldLogo ) {
                        
                        $img = explode(',', $data['logo']);
                        $ini =substr($img[0], 11);
                        $type = explode(';', $ini);
                        $extention = str_replace('+xml', '', $type[0]);
                        $logo_name = Carbon::now()->timestamp . 'l.' .$extention;

                        $path = storage_path('app') . "/" . $subpath;

                        if( !File::isDirectory($path) ){
                            File::makeDirectory($path, 0755, true, true);
                        }

                        $data['logo'] = substr($data['logo'], strpos($data['logo'], ",")+1);
                        $data1 = base64_decode($data['logo']);

                        // delete old file
                        if( $oldLogo != '' && Storage::exists($subpath . $oldLogo) ){
                            Storage::delete($subpath . $oldLogo);
                        }
                        
                        if ( Storage::put($subpath . $logo_name, $data1) ) {
                            Vendor::where('id', $vendor->id)->update(array('logo' => $logo_name));
                        }

                    }
                } else {
                    if ( $oldLogo != '' && Storage::exists($subpath . $oldLogo) ) {
                        Storage::delete($subpath . $oldLogo);
                        Vendor::find($id)->update(array('logo' => ''));
                    }
                }

                // Vendor GST_certificate
                if ( isset($data['GST_certificate']) && $data['GST_certificate'] != '' ) {
                    if ( $data['GST_certificate'] != $oldGST_certificate ) {
                        
                        $img = explode(',', $data['GST_certificate']);
                        $temp1 = explode('/', $img[0]);
                        $temp2 = explode(';', $temp1[1]);
                        $ini = $temp2[0];
                        //$ini = substr($img[0], 11);
                        $type = explode(';', $ini);
                        $extention = str_replace('+xml', '', $type[0]);
                        $GST_certificate_name = Carbon::now()->timestamp . 'g.' .$extention;

                        $path = storage_path('app') . "/" . $subpath;

                        if( !File::isDirectory($path) ){
                            File::makeDirectory($path, 0755, true, true);
                        }

                        $data['GST_certificate'] = substr($data['GST_certificate'], strpos($data['GST_certificate'], ",")+1);
                        $data1 = base64_decode($data['GST_certificate']);

                        // delete old file
                        if( $oldGST_certificate != '' && Storage::exists($subpath . $oldGST_certificate) ){
                            Storage::delete($subpath . $oldGST_certificate);
                        }
                        
                        if ( Storage::put($subpath . $GST_certificate_name, $data1) ) {
                            Vendor::where('id', $vendor->id)->update(array('GST_certificate' => $GST_certificate_name));
                        }

                    }
                } else {
                    if ( $oldGST_certificate != '' && Storage::exists($subpath . $oldGST_certificate) ) {
                        Storage::delete($subpath . $oldGST_certificate);
                        Vendor::find($id)->update(array('GST_certificate' => ''));
                    }
                }

                // Vendor agreement
                if ( isset($data['agreement']) && $data['agreement'] != '' ) {
                    if ( $data['agreement'] != $oldagreement ) {
                        
                        $img = explode(',', $data['agreement']);
                        $temp1 = explode('/', $img[0]);
                        $temp2 = explode(';', $temp1[1]);
                        $ini = $temp2[0];
                        //$ini = substr($img[0], 11);
                        $type = explode(';', $ini);
                        $extention = str_replace('+xml', '', $type[0]);
                        $agreement_name = Carbon::now()->timestamp . 'a.' .$extention;

                        $path = storage_path('app') . "/" . $subpath;

                        if( !File::isDirectory($path) ){
                            File::makeDirectory($path, 0755, true, true);
                        }

                        $data['agreement'] = substr($data['agreement'], strpos($data['agreement'], ",")+1);
                        $data1 = base64_decode($data['agreement']);

                        // delete old file
                        if( $oldagreement != '' && Storage::exists($subpath . $oldagreement) ){
                            Storage::delete($subpath . $oldagreement);
                        }
                        
                        if ( Storage::put($subpath . $agreement_name, $data1) ) {
                            Vendor::where('id', $vendor->id)->update(array('agreement' => $agreement_name));
                        }

                    }
                } else {
                    if ( $oldagreement != '' && Storage::exists($subpath . $oldagreement) ) {
                        Storage::delete($subpath . $oldagreement);
                        Vendor::find($id)->update(array('agreement' => ''));
                    }
                }

                $vendorAdmin = User::where('role', 'vendor_admin')->where('vendor_id', $id)->first();
                $vendorAdmin->name = $data['owner_name'];
                $vendorAdmin->contact_no = $data['owner_contact_no'];
                $vendorAdmin->aadhar_card_number = $data['aadhar_card_number'];
                $vendorAdmin->PAN_number = $data['PAN_number'];

                $oldaadhar_card = $vendorAdmin->aadhar_card;
                $oldPAN_card = $vendorAdmin->PAN_card;
                $subpath1 = "public/uploads/users/".$vendorAdmin->id."/";

                if ( $vendorAdmin->save() ) {

                    // Vendor aadhar_card
                    if ( isset($data['aadhar_card']) && $data['aadhar_card'] != '' ) {
                        if ( $data['aadhar_card'] != $oldaadhar_card ) {
                            
                            $img = explode(',', $data['aadhar_card']);
                            $temp1 = explode('/', $img[0]);
                            $temp2 = explode(';', $temp1[1]);
                            $ini = $temp2[0];
                            //$ini = substr($img[0], 11);
                            $type = explode(';', $ini);
                            $extention = str_replace('+xml', '', $type[0]);
                            $aadhar_card_name = Carbon::now()->timestamp . 'aa.' .$extention;
                            
                            $path = storage_path('app') . "/" . $subpath1;

                            if( !File::isDirectory($path) ){
                                File::makeDirectory($path, 0755, true, true);
                            }

                            $data['aadhar_card'] = substr($data['aadhar_card'], strpos($data['aadhar_card'], ",")+1);
                            $data1 = base64_decode($data['aadhar_card']);

                            // delete old file
                            if( $oldaadhar_card != '' && Storage::exists($subpath1 . $oldaadhar_card) ){
                                Storage::delete($subpath1 . $oldaadhar_card);
                            }
                            
                            if ( Storage::put($subpath1 . $aadhar_card_name, $data1) ) {
                                User::where('id', $vendorAdmin->id)->update(array('aadhar_card' => $aadhar_card_name));
                            }

                        }
                    } else {
                        if ( $oldaadhar_card != '' && Storage::exists($subpath1 . $oldaadhar_card) ) {
                            Storage::delete($subpath1 . $oldaadhar_card);
                            User::find($vendorAdmin->id)->update(array('aadhar_card' => ''));
                        }
                    }

                    // Vendor PAN_card
                    if ( isset($data['PAN_card']) && $data['PAN_card'] != '' ) {
                        if ( $data['PAN_card'] != $oldPAN_card ) {
                            
                            $img = explode(',', $data['PAN_card']);
                            $temp1 = explode('/', $img[0]);
                            $temp2 = explode(';', $temp1[1]);
                            $ini = $temp2[0];
                            //$ini = substr($img[0], 11);
                            $type = explode(';', $ini);
                            $extention = str_replace('+xml', '', $type[0]);
                            $PAN_card_name = Carbon::now()->timestamp . 'pl.' .$extention;

                            $path = storage_path('app') . "/" . $subpath1;

                            if( !File::isDirectory($path) ){
                                File::makeDirectory($path, 0755, true, true);
                            }

                            $data['PAN_card'] = substr($data['PAN_card'], strpos($data['PAN_card'], ",")+1);
                            $data1 = base64_decode($data['PAN_card']);

                            // delete old file
                            if( $oldPAN_card != '' && Storage::exists($subpath1 . $oldPAN_card) ){
                                Storage::delete($subpath1 . $oldPAN_card);
                            }
                            
                            if ( Storage::put($subpath1 . $PAN_card_name, $data1) ) {
                                User::where('id', $vendorAdmin->id)->update(array('PAN_card' => $PAN_card_name));
                            }

                        }
                    } else {
                        if ( $oldPAN_card != '' && Storage::exists($subpath1 . $oldPAN_card) ) {
                            Storage::delete($subpath1 . $oldPAN_card);
                            User::find($vendorAdmin->id)->update(array('PAN_card' => ''));
                        }
                    }

                    Session::flash('success', 'Vendor changes successfully updated!.');
                    return Redirect::to('vendor/'.$id.'/edit?s=basic');
                } else {
                    Session::flash('danger', 'Some thing is wrong. Please try again');
                    return Redirect::to('vendor/'.$id.'/edit?s=basic');
                }
            } else {

                Session::flash('danger', 'Some thing is wrong. Please try again');
                return Redirect::to('vendor/'.$id.'/edit?s=basic');
            }
            
        } else {
            return response( 'Permissions insuffisantes !', 403 );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateManufacturers(Request $request, $id)
    {
        $currentUser = Auth::user();
        $vendor = Vendor::find($id);
        if ( $currentUser['role'] == 'admin' || ($currentUser['role'] == 'vendor_admin' && $currentUser['vendor_id'] == $id) ) {
            
            $data = $request->all();
            $validator = Validator::make($data, array(
                'brands' => 'required'
            ));
    
            // process the login
            if ($validator->fails()) {
                return Redirect::back()
                    ->withErrors($validator)
                    ->withInput();
            }
            
            // sync brands
            $syncBrands = $vendor->brands()->sync($data['brands']);
            if ( $syncBrands ) {

                $account_section = unserialize($vendor->account_section);
                $account_section['manufacturers'] = true;
                $vendor->account_section = serialize($account_section);
                $vendor->save();

                Session::flash('success', "Vendor's brands successfully updated!");
                return Redirect::to('vendor/'.$id.'/edit?s=manufacturers');
            } else {
                Session::flash('danger', 'Some thing is wrong. Please try again');
                return Redirect::to('vendor/'.$id.'/edit?s=manufacturers');
            }
            
        } else {
            return response( 'Permissions insuffisantes !', 403 );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function syncSegments(Request $request, $id)
    {
        $currentUser = Auth::user();
        $vendor = Vendor::find($id);
        if ( $currentUser['role'] == 'admin' || ($currentUser['role'] == 'vendor_admin' && $currentUser['vendor_id'] == $id) ) {
            
            $data = $request->all();
            $validator = Validator::make($data, array(
                'model' => 'required'
            ));
    
            // process the login
            if ($validator->fails()) {
                return Redirect::back()
                    ->withErrors($validator)
                    ->withInput();
            }
            
            $syncModels = true;
            // sync sub services cost
            if ( isset($data['model']) && count($data['model']) ) {
                foreach ($data['model'] as $car_model_id => $segment) {
                    
                    $segmentSetting = array(
                        'vendor_id' => $vendor->id,
                        'car_model_id' => $car_model_id,
                        'segment' => $segment
                    );
                    $storeVendorCarModelSetting = VendorCarModelSetting::updateOrCreate(
                        ['vendor_id' => $vendor->id, 'car_model_id' => $car_model_id],
                        $segmentSetting
                    );
                    if ( !$storeVendorCarModelSetting ) {
                        $syncModels = false;
                    }
                }
            }

            // sync brands
            if ( $syncModels ) {

                $account_section = unserialize($vendor->account_section);
                $account_section['segment'] = true;
                $vendor->account_section = serialize($account_section);
                $vendor->save();

                Session::flash('success', "Vendor's segment successfully synced!");
                return Redirect::to('vendor/'.$id.'/edit?s=segment');
            } else {
                Session::flash('danger', 'Some thing is wrong. Please try again');
                return Redirect::to('vendor/'.$id.'/edit?s=segment');
            }
            
        } else {
            return response( 'Permissions insuffisantes !', 403 );
        }
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateServices(Request $request, $id)
    {
        $currentUser = Auth::user();
        $vendor = Vendor::find($id);
        if ( $currentUser['role'] == 'admin' || ($currentUser['role'] == 'vendor_admin' && $currentUser['vendor_id'] == $id) ) {
            
            $data = $request->all();
            $validator = Validator::make($data, array(
                'services' => 'required',
                'serice' => 'required'
            ));
    
            // process the login
            if ($validator->fails()) {
                return Redirect::back()
                    ->withErrors($validator)
                    ->withInput();
            }
            
            // sync services
            $syncServices = $vendor->services()->sync($data['services']);
            if ( $syncServices ) {

                // sync sub services cost
                if ( isset($data['serice']) && count($data['serice']) ) {
                    foreach ($data['serice'] as $service_id => $cost) {
                        
                        $segmentSetting = array(
                            'vendor_id' => $vendor->id,
                            'service_id' => $service_id,
                            'small' => $cost['small'],
                            'medium' => $cost['medium'],
                            'large' => $cost['large'],
                            'premium' => $cost['premium'],
                            'status' => ((isset($cost['status']) && $cost['status'] == '1') ? $cost['status'] : '0'),
                        );
                        $storeVendorServiceSetting = VendorServiceSetting::updateOrCreate(
                            ['vendor_id' => $vendor->id, 'service_id' => $service_id],
                            $segmentSetting
                        );
                    }
                }
                
                $account_section = unserialize($vendor->account_section);
                $account_section['services'] = true;
                $vendor->account_section = serialize($account_section);
                $vendor->save();

                Session::flash('success', "Vendor's services successfully updated!");
                return Redirect::to('vendor/'.$id.'/edit?s=services');
            } else {
                Session::flash('danger', 'Some thing is wrong. Please try again');
                return Redirect::to('vendor/'.$id.'/edit?s=services');
            }
            
        } else {
            return response( 'Permissions insuffisantes !', 403 );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateOld(Request $request, $id)
    {
        $currentUser = Auth::user();
        if ( $currentUser['role'] == 'admin' ) {
            
            $data = $request->all();

            $vendor = Vendor::find($id);
            //dd($data, $vendor, $id);

            $vendor->name = $data['vendor-name'];
            $vendor->contact_no = $data['vendor-contact_no'];
            $vendor->email = $data['vendor-email'];
            $vendor->address_line_1 = $data['vendor-address_line_1'];
            $vendor->address_line_2 = $data['vendor-address_line_2'];
            $vendor->city_id = $data['vendor-city_id'];
            $vendor->state_id = $data['vendor-state_id'];
            $vendor->zipcode = $data['vendor-zipcode'];
            $vendor->GST_number = $data['vendor-GST_number'];
            $vendor->agreement_number = $data['vendor-agreement_number'];

            $oldLogo = $vendor->logo;
            $oldGST_certificate = $vendor->GST_certificate;
            $oldagreement = $vendor->agreement;
            
            if ( $vendor->save() ) {
                
                $subpath = "public/uploads/vendors/".$id."/";

                // Vendor Logo
                if ( isset($data['logo']) && $data['logo'] != '' ) {
                    if ( $data['logo'] != $oldLogo ) {
                        
                        $img = explode(',', $data['logo']);
                        $ini =substr($img[0], 11);
                        $type = explode(';', $ini);
                        $extention = str_replace('+xml', '', $type[0]);
                        $logo_name = Carbon::now()->timestamp . 'l.' .$extention;

                        $path = storage_path('app') . "/" . $subpath;

                        if( !File::isDirectory($path) ){
                            File::makeDirectory($path, 0755, true, true);
                        }

                        $data['logo'] = substr($data['logo'], strpos($data['logo'], ",")+1);
                        $data1 = base64_decode($data['logo']);

                        // delete old file
                        if( $oldLogo != '' && Storage::exists($subpath . $oldLogo) ){
                            Storage::delete($subpath . $oldLogo);
                        }
                        
                        if ( Storage::put($subpath . $logo_name, $data1) ) {
                            Vendor::where('id', $vendor->id)->update(array('logo' => $logo_name));
                        }

                    }
                } else {
                    if ( $oldLogo != '' && Storage::exists($subpath . $oldLogo) ) {
                        Storage::delete($subpath . $oldLogo);
                        Vendor::find($id)->update(array('logo' => ''));
                    }
                }

                // Vendor GST_certificate
                if ( isset($data['GST_certificate']) && $data['GST_certificate'] != '' ) {
                    if ( $data['GST_certificate'] != $oldGST_certificate ) {
                        
                        $img = explode(',', $data['GST_certificate']);
                        $ini =substr($img[0], 11);
                        $type = explode(';', $ini);
                        $extention = str_replace('+xml', '', $type[0]);
                        $GST_certificate_name = Carbon::now()->timestamp . 'g.' .$extention;

                        $path = storage_path('app') . "/" . $subpath;

                        if( !File::isDirectory($path) ){
                            File::makeDirectory($path, 0755, true, true);
                        }

                        $data['GST_certificate'] = substr($data['GST_certificate'], strpos($data['GST_certificate'], ",")+1);
                        $data1 = base64_decode($data['GST_certificate']);

                        // delete old file
                        if( $oldGST_certificate != '' && Storage::exists($subpath . $oldGST_certificate) ){
                            Storage::delete($subpath . $oldGST_certificate);
                        }
                        
                        if ( Storage::put($subpath . $GST_certificate_name, $data1) ) {
                            Vendor::where('id', $vendor->id)->update(array('GST_certificate' => $GST_certificate_name));
                        }

                    }
                } else {
                    if ( $oldGST_certificate != '' && Storage::exists($subpath . $oldGST_certificate) ) {
                        Storage::delete($subpath . $oldGST_certificate);
                        Vendor::find($id)->update(array('GST_certificate' => ''));
                    }
                }

                // Vendor agreement
                if ( isset($data['agreement']) && $data['agreement'] != '' ) {
                    if ( $data['agreement'] != $oldagreement ) {
                        
                        $img = explode(',', $data['agreement']);
                        $ini =substr($img[0], 11);
                        $type = explode(';', $ini);
                        $extention = str_replace('+xml', '', $type[0]);
                        $agreement_name = Carbon::now()->timestamp . 'a.' .$extention;

                        $path = storage_path('app') . "/" . $subpath;

                        if( !File::isDirectory($path) ){
                            File::makeDirectory($path, 0755, true, true);
                        }

                        $data['agreement'] = substr($data['agreement'], strpos($data['agreement'], ",")+1);
                        $data1 = base64_decode($data['agreement']);

                        // delete old file
                        if( $oldagreement != '' && Storage::exists($subpath . $oldagreement) ){
                            Storage::delete($subpath . $oldagreement);
                        }
                        
                        if ( Storage::put($subpath . $agreement_name, $data1) ) {
                            Vendor::where('id', $vendor->id)->update(array('agreement' => $agreement_name));
                        }

                    }
                } else {
                    if ( $oldagreement != '' && Storage::exists($subpath . $oldagreement) ) {
                        Storage::delete($subpath . $oldagreement);
                        Vendor::find($id)->update(array('agreement' => ''));
                    }
                }

                $vendorAdmin = User::where('role', 'vendor_admin')->where('vendor_id', $id)->first();
                $vendorAdmin->name = $data['vendorAdmin-name'];
                $vendorAdmin->contact_no = $data['vendorAdmin-contact_no'];
                $vendorAdmin->aadhar_card_number = $data['vendorAdmin-aadhar_card_number'];
                $vendorAdmin->PAN_number = $data['vendorAdmin-PAN_number'];

                $oldaadhar_card = $vendorAdmin->aadhar_card;
                $oldPAN_card = $vendorAdmin->PAN_card;
                $subpath1 = "public/uploads/users/".$vendorAdmin->id."/";

                if ( $vendorAdmin->save() ) {

                    // Vendor aadhar_card
                    if ( isset($data['aadhar_card']) && $data['aadhar_card'] != '' ) {
                        if ( $data['aadhar_card'] != $oldaadhar_card ) {
                            
                            $img = explode(',', $data['aadhar_card']);
                            $ini =substr($img[0], 11);
                            $type = explode(';', $ini);
                            $extention = str_replace('+xml', '', $type[0]);
                            $aadhar_card_name = Carbon::now()->timestamp . 'aa.' .$extention;

                            $path = storage_path('app') . "/" . $subpath1;

                            if( !File::isDirectory($path) ){
                                File::makeDirectory($path, 0755, true, true);
                            }

                            $data['aadhar_card'] = substr($data['aadhar_card'], strpos($data['aadhar_card'], ",")+1);
                            $data1 = base64_decode($data['aadhar_card']);

                            // delete old file
                            if( $oldaadhar_card != '' && Storage::exists($subpath1 . $oldaadhar_card) ){
                                Storage::delete($subpath1 . $oldaadhar_card);
                            }
                            
                            if ( Storage::put($subpath1 . $aadhar_card_name, $data1) ) {
                                User::where('id', $vendorAdmin->id)->update(array('aadhar_card' => $aadhar_card_name));
                            }

                        }
                    } else {
                        if ( $oldaadhar_card != '' && Storage::exists($subpath1 . $oldaadhar_card) ) {
                            Storage::delete($subpath1 . $oldaadhar_card);
                            User::find($vendorAdmin->id)->update(array('aadhar_card' => ''));
                        }
                    }

                    // Vendor PAN_card
                    if ( isset($data['PAN_card']) && $data['PAN_card'] != '' ) {
                        if ( $data['PAN_card'] != $oldPAN_card ) {
                            
                            $img = explode(',', $data['PAN_card']);
                            $ini =substr($img[0], 11);
                            $type = explode(';', $ini);
                            $extention = str_replace('+xml', '', $type[0]);
                            $PAN_card_name = Carbon::now()->timestamp . 'pl.' .$extention;

                            $path = storage_path('app') . "/" . $subpath1;

                            if( !File::isDirectory($path) ){
                                File::makeDirectory($path, 0755, true, true);
                            }

                            $data['PAN_card'] = substr($data['PAN_card'], strpos($data['PAN_card'], ",")+1);
                            $data1 = base64_decode($data['PAN_card']);

                            // delete old file
                            if( $oldPAN_card != '' && Storage::exists($subpath1 . $oldPAN_card) ){
                                Storage::delete($subpath1 . $oldPAN_card);
                            }
                            
                            if ( Storage::put($subpath1 . $PAN_card_name, $data1) ) {
                                User::where('id', $vendorAdmin->id)->update(array('PAN_card' => $PAN_card_name));
                            }

                        }
                    } else {
                        if ( $oldPAN_card != '' && Storage::exists($subpath1 . $oldPAN_card) ) {
                            Storage::delete($subpath1 . $oldPAN_card);
                            User::find($vendorAdmin->id)->update(array('PAN_card' => ''));
                        }
                    }

                    Session::flash('success', 'Vendor changes successfully updated!.');
                    return Redirect::to('vendor/'.$id);
                } else {
                    Session::flash('danger', 'Some thing is wrong. Please try again');
                    return Redirect::to('vendor/'.$id);
                }
                
            } else {

                Session::flash('danger', 'Some thing is wrong. Please try again');
                return Redirect::to('vendor/'.$id);
            }
            
        } else {
            return response( 'Permissions insuffisantes !', 403 );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $currentUser = Auth::user();
        $data['is_deleted'] = '1';

        $itme = Vendor::find($id);
        if ( $itme->update($data) ) {

            $itmeName = 'Vendor';
            
            // redirect
            Session::flash('success', $itmeName.' successfully deleted!');
            return Redirect::to('vendor');
        } else {

            // redirect
            Session::flash('danger', 'Some thing is wrong. Please try again');
            return Redirect::to('vendor');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function bulkAction(Request $request)
    {
        $data = $request->all();
        $currentUser = Auth::user();
        $validator = Validator::make($data, array(
            'action' => 'required',
            'ids' => 'required',
            'type' => 'required',
        ));

        // process the login
        if ($validator->fails()) {

            $response = array(
                'status' => 500,
                'message' => 'Invalid data',
                'data' => $validator->errors()
            );
            return response()->json($response, $response['status']);
        }

        $dataUpdate = array();
        if ( isset($data['action']) && $data['action'] == 'active' ) {
            $dataUpdate['status'] = '1';
        } else if ( isset($data['action']) && $data['action'] == 'inactive' ) {
            $dataUpdate['status'] = '0';
        } else if ( isset($data['action']) && $data['action'] == 'delete' ) {
            $dataUpdate['is_deleted'] = '1';
        }

        $item = Vendor::find($data['ids']);
        if ( $item->update($dataUpdate) ) {
            
            Session::flash('success', 'All selected items successfully '.$data['action'].'d!');
            $response = array(
                'status' => 200,
                'message' => ''
            );
        } else {
            
            Session::flash('danger', 'Some thing is wrong. Please try again');
            $response = array(
                'status' => 500,
                'message' => ''
            );
        }

        return response()->json($response, $response['status']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pickupPersonList(Request $request, $id)
    {
        $pickup_persons = User::select('id', 'name')
            ->where('role', 'pickup_person')
            ->where('vendor_id', $id)
            ->where('status', '1')
            ->where('is_deleted', '0')
            ->get();

        $response = array(
            'response_code' => 200,
            'response_message' => '',
            'response_data' => $pickup_persons
        );

        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storePickupPerson(Request $request)
    {
        $data = $request->all();
        $currentUser = Auth::user();
        
        $validator = Validator::make($data, array(
            'vendor_id' => 'required',
            'name' => 'required',
            //'contact_no' => 'required',
            'contact_no' => [ 'required',
                Rule::unique('users', 'contact_no')->where(function ($query) {
                    return $query->whereIn('role', array('admin', 'vendor_admin','manager','pickup_person'))
                        ->where('is_deleted', 0);
                })
            ],
        ));

        // process the login
        if ($validator->fails()) {
            /* return Redirect::back()
                ->withErrors($validator)
                ->withInput(); */
            $validatorArray = array_combine(
                array_map(function($key){ return $key.'_error'; }, array_keys($validator->errors()->toArray())),
                $validator->errors()->toArray()
            );
            $response = array(
                'status' => 500,
                'message' => '',
                'data' => $validatorArray
            );
            return response()->json($response, 200);
        }

        $data['role'] = 'pickup_person';
        $data['password'] = Hash::make('p123');
        
        $pickupPerson = User::create($data);
        if ( $pickupPerson ) {

            // redirect
            Session::flash('success', 'Pickup person successfully created!');
            //return Redirect::to('vendor/'.$data['vendor_id']);
            $response = array(
                'status' => 200,
                'message' => ''
            );
        } else {
            // redirect
            Session::flash('danger', 'Some thing is wrong. Please try again');
            //return Redirect::to('vendor/'.$data['vendor_id']);
            $response = array(
                'status' => 500,
                'message' => ''
            );
        }

        return response()->json($response, $response['status']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updatePickupPerson(Request $request, $id)
    {
        $data = $request->all();
        $currentUser = Auth::user();
        
        $validator = Validator::make($data, array(
            'vendor_id' => 'required',
            'name' => 'required',
            //'contact_no' => 'required',
            'contact_no' => [ 'required',
                Rule::unique('users', 'contact_no')->where(function ($query) use($id) {
                    return $query->where('id', '!=', $id)
                        ->whereIn('role', array('admin', 'vendor_admin','manager','pickup_person'))
                        ->where('is_deleted', 0);
                })
            ]
        ));

        // process the login
        if ($validator->fails()) {
            /* return Redirect::back()
                ->withErrors($validator)
                ->withInput(); */
                
            $validatorArray = array_combine(
                array_map(function($key){ return $key.'_error'; }, array_keys($validator->errors()->toArray())),
                $validator->errors()->toArray()
            );
            $response = array(
                'status' => 500,
                'message' => '',
                'data' => $validatorArray
            );
            return response()->json($response, 200);
        }

        $pickupPerson = User::find($id);
        if ( $pickupPerson->update($data) ) {

            // redirect
            Session::flash('success', 'Pickup person successfully updated!');
            //return Redirect::to('vendor/'.$data['vendor_id']);
            $response = array(
                'status' => 200,
                'message' => ''
            );
        } else {
            // redirect
            Session::flash('danger', 'Some thing is wrong. Please try again');
            //return Redirect::to('vendor/'.$data['vendor_id']);
            $response = array(
                'status' => 500,
                'message' => ''
            );
        }

        return response()->json($response, $response['status']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyPickupPerson($id, Request $request)
    {
        $currentUser = Auth::user();
        $data['is_deleted'] = '1';

        $item = User::find($id);
        if ( $item->update($data) ) {

            // redirect
            Session::flash('success', 'Pickup person successfully deleted!');
            return Redirect::to('vendor/'.$item['vendor_id']);
        } else {

            // redirect
            Session::flash('danger', 'Some thing is wrong. Please try again');
            return Redirect::to('vendor/'.$item['vendor_id']);
        }
    }
}
