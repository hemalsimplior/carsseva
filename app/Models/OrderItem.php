<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id', 'associate_id', 'parent_service_id', 'service_id', 'service_name', 'cost_type', 'cost', 'tax', 'total', 'status', 'is_custom', 'is_deleted'
    ];

    /**
     * Get the post that owns the comment.
     */
    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }

    /**
     * Get the post that owns the comment.
     */
    public function parent_service()
    {
        return $this->belongsTo('App\Models\Service', 'parent_service_id');
    }

    /**
     * Get the post that owns the comment.
     */
    public function service()
    {
        return $this->belongsTo('App\Models\Service', 'service_id');
    }
}
