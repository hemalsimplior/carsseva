<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Storage;

class Vendor extends Model
{
    protected $appends = ['logo_url'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sequence_id', 'code', 'name', 'contact_no', 'email', 'address_line_1', 'address_line_2', 'city_id', 'state_id', 'latitude', 'longitude', 'zipcode', 'logo', 'GST_number', 'GST_certificate', 'agreement_number', 'agreement', 'emergency_contact_no', 'account_section', 'status', 'is_deleted'
    ];

    /**
     * Get the logo full URL
     */
    public function getLogoUrlAttribute()
    {
        $value = $this->logo;
        if ( $value != '' && $value != null ) {
            return Storage::disk('public')->url('uploads/vendors/'.$this->id.'/' . $value);
        } else {
            return asset('images/portrait/default.jpg');
        }
    }

    /**
     * Get the logo full URL
     */
    public function getGSTCertificateUrlAttribute()
    {
        $value = $this->GST_certificate;
        if ( $value != '' && $value != null ) {
            return Storage::disk('public')->url('uploads/vendors/'.$this->id.'/' . $value);
        } else {
            return asset('images/portrait/default.jpg');
        }
    }

    /**
     * Get the logo full URL
     */
    public function getAgreementUrlAttribute()
    {
        $value = $this->agreement;
        if ( $value != '' && $value != null ) {
            return Storage::disk('public')->url('uploads/vendors/'.$this->id.'/' . $value);
        } else {
            return asset('images/portrait/default.jpg');
        }
    }

    /**
     * Get the comments for the blog post.
     */
    public function admin()
    {
        return $this->hasOne('App\User')
            ->where('role', 'vendor_admin')
            ->where('is_deleted', '0')
            ->orderBy('id', 'asc');
    }

    /**
     * Get the comments for the blog post.
     */
    public function pickup_persons()
    {
        return $this->hasMany('App\User')
            ->where('role', 'pickup_person')
            ->where('is_deleted', '0')
            ->orderBy('id', 'desc');
    }

    /**
     * Get the comments for the blog post.
     */
    public function reviews()
    {
        return $this->hasMany('App\Models\Review')
            ->where('status', '1')
            ->orderBy('id', 'desc');
    }

    /**
     * Get the comments for the blog post.
     */
    public function brands()
    {
        return $this->belongsToMany('App\Models\Brand')
            ->withTimestamps()
            ->using('App\Models\BrandVendor')
            ->orderBy('id', 'desc');
    }

    /**
     * Get the comments for the blog post.
     */
    public function services()
    {
        return $this->belongsToMany('App\Models\Service')
            ->withTimestamps()
            ->using('App\Models\ServiceVendor')
            ->orderBy('id', 'desc');
    }

    /**
     * Get the comments for the blog post.
     */
    public function orders()
    {
        return $this->hasMany('App\Models\Order')
            ->orderBy('id', 'desc');
    }

    /**
     * Get the comments for the blog post.
     */
    public function active_orders()
    {
        return $this->hasMany('App\Models\Order')
            ->where('status', '!=', 'delivered')
            ->orderBy('id', 'desc');
    }

    /**
     * Get the comments for the blog post.
     */
    public function completed_orders()
    {
        return $this->hasMany('App\Models\Order')
            ->whereIn('status', array('service_completed', 'delivered'))
            ->orderBy('id', 'desc');
    }

    /**
     * Get the post that owns the comment.
     */
    public function state()
    {
        return $this->belongsTo('App\Models\State');
    }

    /**
     * Get the post that owns the comment.
     */
    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }
}
