<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'vendor_id', 'rating', 'title', 'description', 'status'
    ];

    /**
     * Get the post that owns the comment.
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * Get the post that owns the comment.
     */
    public function vendor()
    {
        return $this->belongsTo('App\Models\Vendor');
    }
}
