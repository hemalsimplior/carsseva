<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
	//protected $appends = ['description'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'common_id', 'action_type', 'action', 'old', 'new', 'description', 'action_by', 'user_ip', 'is_read'
    ];

    /**
     * Get the post that owns the comment.
     */
    public function action_by()
    {
        return $this->belongsTo('App\User', 'action_by');
    }

    /**
     * Get the post that owns the comment.
     */
    public function order()
    {
        return $this->belongsTo('App\Models\Order', 'common_id')->with('payments');
    }

    public function getDescriptionAttribute($value)
    {
        $description = '';
        if ( $this->action == 'logged_in' || $this->action == 'logged_out' ) {
            $description = $this->action_by->name . ' ' . str_replace('_', ' ', $this->action) . ' via ' . $this->user_ip;

        } else if ($this->action == 'forgot_password') {
            $description = $this->action_by->name . ' requested for ' . str_replace('_', ' ', $this->action);

        } else if ($this->action == 'password_changed') {
            $description = $this->action_by->name . "'s password changed";

        } else if ($this->action == 'add') {

            if ( $this->action_type == 'order' ) {
                $description = "New " . $this->action_type . " #" . $this[$this->action_type]->sequence_id . " has been received.";
            } else {
                $description = "New " . $this->action_type . " " . $this[$this->action_type]->name . " has been created.";
            }

        } else if ($this->action == 'edit') {
            $description = $this[$this->action_type]->name . " " . $this->action_type . "'s data has been updated.";

        } else if ($this->action == 'delete') {
            $description = $this[$this->action_type]->name . " " . $this->action_type . " has been deleted.";

        } else if ($this->action == 'change_status') {

            if ( $this->action_type == 'order' ) {
                $description = "#".$this[$this->action_type]->sequence_id . " " . $this->action_type . "'s status changed to " . str_replace('_', ' ', $this->new) . ".";
            } else {
                $description = $this[$this->action_type]->name . " " . $this->action_type . "'s status changed to " . $this->new . ".";
            }
        } else if ($this->action == 'payment_failed') {
            $temp = unserialize($value);
            $description = $temp['error_description'];
        }

        return $description;
    }
}
