<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'friendly_name', 'address_type', 'address_line_1', 'address_line_2', 'state_id', 'city_id', 'latitude', 'longitude', 'zipcode', 'is_primary', 'is_deleted'
    ];

    /**
     * Get the post that owns the comment.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the post that owns the comment.
     */
    public function state()
    {
        return $this->belongsTo('App\Models\State');
    }

    /**
     * Get the post that owns the comment.
     */
    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }
}
