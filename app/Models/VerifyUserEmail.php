<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VerifyUserEmail extends Model
{
    protected $table = 'verify_user_email';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'email_id', 'token'
    ];

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }
}
