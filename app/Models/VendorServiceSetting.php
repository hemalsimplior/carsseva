<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VendorServiceSetting extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vendor_id', 'service_id', 'small', 'medium', 'large', 'premium', 'status'
    ];
}
