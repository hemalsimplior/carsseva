<?php

namespace App\Imports;

use App\Models\Brand, App\Models\CarModel, App\Models\VendorCarModelSetting;
use Maatwebsite\Excel\Concerns\ToModel;

class BrandsImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if ( isset($row['1']) && $row['1'] != '' ) {
            
            $row['2'] = strtolower($row['2']);

            $brand = Brand::where('name', $row['0'])->first();
            if ( !isset($brand->id) ) {
                $brand = Brand::create(array(
                    'name' => $row['0'],
                    'image' => str_replace(' ', '-', strtolower($row['0'])).'.png',
                ));
            }

            $carModel = CarModel::where('name', $row['1'])->first();
            if ( !isset($carModel->id) ) {
                $model = CarModel::create(array(
                    'brand_id' => $brand->id,
                    'name' => $row['1'],
                    'image' => (($row['3'] != '') ? $row['3'].'.jpg' : null),
                ));

                if ( $model ) {
                    VendorCarModelSetting::create(array(
                        'vendor_id' => '0',
                        'car_model_id' => $model->id,
                        'segment' => ((isset($row['2']) && ($row['2'] == 'primium' || $row['2'] == 'super primium')) ? 'premium' : $row['2']),
                    ));
                }
            }

        }
        dd($row);

        /*return new Brand([
            //
        ]);*/
    }
}
